﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Services.Interfaces
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string destination, string subject, string message);
    }
}
