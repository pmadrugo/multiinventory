﻿namespace MI.Web.Services.Interfaces
{
    public interface ISyncService
    {
        System.Threading.Tasks.Task<Common.Models.Response> SyncInventoryWithDatabaseAsync(Common.JsonModels.Inventory.JsonInventoryFull inventoryFull);
    }
}