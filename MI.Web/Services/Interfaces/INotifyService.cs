﻿using MI.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Services.Interfaces
{
    public interface INotifyService
    {
        Task<Response> NotifyInventoryUsersByEmailAsync(int inventoryId, string subject, string message);
    }
}
