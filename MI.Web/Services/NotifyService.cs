﻿
using MI.Common.Models;
using MI.Web.Data.Interfaces;
using MI.Web.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Services
{
    public class NotifyService : INotifyService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEmailSender _emailSender;

        public NotifyService(IUnitOfWork unitOfWork, IEmailSender emailSender)
        {
            _emailSender = emailSender;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Notifies inventory users by email
        /// </summary>
        /// <param name="inventoryId">Inventory ID</param>
        /// <param name="subject">Email Subject</param>
        /// <param name="message">Email Message</param>
        /// <returns></returns>
        public async Task<Response> NotifyInventoryUsersByEmailAsync(int inventoryId, string subject, string message)
        {
            foreach (var user in _unitOfWork.NotifiableUsers.GetAllNotifiableUsersOfInventory(inventoryId))
            {
                await _emailSender.SendEmailAsync(user.UserName, user.FullName, subject, message);
            }

            return new Response()
            {
                IsSuccess = true,
                Message = "All emails sent"
            };
        }
    }
}
