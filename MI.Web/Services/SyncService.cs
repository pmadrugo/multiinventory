﻿using MI.Common.JsonModels.Inventory;
using MI.Common.JsonModels.JSONInterfaces;
using MI.Common.Models;
using MI.Web.Services.Interfaces;
using MI.Web.Data;
using MI.Web.Data.Entities.ApplicationSection;
using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MI.Web.Services
{
    public class SyncService : ISyncService
    {
        private readonly MIDbContext _dbContext;
        public SyncService(MIDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Sync an incoming serialized inventory with the database.
        /// </summary>
        /// <param name="inventoryFull"></param>
        /// <returns></returns>
        public async Task<Response> SyncInventoryWithDatabaseAsync(JsonInventoryFull inventoryFull)
        {
            var inventory = inventoryFull.Inventory;

            var isInventorySynced = await SyncSingleInventoryAsync(inventory);
            if (!isInventorySynced.IsSuccess)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = isInventorySynced.Message
                };
            }

            var inventoryInDb = ((Inventory)isInventorySynced.Result);
            
            var inventoryItems = inventoryFull.Items;

            var inventoryItemModels = inventoryFull.ItemModels;

            #region Entity Syncing

            #region Item States Sync

            var isItemStatesSynced = await SyncInventoryEntitiesAsync
                <JsonItemState, ItemState>(
                inventory.UUId.ToString(), 
                inventoryFull.ItemStates,
                modelOnCreate: (json) =>
                {
                    return new ItemState()
                    {
                        UUId = json.UUId,
                        State = json.State,
                        IsDisabled = json.IsDisabled,
                        LastModified = json.LastModified,
                        InventoryId = inventoryInDb.Id
                    };
                },
                modelOnUpdate: (json, entityInDb) =>
                {
                    entityInDb.InventoryId = inventoryInDb.Id;
                    entityInDb.IsDisabled = json.IsDisabled;
                    entityInDb.State = json.State;
                    entityInDb.UUId = json.UUId;
                    entityInDb.LastModified = json.LastModified;

                    return entityInDb;
                }
            );

            #endregion

            #region Item Types Sync

            var isItemTypesSynced = await SyncInventoryEntitiesAsync
                <JsonItemType, ItemType>(
                inventory.UUId.ToString(),
                inventoryFull.ItemTypes,
                modelOnCreate: (json) =>
                {
                    return new ItemType()
                    {
                        UUId = json.UUId,
                        Name = json.Name,
                        IsDisabled = json.IsDisabled,
                        LastModified = json.LastModified,
                        InventoryId = inventoryInDb.Id
                    };
                },
                modelOnUpdate: (json, entityInDb) =>
                {
                    entityInDb.InventoryId = inventoryInDb.Id;
                    entityInDb.IsDisabled = json.IsDisabled;
                    entityInDb.Name = json.Name;
                    entityInDb.UUId = json.UUId;
                    entityInDb.LastModified = json.LastModified;

                    return entityInDb;
                }
            );
            #endregion

            #region Item Places Sync

            var isItemPlacesSynced = await SyncInventoryEntitiesAsync
                <JsonItemPlace, ItemPlace>(
                inventory.UUId.ToString(),
                inventoryFull.ItemPlaces,
                modelOnCreate: (json) =>
                {
                    return new ItemPlace()
                    {
                        UUId = json.UUId,
                        Name = json.Name,
                        IsDisabled = json.IsDisabled,
                        LastModified = json.LastModified,
                        InventoryId = inventoryInDb.Id
                    };
                },
                modelOnUpdate: (json, entityInDb) =>
                {
                    entityInDb.InventoryId = inventoryInDb.Id;
                    entityInDb.IsDisabled = json.IsDisabled;
                    entityInDb.Name = json.Name;
                    entityInDb.UUId = json.UUId;
                    entityInDb.LastModified = json.LastModified;

                    return entityInDb;
                }
            );

            #endregion

            #region Item Brands Sync

            var isItemBrandsSync = await SyncInventoryEntitiesAsync
                <JsonItemBrand, ItemBrand>(
                inventory.UUId.ToString(),
                inventoryFull.ItemBrands,
                modelOnCreate: (json) =>
                {
                    return new ItemBrand()
                    {
                        UUId = json.UUId,
                        Name = json.Name,
                        IsDisabled = json.IsDisabled,
                        LastModified = json.LastModified,
                        InventoryId = inventoryInDb.Id
                    };
                },
                modelOnUpdate: (json, entityInDb) =>
                {
                    entityInDb.InventoryId = inventoryInDb.Id;
                    entityInDb.IsDisabled = json.IsDisabled;
                    entityInDb.Name = json.Name;
                    entityInDb.UUId = json.UUId;
                    entityInDb.LastModified = json.LastModified;

                    return entityInDb;
                }
            );

            #endregion

            #region Item Models Sync

            List<ItemBrand> brandsInDB = new List<ItemBrand>();

            brandsInDB.AddRange(await _dbContext.Set<ItemBrand>().ToListAsync());

            var isItemModelsSync = await SyncInventoryEntitiesAsync
               <JsonItemModel, ItemModel>(
               inventory.UUId.ToString(),
               inventoryFull.ItemModels,
               modelOnCreate: (json) =>
               {
                   var modelBrand = brandsInDB
                    .Where(brand => brand.UUId == json.BrandUUId)
                    .FirstOrDefault();

                   return new ItemModel()
                   {
                       UUId = json.UUId,
                       Name = json.Name,
                       IsDisabled = json.IsDisabled,
                       LastModified = json.LastModified,
                       InventoryId = inventoryInDb.Id,
                       BrandId = modelBrand.Id
                   };
               },
               modelOnUpdate: (json, entityInDb) =>
               {
                   var modelBrand = brandsInDB
                   .Where(brand => brand.UUId == json.BrandUUId)
                   .FirstOrDefault();

                   entityInDb.InventoryId = inventoryInDb.Id;
                   entityInDb.IsDisabled = json.IsDisabled;
                   entityInDb.Name = json.Name;
                   entityInDb.UUId = json.UUId;
                   entityInDb.LastModified = json.LastModified;
                   entityInDb.BrandId = modelBrand.Id;

                   return entityInDb;
               }
            );

            #endregion

            #region Items Sync

            List<ItemModel> modelsInDb = new List<ItemModel>();
            List<ItemPlace> placesInDb = new List<ItemPlace>();
            List<ItemState> statesInDb = new List<ItemState>();
            List<ItemType> typesInDb = new List<ItemType>();

            modelsInDb.AddRange(await _dbContext.Set<ItemModel>().ToListAsync());
            placesInDb.AddRange(await _dbContext.Set<ItemPlace>().ToListAsync());
            statesInDb.AddRange(await _dbContext.Set<ItemState>().ToListAsync());
            typesInDb.AddRange(await _dbContext.Set<ItemType>().ToListAsync());

            var isItemsSync = await SyncInventoryEntitiesAsync
                <JsonItem, Item>(
                    inventory.UUId.ToString(),
                    inventoryFull.Items,
                    modelOnCreate: (json) =>
                    {
                        var itemModel = modelsInDb.Where(iM => iM.UUId == json.ModelUUId).FirstOrDefault();
                        var itemPlace = placesInDb.Where(iP => iP.UUId == json.PlaceUUId).FirstOrDefault();
                        var itemState = statesInDb.Where(iS => iS.UUId == json.ItemStateUUId).FirstOrDefault();
                        var itemType = typesInDb.Where(iT => iT.UUId == json.TypeUUId).FirstOrDefault();

                        return new Item()
                        {
                            UUId = json.UUId,
                            Name = json.Name,
                            Description = json.Description,
                            IsDisabled = json.IsDisabled,
                            ItemInput = json.ItemInput,
                            ItemOutput = json.ItemOutput,
                            LastModified = json.LastModified,
                            InventoryId = inventoryInDb.Id,
                            ItemStateId = itemState.Id,
                            ModelId = itemModel.Id,
                            PlaceId = itemPlace.Id,
                            TypeId = itemType.Id,
                        };
                    },
                    modelOnUpdate: (json, entityInDb) =>
                    {
                        var itemModel = modelsInDb.Where(iM => iM.UUId == json.ModelUUId).FirstOrDefault();
                        var itemPlace = placesInDb.Where(iP => iP.UUId == json.PlaceUUId).FirstOrDefault();
                        var itemState = statesInDb.Where(iS => iS.UUId == json.ItemStateUUId).FirstOrDefault();
                        var itemType = typesInDb.Where(iT => iT.UUId == json.TypeUUId).FirstOrDefault();

                        entityInDb.InventoryId = inventoryInDb.Id;
                        entityInDb.IsDisabled = json.IsDisabled;
                        entityInDb.Name = json.Name;
                        entityInDb.UUId = json.UUId;
                        entityInDb.LastModified = json.LastModified;
                        entityInDb.Description = json.Description;
                        entityInDb.ItemInput = json.ItemInput;
                        entityInDb.ItemOutput = json.ItemOutput;
                        entityInDb.ItemStateId = itemState.Id;
                        entityInDb.ModelId = itemModel.Id;
                        entityInDb.PlaceId = itemPlace.Id;
                        entityInDb.TypeId = itemType.Id;

                        return entityInDb;
                    }
                );

            #endregion

            #endregion

            return new Response()
            {
                IsSuccess = true,
                Message = "Sync completed"
            };
        }

        /// <summary>
        /// Sync a single inventory (without it's children) with the database
        /// </summary>
        /// <param name="jsonInventory"></param>
        /// <returns></returns>
        private async Task<Response> SyncSingleInventoryAsync(JsonInventory jsonInventory)
        {
            try
            {
                var inventoryInDB = await _dbContext
                    .Set<Inventory>()
                    .Where(i => i.UUId == jsonInventory.UUId)
                    .FirstOrDefaultAsync();

                if (inventoryInDB != null)
                {
                    if (jsonInventory.LastModified.Value.CompareTo(inventoryInDB.LastModified.Value) >= 0)
                    {
                        inventoryInDB.UUId = jsonInventory.UUId;
                        inventoryInDB.Name = jsonInventory.Name;
                        inventoryInDB.IsDisabled = jsonInventory.IsDisabled;
                        inventoryInDB.LastModified = jsonInventory.LastModified;

                        _dbContext.Entry<Inventory>(inventoryInDB).CurrentValues.SetValues(
                            inventoryInDB
                        );

                        _dbContext.Entry<Inventory>(inventoryInDB).State = EntityState.Modified;

                        await _dbContext.SaveChangesAsync();
                    }
                }
                else
                {
                    inventoryInDB = 
                        new Inventory()
                        {
                            UUId = jsonInventory.UUId,
                            Name = jsonInventory.Name,
                            LastModified = jsonInventory.LastModified,
                            IsDisabled = jsonInventory.IsDisabled
                        };

                    await _dbContext.AddAsync(
                        inventoryInDB
                    );

                    await _dbContext.SaveChangesAsync();
                }

                return new Response()
                {
                    IsSuccess = true,
                    Message = "Inventory synchronization completed.",
                    Result = inventoryInDB
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Syncs the inventory entity with the database.
        /// </summary>
        /// <typeparam name="TJSONModel">JSON Model Type</typeparam>
        /// <typeparam name="TEntityModel">Database Model Type</typeparam>
        /// <param name="inventoryUUID">Inventory UUID it belongs to</param>
        /// <param name="jsonEntities">Json Entities collection</param>
        /// <param name="modelOnCreate">Model to return/insert (Function)</param>
        /// <param name="modelOnUpdate">Model to return/update (Function)</param>
        /// <returns></returns>
        private async Task<List<Response>> SyncInventoryEntitiesAsync<TJSONModel, TEntityModel>(
            string inventoryUUID,
            ICollection<TJSONModel> jsonEntities,
            Func<TJSONModel, TEntityModel> modelOnCreate,
            Func<TJSONModel, TEntityModel, TEntityModel> modelOnUpdate)
            
            where TJSONModel : class, IJSONInventoryEntity 
            where TEntityModel : class, IEntity
        {
            List<Response> response = new List<Response>();

            foreach (var jsonEntity in jsonEntities)
            {
                try
                {
                    var entityInDb = await _dbContext
                        .Set<TEntityModel>()
                        .Where(entity => entity.UUId == jsonEntity.UUId)
                        .FirstOrDefaultAsync();

                    #region Diff between Remote and Local

                    if (entityInDb != null)
                    {
                        // If remote is newer
                        if (jsonEntity.LastModified.Value.CompareTo(entityInDb.LastModified.Value) >= 0)
                        {
                            _dbContext.Entry<TEntityModel>(entityInDb).CurrentValues.SetValues(
                                modelOnUpdate(jsonEntity, entityInDb)
                            );

                            _dbContext.Entry<TEntityModel>(entityInDb).State = EntityState.Modified;

                            await _dbContext.SaveChangesAsync();

                            response.Add(
                                new Response()
                                {
                                    IsSuccess = true,
                                    Message = "localOlder",
                                    Result = entityInDb
                                }
                            );

                            continue;
                        }
                        // If local is newer
                        else
                        {
                            response.Add(
                                new Response()
                                {
                                    IsSuccess = true,
                                    Message = "localNewer",
                                    Result = entityInDb
                                }
                            );

                            continue;
                        }
                    }

                    #endregion
                        
                    // If remote is new
                    var inventory = await _dbContext
                        .Set<Inventory>()
                        .Where(inv => inv.UUId == jsonEntity.InventoryUUId)
                        .FirstOrDefaultAsync();

                    if (inventory != null)
                    {                        

                        if(jsonEntity is JsonItem)
                        {
                            var jsonItem = jsonEntity as JsonItem;
                            jsonItem.ImagePath = "~/media/images/common/no_preview.jpg";

                            await _dbContext.AddAsync(
                                modelOnCreate(jsonEntity)
                                );
                        }
                        else
                        {
                            await _dbContext.AddAsync(
                            modelOnCreate(jsonEntity)
                        );
                        }

                        await _dbContext.SaveChangesAsync();

                        continue;
                    }

                    response.Add(
                        new Response()
                        {
                            IsSuccess = false,
                            Message = $"Inventory could not be found. {jsonEntity.InventoryUUId}"
                        }
                    );
                }
                catch (Exception ex)
                {
                    // If it fails
                    response.Add(
                        new Response()
                        {
                            IsSuccess = false,
                            Message = $"Unexpected error in {jsonEntity.UUId}: {ex.Message}"
                        }
                    );
                }
            }
            return response;
        }
    }
}
