﻿
using MI.Web.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MI.Web.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly IConfiguration _configuration;

        public EmailSender(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Sends an email using appsettings.json configurations
        /// </summary>
        /// <param name="email">Destination E-Mail</param>
        /// <param name="destination">Destination Display Name</param>
        /// <param name="subject">Email Subject</param>
        /// <param name="message">Email Message</param>
        /// <returns></returns>
        public Task SendEmailAsync(string email, string destination, string subject, string message)
        {
            try
            {
                var senderEmail = new MailAddress(_configuration["EmailSettings:Sender"], _configuration["EmailSettings:SenderName"]);
                var receiverEmail = new MailAddress(email, destination);

                var password = _configuration["EmailSettings:Password"];

                var smtp = new SmtpClient()
                {
                    Host = _configuration["EmailSettings:Server"],
                    Port = int.Parse(_configuration["EmailSettings:Port"]),
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(senderEmail.Address, password)
                };

                using (var mess = new MailMessage(senderEmail, receiverEmail)
                {
                    Subject = subject,
                    Body = message,
                    IsBodyHtml = true,
                    Sender = senderEmail
                })
                {
                    try
                    {
                        smtp.Send(mess);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
