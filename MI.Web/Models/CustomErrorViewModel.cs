﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models
{
    public class CustomErrorViewModel
    {
        public int StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public string ImageUrl { get; set; }
    }
}
