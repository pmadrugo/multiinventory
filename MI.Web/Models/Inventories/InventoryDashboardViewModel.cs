﻿using MI.Web.Data.Entities.InventorySection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models.Inventories
{
    public class InventoryDashboardViewModel
    {
        public Inventory Inventory { get; set; }

        public IQueryable<Item> LastTenItemsAdded { get; set; }

        public IQueryable<Item> LastTenItemsModified { get; set; }

        public int ItemCount { get; set; }
    }
}
