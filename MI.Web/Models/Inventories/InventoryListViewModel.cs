﻿using MI.Web.Data.Entities.InventorySection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models.Inventories
{
    public class InventoryListViewModel
    {
        public IQueryable<Inventory> OwnedInventories { get; set; }

        public IQueryable<Inventory> InvitedInventories { get; set; }
    }
}
