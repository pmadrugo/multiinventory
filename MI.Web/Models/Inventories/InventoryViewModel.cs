﻿using MI.Web.Data.Entities.InventorySection;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models.Inventories
{
    public class InventoryViewModel : Inventory
    {
        [Display(Name = "Responsável pelo Inventário")]
        public string OwnerID { get; set; }

        [Display(Name = "Imagem do Inventário")]
        public IFormFile ImageFile { get; set; }
    }
}
