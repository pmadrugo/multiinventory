﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models.Inventories
{
    public class InventoryUsersViewModel
    {
        public string UserId { get; set; }

        public string UserFullName { get; set; }

        public string UserEmail { get; set; }

        public string UserImagePath { get; set; }

        public int RoleId { get; set; }

        public string RoleName { get; set; }
    }
}
