﻿using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models.Inventories
{
    public class ItemViewModel : Item
    {
        [Display(Name="Imagem")]
        public IFormFile ImageFile { get; set; }

        public int BrandId { get; set; }
    }
}
