﻿using MI.Web.Extensions.DataAttributes;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models.Accounts
{
    public class RegisterNewViewModel : ProfileViewModel
    {
        //[IsPassword(ErrorMessage = "Password inválida.")]
        [Required(ErrorMessage = "É necessario preencher uma {0}.")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "É necessário confirmar uma password.")]
        [Compare("Password", ErrorMessage = "A password não coincide com a confirmação.")]
        [Display(Name = "Confirme Password")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
