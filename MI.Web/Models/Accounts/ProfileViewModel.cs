﻿using MI.Web.Extensions.DataAttributes;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models.Accounts
{
    public class ProfileViewModel
    {
        [Required(ErrorMessage = "É necessário preencher um {0}.")]
        [Display(Name = "E-Mail")]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        public string UserName { get; set; }

        [Required(ErrorMessage = "É necessario preencher {0}.")]
        [Display(Name = "Primeiro Nome")]
        [StringLength(50, ErrorMessage = "{0} só pode ter até {1} caractéres.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "É necessário preencher {0}.")]
        [Display(Name = "Apelido")]
        [StringLength(50, ErrorMessage = "{0} só pode ter até {1} caractéres.")]
        public string LastName { get; set; }

        public string DisplayName { get; set; }

        [Display(Name = "Imagem de Perfil")]
        public IFormFile ImageFile { get; set; }

        public string ImagePath { get; set; }

        [Required(ErrorMessage = "É necessário preencher {0}.")]
        [Display(Name = "Número de Identificação Fiscal")]
        [RegularExpression("^[1-35][0-9]{8,}$", ErrorMessage = "Tem de inserir um {0} válido.")]
        public string VATNumber { get; set; }

        [Display(Name = "Data de Nascimento")]
        [Required(ErrorMessage = "É necessário preencher uma {0}")]
        [BirthdateOfAgeRange(ErrorMessage = "É necessario que seja maior de idade.")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }
    }
}
