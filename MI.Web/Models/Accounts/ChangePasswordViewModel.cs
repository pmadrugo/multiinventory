﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models.Accounts
{
    public class ChangePasswordViewModel
    {
        [Required]
        [Display(Name = "Password Actual")]
        public string CurrentPassword { get; set; }

        [Required]
        [Display(Name = "Nova Password")]
        public string NewPassword { get; set; }

        [Required]
        [Compare("NewPassword")]
        [Display(Name = "Confirmar Password")]
        public string NewPasswordConfirm { get; set; }
    }
}
