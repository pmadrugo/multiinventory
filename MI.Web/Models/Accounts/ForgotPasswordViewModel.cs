﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models.Accounts
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Tem de preencher {0}.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}
