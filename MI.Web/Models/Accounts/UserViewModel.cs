﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models.Accounts
{
    public class UserViewModel
    {
        public string UserID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ImagePath { get; set; }

        public string FullName { get => $"{FirstName} {LastName}"; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public RoleViewModel Role { get; set; }
    }
}
