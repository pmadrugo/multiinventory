﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models.Accounts
{
    public class ExternalRegisterViewModel : RegisterNewViewModel
    {
        public string ReturnURL { get; set; }

        public string LoginProvider { get; set; }
    }
}
