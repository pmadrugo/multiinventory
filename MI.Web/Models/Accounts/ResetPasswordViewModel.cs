﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models.Accounts
{
    public class ResetPasswordViewModel
    {
        public string Token { get; set; }

        [Required(ErrorMessage = "Por favor insira o seu email.")]
        [DataType(DataType.EmailAddress)]
        public string Username { get; set; }
        
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Por favor insira uma password.")]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "A confirmação não coincide com a password inserida.")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
