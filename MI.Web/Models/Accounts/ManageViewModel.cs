﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models.Accounts
{
    public class ManageViewModel : ProfileViewModel
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "É necessario preencher uma {0}.")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
