﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Models
{
    public class EmailSettingsViewModel
    {
        public string Server { get; set; }
        public int Port { get; set; }

        public string SenderName { get; set; }
        public string Sender { get; set; }
        public string Password { get; set; }
    }
}
