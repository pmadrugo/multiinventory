﻿using MI.Web.Data.Entities.ApplicationSection;
using MI.Web.Data.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace MI.Web.Extensions
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class InventoryMemberAuthorize : AuthorizeAttribute, IAuthorizationFilter
    {
        public string Roles { get; set; }

        /// <summary>
        /// Data Annotation similar to default Authorize but configured to also use the Inventory user subsystem
        /// </summary>
        /// <param name="Roles">Roles to check for</param>
        public InventoryMemberAuthorize(string Roles = "")
        {
            this.Roles = Roles; 
        }

        public void OnAuthorization (AuthorizationFilterContext context)
        {
            var user = context.HttpContext.User;
            if (!user.Identity.IsAuthenticated)
            {
                return;
            }

            var routeData = context.RouteData.Values.GetValueOrDefault("inventoryId");
            if (routeData == null)
            {
                //context.Result = new RedirectToRouteResult("default", new { controller = "Account", action = "Login" });
                context.Result = new BadRequestResult();

                return;
            }

            Guid inventoryId;
            if (!Guid.TryParse(context.RouteData.Values.GetValueOrDefault("inventoryId").ToString(), out inventoryId))
            {
                //context.Result = new RedirectToRouteResult("default", new { controller = "Account", action = "Login" });
                context.Result = new NotFoundResult();

                return;
            }

            var roleList = this.Roles.Split(',');
            var userId = user.GetUserId();
            var unitOfWork = context.HttpContext.RequestServices.GetService<IUnitOfWork>();

            bool isAuthorized = false;
            if (user.IsInRole("Admin") || user.IsInRole("Superuser"))
            {
                isAuthorized = true;
            }

            var userRole = unitOfWork.Inventories.GetUserRoleInInventory(user.GetUserId(), inventoryId);

            if (string.IsNullOrEmpty(this.Roles))
            {
                if (userRole != null)
                {
                    isAuthorized = true;
                }
            }
            else
            {
                if (roleList.Contains(userRole.Role))
                {
                    isAuthorized = true;
                }
            }

            if (!isAuthorized)
            {
                context.Result = new RedirectToRouteResult("default", new { controller = "Account", action = "Login" });
            }
        }

    }
}
