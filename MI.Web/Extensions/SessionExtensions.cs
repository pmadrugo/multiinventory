﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Extensions
{
    public static class SessionExtensions
    {
        /// <summary>
        /// Serialize object into Session data.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="session">Session object</param>
        /// <param name="key">Session Key</param>
        /// <param name="value">Object to serialize</param>
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        /// <summary>
        /// Deserialize object from Session data.
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="session">Session object</param>
        /// <param name="key">Session Key</param>
        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) :
                            JsonConvert.DeserializeObject<T>(value);
        }
    }
}
