﻿
using MI.Web.Data.Entities;
using MI.Web.Data.Entities.ApplicationSection;
using MI.Web.Data.Interfaces;
using MI.Web.Helpers;
using MI.Web.Helpers.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MI.Web.Extensions
{
    public static class UserExtensions
    {
        /// <summary>
        /// Gets current user ID
        /// </summary>
        /// <param name="principal">Current Claims Principal</param>
        /// <returns>Exception or the current userID</returns>
        public static string GetUserId(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));

            return principal.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        }

        /// <summary>
        /// Gets current user full name
        /// </summary>
        /// <param name="principal">Current Claims Principal</param>
        /// <param name="userManager">Usermanager</param>
        /// <returns>"Convidado" or the current user full name</returns>
        public static async Task<string> GetUserFullNameAsync(this ClaimsPrincipal principal, UserManager<User> userManager)
        {
            var userId = GetUserId(principal);
            if (userId != null)
            {                

                var user = await userManager.FindByIdAsync(userId);
                if (user != null)
                {
                    return user.FullName;
                }
            }

            return "Convidado";
        }

        /// <summary>
        /// Gets current user profile image
        /// </summary>
        /// <param name="principal">Current Claims Principal</param>
        /// <param name="userManager">Usermanager</param>
        /// <returns>Default user image or the current user image URL</returns>
        public static async Task<string> GetUserImagePathAsync(this ClaimsPrincipal principal, UserManager<User> userManager)
        {
            var userId = GetUserId(principal);
            if (userId != null)
            {
                var user = await userManager.FindByIdAsync(userId);
                if (user != null)
                {
                    if (!string.IsNullOrEmpty(user.ImagePath))
                    {
                        return user.ImagePath;
                    }
                }
            }

            return "/media/images/common/default_user_image.jpg";
        }

        /// <summary>
        /// Checks if user is in inventory
        /// </summary>
        /// <param name="inventoryId">Inventory ID to check</param>
        /// <param name="principal">Current ClaimsPrincipal</param>
        /// <param name="unitOfWork">The Unit of Work to be used</param>
        /// <returns>Default user image or the current user image URL</returns>
        public static async Task<bool> IsUserInInventory(this ClaimsPrincipal principal, IUnitOfWork unitOfWork, int inventoryId)
        {
            var userId = GetUserId(principal);
            if(userId != null)
            {
                if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(userId,inventoryId) != null)
                {
                    return true;
                }                
            }
            return false;
        }

        /// <summary>
        /// Checks if the user is notified in inventory
        /// </summary>
        /// <param name="inventoryId">Inventory ID to check</param>
        /// <param name="principal">Current ClaimsPrincipal</param>
        /// <param name="unitOfWork">The Unit of Work to be used</param>
        /// <returns>Default user image or the current user image URL</returns>
        public static async Task<bool> IsUserNotifiedInInventory(this ClaimsPrincipal principal, IUnitOfWork unitOfWork, int inventoryId)
        {
            var userId = GetUserId(principal);

            if (userId != null)
            {
                if (await unitOfWork.NotifiableUsers.IsUserNotifiedOfInventory(inventoryId, userId))
                {
                    return true;
                }
            }
            return false;
        }

        public static async Task<bool> IsUserAdminInInventory(this ClaimsPrincipal principal, IUnitOfWork unitOfWork, int inventoryId)
        {
            var userId = GetUserId(principal);

            var result = unitOfWork.Inventories.GetUserRoleInInventory(userId,inventoryId);

            if(result.Role == "Employee")
            {
                return false;
            }

            return true;
        }
    }
}
