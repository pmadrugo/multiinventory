﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Extensions.TagHelpers
{
    using Microsoft.AspNetCore.Mvc.TagHelpers;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    using Microsoft.AspNetCore.Razor.TagHelpers;
    using Microsoft.AspNetCore.Routing;
    using System.Text;

    [HtmlTargetElement("action-icon")]
    public class ActionIconTagHelper : AnchorTagHelper
    {
        /// <summary>
        /// Generate an action-icon tag helper, based on the a (anchor) tag, but supporting the properties for text and icon. Icon being (fas) font-awesome based.
        /// </summary>
        /// <param name="generator">Internal HTML Generator</param>
        public ActionIconTagHelper(IHtmlGenerator generator) : base(generator)
        {
        }

        public string Icon { get; set; }
        public string Text { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "a";

            output.Content.SetHtmlContent($"<div class='actionIcon'><i class='actionIcon_Icon fas fa-{Icon}'></i><span class='actionIcon_Text'>{Text}</span></div>");
           
            base.Process(context, output);
        }
    }

    [HtmlTargetElement("action-go-back")]
    public class ActionGoBackTagHelper : AnchorTagHelper
    {
        /// <summary>
        /// Generate an action-go-back tag helper, based on the a (anchor) tag, but supporting the properties for text and icon. Icon being (fas) font-awesome based.
        /// This tag helper adds the previous page as href if supported.
        /// </summary>
        /// <param name="generator">Internal HTML Generator</param>
        public ActionGoBackTagHelper(IHtmlGenerator generator) : base(generator)
        {
            
        }

        public string Icon { get; set; }
        public string Text { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "a";

            var referer = this.ViewContext.HttpContext.Request.Headers["Referer"];
            
            output.Content.SetHtmlContent($"<div class='actionIcon'><i class='actionIcon_Icon fas fa-{Icon}'></i><span class='actionIcon_Text'>{Text}</span></div>");

            base.Process(context, output);

            if (!string.IsNullOrEmpty(referer))
            {
                var attributes = output.Attributes;
                var href = attributes.FirstOrDefault(x => x.Name == "href");
                if (href != null)
                {
                    attributes.Remove(href);
                    attributes.Add(new TagHelperAttribute("href", referer));
                }
            }
        }
    }
}
