﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Extensions
{
    public static class ControllerExtensions
    {
        /// <summary>
        /// Receives error message, saved in ViewBag.ErrorMessage
        /// </summary>
        /// <param name="controller"></param>
        public static void ExpectingError(this Controller controller)
        {
            if (controller.TempData["ErrorMessage"] != null)
            {
                controller.ViewBag.ErrorMessage = controller.TempData["ErrorMessage"].ToString();
                controller.TempData.Remove("ErrorMessage");
            }
        }

        /// <summary>
        /// Sends error message to TempData["ErrorMessage"]
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="errorMessage"></param>
        public static void SendError(this Controller controller, string errorMessage)
        {
            controller.TempData["ErrorMessage"] = errorMessage;
        }

        /// <summary>
        /// Receives success message, saved in ViewBag.SuccessMessage
        /// </summary>
        /// <param name="controller"></param>
        public static void ExpectingSuccess(this Controller controller)
        {
            if (controller.TempData["SuccessMessage"] != null)
            {
                controller.ViewBag.SuccessMessage = controller.TempData["SuccessMessage"].ToString();
                controller.TempData.Remove("SuccessMessage");
            }
        }

        /// <summary>
        /// Sends success message to TempData["SuccessMessage"]
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="successMessage"></param>
        public static void SendSuccess(this Controller controller, string successMessage)
        {
            controller.TempData["SuccessMessage"] = successMessage;
        }
    }
}
