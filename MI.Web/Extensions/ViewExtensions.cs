﻿using MI.Web.Data.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Extensions
{
    public static class ViewExtensions
    {
        /// <summary>
        /// Get the indicated inventory's name
        /// </summary>
        /// <param name="viewContext">The current ViewContext</param>
        /// <param name="unitOfWork">Unit of Work</param>
        /// <param name="inventoryId">Inventory GUID</param>
        /// <returns></returns>
        public static async Task<string> GetInventoryNameAsync(this ViewContext viewContext, IUnitOfWork unitOfWork, string inventoryId)
        {
            if (!string.IsNullOrEmpty(inventoryId))
            {
                var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId);
                if (inventory != null)
                {
                    return inventory.Name;
                }
            }

            return "";
        }
    }
}
