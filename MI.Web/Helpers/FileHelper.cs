﻿using MI.Common.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Helpers
{
    public static class FileHelper
    {
        /// <summary>
        /// Saves the image to the server and returns it's path.
        /// </summary>
        /// <param name="imageFile">The file being uploaded</param>
        /// <param name="folder">The directory path</param>
        /// <param name="imagePath">The image path</param>
        /// <returns>A response carrying success status, the image path and a message</returns>
        public static async Task<Response> SaveImage(IFormFile imageFile,string folder, string imagePath = null)
        {
            var path = imagePath == null ? "~/media/images/common/no_preview.jpg" : imagePath;

            if (folder == "profile")
            {
                path = "~/media/images/common/default_user_image.jpg";
            }

            if(imageFile != null && imageFile.Length > 0)
            {
                var guid = Guid.NewGuid().ToString();
                var file = $"{guid}.png";

                if (imagePath != null)
                {
                    if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), $"wwwroot/media/images/{folder}", imagePath.Split("/").Last())))
                    {
                        file = imagePath.Split("/").Last();
                    }
                }

                path = Path.Combine(
                    Directory.GetCurrentDirectory(),
                    $"wwwroot/media/images/{folder}",
                    file
                    );

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    try
                    {
                        await imageFile.CopyToAsync(stream);
                    }
                    catch (Exception ex)
                    {
                        return new Response
                        {
                            IsSuccess = false,
                            Result = "~/media/images/common/no_preview.jpg",
                            Message = $"Failed to save image on server \nUsing default image \nError:{ex.Message}"
                        };
                    }
                }

                path = $"/media/images/{folder}/{file}";
            }

            return new Response {
                IsSuccess = true,
                Result = path,
                Message = "Image has been saved on server"
            };
        }
    }
}
