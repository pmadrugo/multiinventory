﻿using MI.Common.Models;
using MI.Web.Data.Entities;
using MI.Web.Data.Entities.ApplicationSection;
using MI.Web.Data.Interfaces;
using MI.Web.Helpers.Interfaces;
using MI.Web.Models.Accounts;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Helpers
{
    public class UserHelper : IUserHelper
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signinManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public UserHelper(UserManager<User> userManager, SignInManager<User> signinManager, RoleManager<IdentityRole> roleManager)
        {
            this._userManager = userManager;
            this._signinManager = signinManager;
            this._roleManager = roleManager;
        }

        /// <summary>
        /// Adds a new user
        /// </summary>
        /// <param name="user">User object</param>
        /// <param name="password">User password</param>
        /// <returns></returns>
        private async Task<IdentityResult> AddUserAsync(User user, string password)
        {
            return await this._userManager.CreateAsync(user, password);
        }

        /// <summary>
        /// Add user to role
        /// </summary>
        /// <param name="user">User object</param>
        /// <param name="roleName">Role to add</param>
        /// <returns></returns>
        public async Task<IdentityResult> AddToRoleAsync(User user, string roleName)
        {
            return await this._userManager.AddToRoleAsync(user, roleName);
        }

        /// <summary>
        /// Change user password
        /// </summary>
        /// <param name="user">User object</param>
        /// <param name="oldPassword">Old password</param>
        /// <param name="newPassword">New password</param>
        /// <returns></returns>
        public async Task<IdentityResult> ChangePasswordAsync(User user, string oldPassword, string newPassword)
        {
            return await this._userManager.ChangePasswordAsync(user, oldPassword, newPassword);
        }

        /// <summary>
        /// Gets user by email
        /// </summary>
        /// <param name="email">Email to search user by</param>
        /// <returns>User</returns>
        public async Task<User> GetUserByEmailAsync(string email)
        {
            return await this._userManager.FindByEmailAsync(email);
        }

        /// <summary>
        /// Gets user by Id
        /// </summary>
        /// <param name="userId">Id to search user by</param>
        /// <returns>User</returns>
        public async Task<User> GetUserByIdAsync(string userId)
        {
            return await this._userManager.FindByIdAsync(userId);
        }

        /// <summary>
        /// Checks if user can be logged in
        /// </summary>
        /// <param name="user">User object</param>
        /// <returns>True or false</returns>
        public async Task<bool> CanSignInAsync(User user)
        {
            var userExists = await _userManager.FindByIdAsync(user.Id);
            if (userExists == null)
            {
                return false;
            }

            if (userExists.EmailConfirmed)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if user can be logged in
        /// </summary>
        /// <param name="userName">Username (E-Mail)</param>
        /// <returns>True or false</returns>
        public async Task<bool> CanSignInAsync(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
                return false;
            }

            return await CanSignInAsync(user);
        }

        /// <summary>
        /// Logs the user in
        /// </summary>
        /// <param name="loginViewModel">LoginViewModel containing the username, password e rememberme.</param>
        /// <returns></returns>
        public async Task<SignInResult> LoginAsync(LoginViewModel loginViewModel)
        {
            return await this._signinManager.PasswordSignInAsync
                (loginViewModel.Username, loginViewModel.Password, loginViewModel.RememberMe, false);
        }

        /// <summary>
        /// Logs out the user
        /// </summary>
        /// <returns></returns>
        public async Task LogoutAsync()
        {
            await this._signinManager.SignOutAsync();
        }

        /// <summary>
        /// Updates user data
        /// </summary>
        /// <param name="user">User object</param>
        /// <returns></returns>
        public async Task<IdentityResult> UpdateUserAsync(User user)
        {
            return await this._userManager.UpdateAsync(user);
        }

        /// <summary>
        /// Validates the password for the user
        /// </summary>
        /// <param name="user">User object</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        public async Task<SignInResult> ValidatePasswordAsync(User user, string password)
        {
            return await this._signinManager.CheckPasswordSignInAsync(user, password, false);
        }

        /// <summary>
        /// Creates a super user if it does not exist
        /// </summary>
        /// <param name="user">User object</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        public async Task<IdentityResult> CreateSuperUserIfNotExistsAsync(User user, string password)
        {
            if (await this.GetUserByEmailAsync(user.Email) == null)
            {
                var identityResult = await this.AddUserAsync(user, password);
                if (identityResult.Succeeded)
                {
                    var addAsSuperUserResult = await _userManager.AddToRoleAsync(user, "Superuser");
                    if (!addAsSuperUserResult.Succeeded)
                    {
                        await _userManager.DeleteAsync(user);
                    }

                    return addAsSuperUserResult;
                }

                return identityResult;
            }

            return new IdentityResult();
        }

        /// <summary>
        /// Creates an admin if it does not exist
        /// </summary>
        /// <param name="user">User object</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        public async Task<IdentityResult> CreateAdminUserAsync(User user, string password)
        {
            if (await this.GetUserByEmailAsync(user.Email) == null)
            {
                var identityResult = await this.AddUserAsync(user, password);
                if (identityResult.Succeeded)
                {
                    var addAsSuperUserResult = await _userManager.AddToRoleAsync(user, "Admin");
                    if (!addAsSuperUserResult.Succeeded)
                    {
                        await _userManager.DeleteAsync(user);
                    }

                    return addAsSuperUserResult;
                }

                return identityResult;
            }

            return new IdentityResult();
        }

        /// <summary>
        /// Creates a user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<IdentityResult> CreateUserAsync(User user, string password)
        {
            if (await this.GetUserByEmailAsync(user.Email) == null)
            {
                var identityResult = await this.AddUserAsync(user, password);
                if (identityResult.Succeeded)
                {
                    var addAsSuperUserResult = await _userManager.AddToRoleAsync(user, "User");
                    if (!addAsSuperUserResult.Succeeded)
                    {
                        await _userManager.DeleteAsync(user);
                    }

                    return addAsSuperUserResult;
                }

                return identityResult;
            }

            return new IdentityResult();
        }

        /// <summary>
        /// Generates email confirmation for user
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Email confirmation token</returns>
        public async Task<string> GenerateEmailConfirmationTokenAsync(User user)
        {
            return await _userManager.GenerateEmailConfirmationTokenAsync(user);
        }

        /// <summary>
        /// Confirms the user email
        /// </summary>
        /// <param name="user">User object</param>
        /// <param name="token">Email confirmation token</param>
        /// <returns></returns>
        public async Task<IdentityResult> ConfirmEmailAsync (User user, string token)
        {
            return await _userManager.ConfirmEmailAsync(user, token);
        }

        /// <summary>
        /// Confirms the user email
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <param name="token">Email confirmation token</param>
        /// <returns></returns>
        public async Task<IdentityResult> ConfirmEmailAsync (string userID, string token)
        {
            if (string.IsNullOrEmpty(userID))
            {
                return new IdentityResult();
            }

            var user = await _userManager.FindByIdAsync(userID);
            if (user == null)
            {
                return new IdentityResult();
            }

            return await ConfirmEmailAsync(user, token);
        }

        /// <summary>
        /// Checks if the user email is confirmed
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <returns></returns>
        public async Task<bool> IsEmailConfirmedAsync(string userID)
        {
            if (string.IsNullOrEmpty(userID))
            {
                return false;
            }

            var user = await _userManager.FindByIdAsync(userID);
            if (user == null)
            {
                return false;
            }

            return await IsEmailConfirmedAsync(user);
        }

        /// <summary>
        /// Checks if the user email is confirmed
        /// </summary>
        /// <param name="user">User</param>
        /// <returns></returns>
        public async Task<bool> IsEmailConfirmedAsync(User user)
        {
            return await _userManager.IsEmailConfirmedAsync(user);
        }

        /// <summary>
        /// Generates password reset token for user
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>User object token reset</returns>
        public async Task<string> GeneratePasswordResetTokenAsync(User user)
        {
            return await _userManager.GeneratePasswordResetTokenAsync(user);
        }

        /// <summary>
        /// Checks if the user account is approved
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns></returns>
        public async Task<bool> IsAccountApprovedAsync(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return false;
            }

            var user = await _userManager.FindByNameAsync(email);
            if (user != null)
            {
                if (user.IsApproved)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if the user account is approved
        /// </summary>
        /// <param name="user">User</param>
        /// <returns></returns>
        public bool IsAccountApproved(User user)
        {
            if (user.IsApproved)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Get accounts that are pending admin approval
        /// </summary>
        /// <returns></returns>
        public IQueryable<User> GetPendingAccounts()
        {
            return _userManager.Users
                .Where(user => !user.IsApproved)
                .AsQueryable();
        }

        /// <summary>
        /// Approves the indicated user account
        /// </summary>
        /// <param name="user">User</param>
        /// <returns></returns>
        public async Task<Response> SetAccountApprovedAsync(User user)
        {
            try
            {
                user.IsApproved = true;

                await _userManager.UpdateAsync(user);

                return new Response()
                {
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    Message = ex.Message,
                    IsSuccess = false
                };
            }
        }

        /// <summary>
        /// Approves the indicated user account
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <returns></returns>
        public async Task<Response> SetAccountApprovedAsync(string userID)
        {
            if (string.IsNullOrEmpty(userID))
            {
                return new Response()
                {
                    Message = "ID inválido",
                    IsSuccess = false
                };
            }

            var user = await _userManager.FindByIdAsync(userID);
            if (user == null)
            {
                return new Response()
                {
                    Message = "Utilizador não encontrado",
                    IsSuccess = false
                };
            }

            return await SetAccountApprovedAsync(user);
        }

        /// <summary>
        /// Gets all users
        /// </summary>
        /// <returns></returns>
        public IQueryable<User> GetAllUsers()
        {
            return _userManager.Users.AsQueryable();
        }

        /// <summary>
        /// Checks the user password reset token
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="newPassword">New password</param>
        /// <param name="token">Password Reset Token</param>
        /// <returns></returns>
        public async Task<IdentityResult> CheckPasswordResetTokenAsync(User user, string newPassword, string token)
        {
            return await _userManager.ResetPasswordAsync(user, token, newPassword);
        }

        /// <summary>
        /// Checks the user password reset token
        /// </summary>
        /// <param name="userEmail">User's Email</param>
        /// <param name="newPassword">New password</param>
        /// <param name="token">Password Reset Token</param>
        /// <returns></returns>
        public async Task<IdentityResult> CheckPasswordResetTokenAsync(string userEmail, string newPassword, string token)
        {
            var user = await _userManager.FindByEmailAsync(userEmail);
            if (user != null)
            {
                return await CheckPasswordResetTokenAsync(user, newPassword, token);
            }

            return new IdentityResult();
        }

        /// <summary>
        /// Gets user current role
        /// </summary>
        /// <param name="user">User</param>
        /// <returns></returns>
        public async Task<Response> GetRoleAsync(User user)
        {
            var role = (await _userManager.GetRolesAsync(user))?.FirstOrDefault();
            if (string.IsNullOrEmpty(role))
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = "No role found"
                };
            }

            return new Response()
            {
                IsSuccess = true,
                Result = await _roleManager.FindByNameAsync(role)
            };
        }

        /// <summary>
        /// Sets current user role
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="roleName">Role</param>
        /// <returns></returns>
        public async Task<Response> SetRoleAsync(User user, string roleName)
        {
            try
            {
                var roleList = await _userManager.GetRolesAsync(user);

                foreach (var role in roleList)
                {
                    await _userManager.RemoveFromRoleAsync(user, role);
                }

                await _userManager.AddToRoleAsync(user, roleName);

                return new Response()
                {
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Removes the indicated user (USE ONLY ON APPROVAL DENIAL)
        /// </summary>
        /// <param name="user">User</param>
        /// <returns></returns>
        public async Task<IdentityResult> RemoveUserAsync(User user)
        {
            try
            {
                var userInDb = await _userManager.FindByIdAsync(user.Id);

                var isDeleted = await _userManager.DeleteAsync(userInDb);

                return isDeleted;
            }
            catch (Exception ex)
            {
                return new IdentityResult();
            }
        }

        /// <summary>
        /// Gets available external authentication schemes
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AuthenticationScheme>> GetExternalAuthenticationSchemesAsync()
        {
            return await _signinManager.GetExternalAuthenticationSchemesAsync();
        }

        /// <summary>
        /// Configures the external authentication properties
        /// </summary>
        /// <param name="provider">Provider</param>
        /// <param name="redirectUrl">URL where external login callback</param>
        /// <returns></returns>
        public AuthenticationProperties ConfigureExternalAuthenticationProperties(string provider, string redirectUrl)
        {
            return _signinManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
        }

        /// <summary>
        /// Gets external login info
        /// </summary>
        /// <returns></returns>
        public async Task<ExternalLoginInfo> GetExternalLoginInfoAsync()
        {
            return await _signinManager.GetExternalLoginInfoAsync();
        }

        /// <summary>
        /// Logins in the user via external authentication
        /// </summary>
        /// <param name="loginProvider">Login Provider</param>
        /// <param name="providerKey">Provider Key</param>
        /// <param name="isPersistent">Is Persistent?</param>
        /// <param name="bypassTwoFactor">Can bypass two factor?</param>
        /// <returns></returns>
        public async Task<SignInResult> ExternalLoginSignInAsync(string loginProvider, string providerKey, bool isPersistent, bool bypassTwoFactor)
        {
            return await _signinManager.ExternalLoginSignInAsync(loginProvider, providerKey, isPersistent, bypassTwoFactor);
        }

        /// <summary>
        /// Adds an external login to user
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="userLoginInfo">External Login User Info</param>
        /// <returns></returns>
        public async Task<IdentityResult> AddLoginAsync(User user, UserLoginInfo userLoginInfo)
        {
            return await _userManager.AddLoginAsync(user, userLoginInfo);
        }

        /// <summary>
        /// Removes an external login from user
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="loginProvider">Login Provider</param>
        /// <param name="providerKey">Provider Key</param>
        /// <returns></returns>
        public async Task<IdentityResult> RemoveLoginAsync(User user, string loginProvider, string providerKey)
        {
            return await _userManager.RemoveLoginAsync(user, loginProvider, providerKey);
        }

        /// <summary>
        /// Gets current user's logins
        /// </summary>
        /// <param name="user">User</param>
        /// <returns></returns>
        public async Task<IList<UserLoginInfo>> GetUserLoginsAsync(User user)
        {
            return await _userManager.GetLoginsAsync(user);
        }
    }
}
