﻿using MI.Web.Helpers.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Helpers
{
    public class RoleHelper : IRoleHelper
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        public RoleHelper(RoleManager<IdentityRole> roleManager)
        {
            this._roleManager = roleManager;
        }

        /// <summary>
        /// Checks if the role exists
        /// </summary>
        /// <param name="roleName">Role Name</param>
        /// <returns>True or false</returns>
        public async Task<bool> RoleExistsAsync(string roleName)
        {
            return await _roleManager.RoleExistsAsync(roleName);
        }

        /// <summary>
        /// Creates a new role
        /// </summary>
        /// <param name="role">IdentityRole object containing role information</param>
        /// <returns>IdentityResult</returns>
        public async Task<IdentityResult> CreateAsync(IdentityRole role)
        {
            return await _roleManager.CreateAsync(role);
        }

        /// <summary>
        /// Deletes a role
        /// </summary>
        /// <param name="role">IdentityRole object containing role information</param>
        /// <returns>IdentityResult</returns>
        public async Task<IdentityResult> DeleteAsync(IdentityRole role)
        {
            return await _roleManager.DeleteAsync(role);
        }

        /// <summary>
        /// Returns all the roles
        /// </summary>
        /// <returns></returns>
        public IQueryable<IdentityRole> GetAllRoles()
        {
            return _roleManager.Roles;
        }
    }
}
