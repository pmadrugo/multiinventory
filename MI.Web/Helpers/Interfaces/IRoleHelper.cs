﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Helpers.Interfaces
{
    public interface IRoleHelper
    {
        Task<IdentityResult> CreateAsync(IdentityRole role);

        Task<IdentityResult> DeleteAsync(IdentityRole role);
        IQueryable<IdentityRole> GetAllRoles();
        Task<bool> RoleExistsAsync(string roleName);
    }
}
