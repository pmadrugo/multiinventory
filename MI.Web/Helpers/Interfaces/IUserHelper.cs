﻿using MI.Common.Models;
using MI.Web.Data.Entities;
using MI.Web.Data.Entities.ApplicationSection;
using MI.Web.Models.Accounts;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Helpers.Interfaces
{
    public interface IUserHelper
    {
        Task<User> GetUserByEmailAsync(string email);

        //Task<IdentityResult> AddUserAsync(User user, string password);

        Task<SignInResult> LoginAsync(LoginViewModel loginViewModel);

        Task LogoutAsync();

        Task<IdentityResult> UpdateUserAsync(User user);

        Task<IdentityResult> ChangePasswordAsync(User user, string oldPassword, string newPassword);

        Task<SignInResult> ValidatePasswordAsync(User user, string password);
        Task<IdentityResult> CreateSuperUserIfNotExistsAsync(User user, string password);
        Task<IdentityResult> CreateAdminUserAsync(User user, string password);
        Task<User> GetUserByIdAsync(string userId);

        Task<string> GenerateEmailConfirmationTokenAsync(User user);
        Task<IdentityResult> ConfirmEmailAsync(string userID, string token);
        Task<IdentityResult> ConfirmEmailAsync(User user, string token);
        Task<bool> IsEmailConfirmedAsync(User user);
        Task<bool> IsEmailConfirmedAsync(string userID);
        Task<bool> CanSignInAsync(string userName);
        Task<bool> CanSignInAsync(User user);
        Task<string> GeneratePasswordResetTokenAsync(User user);
        Task<bool> IsAccountApprovedAsync(string email);
        bool IsAccountApproved(User user);
        IQueryable<User> GetPendingAccounts();
        Task<Response> SetAccountApprovedAsync(User user);
        Task<Response> SetAccountApprovedAsync(string userID);
        IQueryable<User> GetAllUsers();
        Task<IdentityResult> CheckPasswordResetTokenAsync(User user, string newPassword, string token);
        Task<IdentityResult> CheckPasswordResetTokenAsync(string userEmail, string newPassword, string token);
        Task<Response> SetRoleAsync(User user, string roleName);
        Task<Response> GetRoleAsync(User user);
        Task<IdentityResult> CreateUserAsync(User user, string password);
        Task<IdentityResult> RemoveUserAsync(User user);
        Task<IEnumerable<Microsoft.AspNetCore.Authentication.AuthenticationScheme>> GetExternalAuthenticationSchemesAsync();
        Microsoft.AspNetCore.Authentication.AuthenticationProperties ConfigureExternalAuthenticationProperties(string provider, string redirectUrl);
        Task<ExternalLoginInfo> GetExternalLoginInfoAsync();
        Task<SignInResult> ExternalLoginSignInAsync(string loginProvider, string providerKey, bool isPersistent, bool bypassTwoFactor);
        Task<IdentityResult> AddLoginAsync(User user, UserLoginInfo userLoginInfo);
        Task<IdentityResult> RemoveLoginAsync(User user, string loginProvider, string providerKey);
        Task<IList<UserLoginInfo>> GetUserLoginsAsync(User user);
    }
}
