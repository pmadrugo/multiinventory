﻿using MI.Common.JsonModels.Inventory;
using MI.Common.Models;
using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers.Interfaces;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.ApiControllers
{
    [Route("api/{inventoryId}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ItemTypesController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly INotifyService notifyService;
        private readonly IUserHelper userHelper;

        public ItemTypesController(IUnitOfWork unitOfWork, INotifyService notifyService, IUserHelper userHelper)
        {
            this.unitOfWork = unitOfWork;
            this.notifyService = notifyService;
            this.userHelper = userHelper;
        }

        [HttpGet]
        public async Task<IActionResult> GetItemTypes([FromRoute] Guid? inventoryId)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Os dados que forneceu estão incorretos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O Inventario não foi encontrado"
                });
            }

            //Checks if the User has some Role in the Inventory
            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            return Ok(new Response
            {
                IsSuccess = true,
                Result = unitOfWork.ItemTypes.GetItemTypesFromInventory(inventory.Id).Select(type => new Common.JsonModels.Inventory.JsonItemType
                {
                    UUId = type.UUId,
                    LastModified = type.LastModified,
                    IsDisabled = type.IsDisabled,
                    InventoryUUId = inventory.UUId,
                    Name = type.Name
                })
            });
        }

        [HttpPost]
        public async Task<IActionResult> CreateItemType([FromRoute] Guid? inventoryId, [FromBody] MI.Common.JsonModels.Inventory.JsonItemType model)
        {

            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (model.UUId == null || model.InventoryUUId == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (inventory.UUId != model.InventoryUUId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            if (!ModelState.IsValid)
            {
                return this.BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O modelo encontra-se invalido",
                    Result = ModelState.Values.Select(x => x.Errors)
                });
            }

            var newItemType = new ItemType
            {
                Name = model.Name,
                InventoryId = inventory.Id
            };

            var result = await this.unitOfWork.ItemTypes.CreateAsync(newItemType);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var itemtype = result.Result as ItemType;

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi adicionado a categoria {itemtype.Name} do inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItemType {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = itemtype.IsDisabled,
                    LastModified = itemtype.LastModified,
                    UUId = itemtype.UUId,
                    Name = itemtype.Name
                }
            });
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditItemType([FromRoute] Guid? inventoryId, [FromRoute] Guid? id, [FromBody] Common.JsonModels.Inventory.JsonItemType model)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (model.UUId == null || model.InventoryUUId == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (inventory.UUId != model.InventoryUUId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            if (!ModelState.IsValid)
            {
                return this.BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O modelo encontra-se invalido",
                    Result = ModelState.Values.Select(x => x.Errors)
                });
            }

            var itemtype = await unitOfWork.ItemTypes.GetByUUIDAsync(id.Value.ToString());

            if (itemtype == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O modeloo que tento modificar não existe"
                });
            }

            if (inventory.Id != itemtype.InventoryId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            itemtype.Name = model.Name;

            var result = await unitOfWork.ItemTypes.UpdateAsync(itemtype);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var itemtypeResult = result.Result as ItemType;

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi editada a categoria {itemtypeResult.Name} do inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItemType {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = itemtype.IsDisabled,
                    LastModified = itemtype.LastModified,
                    UUId = itemtype.UUId,
                    Name = itemtype.Name
                }
            });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteItemType([FromRoute] Guid? inventoryId, [FromRoute] Guid? id)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (id == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            var itemType = await unitOfWork.ItemTypes.GetByUUIDAsync(id.Value.ToString());

            if (itemType == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O modeloo que tento modificar não existe"
                });
            }

            if (inventory.Id != itemType.InventoryId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            var result = await unitOfWork.ItemTypes.DisableAsync(itemType);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var itemtypeResult = result.Result as ItemType;

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi eliminada a categoria {itemtypeResult.Name} do inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Message = "Modelo removido",
                Result = new JsonItemType {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = itemtypeResult.IsDisabled,
                    LastModified = itemtypeResult.LastModified,
                    UUId = itemtypeResult.UUId,
                    Name = itemtypeResult.Name
                }
            });
        }
    }
}