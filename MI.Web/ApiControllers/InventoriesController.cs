﻿using MI.Common.JsonModels.Inventory;
using MI.Common.Models;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.ApiControllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class InventoriesController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISyncService _syncService;

        public InventoriesController(IUnitOfWork unitOfWork, ISyncService syncService)
        {
            _unitOfWork = unitOfWork;
            _syncService = syncService;
        }

        public async Task<IActionResult> GetInventories()
        {                        
            return Ok(new Response
            {
                IsSuccess = true,
                Result = _unitOfWork.Inventories.GetActiveAndNonActiveInventoriesFromUser(this.User.GetUserId()).Select(inv => new JsonInventory
                {
                    UUId = inv.UUId,
                    LastModified = inv.LastModified,
                    IsDisabled = inv.IsDisabled,
                    Name = inv.Name
                })
            });
        }

        [HttpGet("[action]/{id?}")]
        public async Task<IActionResult> GetAllInformationFromInventory([FromRoute] Guid? id)
        {
            if (id == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos incorretos"
                });
            }

            var inv = await _unitOfWork.Inventories.GetByUUIDAsync(id.Value.ToString());

            if (inv == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (await _unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inv.Id) == null)
            {
                return BadRequest(new Response {
                    IsSuccess = false,
                    Message = "Não tem permissões neste inventario"
                });
            }

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonInventoryFull()
                {
                    Inventory = new JsonInventory
                    {
                        UUId = inv.UUId,
                        LastModified = inv.LastModified,
                        IsDisabled = inv.IsDisabled,
                        Name = inv.Name,
                    },
                    Items = _unitOfWork.Items
                        .GetActiveAndNonActiveItemsFromInventory(inv.Id)
                        .Select(item =>
                            new JsonItem
                            {
                                IsDisabled = item.IsDisabled,
                                LastModified = item.LastModified,
                                InventoryUUId = inv.UUId,
                                BrandUUId = item.Model.Brand.UUId,
                                ModelUUId = item.Model.UUId,
                                PlaceUUId = item.Place.UUId,
                                TypeUUId = item.Type.UUId,
                                ItemStateUUId = item.ItemState.UUId,
                                UUId = item.UUId,
                                Name = item.Name,
                                Description = item.Description,
                                ItemInput = item.ItemInput,
                                ItemOutput = item.ItemOutput
                            }
                        ).ToList(),

                    ItemStates = _unitOfWork.ItemsStates
                        .GetAllItemStatesFromInventory(inv.Id)
                        .Select(state =>
                            new JsonItemState
                            {
                                IsDisabled = state.IsDisabled,
                                LastModified = state.LastModified,
                                InventoryUUId = inv.UUId,
                                UUId = state.UUId,
                                State = state.State
                            }
                        ).ToList(),

                    ItemTypes = _unitOfWork.ItemTypes
                        .GetAllItemTypesFromInventory(inv.Id)
                        .Select(type =>
                            new JsonItemType
                            {
                                IsDisabled = type.IsDisabled,
                                LastModified = type.LastModified,
                                InventoryUUId = inv.UUId,
                                UUId = type.UUId,
                                Name = type.Name
                            }
                        ).ToList(),

                    ItemPlaces = _unitOfWork.ItemPlaces
                        .GetAllItemPlacesFromInventory(inv.Id)
                        .Select(place =>
                            new JsonItemPlace
                            {
                                IsDisabled = place.IsDisabled,
                                LastModified = place.LastModified,
                                InventoryUUId = inv.UUId,
                                Name = place.Name,
                                UUId = place.UUId
                            }
                        ).ToList(),

                    ItemBrands = _unitOfWork.ItemBrands
                        .GetAllItemBrandsFromInventory(inv.Id)
                        .Select(brand =>
                            new JsonItemBrand
                            {
                                UUId = brand.UUId,
                                LastModified = brand.LastModified,
                                IsDisabled = brand.IsDisabled,
                                InventoryUUId = inv.UUId,
                                Name = brand.Name
                            }
                        ).ToList(),

                    ItemModels = _unitOfWork.ItemModels
                        .GetAllItemModelsFromInventory(inv.Id)
                        .Select(model =>
                            new JsonItemModel
                            {
                                IsDisabled = model.IsDisabled,
                                LastModified = model.LastModified,
                                InventoryUUId = inv.UUId,
                                BrandUUId = model.Brand.UUId,
                                UUId = model.UUId,
                                Name = model.Name
                            }
                        ).ToList()
                }
            });
        }

        [HttpPost("[action]/{id?}")]
        public async Task<IActionResult> SyncInformationToInventory([FromRoute] Guid? id, [FromBody] JsonInventoryFull jsonModel)
        {
            if (id == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos incorretos"
                });
            }

            var inv = await _unitOfWork.Inventories.GetByUUIDAsync(id.Value.ToString());
            if (inv == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (await _unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inv.Id) == null)
            {
                return BadRequest(new Response {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            if (jsonModel == null)
                return BadRequest(
                    new Response()
                    {
                        IsSuccess = false,
                        Message = "Dados em falta"
                    }
                );

            var sync = await _syncService.SyncInventoryWithDatabaseAsync(jsonModel);
            if (!sync.IsSuccess)
            {
                return BadRequest(
                    new Response()
                    {
                        IsSuccess = true,
                        Result = new Response()
                        {
                            IsSuccess = false,
                            Message = sync.Message,
                        }
                    }

                );
            }
            else
            {
                return Ok(
                    new Response()
                    {
                        IsSuccess = true,
                        Result = new Response()
                        {
                            IsSuccess = true
                        }
                    }
                );
            }
        }
    }
}