﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.Common.JsonModels.Inventory;
using MI.Common.Models;
using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers.Interfaces;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MI.Web.ApiControllers
{
    [Route("api/{inventoryId}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ItemStatesController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly INotifyService notifyService;
        private readonly IUserHelper userHelper;

        public ItemStatesController(IUnitOfWork unitOfWork, INotifyService notifyService, IUserHelper userHelper)
        {
            this.unitOfWork = unitOfWork;
            this.notifyService = notifyService;
            this.userHelper = userHelper;
        }

        [HttpGet]
        public async Task<IActionResult> GetItemStates([FromRoute] Guid? inventoryId)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Os dados que forneceu estão incorretos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O Inventario não foi encontrado"
                });
            }

            //Checks if the User has some Role in the Inventory
            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            return Ok(new Response
            {
                IsSuccess = true,
                Result = unitOfWork.ItemsStates.GetItemStatesFromInventory(inventory.Id).Select(state => new Common.JsonModels.Inventory.JsonItemState
                {
                    UUId = state.UUId,
                    LastModified = state.LastModified,
                    IsDisabled = state.IsDisabled,
                    InventoryUUId = inventory.UUId,
                    State = state.State
                })
            });
        }

        [HttpPost]
        public async Task<IActionResult> CreateItemStates([FromRoute] Guid? inventoryId, [FromBody] MI.Common.JsonModels.Inventory.JsonItemState model)
        {

            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (model.UUId == null || model.InventoryUUId == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (inventory.UUId != model.InventoryUUId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            if (!ModelState.IsValid)
            {
                return this.BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O modelo encontra-se invalido",
                    Result = ModelState.Values.Select(x => x.Errors)
                });
            }

            var newItemState = new ItemState
            {
                State = model.State,
                InventoryId = inventory.Id
            };

            var result = await this.unitOfWork.ItemsStates.CreateAsync(newItemState);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var state = result.Result as ItemState;

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi adicionado o estado {state.State} no inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItemState {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = state.IsDisabled,
                    LastModified = state.LastModified,
                    UUId = state.UUId,
                    State = state.State
                }
            });
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditItemStates([FromRoute] Guid? inventoryId, [FromRoute] Guid? id, [FromBody] Common.JsonModels.Inventory.JsonItemState model)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (model.UUId == null || model.InventoryUUId == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (inventory.UUId != model.InventoryUUId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            if (!ModelState.IsValid)
            {
                return this.BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O modelo encontra-se invalido",
                    Result = ModelState.Values.Select(x => x.Errors)
                });
            }

            var itemState = await unitOfWork.ItemsStates.GetByUUIDAsync(id.Value.ToString());

            if (itemState == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O modeloo que tento modificar não existe"
                });
            }

            if (inventory.Id != itemState.InventoryId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            itemState.State = model.State;

            var result = await unitOfWork.ItemsStates.UpdateAsync(itemState);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var state = result.Result as ItemState;

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi editado o estado {state.State} do inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItemState
                {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = state.IsDisabled,
                    LastModified = state.LastModified,
                    UUId = state.UUId,
                    State = state.State
                }
            });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteItemStates([FromRoute] Guid? inventoryId, [FromRoute] Guid? id)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (id == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            var itemState = await unitOfWork.ItemsStates.GetByUUIDAsync(id.Value.ToString());

            if (itemState == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O modeloo que tento modificar não existe"
                });
            }

            if (inventory.Id != itemState.InventoryId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            var result = await unitOfWork.ItemsStates.DisableAsync(itemState);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var state = result.Result as ItemState;

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi eliminada o estado {state.State} do inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItemState
                {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = state.IsDisabled,
                    LastModified = state.LastModified,
                    UUId = state.UUId,
                    State = state.State
                }
            });
        }
    }
}