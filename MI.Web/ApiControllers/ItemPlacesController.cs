﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.Common.JsonModels.Inventory;
using MI.Common.Models;
using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers.Interfaces;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MI.Web.ApiControllers
{
    [Route("api/{inventoryId}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ItemPlacesController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly INotifyService notifyService;
        private readonly IUserHelper userHelper;

        public ItemPlacesController(IUnitOfWork unitOfWork, INotifyService notifyService, IUserHelper userHelper)
        {
            this.unitOfWork = unitOfWork;
            this.notifyService = notifyService;
            this.userHelper = userHelper;
        }

        [HttpGet]
        public async Task<IActionResult> GetItemPlaces([FromRoute] Guid? inventoryId)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Os dados que forneceu estão incorretos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O Inventario não foi encontrado"
                });
            }

            //Checks if the User has some Role in the Inventory
            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            return Ok(new Response
            {
                IsSuccess = true,
                Result = unitOfWork.ItemPlaces.GetItemPlacesFromInventory(inventory.Id).Select(place => new Common.JsonModels.Inventory.JsonItemPlace
                {
                    UUId = place.UUId,
                    LastModified = place.LastModified,
                    IsDisabled = place.IsDisabled,
                    InventoryUUId = inventory.UUId,
                    Name = place.Name
                })
            });
        }

        [HttpPost]
        public async Task<IActionResult> CreateItemPlace([FromRoute] Guid? inventoryId, [FromBody] MI.Common.JsonModels.Inventory.JsonItemPlace model)
        {

            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (model.UUId == null || model.InventoryUUId == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (inventory.UUId != model.InventoryUUId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            if (!ModelState.IsValid)
            {
                return this.BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O modelo encontra-se invalido",
                    Result = ModelState.Values.Select(x => x.Errors)
                });
            }

            var newItemPlace = new ItemPlace
            {
                Name = model.Name,
                InventoryId = inventory.Id
            };

            var result = await this.unitOfWork.ItemPlaces.CreateAsync(newItemPlace);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var place = result.Result as ItemPlace;

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi adicionado o lugar {place.Name} no inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItemPlace
                {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = place.IsDisabled,
                    LastModified = place.LastModified,
                    UUId = place.UUId,
                    Name = place.Name
                }
            });
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditItemPlace([FromRoute] Guid? inventoryId, [FromRoute] Guid? id, [FromBody] Common.JsonModels.Inventory.JsonItemPlace model)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (model.UUId == null || model.InventoryUUId == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (inventory.UUId != model.InventoryUUId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            if (!ModelState.IsValid)
            {
                return this.BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O modelo encontra-se invalido",
                    Result = ModelState.Values.Select(x => x.Errors)
                });
            }

            var itemPlace = await unitOfWork.ItemPlaces.GetByUUIDAsync(id.Value.ToString());

            if (itemPlace == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O modeloo que tento modificar não existe"
                });
            }

            if (inventory.Id != itemPlace.InventoryId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            itemPlace.Name = model.Name;

            var result = await unitOfWork.ItemPlaces.UpdateAsync(itemPlace);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var place = result.Result as ItemPlace;

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi editado o lugar {place.Name} do inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItemPlace
                {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = place.IsDisabled,
                    LastModified = place.LastModified,
                    UUId = place.UUId,
                    Name = place.Name
                }
            });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteItemPlace([FromRoute] Guid? inventoryId, [FromRoute] Guid? id)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (id == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            var itemPlace = await unitOfWork.ItemPlaces.GetByUUIDAsync(id.Value.ToString());

            if (itemPlace == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O modeloo que tento modificar não existe"
                });
            }

            if (inventory.Id != itemPlace.InventoryId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            var result = await unitOfWork.ItemPlaces.DisableAsync(itemPlace);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var place = result.Result as ItemPlace;

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi eliminada o lugar {place.Name} do inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItemPlace
                {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = place.IsDisabled,
                    LastModified = place.LastModified,
                    UUId = place.UUId,
                    Name = place.Name
                }
            });
        }
    }
}