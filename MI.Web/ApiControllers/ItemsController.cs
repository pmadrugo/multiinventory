﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.Common.JsonModels.Inventory;
using MI.Common.Models;
using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers;
using MI.Web.Helpers.Interfaces;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MI.Web.ApiControllers
{
    [Route("api/{inventoryId}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ItemsController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly INotifyService notifyService;
        private readonly IUserHelper userHelper;

        public ItemsController(IUnitOfWork unitOfWork, INotifyService notifyService, IUserHelper userHelper)
        {
            this.unitOfWork = unitOfWork;
            this.notifyService = notifyService;
            this.userHelper = userHelper;
        }

        [HttpGet]
        public async Task<IActionResult> GetItems([FromRoute] Guid? inventoryId)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Os dados que forneceu estão incorretos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O Inventario não foi encontrado"
                });
            }

            //Checks if the User has some Role in the Inventory
            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            return Ok(new Response
            {
                IsSuccess = true,
                Result = unitOfWork.Items.GetItemsFromInventory(inventory.Id).Select(item => new Common.JsonModels.Inventory.JsonItem
                {
                    UUId = item.UUId,
                    LastModified = item.LastModified,
                    IsDisabled = item.IsDisabled,
                    InventoryUUId = inventory.UUId,
                    Name = item.Name,
                    Description = item.Description,
                    ItemInput = item.ItemInput,
                    ItemOutput = item.ItemOutput,                    
                    BrandUUId = item.Model.Brand.UUId,
                    ModelUUId = item.Model.UUId,
                    ItemStateUUId = item.ItemState.UUId,
                    PlaceUUId = item.Place.UUId,
                    TypeUUId = item.Type.UUId                    
                })
            });
        }

        [HttpPost]
        public async Task<IActionResult> CreateItem([FromRoute] Guid? inventoryId, [FromBody] MI.Common.JsonModels.Inventory.JsonItem model)
        {

            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (model.InventoryUUId == null || model.BrandUUId == null || model.ModelUUId == null || model.ItemStateUUId == null || model.PlaceUUId == null || model.TypeUUId == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (inventory.UUId != model.InventoryUUId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }            

            var brand = await unitOfWork.ItemBrands.GetByUUIDAsync(model.BrandUUId.ToString());
            var modelBrand = await unitOfWork.ItemModels.GetByUUIDAsync(model.ModelUUId.ToString());
            var state = await unitOfWork.ItemsStates.GetByUUIDAsync(model.ItemStateUUId.ToString());
            var place = await unitOfWork.ItemPlaces.GetByUUIDAsync(model.PlaceUUId.ToString());
            var type = await unitOfWork.ItemTypes.GetByUUIDAsync(model.TypeUUId.ToString());

            if(brand == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Esta em falta dados marca",
                    Result = brand
                });
            }

            if(modelBrand == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Esta em falta dados modelo",
                    Result = modelBrand
               });
            }

            if(state == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Esta em falta dados estado",
                    Result = state
                });
            }

            if(place == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Esta em falta dados lugar",
                    Result = place
                });
            }

            if(type == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Esta em falta dados do tipo de item"
                });
            }

            if(brand.InventoryId != inventory.Id || modelBrand.InventoryId != inventory.Id || state.InventoryId != inventory.Id || place.InventoryId != inventory.Id || type.InventoryId != inventory.Id)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Um dos dados fornecido, o iventario não corresponde ao inventario fornecido (Marca/Modelo/Estado/Lugar/Tipo)"
                });
            }

            if (!ModelState.IsValid)
            {
                return this.BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O modelo encontra-se invalido",
                    Result = ModelState.Values.Select(x => x.Errors)
                });
            }

            var image = await FileHelper.SaveImage(null, "items");
            var newItem = new Item
            {
                Name = model.Name,
                Description = model.Description,
                ImagePath = image.Result.ToString(),
                InventoryId = inventory.Id,
                ModelId = modelBrand.Id,
                ItemStateId = state.Id,
                PlaceId = place.Id,
                TypeId = type.Id
            };

            var result = await this.unitOfWork.Items.CreateAsync(newItem);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var item = result.Result as Item;

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi adicionado o item {item.Name} no inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItem {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = item.IsDisabled,
                    LastModified = item.LastModified,
                    UUId = item.UUId,
                    Name = item.Name,
                    Description = item.Description,
                    ItemInput = item.ItemInput,
                    ItemOutput = item.ItemOutput,
                    BrandUUId = brand.UUId,
                    ModelUUId = modelBrand.UUId,
                    ItemStateUUId = state.UUId,
                    PlaceUUId = place.UUId,
                    TypeUUId = type.UUId
                }
            });
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditItem([FromRoute] Guid? inventoryId, [FromRoute] Guid? id, [FromBody] Common.JsonModels.Inventory.JsonItem model)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (model.UUId == null || model.InventoryUUId == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (inventory.UUId != model.InventoryUUId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            var brand = await unitOfWork.ItemBrands.GetByUUIDAsync(model.BrandUUId.ToString());
            var modelBrand = await unitOfWork.ItemModels.GetByUUIDAsync(model.ModelUUId.ToString());
            var state = await unitOfWork.ItemsStates.GetByUUIDAsync(model.ItemStateUUId.ToString());
            var place = await unitOfWork.ItemPlaces.GetByUUIDAsync(model.PlaceUUId.ToString());
            var type = await unitOfWork.ItemTypes.GetByUUIDAsync(model.TypeUUId.ToString());

            if (brand == null || modelBrand == null || state == null || place == null || type == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Esta em falta dados (Marca/Modelo/Estado/Lugar/Tipo)"
                });
            }

            if (brand.InventoryId != inventory.Id || modelBrand.InventoryId != inventory.Id || state.InventoryId != inventory.Id || place.InventoryId != inventory.Id || type.InventoryId != inventory.Id)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Um dos dados fornecido, o iventario não corresponde ao inventario fornecido (Marca/Modelo/Estado/Lugar/Tipo)"
                });
            }

            if (!ModelState.IsValid)
            {
                return this.BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O modelo encontra-se invalido",
                    Result = ModelState.Values.Select(x => x.Errors)
                });
            }

            var item = await unitOfWork.Items.GetByUUIDAsync(id.Value.ToString());

            if(item == null)
            {
                return NotFound(new Response {
                    IsSuccess= false,
                    Message = "O modeloo que tento modificar não existe"
                });
            }


            item.Name = model.Name;
            item.Description = model.Description;           
            item.ItemOutput = model.ItemOutput;
            item.ImagePath = item.ImagePath;
            item.InventoryId = inventory.Id;
            item.ModelId = modelBrand.Id;
            item.ItemStateId = state.Id;
            item.PlaceId = place.Id;
            item.TypeId = type.Id;

            var result = await unitOfWork.Items.UpdateAsync(item);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }


            var itemresult = result.Result as Item;
            var itemGoted = unitOfWork.Items.GetFullItemByUUID(itemresult.UUId);

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi editada o item {itemGoted.Name} do inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItem
                {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = itemGoted.IsDisabled,
                    LastModified = itemGoted.LastModified,
                    UUId = itemGoted.UUId,
                    Name = itemGoted.Name,
                    Description = itemGoted.Description,
                    ItemInput = itemGoted.ItemInput,
                    ItemOutput = itemGoted.ItemOutput,
                    BrandUUId = itemGoted.Model.Brand.UUId,
                    ModelUUId = itemGoted.Model.UUId,
                    ItemStateUUId = itemGoted.ItemState.UUId,
                    PlaceUUId = itemGoted.Place.UUId,
                    TypeUUId = itemGoted.Type.UUId
                }
            });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteItemBrand([FromRoute] Guid? inventoryId, [FromRoute] Guid? id)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (id == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            var item = await unitOfWork.Items.GetByUUIDAsync(id.Value.ToString());

            if (item == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O modeloo que tento modificar não existe"
                });
            }

            if (inventory.Id != item.InventoryId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            var result = await unitOfWork.Items.DisableAsync(item);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var itemresult = result.Result as Item;
            var itemGoted = unitOfWork.Items.GetFullItemByUUID(itemresult.UUId);

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi eliminado o item {itemGoted.Name} do inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItem
                {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = itemGoted.IsDisabled,
                    LastModified = itemGoted.LastModified,
                    UUId = itemGoted.UUId,
                    Name = itemGoted.Name,
                    Description = itemGoted.Description,
                    ItemInput = itemGoted.ItemInput,
                    ItemOutput = itemGoted.ItemOutput,
                    BrandUUId = itemGoted.Model.Brand.UUId,
                    ModelUUId = itemGoted.Model.UUId,
                    ItemStateUUId = itemGoted.ItemState.UUId,
                    PlaceUUId = itemGoted.Place.UUId,
                    TypeUUId = itemGoted.Type.UUId
                }
            });
        }
    }
}