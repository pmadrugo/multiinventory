﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.Common.JsonModels.Inventory;
using MI.Common.Models;
using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers.Interfaces;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MI.Web.ApiControllers
{
    [Route("api/{inventoryId}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ItemModelsController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly INotifyService notifyService;
        private readonly IUserHelper userHelper;

        public ItemModelsController(IUnitOfWork unitOfWork, INotifyService notifyService, IUserHelper userHelper)
        {
            this.unitOfWork = unitOfWork;
            this.notifyService = notifyService;
            this.userHelper = userHelper;
        }

        [HttpGet]
        public async Task<IActionResult> GetItemModels([FromRoute] Guid? inventoryId)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Os dados que forneceu estão incorretos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O Inventario não foi encontrado"
                });
            }

            //Checks if the User has some Role in the Inventory
            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            return Ok(new Response
            {
                IsSuccess = true,
                Result = unitOfWork.ItemModels.GetItemModelsFromInventory(inventory.Id).Select(model => new Common.JsonModels.Inventory.JsonItemModel
                {
                    UUId = model.UUId,
                    LastModified = model.LastModified,
                    IsDisabled = model.IsDisabled,
                    InventoryUUId = inventory.UUId,
                    Name = model.Name,
                    BrandUUId = model.Brand.UUId
                })
            });
        }

        [HttpPost]
        public async Task<IActionResult> CreateItemModels([FromRoute] Guid? inventoryId, [FromBody] MI.Common.JsonModels.Inventory.JsonItemModel model)
        {

            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (model.UUId == null || model.InventoryUUId == null || model.BrandUUId == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (inventory.UUId != model.InventoryUUId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            var brand = await unitOfWork.ItemBrands.GetByUUIDAsync(model.BrandUUId.ToString());

            if(brand == null)
            {
                return NotFound(new Response {
                    IsSuccess=false,
                    Message = "Não foi encontrada a marca"
                });
            }

            if(brand.InventoryId != inventory.Id)
            {
                return BadRequest(new Response {
                    IsSuccess = false,
                    Message = "O inventario da marca  não corresponde ao inventario dado"
                });
            }

            if (!ModelState.IsValid)
            {
                return this.BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O modelo encontra-se invalido",
                    Result = ModelState.Values.Select(x => x.Errors)
                });
            }

            var newItemModel = new ItemModel
            {
                Name = model.Name,
                InventoryId = inventory.Id,
                BrandId = brand.Id,                
            };

            var result = await this.unitOfWork.ItemModels.CreateAsync(newItemModel);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var modelItem = result.Result as ItemModel;

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi adicionado o modelo {modelItem.Name} a marca {brand.Name} do inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItemModel {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = modelItem.IsDisabled,
                    LastModified = modelItem.LastModified,
                    UUId = modelItem.UUId,
                    Name = modelItem.Name,
                    BrandUUId = brand.UUId
                }
            });
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditItemModel([FromRoute] Guid? inventoryId, [FromRoute] Guid? id, [FromBody] Common.JsonModels.Inventory.JsonItemModel model)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (model.UUId == null || model.InventoryUUId == null || model.BrandUUId == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (inventory.UUId != model.InventoryUUId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            var brand = await unitOfWork.ItemBrands.GetByUUIDAsync(model.BrandUUId.ToString());

            if (brand == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Não foi encontrada a marca"
                });
            }

            if (brand.InventoryId != inventory.Id)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario da marca  não corresponde ao inventario dado"
                });
            }

            if (!ModelState.IsValid)
            {
                return this.BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O modelo encontra-se invalido",
                    Result = ModelState.Values.Select(x => x.Errors)
                });
            }

            var itemModel = await unitOfWork.ItemModels.GetByUUIDAsync(id.Value.ToString());

            if (itemModel == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O modeloo que tento modificar não existe"
                });
            }

            if (inventory.Id != itemModel.InventoryId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            itemModel.Name = model.Name;
            itemModel.BrandId = brand.Id;

            var result = await unitOfWork.ItemModels.UpdateAsync(itemModel);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var modelItem = result.Result as ItemModel;

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi editada o modelo {modelItem.Name} da marca {brand.Name} do inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItemModel
                {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = modelItem.IsDisabled,
                    LastModified = modelItem.LastModified,
                    UUId = modelItem.UUId,
                    Name = modelItem.Name,
                    BrandUUId = brand.UUId
                }
            });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteItemModel([FromRoute] Guid? inventoryId, [FromRoute] Guid? id)
        {
            if (!inventoryId.HasValue)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            var inventory = await unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (id == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos invalidos"
                });
            }

            if (await unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não tem permissões para esta acção"
                });
            }

            var itemModel = await unitOfWork.ItemModels.GetByUUIDAsync(id.Value.ToString());

            if (itemModel == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O modeloo que tento modificar não existe"
                });
            }

            if (inventory.Id != itemModel.InventoryId)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O inventario do modelo não coincide com o inventario dado"
                });
            }

            var result = await unitOfWork.ItemModels.DisableAsync(itemModel);

            if (!result.IsSuccess)
            {
                return StatusCode(500, new Response
                {
                    IsSuccess = false,
                    Message = $"Erro interno do servidor, porfavor tente novamente\n {result.Message}",
                });
            }

            var modelResult = result.Result as ItemModel;

            var modelitem = unitOfWork.ItemModels.GetItemModelWithBrandByUUID(modelResult.UUId);

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            await notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi eliminada o modelo {modelitem.Name} do inventario {inventory.Name} por {user.FullName}");

            return Ok(new Response
            {
                IsSuccess = true,
                Result = new JsonItemModel
                {
                    InventoryUUId = inventory.UUId,
                    IsDisabled = modelitem.IsDisabled,
                    LastModified = modelitem.LastModified,
                    UUId = modelitem.UUId,
                    Name = modelitem.Name,
                    BrandUUId = modelitem.Brand.UUId
                }
            });
        }
    }
}