﻿using MI.Common.JsonModels;
using MI.Common.JsonModels.Application;
using MI.Common.JsonModels.Authentication;
using MI.Common.JsonModels.JSONInterfaces;
using MI.Common.Models;
using MI.Web.Data.Entities.ApplicationSection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers;
using MI.Web.Helpers.Interfaces;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MI.Web.ApiControllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserHelper userHelper;
        private readonly IRoleHelper roleHelper;
        private readonly IConfiguration configuration;
        private readonly IUnitOfWork unitOfWork;
        private readonly IEmailSender emailSender;

        public AccountController(IUserHelper userHelper, IRoleHelper roleHelper, IConfiguration configuration, IUnitOfWork unitOfWork, IEmailSender emailSender)
        {
            this.userHelper = userHelper;
            this.roleHelper = roleHelper;
            this.configuration = configuration;
            this.unitOfWork = unitOfWork;
            this.emailSender = emailSender;
        }

        [HttpPost]
        public async Task<IActionResult> RegisterUser([FromBody] JsonRegisterAccount model)
        {
            if(model == null)
            {
                return BadRequest(new Response {
                    IsSuccess = false,
                    Message = "Não foi recebido nenhum modelo"
                });
            }

            var user = await this.userHelper.GetUserByEmailAsync(model.Email);

            if (user != null)
            {
                return BadRequest(new Response { IsSuccess = false, Message = "O utilizador já existe" });
            }

            var image = await FileHelper.SaveImage(null, "profile");
            user = new User()
            {
                Email = model.Email,
                UserName = model.Email,
                BirthDate = model.BirthDate,
                FirstName = model.FirstName,
                LastName = model.LastName,
                VATNumber = model.VATNumber,
                IsApproved = false,
                EmailConfirmed = false,
                LastModified = DateTime.Now,
                ImagePath = image.Result.ToString()
            };

            //TODO image upload

            var result = await this.userHelper.CreateUserAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return BadRequest(new Response { IsSuccess = false, Message = "Erro inesperado, Tente de novo" });
            }

            var code = await this.userHelper.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = Url.Action(
                        "ConfirmEmail", "Account",
                        new { userId = user.Id, code = code },
                        protocol: Request.Scheme);

            await emailSender.SendEmailAsync(user.Email, user.FullName, "Confirme o seu email",
                $"Por favor confirme a sua conta em <a href=\"{callbackUrl}\">Confirmar</a>");

            return Ok(new Response { IsSuccess = true, Message = "Foi enviado um email de confirmação, porfavor utilize o link primeiro para ativar a conta", Result=null });
        }

        [HttpPost]
        public async Task<IActionResult> RegisterUserExternal([FromBody] JsonRegisterAccount model)
        {
            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não foi recebido nenhum modelo"
                });
            }

            var user = await this.userHelper.GetUserByEmailAsync(model.Email);

            if (user != null)
            {
                return BadRequest(new Response { IsSuccess = false, Message = "O utilizador já existe" });
            }

            var image = await FileHelper.SaveImage(null, "profile");
            user = new User()
            {
                Email = model.Email,
                UserName = model.Email,
                BirthDate = model.BirthDate,
                FirstName = model.FirstName,
                LastName = model.LastName,
                VATNumber = model.VATNumber,
                IsApproved = false,
                EmailConfirmed = true,
                LastModified = DateTime.Now,
                ImagePath = image.Result.ToString()
            };

            //TODO image upload

            var result = await this.userHelper.CreateUserAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return BadRequest(new Response { IsSuccess = false, Message = "Erro inesperado, Tente de novo" });
            }

            return Ok(new Response { IsSuccess = true, Message = "Tera de aguarda pela aprovação do administrador", Result = null });
        }

        [HttpPost]
        public async Task<IActionResult> CreateToken([FromBody] JsonAccount model)
        {
            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não foi recebido nenhum modelo"
                });
            }

            if (this.ModelState.IsValid)
            {
                var user = await this.userHelper.GetUserByEmailAsync(model.Email);

                if (user != null)
                {

                    if(!await this.userHelper.IsEmailConfirmedAsync(user))
                    {
                        return BadRequest(new Response
                        {
                            IsSuccess = false,
                            Message = "Tem de ativar o email"
                        });
                    }

                    if(!await this.userHelper.IsAccountApprovedAsync(user.Email))
                    {
                        return BadRequest(new Response
                        {
                            IsSuccess = false,
                            Message = "Um administrador tem de aprovar a conta"
                        });
                    }

                    var result = await this.userHelper.ValidatePasswordAsync(user,
                                                                             model.Password);

                    if (result.Succeeded)
                    {
                        var claims = new[]
                        {
                            new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        };

                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.configuration["Tokens:Key"]));
                        var credencials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                        var token = new JwtSecurityToken(
                            this.configuration["Tokens:Issuer"],
                            this.configuration["Tokens:Audience"],
                            claims,
                            expires: DateTime.UtcNow.AddDays(1),
                            signingCredentials: credencials);

                        var results = new
                        {
                            token = new JwtSecurityTokenHandler().WriteToken(token),
                            expiration = token.ValidTo
                        };

                        return Ok(new Response {
                            IsSuccess = true,
                            Result = new JsonTokenResponse
                            {
                                Token = new JwtSecurityTokenHandler().WriteToken(token),
                                Expiration = token.ValidTo
                            }
                        });
                    }

                    return BadRequest(new Response {
                        IsSuccess = false,
                        Message = "O email ou password estão incorretos"
                    });
                }

                return BadRequest(new Response {
                    IsSuccess = false,
                    Message = "Utilizador não foi encontrado"
                });
            }

            return BadRequest(new Response {
                IsSuccess = false,
                Message ="Falta de dados"
            });
        }

        [HttpPost]
        public async Task<IActionResult> CreateTokenExternal([FromBody] JsonExternalUser model)
        {
            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não foi recebido nenhum modelo"
                });
            }

            if (this.ModelState.IsValid)
            {
                var user = await this.userHelper.GetUserByEmailAsync(model.Email);

                if (user != null)
                {

                    if (!await this.userHelper.IsEmailConfirmedAsync(user))
                    {
                        user.EmailConfirmed = true;
                        var resultUser = await userHelper.UpdateUserAsync(user);

                        if (!resultUser.Succeeded)
                        {
                            return BadRequest(new Response {
                                IsSuccess = false,
                                Message = "Pedimos desculpa, ocorreu um erro interno, tente de novo..."
                            });
                        }
                    }

                    if (!await this.userHelper.IsAccountApprovedAsync(user.Email))
                    {
                        return BadRequest(new Response
                        {
                            IsSuccess = false,
                            Message = "Um administrador tem de aprovar a conta"
                        });
                    }

                    if (model.VerifiedEmail)
                    {
                        var claims = new[]
                        {
                            new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        };

                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.configuration["Tokens:Key"]));
                        var credencials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                        var token = new JwtSecurityToken(
                            this.configuration["Tokens:Issuer"],
                            this.configuration["Tokens:Audience"],
                            claims,
                            expires: DateTime.UtcNow.AddDays(1),
                            signingCredentials: credencials);

                        var results = new
                        {
                            token = new JwtSecurityTokenHandler().WriteToken(token),
                            expiration = token.ValidTo
                        };

                        return Ok(new Response
                        {
                            IsSuccess = true,
                            Result = new JsonTokenResponse
                            {
                                Token = new JwtSecurityTokenHandler().WriteToken(token),
                                Expiration = token.ValidTo
                            }
                        });
                    }

                    return BadRequest(new Response
                    {
                        IsSuccess = false,
                        Message = "O e-mail ou password esta errada"
                    });
                }

                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Utilizador não foi encontrado",
                    Result = 404
                });
            }

            return BadRequest(new Response
            {
                IsSuccess = false,
                Message = "Falta de dados"
            });
        }

        [HttpPost]
        public async Task<IActionResult> GetUserInformation([FromBody] JsonAccount model)
        {
            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não foi recebido nenhum modelo"
                });
            }

            if (model.Email == null)
            {
                return NotFound(new Response {
                    IsSuccess = false,
                    Message = "O Utilizador não existe"
                });
            }

            if(model.Password == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "O Utilizador ou a Password esta errada"
                });
            }

            var user = await userHelper.GetUserByEmailAsync(model.Email);

            var result = await userHelper.ValidatePasswordAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "O Utilizador ou a Password esta errada"
                });
            }

            return Ok(new Response {
                IsSuccess = true,
                Result = new JsonRegisterAccount
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    VATNumber = user.VATNumber,
                    BirthDate = user.BirthDate,
                    UserId = user.Id,
                    
                }
            });
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> ChangePassword([FromBody] JsonChangePasswordAccount model)
        {
            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não foi recebido nenhum modelo"
                });
            }

            var user = await userHelper.GetUserByIdAsync(User.GetUserId());
            if (user == null)
            {
                return NotFound(new Response { IsSuccess = false, Message = "Utilizador não econtrado" });
            }

            var result = await this.userHelper.ChangePasswordAsync(user, model.Password, model.NewPassword);
            if (!result.Succeeded)
            {
                return BadRequest(new Response { IsSuccess = false, Message = result.Errors.FirstOrDefault()?.Description });
            }

            return Ok(new Response { IsSuccess = true, Message = "Password alterada com sucesso", Result= null });
        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword([FromBody] JsonAccount model)
        {
            if (model == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Não foi recebido nenhum modelo"
                });
            }

            var user = await userHelper.GetUserByEmailAsync(model.Email);

            if (user == null || !(await userHelper.IsEmailConfirmedAsync(user) || !(await userHelper.IsAccountApprovedAsync(user.UserName))))
            {
                return BadRequest(new Response {
                    IsSuccess = false,
                    Message = "Ocorreu um erro intenrno..."
                });
            }

            var code = await userHelper.GeneratePasswordResetTokenAsync(user);
            var callbackUrl = Url.Action("ResetPassword", "Account",
                    new { UserId = user.Id, code = code }, protocol: Request.Scheme);

            await emailSender.SendEmailAsync(user.Email, user.FullName, "Recuperar Password",
                    $"Para recuperar a sua palavra-passe, por favor carregue neste link: <a href=\"{callbackUrl}\">Recuperar Password</a>");

            return Ok(new Response {
                IsSuccess = true,
                Message="Foi enviado um e-mail para recuperar a sua palavra-passe",
                Result = null
            });
        }

    }
}