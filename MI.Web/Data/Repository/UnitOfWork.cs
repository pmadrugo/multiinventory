﻿using MI.Common.Models;
using MI.Web.Data.Interfaces;
using MI.Web.Helpers;
using MI.Web.Helpers.Interfaces;
using Microsoft.EntityFrameworkCore.Storage;
using System;

namespace MI.Web.Data.Repository
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        private readonly MIDbContext _dbContext;
        private IDbContextTransaction _transaction;

        public UnitOfWork(MIDbContext dbContext)
        {
            this._dbContext = dbContext;
            this._transaction = _dbContext.Database.BeginTransaction();
        }

        #region Project Repositories / Attributes
        // Repository Internal Variables
        private InventoryRoleRepository _inventoryRoles;
        private InventoryRepository _inventory;
        private ItemBrandRepository _itemBrands;
        private ItemModelRepository _itemModels;
        private ItemTypeRepository _itemTypes;
        private ItemPlaceRepository _itemPlaces;
        private ItemStateRepository _itemStates;
        private ItemRepository _items;
        private InventoryReferralRepository _inventoryReferrals;
        private InventoryNotifyRepository _inventoryNotify;
        #endregion

        #region Repository Instances / Properties       

        public InventoryRoleRepository InventoryRoles
        {
            get
            {
                if (_inventoryRoles == null)
                    _inventoryRoles = new InventoryRoleRepository(_dbContext);
                return _inventoryRoles;
            }
        }

        public InventoryRepository Inventories
        {
            get
            {
                if (_inventory == null)
                    _inventory = new InventoryRepository(_dbContext);
                return _inventory;
            }
        }

        public ItemBrandRepository ItemBrands
        {
            get
            {
                if (_itemBrands == null)
                {
                    _itemBrands = new ItemBrandRepository(_dbContext);
                }
                return _itemBrands;
            }
        }

        public ItemModelRepository ItemModels
        {
            get
            {
                if(_itemModels == null)
                {
                    _itemModels = new ItemModelRepository(_dbContext);
                }
                return _itemModels;
            }
        }

        public ItemTypeRepository ItemTypes
        {
            get
            {
                if (_itemTypes == null)
                {
                    _itemTypes = new ItemTypeRepository(_dbContext);
                }
                return _itemTypes;
            }
        }

        public ItemPlaceRepository ItemPlaces
        {
            get
            {
                if(_itemPlaces == null)
                {
                    _itemPlaces = new ItemPlaceRepository(_dbContext);
                }
                return _itemPlaces;
            }
        }

        public ItemStateRepository ItemsStates
        {
            get
            {
                if(_itemStates == null)
                {
                    _itemStates = new ItemStateRepository(_dbContext);
                }
                return _itemStates;
            }
        }

        public ItemRepository Items
        {
            get
            {
                if(_items == null)
                {
                    _items = new ItemRepository(_dbContext);
                }
                return _items;
            }
        }

        public InventoryReferralRepository Referrals
        {
            get
            {
                if (_inventoryReferrals == null)
                { 
                    _inventoryReferrals = new InventoryReferralRepository(_dbContext);
                }
                return _inventoryReferrals;
            }
        }

        public InventoryNotifyRepository NotifiableUsers
        {
            get
            {
                if (_inventoryNotify == null)
                    _inventoryNotify = new InventoryNotifyRepository(_dbContext);
                return _inventoryNotify;
            }
        }

        #endregion

        public Response Commit(bool lastTransaction = false)
        {
            try
            {
                _transaction.Commit();
            }
            catch (Exception ex)
            {
                _transaction.Rollback();

                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
            finally
            {
                _transaction.Dispose();
                if (!lastTransaction)
                {
                    _transaction = _dbContext.Database.BeginTransaction();
                }
                // TODO : Reset Repository access?
            }

            return new Response()
            {
                IsSuccess = true
            };
        }

        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        Commit(true);
                        _transaction.Dispose();
                    }

                    _dbContext.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
