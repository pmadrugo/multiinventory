﻿using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Repository
{
    public class ItemModelRepository : GenericRepository<ItemModel>, IItemModelRepository
    {
        public ItemModelRepository(MIDbContext dbContext) : base(dbContext) { }

        public IQueryable<ItemModel> GetItemModelsFromInventory(int inventoryId)
        {
            return _dbContext.Set<ItemModel>()
                .Include(im => im.Brand)
                .Where(im => im.InventoryId == inventoryId &&
                                !im.IsDisabled)
                .OrderBy(x => x.Name);
        }

        public IQueryable<ItemModel> GetAllItemModelsFromInventory(int inventoryId)
        {
            return _dbContext.Set<ItemModel>()
                .Include(im => im.Brand)
                .Where(im => im.InventoryId == inventoryId
                                )
                .OrderBy(x => x.Name);
        }

        public ItemModel GetItemModelWithBrandByUUID(Guid UUID)
        {
            return _dbContext.Set<ItemModel>()
                .Include(x => x.Brand)
                .Where(x => x.UUId == UUID)
                .FirstOrDefault();
        }
    }
}
