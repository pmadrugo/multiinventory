﻿using MI.Common.Models;
using MI.Web.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Repository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, IEntity
    {
        protected readonly MIDbContext _dbContext;
        public GenericRepository(MIDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        /// <summary>
        /// Add the entity to database
        /// </summary>
        /// <param name="entity">The entity to add</param>
        /// <param name="saveChanges">Saves all changes made to the database context</param>
        /// <returns></returns>
        public virtual async Task<Response> CreateAsync(TEntity entity, bool saveChanges = true)
        {
            try
            {
                entity.LastModified = DateTime.Now;
                entity.UUId = Guid.NewGuid();
                entity.IsDisabled = false;

                await this._dbContext.Set<TEntity>().AddAsync(entity);
                if (saveChanges)
                {
                    await this.SaveAllAsync();
                }

                return new Response()
                {
                    IsSuccess = true,
                    Result = entity
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Updates the entity on database
        /// </summary>
        /// <param name="entity">The entity to update</param>
        /// <param name="saveChanges">Saves all changes made to the database context</param>
        /// <returns></returns>
        public virtual async Task<Response> UpdateAsync(TEntity entity, bool saveChanges = true)
        {
            try
            {
                var existing = await GetByIdAsync(entity.Id);
                if (existing != null)
                {
                    entity.LastModified = DateTime.Now;

                    this._dbContext.Entry<TEntity>(existing).CurrentValues.SetValues(entity);
                    this._dbContext.Entry<TEntity>(existing).State = EntityState.Modified;

                    if (saveChanges)
                    {
                        await this.SaveAllAsync();
                    }

                    return new Response()
                    {
                        IsSuccess = true,
                        Result = existing
                    };
                }
                else
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = "Could not find matching result in database"
                    };
                }
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Disables the entity (Does not delete from database)
        /// </summary>
        /// <param name="entity">The entity to disable</param>
        /// <param name="saveChanges">Saves all changes made to the database context</param>
        /// <returns></returns>
        public virtual async Task<Response> DisableAsync(TEntity entity, bool saveChanges = true)
        {
            try
            {
                var existing = await GetByIdAsync(entity.Id);
                if (existing != null)
                {
                    existing.IsDisabled = true;
                    existing.LastModified = DateTime.Now;

                    this._dbContext.Entry<TEntity>(existing).State = EntityState.Modified;

                    if (saveChanges)
                    {
                        await this.SaveAllAsync();
                    }

                    return new Response()
                    {
                        IsSuccess = true,
                        Result = existing
                    };
                }
                else
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = "Could not find matching result in database"
                    };
                }
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Enables the entity
        /// </summary>
        /// <param name="entity">The entity to enable</param>
        /// <param name="saveChanges">Saves all changes made to the database context</param>
        /// <returns></returns>
        public virtual async Task<Response> EnableAsync(TEntity entity, bool saveChanges = true)
        {
            try
            {
                var existing = await GetByIdAsync(entity.Id);
                if (existing != null)
                {
                    existing.IsDisabled = false;
                    existing.LastModified = DateTime.Now;

                    this._dbContext.Entry<TEntity>(existing).State = EntityState.Modified;

                    if (saveChanges)
                    {
                        await this.SaveAllAsync();
                    }

                    return new Response()
                    {
                        IsSuccess = true,
                        Result = existing
                    };
                }
                else
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = "Could not find matching result in database"
                    };
                }
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Deletes permanently the entity from the database
        /// </summary>
        /// <param name="entity">The entity to delete</param>
        /// <param name="saveChanges">Saves all changes made to the database context</param>
        /// <returns></returns>
        public virtual async Task<Response> DeleteAsync(TEntity entity, bool saveChanges = true)
        {
            try
            {
                var entityToDelete = await this._dbContext.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == entity.Id);
                if (entityToDelete != null)
                {
                    this._dbContext.Set<TEntity>().Remove(entityToDelete);
                    if (saveChanges)
                    {
                        await this.SaveAllAsync();
                    }

                    return new Response()
                    {
                        IsSuccess = true,
                        Result = entity
                    };
                }


                return new Response()
                {
                    IsSuccess = false,
                    Message = "Could not find entity in database"
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Checks if entity exists on database
        /// </summary>
        /// <param name="id">The entity Id</param>
        /// <returns></returns>
        public virtual async Task<bool> ExistsAsync(int id)
        {
            return await this._dbContext.Set<TEntity>().AnyAsync(entity => entity.Id == id);
        }

        /// <summary>
        /// Gets all entities from table that are enabled
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetAll()
        {
            return this._dbContext.Set<TEntity>()
                .Where(x => !x.IsDisabled)
                .AsNoTracking();
        }

        /// <summary>
        /// Gets all entities from table that are disabled
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetAllDisabled()
        {
            return this._dbContext.Set<TEntity>()
                .Where(x => x.IsDisabled)
                .AsNoTracking();
        }

        public virtual async Task<List<TEntity>> GetListAsync()
        {
            return await GetAll().ToListAsync();
        }

        /// <summary>
        /// Get item by UUID
        /// </summary>
        /// <param name="UUID"></param>
        /// <returns></returns>
        public async Task<TEntity> GetByUUIDAsync(string UUID)
        {
            Guid itemUUID;
            if (!Guid.TryParse(UUID, out itemUUID))
            {
                return null;
            }

            return await _dbContext.Set<TEntity>()
                .Where(x => x.UUId == itemUUID)                
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Gets entity by searching for an id
        /// </summary>
        /// <param name="id">Entity Id</param>
        /// <returns></returns>
        public virtual async Task<TEntity> GetByIdAsync(int id)
        {
            return await this._dbContext.Set<TEntity>()
                .FirstOrDefaultAsync(entity => entity.Id == id);
        }

        protected async Task<bool> SaveAllAsync()
        {
            return await this._dbContext.SaveChangesAsync() > 0;
        }

        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
