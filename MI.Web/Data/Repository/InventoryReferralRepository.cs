﻿using MI.Common.Models;
using MI.Web.Data.Entities.ApplicationSection;
using MI.Web.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Repository
{
    public class InventoryReferralRepository : IInventoryReferralRepository
    {
        private MIDbContext _dbContext;

        public InventoryReferralRepository(MIDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Response> GenerateAsync(int inventoryId)
        {
            try
            {
                var newReferral = new InventoryReferral()
                {
                    InventoryId = inventoryId,
                    IsUsed = false,
                    Expires = DateTime.Now.AddDays(15),
                    ReferralLink = Guid.NewGuid()
                };

                await this._dbContext.Set<InventoryReferral>()
                    .AddAsync(newReferral);

                if (await this._dbContext.SaveChangesAsync() > 0)
                {
                    return new Response()
                    {
                        IsSuccess = true,
                        Result = newReferral
                    };
                }

                return new Response()
                {
                    IsSuccess = false,
                    Message = "Referral was not created into database."
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<InventoryReferral> GetReferralAsync(Guid referralGuid)
        {
            return await this._dbContext.Set<InventoryReferral>()
                .FirstOrDefaultAsync(referral => referral.ReferralLink == referralGuid);
        }

        public async Task<bool> ExistsReferralAsync(Guid referralGuid)
        {
            return await this._dbContext.Set<InventoryReferral>()
                .AnyAsync(referral => referral.ReferralLink == referralGuid);
        }

        public async Task<Response> UseReferralAsync(Guid referralGuid)
        {
            try
            {
                var existing = await GetReferralAsync(referralGuid);
                if (existing == null)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = "Referral no longer exists."
                    };
                }

                existing.IsUsed = true;

                this._dbContext.Entry<InventoryReferral>(existing).CurrentValues.SetValues(existing);
                this._dbContext.Entry<InventoryReferral>(existing).State = EntityState.Modified;

                if (await this._dbContext.SaveChangesAsync() > 0)
                {
                    return new Response()
                    {
                        IsSuccess = true,
                        Result = existing
                    };
                }

                return new Response()
                {
                    IsSuccess = false,
                    Message = "Referral not used."
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
