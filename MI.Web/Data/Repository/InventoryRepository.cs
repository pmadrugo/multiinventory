﻿using MI.Common.Models;
using MI.Web.Data.Entities.ApplicationSection;
using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Repository
{
    public class InventoryRepository : GenericRepository<Inventory>, IInventoryRepository
    {
        public InventoryRepository (MIDbContext dbContext) : base(dbContext) { }

        public async Task<InventoryRole> GetUserRoleInInventoryAsync(string userId, int inventoryId)
        {
            return await _dbContext.Set<UserInventory>()
                .Include(ui => ui.Role)
                .Where(x => x.UserId == userId &&
                            x.InventoryId == inventoryId)
                .Select(x => x.Role)
                .FirstOrDefaultAsync();
        }

        public async Task<InventoryRole> GetUserRoleInInventoryAsync(string userId, Guid inventoryGuid)
        {
            var inventory = _dbContext.Set<Inventory>()
                .Where(x => x.UUId == inventoryGuid)
                .FirstOrDefaultAsync();

            if (inventory == null)
            {
                return null;
            }

            return await GetUserRoleInInventoryAsync(userId, inventory.Id);
        }

        public InventoryRole GetUserRoleInInventory(string userId, int inventoryId)
        {
            return _dbContext.Set<UserInventory>()
                .Include(ui => ui.Role)
                .Where(x => x.UserId == userId &&
                            x.InventoryId == inventoryId)
                .Select(x => x.Role)
                .FirstOrDefault();
        }

        public InventoryRole GetUserRoleInInventory(string userId, Guid inventoryGuid)
        {
            var inventory = _dbContext.Set<Inventory>()
                .Where(x => x.UUId == inventoryGuid)
                .FirstOrDefault();

            if (inventory == null)
            {
                return null;
            }

            return GetUserRoleInInventory(userId, inventory.Id);
        }

        public async Task<Response> SetUserRoleInInventoryAsync(string userId, int inventoryId, string Role)
        {
            try
            {
                var role = await _dbContext.Set<InventoryRole>()
                    .Where(x => x.Role == Role)
                    .FirstOrDefaultAsync();

                if (role == null)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = "Role not found."
                    };
                }

                var user = await _dbContext.Set<User>()
                    .Where(x => x.Id == userId)
                    .FirstOrDefaultAsync();

                if (user == null)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = "User not found."
                    };
                }

                var inventory = await _dbContext.Set<Inventory>()
                    .Where(x => x.Id == inventoryId)
                    .FirstOrDefaultAsync();

                if (inventory == null)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = "Inventory not found."
                    };
                }

                var roleInInventory = await _dbContext.Set<UserInventory>()
                    .Where(x => x.InventoryId == inventory.Id &&
                                x.UserId == user.Id)
                    .FirstOrDefaultAsync();

                if (roleInInventory != null)
                {
                    roleInInventory.RoleId = role.Id;

                    _dbContext.Entry<UserInventory>(roleInInventory).CurrentValues.SetValues(roleInInventory);
                    _dbContext.Entry<UserInventory>(roleInInventory).State = EntityState.Modified;
                }
                else
                {
                    roleInInventory = new UserInventory()
                    {
                        InventoryId = inventory.Id,
                        RoleId = role.Id,
                        UserId = user.Id
                    };

                    await _dbContext.Set<UserInventory>().AddAsync(roleInInventory);
                    
                }

                await _dbContext.SaveChangesAsync();

                return new Response()
                {
                    IsSuccess = true,
                    Result = roleInInventory
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response> SetUserRoleInInventoryAsync(string userId, Guid inventoryUUID, string Role)
        {
            var inventory = await _dbContext.Set<Inventory>()
                .Where(x => x.UUId == inventoryUUID)
                .FirstOrDefaultAsync();

            if (inventory == null)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = "Inventory not found."
                };
            }

            return await SetUserRoleInInventoryAsync(userId, inventory.Id, Role);
        }


        public IQueryable<Inventory> GetInventoriesOwnedByUser(string userId)
        {
            return _dbContext.Set<UserInventory>()
                .Where(ui => ui.UserId == userId &&
                             ui.Role.Role == "Owner" &&
                             !ui.Inventory.IsDisabled)
                .Select(ui => ui.Inventory)
                .OrderBy(i => i.Name);
        }

        public IQueryable<Inventory> GetInventoriesUserIsInvited(string userId)
        {
            return _dbContext.Set<UserInventory>()
                .Where(ui => ui.UserId == userId &&
                             ui.Role.Role != "Owner" &&
                             !ui.Inventory.IsDisabled)
                .Select(ui => ui.Inventory)
                .OrderBy(i => i.Name);
        }

        public IQueryable<Inventory> GetInventoriesFromUser(string userId)
        {
            return _dbContext.Set<UserInventory>()               
                .Where(ui => ui.UserId == userId &&
                             !ui.Inventory.IsDisabled)       
                .Select(ui => ui.Inventory)
                .OrderBy(i => i.Name);                
        }

        public IQueryable<Inventory> GetActiveAndNonActiveInventoriesFromUser(string userId)
        {
            return _dbContext.Set<UserInventory>()
                .Where(ui => ui.UserId == userId)
                .Select(ui => ui.Inventory)
                .OrderBy(i => i.Name);
        }

        public override IQueryable<Inventory> GetAll()
        {
            return base.GetAll().OrderBy(inv => inv.Name)                                
                                .Include(inv => inv.NotifiableUsers)
                                .Include(inv => inv.Users);
        }

        public async Task<User> GetInventoryOwnerAsync(int inventoryId)
        {
            return await _dbContext.Set<UserInventory>()
                .Where(ui => ui.Role.Role == "Owner" &&
                             ui.InventoryId == inventoryId)
                .Select(ui => ui.User)
                .FirstOrDefaultAsync();
        }

        public async Task<Response> SetInventoryOwnerAsync(string userId, int inventoryId)
        {
            var ownerRecord = await _dbContext.Set<UserInventory>()
                .Where(ui => ui.InventoryId == inventoryId &&
                             ui.Role.Role == "Owner")
                .FirstOrDefaultAsync();

            if (ownerRecord == null)
                return await this.SetUserRoleInInventoryAsync(userId, inventoryId, "Owner");

            if (ownerRecord.UserId == userId)
                return new Response() { IsSuccess = true };

            ownerRecord.UserId = userId;

            try
            {
                _dbContext.Entry<UserInventory>(ownerRecord).CurrentValues.SetValues(ownerRecord);
                _dbContext.Entry<UserInventory>(ownerRecord).State = EntityState.Modified;

                await _dbContext.SaveChangesAsync();

                return new Response()
                {
                    IsSuccess = true,
                    Result = ownerRecord
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response> CreateForAsync(Inventory entity, string userId)
        {
            try
            {
                var createdInventory = await this.CreateAsync(entity);
                if (createdInventory.IsSuccess)
                {
                    var ownerPermission = await this.SetInventoryOwnerAsync(userId, ((Inventory)createdInventory.Result).Id);
                    if (ownerPermission.IsSuccess)
                    {
                        return new Response() { IsSuccess = true, Result = (Inventory)createdInventory.Result };
                    }
                }

                return new Response() { IsSuccess = false, Message = "" };
            }
            catch (Exception ex)
            {
                return new Response() { IsSuccess = false, Message = ex.Message };
            }
        }

        public async Task<bool> ExistsAsync(Guid UUID)
        {
            return await _dbContext.Set<Inventory>()
                .AnyAsync(inv => inv.UUId == UUID);
        }

        public IQueryable<UserInventory> GetUsersInInventory(int id)
        {
            return _dbContext.Set<UserInventory>()
                .Include(ui => ui.User)
                .Include(ui => ui.Role)
                .Where(ui => ui.InventoryId == id)
                .OrderBy(ui => ui.Role.Id)
                .ThenBy(ui => ui.User.FullName);
        }

        public async Task<Response> RemoveUserFromInventory(string userId, int inventoryId)
        {
            try
            {

                var user = await _dbContext.Set<User>()
                    .Where(x => x.Id == userId)
                    .FirstOrDefaultAsync();

                if (user == null)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = "User not found."
                    };
                }

                var inventory = await _dbContext.Set<Inventory>()
                    .Where(x => x.Id == inventoryId)
                    .FirstOrDefaultAsync();

                if (inventory == null)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = "Inventory not found."
                    };
                }

                var roleInInventory = await _dbContext.Set<UserInventory>()
                    .Where(x => x.InventoryId == inventory.Id &&
                                x.UserId == user.Id)
                    .FirstOrDefaultAsync();

                if (roleInInventory != null)
                {
                    _dbContext.Remove(roleInInventory);
                }
                else
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = "User doesn't have any role in this inventory"
                    };
                }

                await _dbContext.SaveChangesAsync();

                return new Response()
                {
                    IsSuccess = true,
                    Result = roleInInventory
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
