﻿using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Repository
{
    public class ItemPlaceRepository : GenericRepository<ItemPlace>, IItemPlaceRepository
    {
        public ItemPlaceRepository(MIDbContext dbContext) : base(dbContext) { }

        public IQueryable<ItemPlace> GetItemPlacesFromInventory(int invenotryId)
        {
            return _dbContext.Set<ItemPlace>()
                .Where(ip => ip.InventoryId == invenotryId &&
                             !ip.IsDisabled)
                .OrderBy(ip => ip.Name);
        }

        public IQueryable<ItemPlace> GetAllItemPlacesFromInventory(int invenotryId)
        {
            return _dbContext.Set<ItemPlace>()
                .Where(ip => ip.InventoryId == invenotryId
                             )
                .OrderBy(ip => ip.Name);
        }
    }
}
