﻿using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Repository
{
    public class ItemBrandRepository : GenericRepository<ItemBrand>, IItemBrandRepository
    {
        public ItemBrandRepository(MIDbContext dbContext) : base(dbContext) { }

        public IQueryable<ItemBrand> GetItemBrandsFromInventory(int inventoryId)
        {
            return _dbContext.Set<ItemBrand>()
                .Where(i => i.InventoryId == inventoryId && 
                            !i.IsDisabled)
                .OrderBy(i => i.Name);
        }

        public IQueryable<ItemBrand> GetAllItemBrandsFromInventory(int inventoryId)
        { 
            return _dbContext.Set<ItemBrand>()
                .Where(i => i.InventoryId == inventoryId)
                .OrderBy(i => i.Name);
        }
    }
}
