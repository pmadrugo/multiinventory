﻿using MI.Common.Models;
using MI.Web.Data.Entities.ApplicationSection;
using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Repository
{
    public class InventoryNotifyRepository
    {
        private MIDbContext _dbContext;

        public InventoryNotifyRepository(MIDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> IsUserNotifiedOfInventory(int inventoryId, string userId)
        {
            return await _dbContext.Set<InventoryNotify>()
                .AnyAsync(iN => iN.InventoryId == inventoryId &&
                                iN.UserId == userId);
        }

        public async Task<Response> AddUserNotifyOfInventory(int inventoryId, string userId)
        {
            if (!await IsUserNotifiedOfInventory(inventoryId, userId))
            {
                try
                {
                    var addedNotify = await _dbContext
                        .Set<InventoryNotify>()
                        .AddAsync(new InventoryNotify()
                        {
                            InventoryId = inventoryId,
                            UserId = userId
                        });

                    await _dbContext.SaveChangesAsync();

                    return new Response()
                    {
                        IsSuccess = true,
                        Result = addedNotify
                    };
                }
                catch (Exception ex)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = ex.Message
                    };
                }
            }

            return new Response()
            {
                IsSuccess = false,
                Message = "Notification already inserted"
            };
        }

        public async Task<Response> RemoveUserNotifyOfInventory(int inventoryId, string userId)
        {
            if (await IsUserNotifiedOfInventory(inventoryId, userId))
            {
                try
                {
                    var notifyToDelete = await _dbContext
                        .Set<InventoryNotify>()
                        .FirstOrDefaultAsync(iN =>
                            iN.InventoryId == inventoryId &&
                            iN.UserId == userId);

                    if (notifyToDelete != null)
                    {
                        _dbContext
                            .Set<InventoryNotify>()
                            .Remove(notifyToDelete);

                        await _dbContext.SaveChangesAsync();

                        return new Response()
                        {
                            IsSuccess = true,
                            Result = notifyToDelete
                        };
                    }
                }
                catch (Exception ex)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = ex.Message
                    };
                }
            }

            return new Response()
            {
                IsSuccess = false,
                Message = "Cannot find record of notifiable user."
            };
        }

        public IQueryable<User> GetAllNotifiableUsersOfInventory(int inventoryId)
        {
            return _dbContext.Set<InventoryNotify>()
                .Where(iN => iN.InventoryId == inventoryId)
                .Select(c => c.User);
        }

        public IQueryable<Inventory> GetAllNotifiableInventoriesOfUser(string userId)
        {
            return _dbContext.Set<InventoryNotify>()
                    .Where(IN => IN.UserId == userId)
                    .Select(c => c.Inventory);
        }


    }
}
