﻿using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Repository
{
    public class ItemTypeRepository: GenericRepository<ItemType>, IItemTypeRepository
    {

        public ItemTypeRepository(MIDbContext dbContext): base (dbContext) { }
        
        public IQueryable<ItemType> GetItemTypesFromInventory(int inventoryId)
        {
            return _dbContext.Set<ItemType>()
                .Where(x => x.InventoryId == inventoryId &&
                            !x.IsDisabled);
        }

        public IQueryable<ItemType> GetAllItemTypesFromInventory(int inventoryId)
        {
            return _dbContext.Set<ItemType>()
                .Where(x => x.InventoryId == inventoryId);
        }

    }
}
