﻿using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Repository
{
    public class ItemStateRepository : GenericRepository<ItemState>, IItemStateRepository
    {
        public ItemStateRepository(MIDbContext dbContext) : base(dbContext) { }

        public IQueryable<ItemState> GetItemStatesFromInventory(int inventoryId)
        {
            return _dbContext.Set<ItemState>()
                .Where(i => i.InventoryId == inventoryId &&
                            !i.IsDisabled)
                .OrderBy(i => i.State);
        }
        
        public IQueryable<ItemState> GetAllItemStatesFromInventory(int inventoryId)
        {
            return _dbContext.Set<ItemState>()
                .Where(i => i.InventoryId == inventoryId)
                .OrderBy(i => i.State);
        }
    }
}
