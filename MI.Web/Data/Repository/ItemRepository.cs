﻿using MI.Common.Models;
using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Repository
{
    public class ItemRepository : GenericRepository<Item>, IItemRepository
    {
        public ItemRepository(MIDbContext dbContext) : base(dbContext) { }

        public IQueryable<Item> GetItemsFromInventory(int inventoryId)
        {
            return _dbContext.Set<Item>()
                .Include(i => i.ItemState)
                .Include(i => i.Place)
                .Include(i => i.Model)
                .Include(i => i.Model.Brand)
                .Include(i => i.Type)
                .Where(i => i.InventoryId == inventoryId &&
                            !i.IsDisabled)
                .OrderBy(items => items.Name);
        }

        public IQueryable<Item> GetActiveAndNonActiveItemsFromInventory(int inventoryId)
        {
            return _dbContext.Set<Item>()
                .Include(i => i.ItemState)
                .Include(i => i.Place)
                .Include(i => i.Model)
                .Include(i => i.Model.Brand)
                .Include(i => i.Type)
                .Where(i => i.InventoryId == inventoryId)
                .OrderBy(items => items.Name);
        }

        public async Task<Item> GetByIdWithBrandAsync(int id)
        {
            return await this._dbContext.Set<Item>()
                .Include(item => item.Model)
                .FirstOrDefaultAsync(entity => entity.Id == id);
        }

        public IQueryable<Item> GetActiveItemsFromInventory(int inventoryId)
        {
            return GetItemsFromInventory(inventoryId)
                .Where(i => i.ItemOutput == null);
        }


        public IQueryable<Item> GetNonActiveItemsFromInventory(int inventoryId)
        {
            return GetItemsFromInventory(inventoryId)
                .Where(i => i.ItemOutput != null);
        }

        public async Task<Response> SetItemOutputAsync(int itemId, DateTime dateOutput, int? stateId = null)
        {
            var item = await this.GetByIdAsync(itemId);
            if (item == null)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = "Item not found"
                };
            }

            item.ItemOutput = dateOutput;
            if (stateId != null)
            {
                item.ItemStateId = stateId.Value;
            }

            return await this.UpdateAsync(item);
        }

        public Item GetFullItemByUUID(Guid UUID)
        {
            return _dbContext.Set<Item>()
                .Include(x => x.ItemState)
                .Include(x => x.Place)
                .Include(x => x.Type)
                .Include(x => x.Model)
                .Include(x => x.Model.Brand)
                .Where(x => x.UUId == UUID)
                .FirstOrDefault();
        }
    }
}
