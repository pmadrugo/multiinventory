﻿using MI.Web.Data.Entities;
using MI.Web.Data.Entities.ApplicationSection;
using MI.Web.Data.Interfaces;
using MI.Web.Helpers.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data
{
    public class SeedDb
    {
        private readonly MIDbContext            _dbContext;

        private readonly IUserHelper            _userHelper;
        private readonly IRoleHelper            _roleHelper;

        private readonly IUnitOfWork            _unitOfWork;

        public SeedDb(MIDbContext dbContext, 
            IUserHelper userHelper, IRoleHelper roleHelper, IUnitOfWork unitOfWork)
        {
            this._dbContext = dbContext;

            this._userHelper = userHelper;
            this._roleHelper = roleHelper;
            this._unitOfWork = unitOfWork;
        }
        
        public async Task SeedAsync()
        {
            await this._dbContext.Database.EnsureCreatedAsync();

            // Setup Application Roles
            #region Setup Roles
            await SetupRolesAsync();
            #endregion

            // Setup Application Default Users
            #region Setup Default Users
            await SetupUsersAsync();
            #endregion

            // Setup Inventory Roles
            #region Setup Inventory Roles
            await SetupInventoryRolesAsync();
            #endregion
        }

        private async Task SetupRolesAsync()
        {
            if (!await _roleHelper.RoleExistsAsync("Superuser"))
            {
                await _roleHelper.CreateAsync(
                    new IdentityRole() { Name = "Superuser" });
            }

            if (!await _roleHelper.RoleExistsAsync("Admin"))
            {
                await _roleHelper.CreateAsync(
                    new IdentityRole() { Name = "Admin" });
            }

            if (!await _roleHelper.RoleExistsAsync("User"))
            {
                await _roleHelper.CreateAsync(
                    new IdentityRole() { Name = "User" });
            }
        }

        private async Task SetupUsersAsync()
        {
            await _userHelper.CreateSuperUserIfNotExistsAsync(
                new User()
                {
                    Email = "pedro.marcelino.madrugo@formandos.cinel.pt",
                    UserName = "pedro.marcelino.madrugo@formandos.cinel.pt",
                    BirthDate = new DateTime(1991, 05, 19),
                    VATNumber = "274246694",
                    FirstName = "Pedro",
                    LastName = "Madrugo",
                    EmailConfirmed = true,
                    IsApproved = true,
                    ImagePath = "~/media/images/common/default_user_image.jpg"
                },
                "P@ssw0rd"
            );

            await _userHelper.CreateSuperUserIfNotExistsAsync(
                new User()
                {
                    Email = "ruben.moleiro.mateus@formandos.cinel.pt",
                    UserName = "ruben.moleiro.mateus@formandos.cinel.pt",
                    BirthDate = new DateTime(1998, 07, 11),
                    VATNumber = "250292190",
                    FirstName = "Ruben",
                    LastName = "Mateus",
                    EmailConfirmed = true,
                    IsApproved = true,
                    ImagePath = "~/media/images/common/default_user_image.jpg"
                },
                "Ma7351!"
            );

            await _userHelper.CreateSuperUserIfNotExistsAsync(
                new User()
                {
                    Email = "fabio.dias.sarreira@formandos.cinel.pt",
                    UserName = "fabio.dias.sarreira@formandos.cinel.pt",
                    BirthDate = new DateTime(1988, 05, 02),
                    VATNumber = "228493404",
                    FirstName = "Fábio",
                    LastName = "Serreira",
                    EmailConfirmed = true,
                    IsApproved = true,
                    ImagePath = "~/media/images/common/default_user_image.jpg"
                },
                "atuaPrima1!"
            );

            // TODO : Generate superuser accounts
        }

        private async Task SetupInventoryRolesAsync()
        {
            // Owner
            if (!await _unitOfWork.InventoryRoles.IsThereAnyAsync("Owner"))
            {
                await _unitOfWork.InventoryRoles.CreateAsync(
                    new InventoryRole() { Role = "Owner" });
            }

            // Admin
            if (!await _unitOfWork.InventoryRoles.IsThereAnyAsync("Admin"))
            {
                await _unitOfWork.InventoryRoles.CreateAsync(
                    new InventoryRole() { Role = "Admin" });
            }

            // Employee
            if (!await _unitOfWork.InventoryRoles.IsThereAnyAsync("Employee"))
            {
                await _unitOfWork.InventoryRoles.CreateAsync(
                    new InventoryRole() { Role = "Employee" });
            }

            _unitOfWork.Commit();
        }
    }
}
