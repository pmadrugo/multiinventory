﻿using MI.Web.Data.Entities.InventorySection;
using MI.Web.Extensions.DataAttributes;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MI.Web.Data.Entities.ApplicationSection
{
    public class User : IdentityUser
    {
        [Required(ErrorMessage = "É necessario preencher {0}.")]
        [Display(Name = "Primeiro Nome")]
        [StringLength(50, ErrorMessage = "{0} só pode ter até {1} caractéres.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "É necessário preencher {0}.")]
        [Display(Name = "Apelido")]
        [StringLength(50, ErrorMessage = "{0} só pode ter até {1} caractéres.")]
        public string LastName { get; set; }

        [Display(Name = "Alcunha")]
        public string DisplayName { get; set; }

        public string ImagePath { get; set; }

        [Required(ErrorMessage = "É necessário preencher {0}.")]
        [Display(Name = "Número de Identificação Fiscal")]
        [RegularExpression("^[1-35][0-9]{8,}$", ErrorMessage = "Tem de inserir um {0} válido.")]
        public string VATNumber { get; set; }

        [Display(Name = "Data de Nascimento")]
        [Required(ErrorMessage = "É necessário preencher uma {0}")]
        [BirthdateOfAgeRange(ErrorMessage = "É necessario que a {0}.")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        [NotMapped]
        public string FullName { get => $"{FirstName} {LastName}"; }

        [ScaffoldColumn(false)]
        public bool IsApproved { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? LastModified { get; set; }
        
        public virtual ICollection<UserInventory> User_Inventories { get; set; }

        public virtual ICollection<InventoryNotify> NotifiedOf { get; set; }
    }
}
