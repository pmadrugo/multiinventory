﻿using MI.Web.Data.Entities.InventorySection;
using System.ComponentModel.DataAnnotations.Schema;

namespace MI.Web.Data.Entities.ApplicationSection
{
    public class UserInventory
    {
        // Data
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public int InventoryId { get; set; }
        [ForeignKey("InventoryId")]
        public virtual Inventory Inventory { get; set; }

        public int RoleId { get; set; }
        public virtual InventoryRole Role { get; set; }
    }
}
