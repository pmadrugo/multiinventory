﻿using MI.Web.Data.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace MI.Web.Data.Entities.ApplicationSection
{
    public class InventoryRole : IEntity
    {
        // Metadata
        [Key]
        public int Id { get; set; }

        [ScaffoldColumn(false)]
        public Guid UUId { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? LastModified { get; set; }

        [ScaffoldColumn(false)]
        public bool IsDisabled { get; set; }

        // Data
        public string Role { get; set; }
    }
}
