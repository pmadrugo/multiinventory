﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Entities.ApplicationSection
{
    public class InventoryReferral
    {
        [Key]
        public Guid ReferralLink { get; set; }

        public int InventoryId { get; set; }

        public bool IsUsed { get; set; }

        public DateTime Expires { get; set; }
    }
}
