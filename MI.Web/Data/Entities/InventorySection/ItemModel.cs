﻿using MI.Web.Data.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Entities.InventorySection
{
    public class ItemModel : IEntity
    {
        #region Metadata
        [Key]
        public int Id { get; set; }

        [ScaffoldColumn(false)]
        public bool IsDisabled { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? LastModified { get; set; }

        [ScaffoldColumn(false)]
        public Guid UUId { get; set; }

        public int InventoryId { get; set; }

        [ForeignKey("InventoryId")]
        [JsonIgnore]
        public virtual Inventory Inventory { get; set; }
        #endregion

        #region Data


        [Display(Name = "Marca")]
        [Range(1, double.MaxValue, ErrorMessage = "Escolha um valor válido...")]
        [Required(ErrorMessage = "É necessário preencher uma {0}.")]
        public int BrandId { get; set; }
        [ForeignKey("BrandId")]
        public virtual ItemBrand Brand { get; set; }

        [Display(Name = "Modelo")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "O {0} tem de ter entre {2} e {1} caractéres.")]
        [Required(ErrorMessage = "É necessário preencher um {0}.")]
        public string Name { get; set; }

        public virtual ICollection<Item> Items { get; set; } 
        #endregion
    }
}
