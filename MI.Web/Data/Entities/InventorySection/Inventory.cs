﻿using MI.Web.Data.Entities.ApplicationSection;
using MI.Web.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MI.Web.Data.Entities.InventorySection
{
    public class Inventory : IEntity
    {
        // Metadata
        [Key]
        public int Id { get; set; }

        [ScaffoldColumn(false)]
        public Guid UUId { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? LastModified { get; set; }

        [ScaffoldColumn(false)]
        public bool IsDisabled { get; set; }

        public virtual ICollection<UserInventory> Users { get; set; }

        // Data
        [Display(Name = "Nome")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "O {0} do Inventário tem que ter entre {2} e {1} caractéres.")]
        [Required(ErrorMessage = "O inventário tem que ter um {0}.")]
        public string Name { get; set; }

        public string ImagePath { get; set; }

        public virtual ICollection<Item> Items { get; set; }

        public virtual ICollection<ItemBrand> Brands { get; set; }

        public virtual ICollection<ItemModel> Models { get; set; }

        public virtual ICollection<ItemType> Item_Types { get; set; }

        public virtual ICollection<ItemState> Item_States { get; set; }

        public virtual ICollection<InventoryNotify> NotifiableUsers { get; set; }
    }
}
