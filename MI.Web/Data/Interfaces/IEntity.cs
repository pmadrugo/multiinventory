﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }

        Guid UUId { get; set; }

        DateTime? LastModified { get; set; }

        bool IsDisabled { get; set; }
    }
}
