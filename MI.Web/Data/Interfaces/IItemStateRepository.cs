﻿using MI.Web.Data.Entities.InventorySection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data.Interfaces
{
    public interface IItemStateRepository : IGenericRepository<ItemState>
    {
    }
}
