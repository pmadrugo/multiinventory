﻿using MI.Common.Models;
using MI.Web.Data.Repository;

namespace MI.Web.Data.Interfaces
{
    public interface IUnitOfWork
    {
        InventoryRoleRepository InventoryRoles { get; }
        InventoryRepository Inventories { get; }
        ItemModelRepository ItemModels { get; }
        ItemTypeRepository ItemTypes { get; }
        ItemBrandRepository ItemBrands { get; }
        ItemPlaceRepository ItemPlaces { get; }
        ItemStateRepository ItemsStates { get; }
        ItemRepository Items { get; }
        InventoryReferralRepository Referrals { get; }
        InventoryNotifyRepository NotifiableUsers { get; }

        Response Commit(bool lastTransaction = false);

    }
}