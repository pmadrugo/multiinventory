﻿namespace MI.Web.Data.Interfaces
{
    public interface IInventoryReferralRepository
    {
        System.Threading.Tasks.Task<bool> ExistsReferralAsync(System.Guid referralGuid);
        System.Threading.Tasks.Task<Common.Models.Response> GenerateAsync(int inventoryId);
        System.Threading.Tasks.Task<Entities.ApplicationSection.InventoryReferral> GetReferralAsync(System.Guid referralGuid);
        System.Threading.Tasks.Task<Common.Models.Response> UseReferralAsync(System.Guid referralGuid);
    }
}