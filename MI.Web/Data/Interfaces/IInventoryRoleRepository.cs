﻿using MI.Web.Data.Entities.ApplicationSection;

namespace MI.Web.Data.Interfaces
{
    public interface IInventoryRoleRepository : IGenericRepository<InventoryRole>
    {
    }
}
