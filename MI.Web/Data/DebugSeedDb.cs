﻿using MI.Web.Data.Interfaces;
using MI.Web.Helpers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Data
{
    public class DebugSeedDb
    {
        private readonly MIDbContext _dbContext;

        private readonly IUserHelper _userHelper;
        private readonly IRoleHelper _roleHelper;

        private readonly IUnitOfWork _unitOfWork;

        public DebugSeedDb(MIDbContext dbContext,
            IUserHelper userHelper, IRoleHelper roleHelper, IUnitOfWork unitOfWork)
        {
            this._dbContext = dbContext;

            this._userHelper = userHelper;
            this._roleHelper = roleHelper;
            this._unitOfWork = unitOfWork;
        }

        public async Task SeedDebugAsync()
        {
            await this._dbContext.Database.EnsureCreatedAsync();

            var invs = _unitOfWork.Inventories.GetAll();
            if (invs.Count() == 0)
            {
                await CreateInventoriesAsync();
            }
        }

        private async Task CreateInventoriesAsync()
        {            
            var user = _userHelper.GetUserByEmailAsync("a@a.a");
            var result = await _unitOfWork.Inventories.CreateForAsync(new Entities.InventorySection.Inventory
            {
                Name = "Talho do Pedro",
                ImagePath = "~/media/images/common/no_preview.jpg"
            }, 
            user.Result.Id);

            Entities.InventorySection.Inventory inventory = (Entities.InventorySection.Inventory)result.Result;

            var resultBrand = await _unitOfWork.ItemBrands.CreateAsync(new Entities.InventorySection.ItemBrand {
                Name = "Nestle",
                InventoryId = inventory.Id
            });
            Entities.InventorySection.ItemBrand itemBrand = (Entities.InventorySection.ItemBrand)resultBrand.Result;

            var resultModel = await _unitOfWork.ItemModels.CreateAsync(new Entities.InventorySection.ItemModel {
                Name = "Cereais",
                BrandId = itemBrand.Id,
                InventoryId = inventory.Id
            });
            Entities.InventorySection.ItemModel itemModel = (Entities.InventorySection.ItemModel)resultModel.Result;

            var resultPlace = await _unitOfWork.ItemPlaces.CreateAsync(new Entities.InventorySection.ItemPlace {
                Name = "Seção Pequeno Almoço",
                InventoryId = inventory.Id
            });
            Entities.InventorySection.ItemPlace itemPlace = (Entities.InventorySection.ItemPlace)resultPlace.Result;

            var resultState = await _unitOfWork.ItemsStates.CreateAsync(new Entities.InventorySection.ItemState
            {
                State = "À venda",
                InventoryId = inventory.Id
            });
            Entities.InventorySection.ItemState itemState = (Entities.InventorySection.ItemState)resultState.Result;

            var resultType = await _unitOfWork.ItemTypes.CreateAsync(new Entities.InventorySection.ItemType
            {
                Name = "Pequeno-Almoço",
                InventoryId = inventory.Id
            });
            Entities.InventorySection.ItemType itemType = (Entities.InventorySection.ItemType)resultType.Result;

            var resultItem = await _unitOfWork.Items.CreateAsync(new Entities.InventorySection.Item
            {
                InventoryId = inventory.Id,
                Name = "Cocaina em Cereais",
                Description = "Os melhores cerais da nestle",
                ItemInput = DateTime.Now,
                ModelId = itemModel.Id,
                PlaceId = itemPlace.Id,
                ItemStateId = itemState.Id,
                TypeId = itemType.Id,
                ImagePath = "~/media/images/common/no_preview.jpg"
            });
        }
    }
}
