﻿using MI.Web.Data.Entities.ApplicationSection;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using MI.Web.Data.Entities.InventorySection;

namespace MI.Web.Data
{
    public class MIDbContext : IdentityDbContext<User>
    {
        public MIDbContext(DbContextOptions<MIDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            #region Decimal Types

            #endregion

            #region Index Attribution
            builder.Entity<ItemBrand>()
                .HasIndex(ib => ib.InventoryId );

            builder.Entity<ItemModel>()
                .HasIndex(im => im.InventoryId );

            builder.Entity<ItemPlace>()
                .HasIndex(ip => ip.InventoryId);

            builder.Entity<ItemState>()
                .HasIndex(iS => iS.InventoryId);

            builder.Entity<ItemType>()
                .HasIndex(it => it.InventoryId);

            builder.Entity<Item>()
                .HasIndex(i => i.InventoryId);

            #endregion

            #region Composite Key Attribution

            builder.Entity<UserInventory>()
                .HasKey(ui =>
                    new { ui.UserId, ui.InventoryId });

            builder.Entity<InventoryNotify>()
                .HasKey(iN =>
                    new { iN.UserId, iN.InventoryId });

            #endregion

            #region Many to Many Relationships

            builder.Entity<UserInventory>()
                .HasOne(ui => ui.User)
                .WithMany(u => u.User_Inventories)
                .HasForeignKey(ui => ui.UserId);

            builder.Entity<UserInventory>()
                .HasOne(ui => ui.Inventory)
                .WithMany(i => i.Users)
                .HasForeignKey(ui => ui.InventoryId);

            builder.Entity<InventoryNotify>()
                .HasOne(iN => iN.Inventory)
                .WithMany(i => i.NotifiableUsers)
                .HasForeignKey(iN => iN.InventoryId);

            builder.Entity<InventoryNotify>()
                .HasOne(iN => iN.User)
                .WithMany(u => u.NotifiedOf)
                .HasForeignKey(iN => iN.UserId);

            #endregion

            #region Setup Indexes

            #endregion

            // Remove On Delete Cascade
            var cascadeFKs = builder.Model
                .GetEntityTypes()
                .SelectMany(x => x.GetForeignKeys())
                .Where(x => !x.IsOwnership && x.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var cascadeFK in cascadeFKs)
            {
                cascadeFK.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(builder);
        }

        #region Entities

        public DbSet<ItemModel> ItemModel { get; set; }

        public DbSet<Inventory> Inventory { get; set; }

        public DbSet<ItemType> ItemType { get; set; }

        public DbSet<ItemBrand> ItemBrand { get; set; }

        public DbSet<ItemPlace> ItemPlace { get; set; }

        public DbSet<ItemState> ItemState { get; set; }

        public DbSet<Item> Item { get; set; }

        public DbSet<InventoryReferral> Referrals { get; set; }

        public DbSet<InventoryRole> InventoryRoles { get; set; }

        #endregion

        #region Methods

        #endregion
    }
}
