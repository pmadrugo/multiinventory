﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MI.Web.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MI.Web.Data.Entities;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using MI.Web.Helpers;
using MI.Web.Helpers.Interfaces;
using MI.Web.Data.Interfaces;
using MI.Web.Data.Repository;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.HttpOverrides;
using MI.Web.Models;
using MI.Web.Services;
using MI.Web.Services.Interfaces;
using MI.Web.Data.Entities.ApplicationSection;
using Microsoft.AspNetCore.Authentication.Facebook;
using Microsoft.Extensions.FileProviders;
using System.IO;
using System.Net;
using System.Net.Http;
using MI.Web.Services;
using MI.Web.Services.Interfaces;

namespace MI.Web
{
    public class Startup
    {
       public Startup(IHostingEnvironment env)
        {
            var builder =
                new ConfigurationBuilder()
                    .SetBasePath(env.ContentRootPath)
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                    .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Setup Email Service
            services.Configure<EmailSettingsViewModel>(Configuration.GetSection("EmailSettings"));
            services.AddSingleton<IEmailSender, EmailSender>();

            // Setup ASP.Net Identity
            #region Identity Service
            services.AddIdentity<User, IdentityRole>(cfg =>
            {
                //cfg.SignIn.RequireConfirmedEmail = true;
                cfg.User.RequireUniqueEmail = true;

                cfg.Password.RequireDigit = true;
                cfg.Password.RequiredUniqueChars = 0;
                cfg.Password.RequireLowercase = true;
                cfg.Password.RequireNonAlphanumeric = true;
                cfg.Password.RequireUppercase = true;
                cfg.Password.RequiredLength = 6;    
            })
                .AddEntityFrameworkStores<MIDbContext>()
                .AddDefaultTokenProviders();
            #endregion

            // Setup Token Service
            #region Token Service
            services.AddAuthentication()
                .AddCookie(options =>
                {
                    options.LoginPath = "/Account/Login";
                    options.LogoutPath = "/Account/Logout";
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                    {
                        ValidIssuer = this.Configuration["Tokens:Issuer"],
                        ValidAudience = this.Configuration["Tokens:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(this.Configuration["Tokens:Key"]))
                    };
                })
                .AddFacebook(fbOptions =>
                {
                    fbOptions.AppId = Configuration["Authentication:Facebook:AppId"];
                    fbOptions.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
                })
                .AddGoogle(googleOptions =>
                {
                    googleOptions.ClientId = Configuration["Authentication:Google:ClientId"];
                    googleOptions.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
                })
                .AddTwitter(twitterOptions =>
                {
                    twitterOptions.ConsumerKey = Configuration["Authentication:Twitter:ConsumerKey"];
                    twitterOptions.ConsumerSecret = Configuration["Authentication:Twitter:ConsumerSecret"];
                    twitterOptions.RetrieveUserDetails = true;
                })
                .AddMicrosoftAccount(microsoftOptions =>
                {
                    microsoftOptions.ClientId = Configuration["Authentication:Microsoft:ClientId"];
                    microsoftOptions.ClientSecret = Configuration["Authentication:Microsoft:ClientSecret"];
                });
            #endregion

            // Context Setup
            #region DbContext Setup
            services.AddDbContext<MIDbContext>(options =>
                options.UseSqlServer(
                    //Configuration.GetConnectionString("RaymanX11Connection")));
                    //Configuration.GetConnectionString("AspNetCoreVMConnection")));
                    //Configuration.GetConnectionString("DefaultConnection")));
                    Configuration.GetConnectionString("LocalhostConnection")));

            #endregion

            // Project Specific Services (Repositories, etc)
            #region Project Specific Services
            services.AddTransient<SeedDb>();
            services.AddTransient<DebugSeedDb>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<INotifyService, NotifyService>();
            services.AddScoped<ISyncService, SyncService>();

            services.AddScoped<IUserHelper, UserHelper>();
            services.AddScoped<IRoleHelper, RoleHelper>();

            //services.AddScoped<IInventoryUserHelper, InventoryUserHelper>();
            #endregion

            // Setup Cookies
            #region Setup Cookies
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            #endregion

            // Setup View Engine
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationFormats.Clear();
                options.ViewLocationFormats.Add("/Views/Application/{1}/{0}" + RazorViewEngine.ViewExtension);
                options.ViewLocationFormats.Add("/Views/Inventory/{1}/{0}" + RazorViewEngine.ViewExtension);
            });

            services.AddSession();

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error/500");
                app.UseStatusCodePagesWithReExecute("/Error/{0}");
                //app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();                
            }

            app.UseForwardedHeaders(
                new ForwardedHeadersOptions
                {
                    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
                }
            );

            app.Use(async (context, next) =>
            {
                // Override scheme to be https
                context.Request.Scheme = "https";
                await next();
            });


            app.UseHttpsRedirection();
            app.UseStaticFiles(
                new StaticFileOptions()
                {
                    ServeUnknownFileTypes = true,
                    DefaultContentType = "text/plain"
                }
            );
            
            app.UseAuthentication();

            app.UseSession();

            app.UseMvc(routes =>
            {
                // Application
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}/{id?}",
                    defaults: new { controller = "Home", action = "Index" });

                // Inventory
                routes.MapRoute(
                    name: "inventoryHome",
                    template: "{inventoryId}/{controller}/{action}/{id?}",
                    defaults: new { inventoryId = "Error", controller = "Inventory", action = "Index" });
            });

            app.UseCookiePolicy();
        }
    }
}
