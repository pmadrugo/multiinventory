using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.Web.Models;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace MI.Web.Controllers.Application
{
    public class ErrorsController : Controller
    {
        [Route("Error/500")]
        public IActionResult Error500()
        {
            //var exceptionData = HttpContext.Features.Get<IExceptionHandlerPathFeature>();           

            return View(new CustomErrorViewModel {
                StatusCode = 500,
                ErrorMessage = "Pedimos desculpa pelo sucedido, ocorreu um erro interno...",
                ImageUrl = "/media/images/error/internalerror.png"
            });
        }

        [Route("Error/{statusCode}")]
        public IActionResult HandleErrorCode(int statusCode)
        {

            //var statusCodeData = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();

            CustomErrorViewModel error = new CustomErrorViewModel();

            switch (statusCode)
            {
                case 400:
                    error.StatusCode = 400;
                    error.ErrorMessage = "N�o foi possivel processar a ac��o pedida!";
                    error.ImageUrl = "/media/images/error/badrequest.png";
                    break;

                case 403:
                    error.StatusCode = 403;
                    error.ErrorMessage = "N�o tem permiss�o para realizar a ac��o!";
                    error.ImageUrl = "/media/images/error/forbid.png";
                    break;

                case 404:
                    error.StatusCode = 404;
                    error.ErrorMessage = "Ups, o que tentou aceder n�o existe!";
                    error.ImageUrl = "/media/images/error/notfound.png";
                    break;
            }

            return View(error);
        }
    }
}