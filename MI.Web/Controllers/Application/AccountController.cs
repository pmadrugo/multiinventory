﻿using MI.Web.Data.Entities;
using MI.Web.Helpers;
using MI.Web.Helpers.Interfaces;
using MI.Web.Models.Accounts;
using MI.Web.Extensions;
using MI.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using MI.Web.Data.Entities.ApplicationSection;
using MI.Web.Data.Interfaces;
using Microsoft.AspNetCore.Authentication;

namespace MI.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserHelper _userHelper;
        private readonly IRoleHelper _roleHelper;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IEmailSender _emailSender;

        private readonly IConfiguration _configuration;

        public AccountController(IUserHelper userHelper, IRoleHelper roleHelper, IConfiguration configuration, IEmailSender emailSender, IUnitOfWork unitOfWork)
        {
            this._userHelper = userHelper;
            this._roleHelper = roleHelper;

            this._unitOfWork = unitOfWork;

            this._emailSender = emailSender;
            this._configuration = configuration;
        }

        #region /Account/ExternalLogin

        public async Task<IActionResult> ExternalLogin()
        {
            return RedirectToRoute("default", new { controller = "Account", action = "Login" });
        }

        [HttpPost]
        public async Task<IActionResult> ExternalLogin(string provider, string returnUrl = null)
        {
            this.HttpContext.Session.Clear();

            await this._userHelper.LogoutAsync();

            // Request a redirect to the external login provider
            var redirectUrl = Url.RouteUrl("default", new { controller = "Account", action = "ExternalLoginCallback", returnUrl = returnUrl });
            var properties = _userHelper.ConfigureExternalAuthenticationProperties(provider, redirectUrl);

            return new ChallengeResult(provider, properties);
        }

        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (remoteError != null)
            {
                this.SendError($"Erro de autenticação externa: {remoteError}");

                return RedirectToRoute("default", new { controller = "Account", action = "Login", returnUrl = returnUrl });
            }

            var info = await _userHelper.GetExternalLoginInfoAsync();
            if (info == null)
            {
                this.SendError($"Erro a carregar informação externa.");

                return RedirectToRoute("default", new { controller = "Account", action = "Login", returnUrl = returnUrl });
            }

            if (info.Principal.HasClaim(c => c.Type == ClaimTypes.Email))
            {
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                var user = await _userHelper.GetUserByEmailAsync(email);

                if (user != null)
                {
                    user.EmailConfirmed = true;
                    await _userHelper.UpdateUserAsync(user);

                    if (!user.IsApproved)
                    {
                        this.SendError("É necessário aprovação do administrador primeiro");

                        return RedirectToRoute("default", new { controller = "Home", action = "Index" });
                    }
                }
            }

            var result = await _userHelper.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);
            if (result.Succeeded)
            {

                return LocalRedirect(returnUrl);
            }
            if (result.IsLockedOut)
            {
                return RedirectToRoute("default", new { controller = "Home", action = "Lockout" });
            }
            else
            {                
                if (info.Principal.HasClaim(c => c.Type == ClaimTypes.Email))
                {
                    var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                    var user = await _userHelper.GetUserByEmailAsync(email);
                    if (user != null)
                    {
                        var wasLoginAdded = await _userHelper.AddLoginAsync(user, info);
                        if (wasLoginAdded.Succeeded)
                        {
                            if (!user.IsApproved)
                            {
                                this.SendError("Aguarde pela aprovação do Administrador");

                                return RedirectToRoute("default", new { controller = "Home", action = "Index" });
                            }

                            var loginSuccess = await _userHelper.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);
                            if (loginSuccess.Succeeded)
                            {
                                return LocalRedirect(returnUrl);
                            }
                        }

                        this.SendError("Associação de login externo falhou...");

                        return RedirectToRoute("default", new { controller = "Home", action = "Index" });
                    }
                }

                var model = new ExternalRegisterViewModel()
                {
                    ReturnURL = returnUrl,
                    LoginProvider = info.LoginProvider
                };

                if (info.Principal.HasClaim(c => c.Type == ClaimTypes.Email))
                {
                    model.UserName = info.Principal.FindFirstValue(ClaimTypes.Email);
                }

                if (info.Principal.HasClaim(c => c.Type == ClaimTypes.GivenName))
                {
                    model.FirstName = info.Principal.FindFirstValue(ClaimTypes.GivenName);
                }

                if (info.Principal.HasClaim(c => c.Type == ClaimTypes.Surname))
                {
                    model.LastName = info.Principal.FindFirstValue(ClaimTypes.Surname);
                }

                return View("RegisterExternal", model);
            }
        }

        [HttpPost]
        public async Task<IActionResult> ExternalLoginConfirmation(ExternalRegisterViewModel model, string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");

            // Get the information about the user from the external login Provider
            var info = await _userHelper.GetExternalLoginInfoAsync();
            if (info == null)
            {
                this.SendError("Erro a carregar autenticação externa.");

                return RedirectToRoute("default", new { controller = "Account", action = "Login", returnUrl = returnUrl });
            }

            if (ModelState.IsValid)
            {
                var user = await this._userHelper.GetUserByEmailAsync(model.UserName);
                if (user == null)
                {
                    user = new User()
                    {
                        Email = model.UserName,
                        UserName = model.UserName,
                        BirthDate = model.BirthDate,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        VATNumber = model.VATNumber,
                        IsApproved = false,
                        EmailConfirmed = true,
                        LastModified = DateTime.Now
                    };

                    var imageUpload = await FileHelper.SaveImage(model.ImageFile, "profile");
                    if (imageUpload.IsSuccess)
                    {
                        user.ImagePath = imageUpload.Result.ToString();
                    }

                    var isUserCreated = await this._userHelper.CreateUserAsync(user, model.Password);
                    if (!isUserCreated.Succeeded)
                    {
                        ModelState.AddModelError(string.Empty, "Unexpected Error");
                        return View(model);
                    }

                    var isLoginAdded = await _userHelper.AddLoginAsync(user, info);
                    if (!isLoginAdded.Succeeded)
                    {
                        this.SendError("Erro ao associar conta externa.");

                        return RedirectToRoute("default", new { controller = "Home", action = "Index" });
                    }

                    //var code = await this._userHelper.GenerateEmailConfirmationTokenAsync(user);
                    //var callbackUrl = Url.Action(
                    //    "ConfirmEmail", "Account",
                    //    new { userId = user.Id, code = code },
                    //    protocol: Request.Scheme);

                    //await _emailSender.SendEmailAsync(user.Email, user.FullName, "Confirme o seu email",
                    //    $"Por favor confirme a sua conta em <a href=\"{callbackUrl}\">Confirmar</a>");

                    return RedirectToRoute("default", new { controller = "Home", action = "Index" });
                }

                ModelState.AddModelError(string.Empty, "Já existe uma conta com os seus dados.");

                return View(model);
            }

            return View("RegisterExternal", model);
        }

        #endregion

        #region /Account/Login
        // GET: /Account/Login
        public async Task<IActionResult> Login(string returnUrl = "")
        {
            //if (this.User.Identity.IsAuthenticated)
            //{
            //    return RedirectToAction("Index", "Home");
            //}

            this.ExpectingError();

            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (await _userHelper.CanSignInAsync(model.Username))
                {
                    if (this.Request.Query.Keys.Contains("ReturnUrl"))
                    {
                        var returnUrl = this.Request.Query["ReturnUrl"].First().ToString();
                        if (returnUrl.ToLower().Contains("referral") == true)
                        {
                            var referralLink = returnUrl.Substring(returnUrl.ToLower().LastIndexOf("referral") + "referral".Length + 1);

                            Guid guidParse;
                            if (!Guid.TryParse(referralLink, out guidParse))
                            {
                                ModelState.AddModelError(string.Empty, "Convite inválido.");

                                return View(model);
                            }

                            var referralExists = await _unitOfWork.Referrals.ExistsReferralAsync(guidParse);
                            if (!referralExists)
                            {
                                ModelState.AddModelError(string.Empty, "Convite inválido.");

                                return View(model);
                            }

                            var result = await this._userHelper.LoginAsync(model);
                            if (result.Succeeded)
                            {
                                return Redirect(this.Request.Query["ReturnUrl"].First());
                            }
                        }
                    }

                    if (await _userHelper.IsAccountApprovedAsync(model.Username))
                    {
                        var result = await this._userHelper.LoginAsync(model);
                        if (result.Succeeded)
                        {
                            if (this.Request.Query.Keys.Contains("ReturnUrl"))
                            {
                                return Redirect(this.Request.Query["ReturnUrl"].First());
                            }

                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Por favor, aguarde por aprovação do administrador ou utilize um código atribuído por outro utilizador.");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Por favor confirme o seu e-mail.");
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Login sem sucesso");
            }

            return View(model);
        }
        #endregion

        #region /Account/Logout
        // GET: /Account/Logout
        public async Task<IActionResult> Logout()
        {
            this.HttpContext.Session.Clear();

            await this._userHelper.LogoutAsync();

            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region /Account/Register
        // GET: /Account/Register
        public async Task<IActionResult> Register()
        {
            return View();
        }

        // POST: /Account/Register
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Register(RegisterNewViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await this._userHelper.GetUserByEmailAsync(model.UserName);
                if (user == null)
                {
                    user = new User()
                    {
                        Email = model.UserName,
                        UserName = model.UserName,
                        BirthDate = model.BirthDate,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        VATNumber = model.VATNumber,
                        IsApproved = false,
                        EmailConfirmed = false,
                        LastModified = DateTime.Now
                    };

                    var imageUpload = await FileHelper.SaveImage(model.ImageFile, "profile");
                    user.ImagePath = imageUpload.Result.ToString();
                    

                    var result = await this._userHelper.CreateUserAsync(user, model.Password);
                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError(string.Empty, "Erro, recuperação sem sucesso.\n" +
                            "Certifique - se que a sua password contém um caractér maiúsculo, um caractér minúsculo, um numérico e um símbolo.");
                        return View(model);
                    }

                    var code = await this._userHelper.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Action(
                        "ConfirmEmail", "Account",
                        new { userId = user.Id, code = code },
                        protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(user.Email, user.FullName, "Confirme o seu email",
                        $"Por favor confirme a sua conta em <a href=\"{callbackUrl}\">Confirmar</a>");

                    return View("PleaseConfirmEmail");
                }

                ModelState.AddModelError(string.Empty, "Já existe uma conta com os seus dados.");

                return View(model);
            }

            return View(model);
        }
        #endregion

        #region /Account/Manage
        // GET: /Account/Manage
        public async Task<IActionResult> Manage()
        {
            var user = await this._userHelper.GetUserByEmailAsync(this.User.Identity.Name);
            if (user == null)
            {
                return BadRequest();
            }

            var model = new ManageViewModel()
            {
                BirthDate = user.BirthDate,
                DisplayName = user.DisplayName,
                FirstName = user.FirstName,
                ImagePath = user.ImagePath,
                LastName = user.LastName,
                UserName = user.UserName,
                VATNumber = user.VATNumber
            };

            this.ExpectingSuccess();

            return View(model);
        }

        // POST: /Account/Manage
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Manage(ManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await this._userHelper.GetUserByEmailAsync(this.User.Identity.Name);
                if (user != null)
                {
                    var checkPassword = await _userHelper.ValidatePasswordAsync(user, model.ConfirmPassword);
                    if (!checkPassword.Succeeded)
                    {
                        ModelState.AddModelError(string.Empty, "Password incorrecta.");

                        return View(model);
                    }

                    user.BirthDate = model.BirthDate;
                    user.DisplayName = model.DisplayName;
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.LastModified = DateTime.Now;
                    user.VATNumber = model.VATNumber;

                    if (model.ImageFile != null)
                    {
                        var imageUpload = await FileHelper.SaveImage(model.ImageFile, "profile");
                        if (imageUpload.IsSuccess)
                        {
                            user.ImagePath = imageUpload.Result.ToString();
                        }
                    }

                    var response = await this._userHelper.UpdateUserAsync(user);
                    if (response.Succeeded)
                    {
                        this.ViewBag.SuccessMessage = "Perfil actualizado.";
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, response.Errors.FirstOrDefault()?.Description);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Utilizador não encontrado.");
                }
            }

            var refreshUser = await this._userHelper.GetUserByEmailAsync(this.User.Identity.Name);
            if (refreshUser == null)
            {
                return BadRequest();
            }

            var resfreshModel = new ManageViewModel()
            {
                BirthDate = refreshUser.BirthDate,
                DisplayName = refreshUser.DisplayName,
                FirstName = refreshUser.FirstName,
                ImagePath = refreshUser.ImagePath,
                LastName = refreshUser.LastName,
                UserName = refreshUser.UserName,
                VATNumber = refreshUser.VATNumber
            };

            return View(resfreshModel);
        }
        #endregion

        #region /Account/ChangePassword
        // GET: /Account/ChangePassword
        public async Task<IActionResult> ChangePassword()
        {
            // TODO
            return View();
        }

        // POST: /Account/ChangePassword
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await this._userHelper.GetUserByEmailAsync(this.User.Identity.Name);
                if (user != null)
                {
                    var result = await this._userHelper.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        this.SendSuccess("Password alterada com sucesso!");

                        return RedirectToAction("Manage");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, result.Errors.FirstOrDefault()?.Description);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Utilizador não encontrado.");
                }
            }

            return View(model);
        }
        #endregion

        #region /Account/CreateToken
        // POST: /Account/CreateToken
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> CreateToken([FromBody] LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await this._userHelper.GetUserByEmailAsync(model.Username);
                if (user != null)
                {
                    var result = await this._userHelper.ValidatePasswordAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        var claims = new[]
                        {
                            new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                        };

                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this._configuration["Tokens:Key"]));
                        var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                        var token = new JwtSecurityToken(
                            this._configuration["Tokens:Issuer"],
                            this._configuration["Tokens:Audience"],
                            claims,
                            expires: DateTime.UtcNow.AddMinutes(30),
                            signingCredentials: credentials);

                        var results = new
                        {
                            token = new JwtSecurityTokenHandler().WriteToken(token),
                            expiration = token.ValidTo
                        };

                        return this.Created(string.Empty, results);
                    }
                }
            }

            return BadRequest();
        }
        #endregion

        #region /Account/ConfirmEmail
        //TODO: Views
        public async Task<IActionResult> ConfirmEmail(string userID, string code)
        {
            if (userID == null || code == null)
            {
                return View("Error");
            }

            var result = await _userHelper.ConfirmEmailAsync(userID, code);
            if (result.Succeeded)
            {
                return View("ConfirmedEmail");
            }

            ViewBag.ErrorMessage = "Invalid confirmation token";

            return View("FailedEmailConfirmation");
        }
        #endregion

        #region /Account/ForgotPassword

        public async Task<IActionResult> ForgotPassword ()
        {
            return View("ForgotPasswordRequest");
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> ForgotPassword (ForgotPasswordViewModel model)
        {
            // TODO: Views
            if (ModelState.IsValid)
            {
                var user = await _userHelper.GetUserByEmailAsync(model.Email);
                if (user == null || !(await _userHelper.IsEmailConfirmedAsync(user)) || !(await _userHelper.IsAccountApprovedAsync(user.UserName)))
                {
                    return View("ForgotPasswordOnSend");
                }

                var code = await _userHelper.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.Action("ResetPassword", "Account",
                    new { UserId = user.Id, code = code }, protocol: Request.Scheme);

                await _emailSender.SendEmailAsync(user.Email, user.FullName, "Recuperar Password",
                    $"Para recuperar a sua palavra-passe, por favor carregue neste link: <a href=\"{callbackUrl}\">Recuperar Password</a>");

                return View("ForgotPasswordOnSend");
            }

            return View("ForgotPasswordRequest", model);
        }

        #endregion

        #region /Account/ResetPassword

        public async Task<IActionResult> ResetPassword(string code)
        {
            return View(
                new ResetPasswordViewModel()
                {
                    Token = code
                }
            );
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userHelper.GetUserByEmailAsync(model.Username);

                var chkPassword = await _userHelper.CheckPasswordResetTokenAsync(user, model.Password, model.Token);
                if (chkPassword.Succeeded)
                {
                    return View("ResetPasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Erro, recuperação sem sucesso.\n" +
                        "Certifique-se que a sua password contém um caractér maiúsculo, um caractér minúsculo, um numérico e um símbolo.");
                }
            }

            return View(model);
        }

        #endregion

        #region /Account/PendingApproval

        [Authorize(Roles = "Superuser,Admin")]
        public async Task<IActionResult> PendingApproval()
        {
            this.ExpectingError();
            this.ExpectingSuccess();

            return View(_userHelper.GetPendingAccounts().OrderBy(u => u.UserName));
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        [Authorize(Roles = "Superuser,Admin")]
        public async Task<IActionResult> Approve(string userID)
        {
            if (string.IsNullOrEmpty(userID))
            {
                this.SendError("Utilizador não encontrado.");

                return RedirectToAction("PendingApproval");
            }

            var user = await _userHelper.GetUserByIdAsync(userID);
            if (user == null)
            {
                this.SendError("Utilizador não encontrado.");

                return RedirectToAction("PendingApproval");
            }

            await _userHelper.SetAccountApprovedAsync(user);

            this.SendSuccess($"Utilizador ({user.UserName}) aprovado.");

            return RedirectToAction("PendingApproval");
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        [Authorize(Roles = "Superuser,Admin")]
        public async Task<IActionResult> Deny(string userID)
        {
            if (string.IsNullOrEmpty(userID))
            {
                this.SendError("Utilizador não encontrado.");

                return RedirectToAction("PendingApproval");
            }

            var user = await _userHelper.GetUserByIdAsync(userID);
            if (user == null)
            {
                this.SendError("Utilizador não encontrado.");

                return RedirectToAction("PendingApproval");
            }

            var userLogins = await _userHelper.GetUserLoginsAsync(user);
            foreach (var login in userLogins)
            {
                var loginIsRemoved = await _userHelper.RemoveLoginAsync(user, login.LoginProvider, login.ProviderKey);
                if (!loginIsRemoved.Succeeded)
                {
                    this.SendError($"AVISO: O login do utilizador {user.UserName} via {login.LoginProvider} não foi removido!");
                }
            }

            var isRemoved = await _userHelper.RemoveUserAsync(user);
            if (isRemoved.Succeeded)
            {
                this.SendSuccess($"Utilizador ({user.UserName}) recusado.");
            }
            else
            {
                this.SendError($"Utilizador ({user.UserName}) recusado sem sucesso!");
            }

            return RedirectToAction("PendingApproval");
        }
        #endregion

        #region /Account/Users
        [Authorize(Roles="Superuser,Admin")]
        public async Task<IActionResult> Users()
        {
            List<UserViewModel> usersVM = new List<UserViewModel>();
            
            foreach (var user in _userHelper.GetAllUsers())
            {
                var role = (await _userHelper.GetRoleAsync(user))?.Result as IdentityRole;

                usersVM.Add(new UserViewModel()
                {
                    FirstName = user.FirstName,
                    ImagePath = user.ImagePath,
                    LastName = user.LastName,
                    Role = new RoleViewModel()
                    {
                        Name = role != null ? role.Name : "N/A",
                        RoleId = role != null ? role.Id : "N/A"
                    },
                    UserID = user.Id,
                    UserName = user.UserName,
                    Email = user.Email
                });
            }

            ViewBag.RoleList = _roleHelper
                    .GetAllRoles()
                    .Where(x => x.Name != "Superuser")
                    .Select(
                        role => new RoleViewModel()
                        {
                            Name = role.Name,
                            RoleId = role.Id
                        })
                    .OrderBy(r => r.Name)
                    .ToList();

            List<string> fixedOrder = new List<string>()
            {
                "Superuser",
                "Admin",
                "User"
            };

            usersVM = usersVM.OrderBy(u =>
                {
                    var index = fixedOrder.IndexOf(u.Role.Name);
                    return index == -1 ? int.MaxValue : index;
                })
                .ThenBy(u => u.FullName)
                .ToList();

            this.ExpectingError();
            this.ExpectingSuccess();

            return View(usersVM);
        }

        [ActionName("User")]
        [Authorize]
        public async Task<IActionResult> UserDetails(string id)
        {
            if (!User.IsInRole("Superuser") && !User.IsInRole("Admin"))
            {
                return RedirectToAction("UserSelfDetails");
            }

            if (string.IsNullOrEmpty(id))
            {
                return BadRequest();
            }

            var user = await _userHelper.GetUserByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            var role = await _userHelper.GetRoleAsync(user);
            if (role.IsSuccess)
            {
                var roleVM = new RoleViewModel()
                {
                    Name = ((IdentityRole)role.Result).Name,
                    RoleId = ((IdentityRole)role.Result).Id
                };

                var userVM = new UserViewModel()
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    ImagePath = user.ImagePath,
                    UserID = user.Id,
                    Role = roleVM,
                    UserName = user.UserName,
                    Email = user.Email
                };

                ViewBag.Title = userVM.FullName;

                return View("User", userVM);
            }

            return BadRequest();
        }

        [ActionName("Me")]
        [Authorize]
        public async Task<IActionResult> UserSelfDetails()
        {
            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            if (user == null)
            {
                return NotFound();
            }

            var role = (await _userHelper.GetRoleAsync(user));
            if (role.IsSuccess)
            {
                var roleVM = new RoleViewModel()
                {
                    Name = ((IdentityRole)role.Result).Name,
                    RoleId = ((IdentityRole)role.Result).Id
                };

                var userVM = new UserViewModel()
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    ImagePath = user.ImagePath,
                    UserID = user.Id,
                    Role = roleVM,
                    UserName = user.UserName,
                    Email = user.Email
                };

                ViewBag.Title = userVM.FullName;

                return View("UserMe", userVM);
            }

            return BadRequest();
        }

        [HttpPost] [AutoValidateAntiforgeryToken]
        [Authorize(Roles = "Superuser,Admin")]
        public async Task<IActionResult> ChangePermission()
        {
            var form = Request.Form;

            var user = await _userHelper.GetUserByEmailAsync(form["Username"]);
            if (user != null)
            {
                var isRoleSet = await _userHelper.SetRoleAsync(user, form["Role"]);
                if (isRoleSet.IsSuccess)
                {
                    this.SendSuccess("Permissão definida com sucesso!");

                    return RedirectToAction("Users");
                }
            }

            this.SendError("Permissão definida sem sucesso...");

            return RedirectToAction("Users");
        }

        #endregion

        #region /Account/AccessDenied

        public IActionResult AccessDenied()
        {
            return View();
        }

        #endregion
    }
}
