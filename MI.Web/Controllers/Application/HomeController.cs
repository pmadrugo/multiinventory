﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MI.Web.Models;
using Microsoft.AspNetCore.Authorization;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers.Interfaces;
using MI.Web.Services.Interfaces;

namespace MI.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserHelper _userHelper;
        private readonly INotifyService _notifyService;
        private readonly IUnitOfWork _unitOfWork;

        public HomeController(IUnitOfWork unitOfWork, IUserHelper userHelper, INotifyService notifyService)
        {
            _userHelper = userHelper;
            this._notifyService = notifyService;
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            this.ExpectingSuccess();
            this.ExpectingError();
            return View();
        }

        [Route("/[Action]")]
        public IActionResult Privacy()
        { 
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        
        public async Task<IActionResult> Referral(Guid? id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account", new { returnUrl = this.Request.Path });
            }

            if (id == null || id == Guid.Empty)
            {
                return BadRequest();
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());

            var referral = await _unitOfWork.Referrals.GetReferralAsync(id.Value);
            if (referral == null)
            {
                if (!user.IsApproved)
                {
                    await _userHelper.LogoutAsync();
                }

                return NotFound();
            }

            if (referral.IsUsed)
            {
                if (!user.IsApproved)
                {
                    await _userHelper.LogoutAsync();
                }

                return View("ReferralUsed");
            }

            if (referral.Expires <= DateTime.Now)
            {
                if (!user.IsApproved)
                {
                    await _userHelper.LogoutAsync();
                }

                return View("ReferralExpired");
            }

            var referralUsed = await _unitOfWork.Referrals.UseReferralAsync(id.Value);
            if (!referralUsed.IsSuccess)
            {
                if (!user.IsApproved)
                {
                    await _userHelper.LogoutAsync();
                }

                return BadRequest();
            }

            var isRoleSet = await _unitOfWork.Inventories.SetUserRoleInInventoryAsync(User.GetUserId(), referral.InventoryId, "Employee");
            if (!isRoleSet.IsSuccess)
            {
                if (!user.IsApproved)
                {
                    await _userHelper.LogoutAsync();
                }

                return BadRequest();
            }
            
            if (!user.IsApproved)
            {
                await _userHelper.SetAccountApprovedAsync(user);
            }

            string inventoryName = "";

            var inventory = await _unitOfWork.Inventories.GetByIdAsync(referral.InventoryId);
            if (inventory != null)
            {
                inventoryName = inventory.Name;
            }

            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi adicionado o utilizador {user.FullName} ao inventario {inventory.Name}");

            return View("ReferralWelcome", inventoryName);
        }
    }
}
