﻿using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers.Interfaces;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Controllers.InventorySection
{
    [InventoryMemberAuthorize]
    public class ItemStatesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly INotifyService _notifyService;
        private readonly IUserHelper _userHelper;

        public ItemStatesController(IUnitOfWork unitOfWork, INotifyService notifyService, IUserHelper userHelper)
        {
            this._unitOfWork = unitOfWork;
            this._notifyService = notifyService;
            this._userHelper = userHelper;
        }


        #region Index

        public async Task<IActionResult> Index([FromRoute] Guid? inventoryId)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            this.ExpectingSuccess();
            this.ExpectingError();

            return View(_unitOfWork.ItemsStates.GetItemStatesFromInventory(inventory.Id));
        }

        #endregion

        #region Create

        public async Task<IActionResult> Create([FromRoute] Guid? inventoryId)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            return View(new ItemState());
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Create([FromRoute] Guid? inventoryId, ItemState model)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            ModelState.Remove("inventoryId");

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.InventoryId = inventory.Id;
            var result = await _unitOfWork.ItemsStates.CreateAsync(model);
            if (!result.IsSuccess)
            {
                ModelState.AddModelError(string.Empty, $"Ocorreu um erro ao criar o tipo de estado {model.State}, tende novamente por favor\n{result.Message}");

                return View(model);
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi criado o tipo de estado {model.State} do inventario {inventory.Name} por {user.FullName}");

            this.SendSuccess($"O tipo de estado {model.State} foi criado com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemStates", action = "Index" });
        }

        #endregion

        #region Edit

        public async Task<IActionResult> Edit([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var itemState = await _unitOfWork.ItemsStates.GetByIdAsync(id.Value);
            if (itemState == null)
            {
                return NotFound();
            }

            return View(itemState);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Edit([FromRoute] Guid? inventoryId, int? id, ItemState model)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            if (id == null)
            {
                return BadRequest();
            }

            if (id.Value != model.Id)
            {
                return BadRequest();
            }

            var stateToEdit = await _unitOfWork.ItemsStates.GetByIdAsync(id.Value);
            if (stateToEdit == null)
            {
                ModelState.AddModelError(string.Empty, "A marca que tentou editar não existe");

                return View(model);
            }

            ModelState.Remove("inventoryId");
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            #endregion

            string originalName = stateToEdit.State;
            stateToEdit.State = model.State;

            var result = await _unitOfWork.ItemsStates.UpdateAsync(stateToEdit);
            if (!result.IsSuccess)
            {
                ModelState.AddModelError(string.Empty, $"Ocorreu um erro ao editar o tipo de estado {model.State}, tende novamente por favor\n{result.Message}");

                return View(model);
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi editado o tipo de estado {model.State} do inventario {inventory.Name} por {user.FullName} (Anteriomente: {originalName})");

            this.SendSuccess($"O tipo de estado {model.State} foi editado com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemStates", action = "Index" });
        }

        #endregion

        #region Enable/Disable

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Enable([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }

            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var stateToEnable = await _unitOfWork.ItemsStates.GetByIdAsync(id.Value);

            if (stateToEnable == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemsStates.EnableAsync(stateToEnable);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar activar o tipo de estado {stateToEnable.State}, tende novamente por favor\n{result.Message}");

                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemStates", action = "Index" });
            }

            

            this.SendSuccess($"O tipo de estado {stateToEnable.State} foi ativada  com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemStates", action = "Index" });
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Disable([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var stateToDisable = await _unitOfWork.ItemsStates.GetByIdAsync(id.Value);

            if (stateToDisable == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemsStates.DisableAsync(stateToDisable);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar eliminar o tipo de estado {stateToDisable.State}, tende novamente por favor\n{result.Message}");

                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemStates", action = "Index" });
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi removido o tipo de estado {stateToDisable.State} do inventario {inventory.Name} por {user.FullName}");

            this.SendSuccess($"O tipo de estado {stateToDisable.State} foi eliminado com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemStates", action = "Index" });
        }

        #endregion

        #region Delete

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Delete([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }

            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var brandToDelete = await _unitOfWork.ItemBrands.GetByIdAsync(id.Value);

            if (brandToDelete == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemBrands.DeleteAsync(brandToDelete);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar eliminar o tipo de estado {brandToDelete.Name}, tende novamente por favor\n{result.Message}");
                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemStates", action = "Index" });
            }

            this.SendSuccess($"O tipo de estado {brandToDelete.Name} foi eliminada com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemStates", action = "Index" });
        }

        #endregion
    }
}
