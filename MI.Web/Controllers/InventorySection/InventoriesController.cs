﻿using MI.Common.JsonModels.Inventory;
using MI.Common.Models;
using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers;
using MI.Web.Helpers.Interfaces;
using MI.Web.Models.Inventories;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Controllers.InventorySection
{
    [Authorize]
    public class InventoriesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserHelper _userHelper;
        private readonly INotifyService _notifyService;
        private readonly IEmailSender _emailSender;

        public InventoriesController(IUnitOfWork unitOfWork, IUserHelper userHelper, INotifyService notifyService, IEmailSender emailSender)
        {
            _userHelper = userHelper;
            this._notifyService = notifyService;
            this._emailSender = emailSender;
            _unitOfWork = unitOfWork;
        }

        #region Index

        public async Task<IActionResult> Index()
        {
            this.ExpectingError();
            this.ExpectingSuccess();

            if (User.IsInRole("Superuser") || User.IsInRole("Admin"))
            {
                return View(new InventoryListViewModel()
                {
                    OwnedInventories = _unitOfWork.Inventories.GetAll()
                });
            }

            var inventories = new InventoryListViewModel()
            {
                InvitedInventories = _unitOfWork.Inventories.GetInventoriesUserIsInvited(User.GetUserId()),
                OwnedInventories = _unitOfWork.Inventories.GetInventoriesOwnedByUser(User.GetUserId())
            };

            return View(inventories);
        }

        public async Task<IActionResult> GetAllInformationFromInventory([FromRoute] Guid? id)
        {
            if (id == null)
            {
                return BadRequest(new Response
                {
                    IsSuccess = false,
                    Message = "Dados fornecidos incorretos"
                });
            }

            var inv = await _unitOfWork.Inventories.GetByUUIDAsync(id.Value.ToString());

            if (inv == null)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (inv.IsDisabled)
            {
                return NotFound(new Response
                {
                    IsSuccess = false,
                    Message = "Inventario não encontrado"
                });
            }

            if (User.IsInRole("User"))
            {
                if (await _unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inv.Id) == null)
                {
                    return Forbid();
                }
            }

            return Ok(
                new Response()
                {
                    IsSuccess = true,
                    Result = new JsonInventoryFull()
                    {
                        Inventory = new JsonInventory()
                        {
                            IsDisabled = inv.IsDisabled,
                            LastModified = inv.LastModified,
                            UUId = inv.UUId,
                            Name = inv.Name
                        },

                        Items = _unitOfWork.Items
                            .GetItemsFromInventory(inv.Id)
                            .Select(item =>
                                new JsonItem
                                {
                                    IsDisabled = item.IsDisabled,
                                    LastModified = item.LastModified,
                                    InventoryUUId = inv.UUId,
                                    BrandUUId = item.Model.Brand.UUId,
                                    ModelUUId = item.Model.UUId,
                                    PlaceUUId = item.Place.UUId,
                                    TypeUUId = item.Type.UUId,
                                    ItemStateUUId = item.ItemState.UUId,
                                    UUId = item.UUId,
                                    Name = item.Name,
                                    Description = item.Description,
                                    ItemInput = item.ItemInput,
                                    ItemOutput = item.ItemOutput
                                }
                            ).ToList(),

                        ItemStates = _unitOfWork.ItemsStates
                            .GetItemStatesFromInventory(inv.Id)
                            .Select(state =>
                                new JsonItemState
                                {
                                    IsDisabled = state.IsDisabled,
                                    LastModified = state.LastModified,
                                    InventoryUUId = inv.UUId,
                                    UUId = state.UUId,
                                    State = state.State
                                }
                            )
                            .ToList(),

                        ItemTypes = _unitOfWork.ItemTypes
                            .GetItemTypesFromInventory(inv.Id)
                            .Select(type =>
                                new JsonItemType
                                {
                                    IsDisabled = type.IsDisabled,
                                    LastModified = type.LastModified,
                                    InventoryUUId = inv.UUId,
                                    UUId = type.UUId,
                                    Name = type.Name
                                }
                            )
                            .ToList(),

                        ItemPlaces = _unitOfWork.ItemPlaces
                            .GetItemPlacesFromInventory(inv.Id)
                            .Select(place =>
                                new JsonItemPlace
                                {
                                    IsDisabled = place.IsDisabled,
                                    LastModified = place.LastModified,
                                    InventoryUUId = inv.UUId,
                                    Name = place.Name,
                                    UUId = place.UUId
                                }
                            )
                            .ToList(),

                        ItemBrands = _unitOfWork.ItemBrands
                            .GetItemBrandsFromInventory(inv.Id)
                            .Select(brand =>
                                new JsonItemBrand
                                {
                                    UUId = brand.UUId,
                                    LastModified = brand.LastModified,
                                    IsDisabled = brand.IsDisabled,
                                    InventoryUUId = inv.UUId,
                                    Name = brand.Name
                                }
                            )
                            .ToList(),

                        ItemModels = _unitOfWork.ItemModels
                            .GetItemModelsFromInventory(inv.Id)
                            .Select(model =>
                                new JsonItemModel
                                {
                                    IsDisabled = model.IsDisabled,
                                    LastModified = model.LastModified,
                                    InventoryUUId = inv.UUId,
                                    BrandUUId = model.Brand.UUId,
                                    UUId = model.UUId,
                                    Name = model.Name
                                }
                            )
                            .ToList(),
                    }
                }
            );
        }

        #endregion

        #region Create

        public async Task<IActionResult> Create()
        {
            ViewData["UserList"] = _userHelper.GetAllUsers();

            return View(new InventoryViewModel());
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Create(InventoryViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewData["UserList"] = _userHelper.GetAllUsers();

                return View(model);
            }

            if (User.IsInRole("Admin") || User.IsInRole("Superuser"))
            {
                if (model.OwnerID == Guid.Empty.ToString())
                {
                    ModelState.AddModelError(string.Empty, "Utilizador inválido.");

                    ViewData["UserList"] = _userHelper.GetAllUsers();

                    return View(model);
                }

                var userExists = await _userHelper.GetUserByIdAsync(model.OwnerID);
                if (userExists == null)
                {
                    ModelState.AddModelError(string.Empty, "Utilizador inválido.");

                    ViewData["UserList"] = _userHelper.GetAllUsers();

                    return View(model);
                }

                model.OwnerID = userExists.Id;
            }
            else
            {
                model.OwnerID = User.GetUserId();
            }

            var image = await FileHelper.SaveImage(model.ImageFile, "inventory");
            model.ImagePath = image.Result.ToString();

            var chkCreate = await _unitOfWork.Inventories.CreateForAsync(model, model.OwnerID);
            if (!chkCreate.IsSuccess)
            {
                ModelState.AddModelError(string.Empty, "Erro inesperado.");

                ViewData["UserList"] = _userHelper.GetAllUsers();

                return View(model);
            }

            this.SendSuccess("Inventário criado com sucesso!");

            return RedirectToRoute(new { controller = "Inventories", action = "Index" });
        }

        #endregion

        #region Edit

        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
                return BadRequest();

            if (id == Guid.Empty)
                return BadRequest();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(id.Value.ToString());
            if (inventory == null)
                return NotFound();

            if (inventory.IsDisabled)
            {
                return NotFound();
            }

            var userRole = await _unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id);
            if (User.IsInRole("Admin") || User.IsInRole("Superuser"))
            {
                var owner = await _unitOfWork.Inventories.GetInventoryOwnerAsync(inventory.Id);

                ViewData["UserList"] = _userHelper.GetAllUsers();

                return View(new InventoryViewModel()
                {
                    Id = inventory.Id,
                    IsDisabled = inventory.IsDisabled,
                    Name = inventory.Name,
                    OwnerID = owner.Id,
                    UUId = inventory.UUId,
                    ImagePath = inventory.ImagePath
                });
            }

            if (userRole.Role == "Owner")
            {
                ViewData["UserList"] = _userHelper.GetAllUsers();

                return View(new InventoryViewModel()
                {
                    Id = inventory.Id,
                    IsDisabled = inventory.IsDisabled,
                    Name = inventory.Name,
                    UUId = inventory.UUId,
                    ImagePath = inventory.ImagePath
                });
            }

            return Forbid();
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Edit([FromRoute] Guid? id, InventoryViewModel model)
        {
            if (id == null)
                return BadRequest();

            if (id == Guid.Empty)
                return BadRequest();

            if (!ModelState.IsValid)
            {
                ViewData["UserList"] = _userHelper.GetAllUsers();

                return View(model);
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(id.Value.ToString());
            if (inventory == null)
                return NotFound();

            if (inventory.IsDisabled)
            {
                return NotFound();
            }

            if (inventory.Id != model.Id)
                return BadRequest();

            var userRole = await _unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id);
            if (userRole != null)
            {
                if (userRole.Role == "Owner")
                {
                    model.OwnerID = User.GetUserId();
                }
            }

            if (User.IsInRole("Admin") || User.IsInRole("Superuser") || (userRole.Role == "Owner"))
            {
                Inventory inv = new Inventory()
                {
                    Id = inventory.Id,
                    IsDisabled = inventory.IsDisabled,
                    Name = model.Name,
                    UUId = inventory.UUId,
                    ImagePath = inventory.ImagePath
                };

                if (model.ImageFile != null)
                {
                    var imageUpload = await FileHelper.SaveImage(model.ImageFile, "inventory");
                    if (imageUpload.IsSuccess)
                    {
                        inv.ImagePath = imageUpload.Result.ToString();
                    }
                }

                await _unitOfWork.Inventories.UpdateAsync(inv);
                await _unitOfWork.Inventories.SetInventoryOwnerAsync(model.OwnerID, inv.Id);

                var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
                await _notifyService.NotifyInventoryUsersByEmailAsync(
                    inventory.Id,
                    $"Multinventory - {inventory.Name}",
                    $"O inventario {inventory.Name} foi modificado por {user.FullName} (Anteriormete: {model.Name})");

                this.SendSuccess("Inventário editado com sucesso!");
                return RedirectToAction("Index");
            }

            return Forbid();
        }

        #endregion

        #region Enable/Disable

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Enable(Guid? id)
        {
            if (id == null)
                return BadRequest();

            if (id == Guid.Empty)
                return BadRequest();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(id.Value.ToString());
            if (inventory == null)
                return NotFound();

            var userRole = await _unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id);
            if (User.IsInRole("Admin") || User.IsInRole("Superuser") || (userRole.Role == "Owner"))
            {
                var chkEnable = await _unitOfWork.Inventories.EnableAsync(inventory);
                if (!chkEnable.IsSuccess)
                {
                    this.SendError("Inventário recuperado sem sucesso!");

                    return RedirectToAction("Index");
                }

                this.SendSuccess("Inventário recuperado com sucesso!");

                return RedirectToAction("Index");
            }

            return View("Index");
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Disable(Guid? id)
        {
            if (id == null)
                return BadRequest();

            if (id == Guid.Empty)
                return BadRequest();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(id.Value.ToString());
            if (inventory == null)
                return NotFound();

            if (inventory.IsDisabled)
                return NotFound();

            var userRole = await _unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id);
            if (User.IsInRole("Admin") || User.IsInRole("Superuser") || (userRole.Role == "Owner"))
            {
                var chkDisabled = await _unitOfWork.Inventories.DisableAsync(inventory);
                if (!chkDisabled.IsSuccess)
                {
                    this.SendError("Inventário apagado sem sucesso!");

                    return RedirectToAction("Index");
                }

                var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
                await _notifyService.NotifyInventoryUsersByEmailAsync(
                    inventory.Id,
                    $"Multinventory - {inventory.Name}",
                    $"O inventario {inventory.Name} foi eliminado por {user.FullName}");

                this.SendSuccess("Inventário apagado com sucesso!");

                return RedirectToAction("Index");
            }

            return View("Index");
        }

        #endregion

        #region Delete

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
                return BadRequest();

            if (id == Guid.Empty)
                return BadRequest();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(id.Value.ToString());
            if (inventory == null)
                return NotFound();

            var userRole = await _unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id);
            if (User.IsInRole("Admin") || User.IsInRole("Superuser") || (userRole.Role == "Owner"))
            {
                var chkDeleted = await _unitOfWork.Inventories.DeleteAsync(inventory);
                if (!chkDeleted.IsSuccess)
                {
                    this.SendError("Inventário eliminado permanentemente sem sucesso!");

                    return RedirectToAction("Index");
                }

                this.SendSuccess("Inventário eliminado permanentemente com sucesso!");

                return RedirectToAction("Index");
            }

            return View("Index");
        }

        #endregion

        #region Users

        public async Task<IActionResult> Users(Guid? id)
        {
            if (id == null)
                return BadRequest();

            if (id == Guid.Empty)
                return BadRequest();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(id.Value.ToString());
            if (inventory == null)
                return NotFound();

            if (inventory.IsDisabled)
                return NotFound();

            this.ExpectingError();
            this.ExpectingSuccess();

            var userRole = await _unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id);
            if (User.IsInRole("Admin") || User.IsInRole("Superuser") || userRole != null)
            {
                var usersInInventory = _unitOfWork.Inventories.GetUsersInInventory(inventory.Id).Select(c =>
                    new InventoryUsersViewModel()
                    {
                        RoleId = c.RoleId,
                        RoleName = c.Role.Role,
                        UserFullName = c.User.FullName,
                        UserId = c.UserId,
                        UserEmail = c.User.Email,
                        UserImagePath = c.User.ImagePath
                    }
                );

                if (userRole?.Role == "Owner" || User.IsInRole("Admin") || User.IsInRole("Superuser") || userRole?.Role == "Admin")
                {
                    ViewData["RolesList"] = _unitOfWork.InventoryRoles.GetUsableRoles();

                    return View("UsersAdminView", usersInInventory);
                }

                return View("UsersReadOnlyView", usersInInventory);
            }

            return Forbid();
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> UserRole(Guid? id)
        {
            if (id == null)
                return BadRequest();

            if (id == Guid.Empty)
                return BadRequest();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(id.Value.ToString());
            if (inventory == null)
                return NotFound();

            if (inventory.IsDisabled)
                return NotFound();

            var userRole = await _unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id);
            if (User.IsInRole("Admin") || User.IsInRole("Superuser") || userRole.Role == "Owner")
            {
                var form = Request.Form;

                var role = await _unitOfWork.InventoryRoles.GetByIdAsync(int.Parse(form["role"]));
                var user = await _userHelper.GetUserByEmailAsync(form["userEmail"]);

                if (role != null && user != null)
                {
                    var roleSet = await _unitOfWork.Inventories.SetUserRoleInInventoryAsync(user.Id, inventory.Id, role.Role);
                    if (roleSet.IsSuccess)
                    {
                        this.SendSuccess("Permissão alterada com sucesso!");

                        await _notifyService.NotifyInventoryUsersByEmailAsync(
                        inventory.Id,
                        $"Multinventory - {inventory.Name}",
                        $"Foi alterado as permissões do utilizador {user.FullName} no inventario {inventory.Name}");

                        return RedirectToAction("Users", new { id = id.Value });
                    }

                    this.SendError("Permissão alterada sem sucesso...");

                    return RedirectToAction("Users", new { id = id.Value });
                }

                return BadRequest();
            }

            return Forbid();
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> RemoveUser(Guid? id)
        {
            if (id == null)
                return BadRequest();

            if (id == Guid.Empty)
                return BadRequest();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(id.Value.ToString());
            if (inventory == null)
                return NotFound();

            if (inventory.IsDisabled)
                return NotFound();

            var userRole = await _unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id);
            var form = Request.Form;
            var user = await _userHelper.GetUserByEmailAsync(form["userEmail"]);

            if (User.GetUserId() == user.Id && userRole.Role == "Owner")
            {
                this.SendError("Sendo o dono(a) do inventario, não pode sair, apenas apagar o inventario");

                return RedirectToAction("Users", new { id = id.Value });
            }

            if (User.IsInRole("Admin") || User.IsInRole("Superuser") || userRole.Role == "Owner" || User.GetUserId() == user.Id)
            {

                if (user != null)
                {
                    if (await _unitOfWork.NotifiableUsers.IsUserNotifiedOfInventory(inventory.Id, user.Id))
                    {
                        var removeNotification = await _unitOfWork.NotifiableUsers.RemoveUserNotifyOfInventory(inventory.Id, user.Id);
                        if (!removeNotification.IsSuccess)
                        {
                            this.SendError("Ocorreu um erro ao remover o utilizador, tente de novo...");

                            return RedirectToAction("Users", new { id = id.Value });
                        }
                    }

                    var removeResult = await _unitOfWork.Inventories.RemoveUserFromInventory(user.Id, inventory.Id);
                    if (removeResult.IsSuccess)
                    {
                        this.SendSuccess("Utilizador removido com sucesso!");

                        await _emailSender.SendEmailAsync(
                            user.UserName,
                            user.FullName,
                            $"Multinventory - {inventory.Name}",
                            $"Pedimos deculpa, mas devido a razões, o Sr(A). {user.FullName} foi removida do nosso inventario {inventory.Name}");

                        await _notifyService.NotifyInventoryUsersByEmailAsync(
                        inventory.Id,
                        $"Multinventory - {inventory.Name}",
                        $"Foi removido o utilizador {user.FullName} do inventario {inventory.Name}");

                        if (User.GetUserId() != user.Id)
                        {
                            return RedirectToAction("Users", new { id = id.Value });
                        }

                        return RedirectToAction("Index");
                    }

                    this.SendError("Utilizador removido sem sucesso...");

                    return RedirectToAction("Users", new { id = id.Value });
                }

                return BadRequest();
            }

            return Forbid();
        }

        #endregion

        #region UserNotifySystem

        public async Task<IActionResult> AddUserNotifyInventory(Guid? id)
        {
            if (id == null)
                return BadRequest();

            if (id == Guid.Empty)
                return BadRequest();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(id.Value.ToString());
            if (inventory == null)
                return NotFound();

            if (inventory.IsDisabled)
                return NotFound();

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            if (user == null)
                return BadRequest();

            if (await _unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
                return Forbid();

            var result = await _unitOfWork.NotifiableUsers.AddUserNotifyOfInventory(inventory.Id, user.Id);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar adicionar notificações ao utilizador {user.Email} no inventario {inventory.Name}");
                return RedirectToAction("Index");
            }

            this.SendSuccess($"O utilizador {user.Email} irá receber notificações sobre o inventario {inventory.Name}");
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> RemoveUserNotifyInventory(Guid? id)
        {
            if (id == null)
                return BadRequest();

            if (id == Guid.Empty)
                return BadRequest();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(id.Value.ToString());
            if (inventory == null)
                return NotFound();

            if (inventory.IsDisabled)
                return NotFound();

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            if (user == null)
                return BadRequest();

            if (await _unitOfWork.Inventories.GetUserRoleInInventoryAsync(User.GetUserId(), inventory.Id) == null)
                return Forbid();

            var result = await _unitOfWork.NotifiableUsers.RemoveUserNotifyOfInventory(inventory.Id, user.Id);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar remover as notificações ao utilizador {user.Email} no inventario {inventory.Name}");
                return RedirectToAction("Index");
            }

            this.SendSuccess($"O utilizador {user.Email} irá parar de receber notificações sobre o inventario {inventory.Name}");
            return RedirectToAction("Index");
        }

        #endregion

    }
}
