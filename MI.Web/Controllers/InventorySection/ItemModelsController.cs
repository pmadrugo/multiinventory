﻿using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers.Interfaces;
using MI.Web.Models.Inventories;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Controllers.InventorySection
{
    [InventoryMemberAuthorize]
    public class ItemModelsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly INotifyService _notifyService;
        private readonly IUserHelper _userHelper;

        public ItemModelsController(IUnitOfWork unitOfWork, INotifyService notifyService, IUserHelper userHelper)
        {
            this._unitOfWork = unitOfWork;
            this._notifyService = notifyService;
            this._userHelper = userHelper;
        }

        #region Index

        public async Task<IActionResult> Index([FromRoute] Guid? inventoryId)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            this.ExpectingSuccess();
            this.ExpectingError();

            return View(_unitOfWork.ItemModels.GetItemModelsFromInventory(inventory.Id).OrderBy(b => b.Brand.Name).ThenBy(m => m.Name));
        }

        #endregion

        #region Create

        public async Task<IActionResult> Create([FromRoute] Guid? inventoryId)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            ViewData["Brands"] = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(inventory.Id).Where(ib => !ib.IsDisabled);

            return View(new ItemModel());
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Create([FromRoute] Guid? inventoryId, ItemModel model)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion            

            ModelState.Remove("inventoryId");

            if (!ModelState.IsValid)
            {
                ViewData["Brands"] = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(inventory.Id);

                return View(model);
            }

            model.InventoryId = inventory.Id;

            var result = await _unitOfWork.ItemModels.CreateAsync(model);
            if (!result.IsSuccess)
            {
                ViewData["Brands"] = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(inventory.Id);

                ModelState.AddModelError(string.Empty, $"Ocorreu um erro ao criar o modelo {model.Name}, tende novamente por favor\n{result.Message}");

                return View(model);
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi adicionado o modelo {model.Name} ao inventario {inventory.Name} por {user.FullName}");

            this.SendSuccess($"O modelo {model.Name} foi criada com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemModels", action = "Index" });
        }

        #endregion

        #region Edit

        public async Task<IActionResult> Edit([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var itemModel = await _unitOfWork.ItemModels.GetByIdAsync(id.Value);
            if (itemModel == null)
            {
                return NotFound();
            }

            ViewData["Brands"] = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(inventory.Id).Where(ib => !ib.IsDisabled);

            return View(itemModel);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Edit([FromRoute] Guid? inventoryId, int? id, ItemModel model)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            if (id == null)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();

            if (id.Value != model.Id)
            {
                return BadRequest();
            }

            #endregion

            ModelState.Remove("inventoryId");
            if (!ModelState.IsValid)
            {
                ViewData["Brands"] = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(inventory.Id);

                return View(model);
            }

            var itemModelToEdit = await _unitOfWork.ItemModels.GetByIdAsync(model.Id);
            if (itemModelToEdit == null)
            {
                ViewData["Brands"] = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(inventory.Id);

                ModelState.AddModelError("", "O modelo que tentou editar não existe");

                return View(model);
            }

            string originalName = itemModelToEdit.Name;
            itemModelToEdit.BrandId = model.BrandId;
            itemModelToEdit.Name = model.Name;

            var result = await _unitOfWork.ItemModels.UpdateAsync(itemModelToEdit);
            if (!result.IsSuccess)
            {
                ModelState.AddModelError(string.Empty, $"Ocorreu um erro ao editar a Marca {model.Name}, tende novamente por favor\n{result.Message}");

                return View(model);
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi editado o modelo {model.Name} do inventario {inventory.Name} por {user.FullName} (Anteriormente {originalName})");

            this.SendSuccess($"A Marca {model.Name} foi editada com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemModels", action = "Index" });
        }

        #endregion

        #region Enable/Disable
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Enable([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var modelToEnable = await _unitOfWork.ItemModels.GetByIdAsync(id.Value);

            if (modelToEnable == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemModels.EnableAsync(modelToEnable);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar ativar o Modelo {modelToEnable.Name}, tende novamente por favor\n{result.Message}");
                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemModels", action = "Index" });
            }

            this.SendSuccess($"O modelo {modelToEnable.Name} foi ativada  com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemModels", action = "Index" });
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Disable([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var modelToDisable = await _unitOfWork.ItemModels.GetByIdAsync(id.Value);

            if (modelToDisable == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemModels.DisableAsync(modelToDisable);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar eliminar o Modelo {modelToDisable.Name}, tende novamente por favor\n{result.Message}");
                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemModels", action = "Index" });
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi removida o modelo {modelToDisable.Name} ao inventario {inventory.Name} por {user.FullName}");

            this.SendSuccess($"O Modelo {modelToDisable.Name} foi eliminado com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemModels", action = "Index" });
        }

        #endregion

        #region Delete

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Delete([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }

            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var modelToDelete = await _unitOfWork.ItemModels.GetByIdAsync(id.Value);

            if (modelToDelete == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemModels.DeleteAsync(modelToDelete);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar eliminar a Marca {modelToDelete.Name}, tende novamente por favor\n{result.Message}");
                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemBrands", action = "Index" });
            }

            this.SendSuccess($"A Marca {modelToDelete.Name} foi eliminada com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemBrands", action = "Index" });
        }

        #endregion
    }
}
