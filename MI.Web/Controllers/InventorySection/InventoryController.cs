﻿using MI.Common.JsonModels.Inventory;
using MI.Common.Models;
using MI.Web.Data.Entities.ApplicationSection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Models.Inventories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Controllers.InventorySection
{
    [InventoryMemberAuthorize]
    public class InventoryController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public InventoryController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IActionResult> Index([FromRoute] Guid? inventoryId)
        {
            if (inventoryId == null)
                return BadRequest();

            if (inventoryId.Value == Guid.Empty)
                return BadRequest();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
                return NotFound();

            if (inventory.IsDisabled)
                return NotFound();

            var lastItemsAdded = _unitOfWork.Items.GetItemsFromInventory(inventory.Id)
                .Where(item => !item.IsDisabled)
                .OrderByDescending(item => item.ItemInput)
                .Take(10);

            var lastItemsEdited = _unitOfWork.Items.GetItemsFromInventory(inventory.Id)
                .Where(item => !item.IsDisabled)
                .OrderByDescending(item => item.LastModified)
                .Take(10);

            var inventoryDashboard = new InventoryDashboardViewModel()
            {
                ItemCount = _unitOfWork.Items.GetItemsFromInventory(inventory.Id).Count(),
                LastTenItemsAdded = lastItemsAdded,
                LastTenItemsModified = lastItemsEdited,
                Inventory = inventory
            };

            return View(inventoryDashboard);
        }

        [HttpPost]
        public async Task<IActionResult> GenerateReferral([FromRoute] Guid? inventoryId)
        {
            if (inventoryId == null)
                return BadRequest();

            if (inventoryId.Value == Guid.Empty)
                return BadRequest();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
                return NotFound();

            if (inventory.IsDisabled)
                return NotFound();

            var referralResult = await _unitOfWork.Referrals.GenerateAsync(inventory.Id);
            if (referralResult.IsSuccess)
            {
                var guid = ((InventoryReferral)referralResult.Result).ReferralLink;

                return Ok(guid);
            }

            return NotFound();
        }

        [ResponseCache(NoStore = true)]
        public async Task<IActionResult> ExportToExcel([FromRoute] Guid? inventoryId)
        {
            if (inventoryId == null)
                return BadRequest();

            if (inventoryId.Value == Guid.Empty)
                return BadRequest();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
                return NotFound();

            if (inventory.IsDisabled)
                return NotFound();

            var inventoryItems = _unitOfWork.Items.GetActiveItemsFromInventory(inventory.Id);

            byte[] fileContents;
            
            using (ExcelPackage excel = new ExcelPackage())
            {
                var workSheet = excel.Workbook.Worksheets.Add("Items");

                // Header Setup
                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;

                workSheet.Cells[1, 1].Value = "Imagem";
                workSheet.Cells[1, 2].Value = "Nome";
                workSheet.Cells[1, 3].Value = "Categoria";
                workSheet.Cells[1, 4].Value = "Localização";
                workSheet.Cells[1, 5].Value = "Estado";
                workSheet.Cells[1, 6].Value = "Marca";
                workSheet.Cells[1, 7].Value = "Modelo";

                // Rows Setup
                int currentRow = 2;
                foreach (var items in inventoryItems)
                {
                    string imagePath = items.ImagePath.StartsWith("~/") ? $"wwwroot/{items.ImagePath.Substring(2)}" : $"wwwroot/{items.ImagePath}";
                    imagePath = Path.GetFullPath(imagePath);

                    workSheet.Column(1).Width = 11.5;
                    workSheet.Row(currentRow).Height = 60;

                    try
                    {
                        using (Image image = Image.FromFile(imagePath, true))
                        {
                            ExcelPicture excelImage = null;
                            if (image != null)
                            {
                                excelImage = workSheet.Drawings.AddPicture(items.Name, image);

                                excelImage.From.Row = currentRow - 1;
                                excelImage.From.Column = 0;

                                excelImage.To.Row = currentRow - 1;
                                excelImage.To.Column = 0;

                                excelImage.SetSize(80, 80);

                                excelImage.AdjustPositionAndSize();
                            }
                        } 
                    }
                    catch (Exception ex)
                    {

                    }

                    workSheet.Cells[currentRow, 2].Value = items.Name;
                    workSheet.Cells[currentRow, 3].Value = items.Type.Name;
                    workSheet.Cells[currentRow, 4].Value = items.Place.Name;
                    workSheet.Cells[currentRow, 5].Value = items.ItemState.State;
                    workSheet.Cells[currentRow, 6].Value = items.Model.Brand.Name;
                    workSheet.Cells[currentRow, 7].Value = items.Model.Name;

                    workSheet.Row(currentRow).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Row(currentRow).Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;

                    currentRow += 1;
                }

                for (int i = 1; i < 7; i++)
                {
                    workSheet.Column(i + 1).AutoFit();
                }

                fileContents = excel.GetAsByteArray();
            
                if (fileContents == null || fileContents.Length == 0)
                {
                    return new NotFoundResult();
                }

                string fileName = $"Inventário-{inventory.Name}-{DateTime.Now.ToShortDateString()}.xlsx";

                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: fileName);
            }
        }

        [ResponseCache(NoStore = true)]
        public async Task<IActionResult> ExportToExcelNonActive([FromRoute] Guid? inventoryId)
        {
            if (inventoryId == null)
                return BadRequest();

            if (inventoryId.Value == Guid.Empty)
                return BadRequest();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
                return NotFound();

            if (inventory.IsDisabled)
                return NotFound();

            var inventoryItems = _unitOfWork.Items.GetNonActiveItemsFromInventory(inventory.Id);

            byte[] fileContents;

            using (ExcelPackage excel = new ExcelPackage())
            {
                var workSheet = excel.Workbook.Worksheets.Add("Items");

                // Header Setup
                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;

                workSheet.Cells[1, 1].Value = "Imagem";
                workSheet.Cells[1, 2].Value = "Nome";
                workSheet.Cells[1, 3].Value = "Categoria";
                workSheet.Cells[1, 4].Value = "Localização";
                workSheet.Cells[1, 5].Value = "Estado";
                workSheet.Cells[1, 6].Value = "Marca";
                workSheet.Cells[1, 7].Value = "Modelo";

                // Rows Setup
                int currentRow = 2;
                foreach (var items in inventoryItems)
                {
                    string imagePath = items.ImagePath.StartsWith("~/") ? $"wwwroot/{items.ImagePath.Substring(2)}" : $"wwwroot/{items.ImagePath}";
                    imagePath = Path.GetFullPath(imagePath);

                    workSheet.Column(1).Width = 11.5;
                    workSheet.Row(currentRow).Height = 60;

                    try
                    {
                        using (Image image = Image.FromFile(imagePath, true))
                        {
                            ExcelPicture excelImage = null;
                            if (image != null)
                            {
                                excelImage = workSheet.Drawings.AddPicture(items.Name, image);

                                excelImage.From.Row = currentRow - 1;
                                excelImage.From.Column = 0;

                                excelImage.To.Row = currentRow - 1;
                                excelImage.To.Column = 0;

                                excelImage.SetSize(80, 80);

                                excelImage.AdjustPositionAndSize();
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                    workSheet.Cells[currentRow, 2].Value = items.Name;
                    workSheet.Cells[currentRow, 3].Value = items.Type.Name;
                    workSheet.Cells[currentRow, 4].Value = items.Place.Name;
                    workSheet.Cells[currentRow, 5].Value = items.ItemState.State;
                    workSheet.Cells[currentRow, 6].Value = items.Model.Brand.Name;
                    workSheet.Cells[currentRow, 7].Value = items.Model.Name;

                    workSheet.Row(currentRow).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Row(currentRow).Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;

                    currentRow += 1;
                }

                for (int i = 1; i < 7; i++)
                {
                    workSheet.Column(i + 1).AutoFit();
                }

                fileContents = excel.GetAsByteArray();

                if (fileContents == null || fileContents.Length == 0)
                {
                    return new NotFoundResult();
                }

                string fileName = $"Inventário-{inventory.Name}-{DateTime.Now.ToShortDateString()}.xlsx";

                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: fileName);
            }
        }

    }
}
