﻿using MI.Common.Models;
using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers;
using MI.Web.Helpers.Interfaces;
using MI.Web.Models.Inventories;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Controllers.InventorySection
{
    [InventoryMemberAuthorize]
    public class ItemsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly INotifyService _notifyService;
        private readonly IUserHelper _userHelper;

        public ItemsController(IUnitOfWork unitOfWork, INotifyService notifyService, IUserHelper userHelper)
        {
            this._unitOfWork = unitOfWork;
            _notifyService = notifyService;
            this._userHelper = userHelper;
        }

        #region Methods
        private Item ToItem(ItemViewModel model, Response imageResponse)
        {
            return new Item
            {
                Name = model.Name,
                Description = model.Description,
                ImagePath = imageResponse.Result.ToString(),
                PlaceId = model.PlaceId,
                ItemStateId = model.ItemStateId,
                ModelId = model.ModelId,
                TypeId = model.TypeId,
            };
        }

        private ItemViewModel ToItemViewModel(Item item)
        {
            return new ItemViewModel
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                ImagePath = item.ImagePath,
                PlaceId = item.PlaceId,
                ItemStateId = item.ItemStateId,
                ModelId = item.ModelId,
                TypeId = item.TypeId,
                BrandId = item.Model.BrandId
            };
        }
        #endregion

        #region Index

        public async Task<IActionResult> Index([FromRoute] Guid? inventoryId)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            this.ExpectingSuccess();
            this.ExpectingError();

            return View(_unitOfWork.Items.GetActiveItemsFromInventory(inventory.Id).OrderBy(i => i.Name));
        }

        #endregion

        #region Create

        public async Task<IActionResult> Create([FromRoute] Guid? inventoryId)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            ViewData["Places"] = _unitOfWork.ItemPlaces.GetItemPlacesFromInventory(inventory.Id);
            ViewData["States"] = _unitOfWork.ItemsStates.GetItemStatesFromInventory(inventory.Id);
            ViewData["Brands"] = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(inventory.Id);
            ViewData["Types"] = _unitOfWork.ItemTypes.GetItemTypesFromInventory(inventory.Id);

            return View(new ItemViewModel());
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Create([FromRoute] Guid? inventoryId, ItemViewModel model)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            ModelState.Remove("inventoryId");

            if (!ModelState.IsValid)
            {
                ViewData["Places"] = _unitOfWork.ItemPlaces.GetItemPlacesFromInventory(inventory.Id);
                ViewData["States"] = _unitOfWork.ItemsStates.GetItemStatesFromInventory(inventory.Id);
                ViewData["Brands"] = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(inventory.Id);
                ViewData["Types"] = _unitOfWork.ItemTypes.GetItemTypesFromInventory(inventory.Id);

                return View(model);
            }

            var newItem = this.ToItem(model, await FileHelper.SaveImage(model.ImageFile,"items"));
            newItem.InventoryId = inventory.Id;
            newItem.ItemInput = DateTime.Now;

            var result = await _unitOfWork.Items.CreateAsync(newItem);
            if (!result.IsSuccess)
            {
                ViewData["Places"] = _unitOfWork.ItemPlaces.GetItemPlacesFromInventory(inventory.Id);
                ViewData["States"] = _unitOfWork.ItemsStates.GetItemStatesFromInventory(inventory.Id);
                ViewData["Brands"] = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(inventory.Id);
                ViewData["Types"] = _unitOfWork.ItemTypes.GetItemTypesFromInventory(inventory.Id);

                ModelState.AddModelError(string.Empty, $"Ocorreu um erro ao criar o item {model.Name}, tende novamente por favor\n{result.Message}");

                return View(model);
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id, 
                $"Multinventory - {inventory.Name}", 
                $"Foi adicionado um item '{newItem.Name}' ao inventario '{inventory.Name}' por {user.FullName}");                               

            this.SendSuccess($"O item {model.Name} foi criado com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "Items", action = "Index" });
        }

        

        #endregion

        #region Edit

        public async Task<IActionResult> Edit([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var item = await _unitOfWork.Items.GetByIdWithBrandAsync(id.Value);
            if (item == null)
            {
                return NotFound();
            }

            ViewData["Places"] = _unitOfWork.ItemPlaces.GetItemPlacesFromInventory(inventory.Id);
            ViewData["States"] = _unitOfWork.ItemsStates.GetItemStatesFromInventory(inventory.Id);
            ViewData["Brands"] = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(inventory.Id);
            ViewData["Types"] = _unitOfWork.ItemTypes.GetItemTypesFromInventory(inventory.Id);

            return View(this.ToItemViewModel(item));
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Edit([FromRoute] Guid? inventoryId, int? id, ItemViewModel model)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            if (id == null)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            if (id.Value != model.Id)
            {
                return BadRequest();
            }

            #endregion


            ModelState.Remove("inventoryId");
            if (!ModelState.IsValid)
            {
                ViewData["Places"] = _unitOfWork.ItemPlaces.GetItemPlacesFromInventory(inventory.Id);
                ViewData["States"] = _unitOfWork.ItemsStates.GetItemStatesFromInventory(inventory.Id);
                ViewData["Brands"] = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(inventory.Id);
                ViewData["Types"] = _unitOfWork.ItemTypes.GetItemTypesFromInventory(inventory.Id);

                return View(model);
            }

            var item = await _unitOfWork.Items.GetByIdWithBrandAsync(model.Id);
            if (item == null)
            {
                ViewData["Places"] = _unitOfWork.ItemPlaces.GetItemPlacesFromInventory(inventory.Id);
                ViewData["States"] = _unitOfWork.ItemsStates.GetItemStatesFromInventory(inventory.Id);
                ViewData["Brands"] = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(inventory.Id);
                ViewData["Types"] = _unitOfWork.ItemTypes.GetItemTypesFromInventory(inventory.Id);

                ModelState.AddModelError(string.Empty, "O item que tentou editar não existe.");

                return View(model);
            }

            var imagePath = await FileHelper.SaveImage(model.ImageFile,"items", item.ImagePath);

            string originalName = item.Name;
            item.Name = model.Name;
            item.Description = model.Description;
            item.TypeId = model.TypeId;
            item.PlaceId = model.PlaceId;
            item.ItemStateId = model.ItemStateId;
            item.ModelId = model.ModelId;
            item.ImagePath = imagePath.Result.ToString();

            var result = await _unitOfWork.Items.UpdateAsync(item);
            if (!result.IsSuccess)
            {
                ModelState.AddModelError(string.Empty, $"Ocorreu um erro ao editar o item {model.Name}, tente novamente por favor\n{result.Message}");

                return View(model);
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",    
                $"Foi editado o item {item.Name} ao inventario {inventory.Name} por {user.FullName} (Anteriomente {originalName})");


            this.SendSuccess($"O item {model.Name} foi editada com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "Items", action = "Index" });
        }

        #endregion

        #region Enable/Disable

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Enable([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }

            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var itemToEnable = await _unitOfWork.Items.GetByIdAsync(id.Value);

            if (itemToEnable == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.Items.EnableAsync(itemToEnable);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar ativar o Item {itemToEnable.Name}, tende novamente por favor\n{result.Message}");

                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "Items", action = "Index" });
            }

            this.SendSuccess($"O Item {itemToEnable.Name} foi ativada  com sucesso");

            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "Items", action = "Index" });
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Disable([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var itemToDisable = await _unitOfWork.Items.GetByIdAsync(id.Value);

            if (itemToDisable == null)
            {
                return NotFound();
            }

            itemToDisable.ItemOutput = DateTime.Now;
            var result = await _unitOfWork.Items.DisableAsync(itemToDisable);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar eliminar o item {itemToDisable.Name}, tende novamente por favor\n{result.Message}");

                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "Items", action = "Index" });
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi removido o item {itemToDisable.Name} do inventario {inventory.Name} por {user.FullName}");

            this.SendSuccess($"O item {itemToDisable.Name} foi eliminado com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "Items", action = "Index" });
        }

        #endregion

        #region Delete

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Delete([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }

            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var itemToDelete = await _unitOfWork.Items.GetByIdAsync(id.Value);

            if (itemToDelete == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.Items.DeleteAsync(itemToDelete);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar eliminar a Marca {itemToDelete.Name}, tende novamente por favor\n{result.Message}");
                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "Items", action = "Index" });
            }

            this.SendSuccess($"A Marca {itemToDelete.Name} foi eliminada com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "Items", action = "Index" });
        }

        #endregion

        #region NonActiveItems

        public async Task<IActionResult> NonActiveItems([FromRoute] Guid? inventoryId)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            this.ExpectingSuccess();
            this.ExpectingError();

            return View(_unitOfWork.Items.GetNonActiveItemsFromInventory(inventory.Id));
        }

        [HttpPost]
        public async Task<IActionResult> SetAsNonActive([FromRoute] Guid? inventoryId, int id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            var form = Request.Form;

            DateTime outputDate;
            if (!DateTime.TryParse(form["outputDate"], out outputDate))
            {
                outputDate = DateTime.Now;
            }

            int itemStateId;
            if (!int.TryParse(form["stateId"], out itemStateId) || itemStateId == 0)
            {
                var isItemOut = await _unitOfWork.Items.SetItemOutputAsync(id, outputDate);
                if (isItemOut.IsSuccess)
                {
                    this.SendSuccess("Saída dada como sucesso!");
                }
                else
                {
                    this.SendError("Erro inesperado. Tente novemente.");
                }
            }
            else
            {
                var isItemOut = await _unitOfWork.Items.SetItemOutputAsync(id, outputDate, itemStateId);
                if (isItemOut.IsSuccess)
                {
                    this.SendSuccess("Saída dada como sucesso!");
                }
                else
                {
                    this.SendError("Erro inesperado. Tente novemente.");
                }
            }

            var item = await _unitOfWork.Items.GetByIdAsync(id);
            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi dada a saida do item {item.Name} por {user.FullName}");

            return RedirectToRoute("inventoryHome", new { controller = "Items", action = "Index", inventoryId = inventoryId });
        }

        [HttpPost]
        public async Task<IActionResult> GetItemStatesAJAX([FromRoute] Guid? inventoryId)
        {

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            return Ok(_unitOfWork.ItemsStates.GetItemStatesFromInventory(inventory.Id));
        }

        #endregion

        #region Details

        [HttpPost]
        public async Task<IActionResult> GetItemDetailsAJAX([FromRoute] Guid? inventoryId, int id)
        {

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            var currentItem = await _unitOfWork.Items.GetByIdAsync(id);

            return Ok(currentItem.Description);
        }

        #endregion

    }
}
