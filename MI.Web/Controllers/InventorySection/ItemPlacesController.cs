﻿using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers.Interfaces;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Controllers.InventorySection
{
    [InventoryMemberAuthorize]
    public class ItemPlacesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly INotifyService _notifyService;
        private readonly IUserHelper _userHelper;

        public ItemPlacesController(IUnitOfWork unitOfWork, INotifyService notifyService, IUserHelper userHelper)
        {
            this._unitOfWork = unitOfWork;
            this._notifyService = notifyService;
            this._userHelper = userHelper;
        }

        #region Index

        public async Task<IActionResult> Index([FromRoute] Guid? inventoryId)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            this.ExpectingSuccess();
            this.ExpectingError();

            return View(_unitOfWork.ItemPlaces.GetItemPlacesFromInventory(inventory.Id));
        }

        #endregion

        #region Create

        public async Task<IActionResult> Create([FromRoute] Guid? inventoryId)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            return View(new ItemPlace());
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Create([FromRoute] Guid? inventoryId, ItemPlace model)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            ModelState.Remove("inventoryId");
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.InventoryId = inventory.Id;

            var result = await _unitOfWork.ItemPlaces.CreateAsync(model);
            if (!result.IsSuccess)
            {
                ModelState.AddModelError(string.Empty, $"Ocorreu um erro ao criar o lugar {model.Name}, tende novamente por favor\n{result.Message}");

                return View(model);
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi adicionado a localização {model.Name} ao inventario {inventory.Name} por {user.FullName}");

            this.SendSuccess($"O lugar {model.Name} foi criada com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemPlaces", action = "Index" });
        }

        #endregion

        #region Edit

        public async Task<IActionResult> Edit([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var itemPlace = await _unitOfWork.ItemPlaces.GetByIdAsync(id.Value);
            if (itemPlace == null)
            {
                return NotFound();
            }

            return View(itemPlace);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Edit([FromRoute] Guid? inventoryId, int? id, ItemBrand model)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            if (id == null)
            {
                return BadRequest();
            }

            if (id.Value != model.Id)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();


            #endregion

            ModelState.Remove("inventoryId");
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var placeToEdit = await _unitOfWork.ItemPlaces.GetByIdAsync(model.Id);
            if (placeToEdit == null)
            {
                ModelState.AddModelError(string.Empty, "A marca que tentou editar não existe");

                return View(model);
            }

            string originalName = placeToEdit.Name;
            placeToEdit.Name = model.Name;

            var result = await _unitOfWork.ItemPlaces.UpdateAsync(placeToEdit);
            if (!result.IsSuccess)
            {
                ModelState.AddModelError(string.Empty, $"Ocorreu um erro ao editar o Lugar {model.Name}, tende novamente por favor\n{result.Message}");

                return View(model);
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi editada a localização {model.Name} do inventario {inventory.Name} por {user.FullName} (Anteriomente {originalName})");

            this.SendSuccess($"O Lugar {model.Name} foi editada com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemPlaces", action = "Index" });
        }

        #endregion

        #region Enable/Disable

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Enable([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }

            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var placeToEnable = await _unitOfWork.ItemBrands.GetByIdAsync(id.Value);

            if (placeToEnable == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemBrands.EnableAsync(placeToEnable);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar activar a localização {placeToEnable.Name}, tende novamente por favor\n{result.Message}");

                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemPlaces", action = "Index" });
            }

            this.SendSuccess($"O Lugar {placeToEnable.Name} foi ativada  com sucesso");

            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemPlaces", action = "Index" });
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Disable([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var placeToDisable = await _unitOfWork.ItemPlaces.GetByIdAsync(id.Value);

            if (placeToDisable == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemPlaces.DisableAsync(placeToDisable);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar eliminar a localização {placeToDisable.Name}, tende novamente por favor\n{result.Message}");

                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemPlaces", action = "Index" });
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi removida a localização {placeToDisable.Name} ao inventario {inventory.Name} por {user.FullName}");

            this.SendSuccess($"A localização {placeToDisable.Name} foi eliminado com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemPlaces", action = "Index" });
        }

        #endregion

        #region Delete

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Delete([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }

            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var placeToDelete = await _unitOfWork.ItemBrands.GetByIdAsync(id.Value);

            if (placeToDelete == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemBrands.DeleteAsync(placeToDelete);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar eliminar a Marca {placeToDelete.Name}, tende novamente por favor\n{result.Message}");
                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemPlaces", action = "Index" });
            }

            this.SendSuccess($"A Marca {placeToDelete.Name} foi eliminada com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemPlaces", action = "Index" });
        }

        #endregion
    }
}
