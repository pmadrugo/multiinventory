﻿ using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers.Interfaces;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Controllers.InventorySection
{
    [InventoryMemberAuthorize]
    public class ItemTypesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly INotifyService _notifyService;
        private readonly IUserHelper _userHelper;

        public ItemTypesController(IUnitOfWork unitOfWork, INotifyService notifyService, IUserHelper userHelper)
        {
            this._unitOfWork = unitOfWork;
            this._notifyService = notifyService;
            this._userHelper = userHelper;
        }

        #region Index
        public async Task<IActionResult> Index([FromRoute] Guid? inventoryId)
        {
            if (inventoryId == null)
            {
                return BadRequest();
            }

            this.ExpectingSuccess();
            this.ExpectingError();

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
                return NotFound();

            if (inventory.IsDisabled)
                return NotFound();

            return View(_unitOfWork.ItemTypes.GetItemTypesFromInventory(inventory.Id));
        }
        #endregion

        #region Create
        public async Task<IActionResult> Create([FromRoute]Guid? inventoryId)
        {
            if (inventoryId == null)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());

            if (inventory == null)
            {
                NotFound();
            }

            if (inventory.IsDisabled)
                return NotFound();

            return View(new ItemType());
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Create([FromRoute] Guid? inventoryId, ItemType model)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            ModelState.Remove("inventoryId");

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.InventoryId = inventory.Id;

            var result = await _unitOfWork.ItemTypes.CreateAsync(model);
            if (!result.IsSuccess)
            {
                ModelState.AddModelError(string.Empty, $"Ocorreu um erro ao criar o tipo de estado {model.Name}, tende novamente por favor\n{result.Message}");

                return View(model);
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi criado a categoria '{model.Name}' no inventario '{inventory.Name}' por {user.FullName}");

            this.SendSuccess($"O tipo de estado {model.Name} foi criado com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemTypes", action = "Index" });
        }
        #endregion

        #region Edit
        public async Task<IActionResult> Edit([FromRoute]Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var itemType = await _unitOfWork.ItemTypes.GetByIdAsync(id.Value);
            if (itemType == null)
            {
                return NotFound();
            }

            return View(itemType);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Edit([FromRoute] Guid? inventoryId, int? id, ItemType model)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            if (id == null)
            {
                return BadRequest();
            }

            if (id.Value != model.Id)
            {
                return BadRequest();
            }

            var typeToEdit = await _unitOfWork.ItemTypes.GetByIdAsync(id.Value);
            if (typeToEdit == null)
            {
                ModelState.AddModelError(string.Empty, "A marca que tentou editar não existe");

                return View(model);
            }

            ModelState.Remove("inventoryId");
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            #endregion

            string originalName = typeToEdit.Name;
            typeToEdit.Name = model.Name;

            var result = await _unitOfWork.ItemTypes.UpdateAsync(typeToEdit);
            if (!result.IsSuccess)
            {
                ModelState.AddModelError(string.Empty, $"Ocorreu um erro ao editar a categoria {model.Name}, tende novamente por favor\n{result.Message}");

                return View(model);
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi editada a categoria '{model.Name}' no inventario '{inventory.Name}' por {user.FullName} (Anteriormente {originalName})");

            this.SendSuccess($"A categoria {model.Name} foi editado com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemTypes", action = "Index" });
        }
        #endregion

        #region Enable/Disable
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Enable([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation
            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var itemTypeToEnable = await _unitOfWork.ItemTypes.GetByIdAsync(id.Value);

            var result = await _unitOfWork.ItemTypes.EnableAsync(itemTypeToEnable);
            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar activar a Categoria {itemTypeToEnable.Name}" +
                    $", tente novamente por favor\n{result.Message}");
                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemTypes", action = "Index" });
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi removida a categoria '{itemTypeToEnable.Name}' no inventario '{inventory.Name}' por {user.FullName}");

            this.SendSuccess($"A categoria {itemTypeToEnable.Name} foi activada com sucesso!!!");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemTypes", action ="Index"});
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Disable([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var itemTypeToDisable = await _unitOfWork.ItemTypes.GetByIdAsync(id.Value);

            if (itemTypeToDisable == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemTypes.DisableAsync(itemTypeToDisable);
            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar eliminar a Categoria {itemTypeToDisable.Name}" +
                    $", tente novamente por favor\n{result.Message}");
                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemTypes", action = "Index" });
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi eliminado a categoria {itemTypeToDisable.Name} do inventario {inventory.Name} por {user.FullName}");

            this.SendSuccess($"A Categoria {itemTypeToDisable.Name} foi eliminado com sucesso.");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemTypes", action = "Index"});
        }

        #endregion

        #region AdminDelete

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Delete([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation
            if(inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            #endregion

            if(id == null)
            {
                return BadRequest();
            }

            var itemTypeToDelete = await _unitOfWork.ItemTypes.GetByIdAsync(id.Value);

            if(itemTypeToDelete == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemTypes.DeleteAsync(itemTypeToDelete);

            if(!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar eliminar a Categoria {itemTypeToDelete.Name}" +
                    $", tente novamente por favor\n{result.Message}");
                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemTypes", action ="Index"});
            }

            this.SendSuccess($"A Categoria {itemTypeToDelete.Name} foi eliminada com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemTypes", action = "Index"});
        }
        #endregion


    }

}
