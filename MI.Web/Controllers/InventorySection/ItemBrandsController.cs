﻿using MI.Web.Data.Entities.InventorySection;
using MI.Web.Data.Interfaces;
using MI.Web.Extensions;
using MI.Web.Helpers.Interfaces;
using MI.Web.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.Web.Controllers.InventorySection
{
    [InventoryMemberAuthorize]
    public class ItemBrandsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly INotifyService _notifyService;
        private readonly IUserHelper _userHelper;

        public ItemBrandsController(IUnitOfWork unitOfWork, INotifyService notifyService, IUserHelper userHelper)
        {
            this._unitOfWork = unitOfWork;
            this._notifyService = notifyService;
            this._userHelper = userHelper;
        }

        #region Index

        public async Task<IActionResult> Index([FromRoute] Guid? inventoryId)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }            

            #endregion

            this.ExpectingSuccess();
            this.ExpectingError();
            return View(_unitOfWork.ItemBrands.GetItemBrandsFromInventory(inventory.Id));
        }

        #endregion

        #region Create

        public async Task<IActionResult> Create([FromRoute] Guid? inventoryId)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            this.ExpectingError();
            return View(new ItemBrand());
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Create([FromRoute] Guid? inventoryId, ItemBrand model)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();

            #endregion

            ModelState.Remove("inventoryId");
            
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.InventoryId = inventory.Id;
            var result = await _unitOfWork.ItemBrands.CreateAsync(model);
            if (!result.IsSuccess)
            {
                ModelState.AddModelError(string.Empty ,$"Ocorreu um erro ao criar a Marca {model.Name}, tente novamente por favor\n{result.Message}");
                return View(model);
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi adicionado a marca {model.Name} do inventario {inventory.Name} por {user.FullName}");

            this.SendSuccess($"A Marca {model.Name} foi criada com sucesso");
            return RedirectToRoute("inventoryHome",new { inventoryId = inventoryId, controller = "ItemBrands", action = "Index" });
        }

        #endregion

        #region Edit

        public async Task<IActionResult> Edit([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }            

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var itemBrand = await _unitOfWork.ItemBrands.GetByIdAsync(id.Value);
            if(itemBrand == null)
            {
                return NotFound();
            }

            this.ExpectingError();
            return View(itemBrand);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Edit([FromRoute] Guid? inventoryId, int? id, ItemBrand model)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            if (id == null)
            {
                return NotFound();
            }

            if (id.Value != model.Id)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            ModelState.Remove("inventoryId");
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var brandToEdit = await _unitOfWork.ItemBrands.GetByIdAsync(model.Id);
            if (brandToEdit == null)
            {
                ModelState.AddModelError(string.Empty,"A marca que tentou editar não existe");

                return View(model);
            }

            string originalName = brandToEdit.Name;
            brandToEdit.Name = model.Name;

            var result = await _unitOfWork.ItemBrands.UpdateAsync(brandToEdit);
            if (!result.IsSuccess)
            {
                ModelState.AddModelError(string.Empty, $"Ocorreu um erro ao editar a Marca {model.Name}, tende novamente por favor\n{result.Message}");

                return View(model);
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi editada a marca {model.Name} do inventario {inventory.Name} por {user.FullName} (Anteriomente: {originalName})");

            this.SendSuccess($"A Marca {model.Name} foi editada com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemBrands", action = "Index" });
        }

        #endregion

        #region Enable/Disable

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Enable([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }

            #endregion

            if(id == null)
            {
                return BadRequest();
            }

            var brandToEnable = await _unitOfWork.ItemBrands.GetByIdAsync(id.Value);

            if(brandToEnable == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemBrands.EnableAsync(brandToEnable);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar ativar a Marca {brandToEnable.Name}, tende novamente por favor\n{result.Message}");
                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemBrands", action = "Index" });
            }

            this.SendSuccess($"A Marca {brandToEnable.Name} foi ativada  com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemBrands", action = "Index" });
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Disable([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }
            if (inventory.IsDisabled)
                return NotFound();
            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var brandToDisable = await _unitOfWork.ItemBrands.GetByIdAsync(id.Value);

            if (brandToDisable == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemBrands.DisableAsync(brandToDisable);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar eliminar a Marca {brandToDisable.Name}, tende novamente por favor\n{result.Message}");
                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemBrands", action = "Index" });
            }

            var user = await _userHelper.GetUserByIdAsync(User.GetUserId());
            await _notifyService.NotifyInventoryUsersByEmailAsync(
                inventory.Id,
                $"Multinventory - {inventory.Name}",
                $"Foi eliminada a marca {brandToDisable.Name} do inventario {inventory.Name} por {user.FullName}");


            this.SendSuccess($"A Marca {brandToDisable.Name} foi eliminada com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemBrands", action = "Index" });
        }

        #endregion

        #region Delete

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Delete([FromRoute] Guid? inventoryId, int? id)
        {
            #region Initial Validation

            if (inventoryId == null || inventoryId.Value == Guid.Empty)
            {
                return BadRequest();
            }

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(inventoryId.Value.ToString());
            if (inventory == null)
            {
                return NotFound();
            }

            #endregion

            if (id == null)
            {
                return BadRequest();
            }

            var brandToDelete = await _unitOfWork.ItemBrands.GetByIdAsync(id.Value);

            if (brandToDelete == null)
            {
                return NotFound();
            }

            var result = await _unitOfWork.ItemBrands.DeleteAsync(brandToDelete);

            if (!result.IsSuccess)
            {
                this.SendError($"Ocorreu um erro ao tentar eliminar a Marca {brandToDelete.Name}, tende novamente por favor\n{result.Message}");
                return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemBrands", action = "Index" });
            }

            this.SendSuccess($"A Marca {brandToDelete.Name} foi eliminada com sucesso");
            return RedirectToRoute("inventoryHome", new { inventoryId = inventoryId, controller = "ItemBrands", action = "Index" });
        }

        #endregion

        #region GetModels

        [HttpPost]
        public async Task<IActionResult> GetModels(string brandId)
        {
            int brId;
            if (!int.TryParse(brandId, out brId))
                return BadRequest();

            if (brId <= 0)
                return BadRequest();

            var models = _unitOfWork.ItemModels.GetAll().Where(model => model.BrandId == brId);
            var result = models.Select(x => new { x.Name, x.Id });

            return Ok(result);
        }
        #endregion
    }
}
