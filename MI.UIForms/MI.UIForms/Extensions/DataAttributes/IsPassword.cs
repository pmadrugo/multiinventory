﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace MI.UIForms.Extensions.DataAttributes
{
    public class IsPassword : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string valueStr = value.ToString();
            if (Regex.Match(valueStr, "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!?@\\/*.]).{8,}$").Success)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("Invalid Password");
            }
        }
    }
}
