﻿using MI.Common.Models;
using MI.UIForms.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.UIForms.Data.Interfaces
{
    public interface IGenericRepository<TEntity> : IDisposable
    {
        IQueryable<TEntity> GetAll();

        Task<Response> CreateAsync(TEntity entity, bool saveChanges);

        Task<Response> UpdateAsync(TEntity entity, bool saveChanges);

        Task<Response> DeleteAsync(TEntity entity, bool saveChanges);

        Task<Response> DisableAsync(TEntity entity, bool saveChanges);

        Task<bool> ExistsAsync(int Id);

        Task<Response> EnableAsync(TEntity entity, bool saveChanges = true);

        IQueryable<TEntity> GetAllDisabled();

        Task<List<TEntity>> GetListAsync();

        Task<TEntity> GetByIdAsync(int id);
    }
}
