﻿using MI.UIForms.Data.Entities.InventorySection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.UIForms.Data.Interfaces
{
    public interface IItemStateRepository : IGenericRepository<ItemState>
    {
    }
}
