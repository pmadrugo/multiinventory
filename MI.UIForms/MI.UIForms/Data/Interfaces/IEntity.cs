﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.UIForms.Data.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }

        Guid UUId { get; set; }

        DateTime? LastModified { get; set; }

        bool IsDisabled { get; set; }
    }
}
