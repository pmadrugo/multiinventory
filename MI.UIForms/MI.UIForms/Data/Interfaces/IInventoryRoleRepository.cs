﻿using MI.UIForms.Data.Entities.ApplicationSection;

namespace MI.UIForms.Data.Interfaces
{
    public interface IInventoryRoleRepository : IGenericRepository<InventoryRole>
    {
    }
}
