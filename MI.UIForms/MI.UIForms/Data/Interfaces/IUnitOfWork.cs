﻿using MI.Common.Models;

namespace MI.UIForms.Data.Interfaces
{
    public interface IUnitOfWork
    {
        Repository.InventoryRoleRepository InventoryRoles { get; }
        Repository.InventoryRepository Inventories { get; }
        Repository.ItemBrandRepository ItemBrands { get; }
        Repository.ItemModelRepository ItemModels { get; }
        Repository.ItemTypeRepository ItemTypes { get; }
        Repository.ItemPlaceRepository ItemPlaces { get; }
        Repository.ItemStateRepository ItemsStates { get; }
        Repository.ItemRepository Items { get; }
    }
}
