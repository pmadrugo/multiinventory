﻿using MI.UIForms.Data.Entities.InventorySection;
using System;
using System.Collections.Generic;
using System.Text;

namespace MI.UIForms.Data.Interfaces
{
    public interface IInventoryEntity : IEntity
    {
        int InventoryId { get; set; }

        Inventory Inventory { get; set; }
    }
}
