﻿using MI.UIForms.Data.Entities.ApplicationSection;
using MI.UIForms.Data.Entities.InventorySection;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MI.UIForms.Data
{
    public class MIDbContext : DbContext
    {
        private string _databaseName = "MI_Lite.db3";
        private string _personalFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        private string _databasePath;

        public MIDbContext()
        {
            _databasePath = Path.Combine(_personalFolderPath, _databaseName);

            this.Database.EnsureCreatedAsync();

            System.Diagnostics.Debug.WriteLine($"Writing to database at: {_databasePath}");
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Filename={_databasePath}");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            #region Decimal Types

            #endregion

            #region Index Attribution
            builder.Entity<ItemBrand>()
                .HasIndex(ib => ib.InventoryId);

            builder.Entity<ItemModel>()
                .HasIndex(im => im.InventoryId);

            builder.Entity<ItemPlace>()
                .HasIndex(ip => ip.InventoryId);

            builder.Entity<ItemState>()
                .HasIndex(iS => iS.InventoryId);

            builder.Entity<ItemType>()
                .HasIndex(it => it.InventoryId);

            builder.Entity<Item>()
                .HasIndex(i => i.InventoryId);

            #endregion

            #region Composite Key Attribution

            builder.Entity<UserInventory>()
                .HasKey(ui =>
                    new { ui.UserId, ui.InventoryId });

            #endregion

            #region Many to Many Relationships

            builder.Entity<UserInventory>()
                .HasOne(ui => ui.User)
                .WithMany(u => u.User_Inventories)
                .HasForeignKey(ui => ui.UserId);

            builder.Entity<UserInventory>()
                .HasOne(ui => ui.Inventory)
                .WithMany(i => i.Users)
                .HasForeignKey(ui => ui.InventoryId);

            #endregion

            #region Setup Indexes

            #endregion

            // Remove On Delete Cascade
            var cascadeFKs = builder.Model
                .GetEntityTypes()
                .SelectMany(x => x.GetForeignKeys())
                .Where(x => !x.IsOwnership && x.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var cascadeFK in cascadeFKs)
            {
                cascadeFK.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(builder);
        }

        #region Entities

        public DbSet<ItemModel> ItemModel { get; set; }

        public DbSet<Inventory> Inventory { get; set; }

        public DbSet<ItemType> ItemType { get; set; }

        public DbSet<ItemBrand> ItemBrand { get; set; }

        public DbSet<ItemPlace> ItemPlace { get; set; }

        public DbSet<ItemState> ItemState { get; set; }

        public DbSet<Item> Item { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<InventoryRole> InventoryRoles { get; set; }

        public DbSet<UserInventory> UserInventories { get; set; }

        #endregion
    }
}
