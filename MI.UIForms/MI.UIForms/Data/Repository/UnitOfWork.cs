﻿using MI.Common.Models;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Entities;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;

namespace MI.UIForms.Data.Repository
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly MIDbContext _dbContext;

        public UnitOfWork(MIDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        // Repository Internal Variables

        // Add Repositories here
        #region Project Repositories / Attributes
        private InventoryRoleRepository _inventoryRoles;
        private InventoryRepository _inventory;
        private ItemBrandRepository _itemBrands;
        private ItemModelRepository _itemModels;
        private ItemTypeRepository _itemTypes;
        private ItemPlaceRepository _itemPlaces;
        private ItemStateRepository _itemStates;
        private ItemRepository _items;
        private UserRepository _users;
        #endregion

        #region Repository Instances / Properties
        public InventoryRoleRepository InventoryRoles
        {
            get
            {
                if (_inventoryRoles == null)
                    _inventoryRoles = new InventoryRoleRepository(_dbContext);
                return _inventoryRoles;
            }
        }

        public InventoryRepository Inventories
        {
            get
            {
                if (_inventory == null)
                    _inventory = new InventoryRepository(_dbContext);
                return _inventory;
            }
        }

        public ItemBrandRepository ItemBrands
        {
            get
            {
                if (_itemBrands == null)
                {
                    _itemBrands = new ItemBrandRepository(_dbContext);
                }
                return _itemBrands;
            }
        }

        public ItemModelRepository ItemModels
        {
            get
            {
                if (_itemModels == null)
                {
                    _itemModels = new ItemModelRepository(_dbContext);
                }
                return _itemModels;
            }
        }

        public ItemTypeRepository ItemTypes
        {
            get
            {
                if (_itemTypes == null)
                {
                    _itemTypes = new ItemTypeRepository(_dbContext);
                }
                return _itemTypes;
            }
        }

        public ItemPlaceRepository ItemPlaces
        {
            get
            {
                if (_itemPlaces == null)
                {
                    _itemPlaces = new ItemPlaceRepository(_dbContext);
                }
                return _itemPlaces;
            }
        }

        public ItemStateRepository ItemsStates
        {
            get
            {
                if (_itemStates == null)
                {
                    _itemStates = new ItemStateRepository(_dbContext);
                }
                return _itemStates;
            }
        }

        public ItemRepository Items
        {
            get
            {
                if (_items == null)
                {
                    _items = new ItemRepository(_dbContext);
                }
                return _items;
            }
        }

        public UserRepository Users
        {
            get
            {
                if (_users == null)
                    _users = new UserRepository(_dbContext);
                return _users;
            }
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    
                }

                disposedValue = true;
            }
        }

        
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

        #endregion

    }
}
