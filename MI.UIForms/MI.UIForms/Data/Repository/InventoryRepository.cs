﻿using MI.Common.Models;
using MI.UIForms.Data.Entities.ApplicationSection;
using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.UIForms.Data.Repository
{
    public class InventoryRepository : GenericRepository<Inventory>, IInventoryRepository
    {
        public InventoryRepository (MIDbContext dbContext) : base(dbContext) { }

        public IQueryable<Inventory> GetInventoriesFromUser(string userId)
        {
            return _dbContext.Set<UserInventory>()               
                .Where(ui => ui.UserId == userId &&
                             !ui.Inventory.IsDisabled)       
                .Select(ui => ui.Inventory)
                .OrderBy(i => i.Name);                
        }

        public override IQueryable<Inventory> GetAll()
        {
            return base.GetAll().OrderBy(inv => inv.Name)                                
                                .Include(inv => inv.Users);
        }

    }
}
