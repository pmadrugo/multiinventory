﻿using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.UIForms.Data.Repository
{
    public class ItemModelRepository : GenericRepository<ItemModel>, IItemModelRepository
    {
        public ItemModelRepository(MIDbContext dbContext) : base(dbContext) { }

        public IQueryable<ItemModel> GetItemModelsFromInventory(int inventoryId)
        {
            return _dbContext.Set<ItemModel>()
                .Include(x => x.Inventory)
                .Include(im => im.Brand)
                .Where(im => im.InventoryId == inventoryId &&
                                !im.IsDisabled)
                .OrderBy(x => x.Name);
        }

        public IQueryable<ItemModel> GetItemModelsFromBrand(int brandId)
        {
            return _dbContext.Set<ItemModel>()
                .Include(x => x.Inventory)
                .Include(im => im.Brand)
                .Where(im => im.BrandId == brandId &&
                        !im.IsDisabled)
                .OrderBy(x => x.Name);
        }

        public IQueryable<ItemModel> GetItemModelsFromBrand(Guid UUID)
        {
            return _dbContext.Set<ItemModel>()
                .Include(x => x.Inventory)
                .Include(im => im.Brand)
                .Where(im => im.Brand.UUId == UUID &&
                            !im.IsDisabled)
                .OrderBy(x => x.Name);
        }

        public ItemModel GetItemModelFromInventoryWithBrand(int id)
        {
            return _dbContext.Set<ItemModel>()
                .Include(iM => iM.Brand)
                .FirstOrDefault(iM => iM.Id == id);
        }
    }
}
