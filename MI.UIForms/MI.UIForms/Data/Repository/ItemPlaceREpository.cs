﻿using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.UIForms.Data.Repository
{
    public class ItemPlaceRepository : GenericRepository<ItemPlace>, IItemPlaceRepository
    {
        public ItemPlaceRepository(MIDbContext dbContext) : base(dbContext) { }

        public IQueryable<ItemPlace> GetItemPlacesFromInventory(int invenotryId)
        {
            return _dbContext.Set<ItemPlace>()
                .Include(x => x.Inventory)
                .Where(ip => ip.InventoryId == invenotryId &&
                             !ip.IsDisabled)
                .OrderBy(ip => ip.Name);
        }
    }
}
