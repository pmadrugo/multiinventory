﻿using MI.UIForms.Data.Entities.ApplicationSection;
using MI.UIForms.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MI.UIForms.Data.Repository
{
    public class UserRepository : IUserRepository, IDisposable
    {
        protected readonly MIDbContext _dbContext;
        public UserRepository(MIDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool ValidatePassword(string userEmail, string password)
        {
            return _dbContext.Set<User>()
                .Any(x => x.UserName == userEmail &&
                          x.Password == password);
        }

        public User GetUserByEmail(string userEmail)
        {
            return _dbContext.Set<User>()
                .FirstOrDefault(x => x.UserName == userEmail);
        }

        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
