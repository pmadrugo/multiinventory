﻿using MI.Common.Models;
using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.UIForms.Data.Repository
{
    public class ItemRepository : GenericRepository<Item>, IItemRepository
    {
        public ItemRepository(MIDbContext dbContext) : base(dbContext) { }

        public IQueryable<Item> GetItemsFromInventory(int inventoryId)
        {
            return _dbContext.Set<Item>()
                .Include(i => i.ItemState)
                .Include(i => i.Place)
                .Include(i => i.Model)
                .Include(i => i.Model.Brand)
                .Include(i => i.Type)
                .Include(x => x.Inventory)
                .Where(i => i.InventoryId == inventoryId &&
                            !i.IsDisabled)
                .OrderBy(items => items.Name);
        }

        public async Task<Item> GetByIdWithBrandAsync(int id)
        {
            return await this._dbContext.Set<Item>()
                .Include(item => item.Model)
                .FirstOrDefaultAsync(entity => entity.Id == id);
        }

        public IQueryable<Item> GetActiveItemsFromInventory(int inventoryId)
        {
            return GetItemsFromInventory(inventoryId)
                .Where(i => i.ItemOutput == null);
        }

        public IQueryable<Item> GetNonActiveItemsFromInventory(int inventoryId)
        {
            return GetItemsFromInventory(inventoryId)
                .Where(i => i.ItemOutput != null);
        }

        public Item GetCompleteItemInformation(int id)
        {
            return _dbContext.Set<Item>()
                .Include(i => i.ItemState)
                .Include(i => i.Place)
                .Include(i => i.Model)
                .Include(i => i.Model.Brand)
                .Include(i => i.Type)
                .FirstOrDefault(i => i.Id == id);
        }

        public async Task<Response> SetItemOutputAsync(int itemId, DateTime dateOutput, int? stateId = null)
        {
            var item = await this.GetByIdAsync(itemId);
            if (item == null)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = "Item not found"
                };
            }

            item.ItemOutput = dateOutput;
            if (stateId != null)
            {
                item.ItemStateId = stateId.Value;
            }

            return await this.UpdateAsync(item);
        }
    }
}
