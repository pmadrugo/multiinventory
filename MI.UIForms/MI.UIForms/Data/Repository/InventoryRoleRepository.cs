﻿using MI.UIForms.Data.Entities.ApplicationSection;
using MI.UIForms.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.UIForms.Data.Repository
{
    public class InventoryRoleRepository : GenericRepository<InventoryRole>, IInventoryRoleRepository
    {
        public InventoryRoleRepository(MIDbContext dbContext) : base(dbContext) { }

        public async Task<InventoryRole> GetByNameAsync(string Role)
        {
            return await _dbContext.Set<InventoryRole>()
                .Where(x => x.Role == Role)
                .FirstOrDefaultAsync();
        }

        public async Task<bool> IsThereAnyAsync(string Role)
        {
            return await _dbContext.Set<InventoryRole>()
                .AnyAsync(x => x.Role == Role);
        }

        public IQueryable<InventoryRole> GetUsableRoles()
        {
            return _dbContext.Set<InventoryRole>()
                .Where(c => c.Role != "Owner");
        }
    }
}
