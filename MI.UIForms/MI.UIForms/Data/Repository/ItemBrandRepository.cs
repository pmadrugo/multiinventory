﻿using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.UIForms.Data.Repository
{
    public class ItemBrandRepository : GenericRepository<ItemBrand>, IItemBrandRepository
    {
        public ItemBrandRepository(MIDbContext dbContext) : base(dbContext) { }

        public IQueryable<ItemBrand> GetItemBrandsFromInventory(int inventoryId)
        {
            return _dbContext.Set<ItemBrand>()
                .Include(x => x.Inventory)
                .Where(i => i.InventoryId == inventoryId && 
                            !i.IsDisabled)
                .OrderBy(i => i.Name);
        }

        public IQueryable<ItemBrand> GetAllItemBrandsFromInventory(int inventoryId)
        { 
            return _dbContext.Set<ItemBrand>()
                .Where(i => i.InventoryId == inventoryId)
                .OrderBy(i => i.Name);
        }
    }
}
