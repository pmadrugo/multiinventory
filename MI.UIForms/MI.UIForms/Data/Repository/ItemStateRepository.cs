﻿using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.UIForms.Data.Repository
{
    public class ItemStateRepository : GenericRepository<ItemState>, IItemStateRepository
    {
        public ItemStateRepository(MIDbContext dbContext) : base(dbContext) { }

        public IQueryable<ItemState> GetItemStatesFromInventory(int inventoryId)
        {
            return _dbContext.Set<ItemState>()
                .Include(x => x.Inventory)
                .Where(i => i.InventoryId == inventoryId &&
                            !i.IsDisabled)
                .OrderBy(i => i.State);
        }
        
        public IQueryable<ItemState> GetAllItemStatesFromInventory(int inventoryId)
        {
            return _dbContext.Set<ItemState>()
                .Where(i => i.InventoryId == inventoryId)
                .OrderBy(i => i.State);
        }
    }
}
