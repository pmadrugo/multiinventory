﻿using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI.UIForms.Data.Repository
{
    public class ItemTypeRepository: GenericRepository<ItemType>, IItemTypeRepository
    {

        public ItemTypeRepository(MIDbContext dbContext): base (dbContext) { }
        
        public IQueryable<ItemType> GetItemTypesFromInventory(int inventoryId)
        {
            return _dbContext.Set<ItemType>()
                .Include(x => x.Inventory)
                .Where(x => x.InventoryId == inventoryId &&
                            !x.IsDisabled);
        }
        

    }
}
