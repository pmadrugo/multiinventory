﻿using MI.UIForms.Data.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MI.UIForms.Data.Entities.InventorySection
{
    public class Item : IEntity, IInventoryEntity
    {
        #region Metadata
        [Key]
        public int Id { get; set; }

        [ScaffoldColumn(false)]
        public Guid UUId { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? LastModified { get; set; }

        [ScaffoldColumn(false)]
        public bool IsDisabled { get; set; }

        public int InventoryId { get; set; }

        [ForeignKey("InventoryId")]
        [JsonIgnore]
        public virtual Inventory Inventory { get; set; }
        #endregion

        #region Data
        [Display(Name = "Local")]
        [Required(ErrorMessage = "É necessário preencher um {0}.")]
        [Range(1, double.MaxValue, ErrorMessage = "Escolha um valor válido...")]
        public int PlaceId { get; set; }
        [ForeignKey("PlaceId")]
        public virtual ItemPlace Place { get; set; }

        [Display(Name = "Nome")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "O {0} do item tem de ter entre {2} e {1} caractéres.")]
        [Required(ErrorMessage = "É necessário preencher um {0}.")]
        public string Name { get; set; }

        [Display(Name = "Descrição")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Imagem")]
        public string ImagePath { get; set; }

        [Display(Name = "Entrada")]
        [ScaffoldColumn(false)]
        public DateTime? ItemInput { get; set; }

        [Display(Name = "Saída")]
        [ScaffoldColumn(false)]
        public DateTime? ItemOutput { get; set; }

        [Display(Name = "Estado do Item")]
        [Range(1, double.MaxValue, ErrorMessage = "Escolha um valor válido...")]
        [Required(ErrorMessage = "É necessário preencher um {0}.")]
        public int ItemStateId { get; set; }
        [ForeignKey("ItemStateId")]
        public virtual ItemState ItemState { get; set; }

        [Display(Name = "Modelo")]
        [Required(ErrorMessage = "É necessário preencher um {0}.")]
        [Range(1, double.MaxValue, ErrorMessage = "Escolha um valor válido...")]
        public int ModelId { get; set; }
        [ForeignKey("ModelId")]
        public virtual ItemModel Model { get; set; }

        [Display(Name = "Categoria")]
        [Required(ErrorMessage = "É necessário seleccionar uma {0}.")]
        [Range(1, double.MaxValue, ErrorMessage = "Escolha um valor válido...")]
        public int TypeId { get; set; }
        [ForeignKey("TypeId")]
        public virtual ItemType Type { get; set; } 
        #endregion
    }
}
