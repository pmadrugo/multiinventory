﻿using MI.UIForms.Data.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MI.UIForms.Data.Entities.InventorySection
{
    public class ItemPlace : IEntity, IInventoryEntity
    {
        #region Metadata
        [Key]
        public int Id { get; set; }

        [ScaffoldColumn(false)]
        public bool IsDisabled { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? LastModified { get; set; }

        [ScaffoldColumn(false)]
        public Guid UUId { get; set; }

        public int InventoryId { get; set; }
        [ForeignKey("InventoryId")]
        [JsonIgnore]
        public virtual Inventory Inventory { get; set; }
        #endregion

        #region Data


        [Display(Name = "Localização")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "A {0} do item tem de ter entre {2} e {1} caractéres.")]
        [Required(ErrorMessage = "É necessário preencher um {0}.")]
        public string Name { get; set; }

        public virtual ICollection<Item> Items { get; set; } 
        #endregion
    }
}
