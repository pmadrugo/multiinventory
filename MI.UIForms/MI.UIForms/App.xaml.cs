﻿using MI.UIForms.ViewModels;
using MI.UIForms.Views;
using MI.UIForms.Views.Application;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MI.UIForms
{
    public partial class App : Application
    {
        #region Properties
        public static NavigationPage Navigator { get; internal set; }

        public static MasterPage Master { get; internal set; }

        #endregion

        public App()
        {
            InitializeComponent();

            MainViewModel.GetInstance().LoginVM = new LoginViewModel();
            MainPage = new NavigationPage( new LoginPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
