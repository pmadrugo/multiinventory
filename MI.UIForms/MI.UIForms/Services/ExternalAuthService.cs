﻿using MI.Common.JsonModels.Authentication;
using MI.UIForms.Helper;
using MI.UIForms.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Auth;
using Xamarin.Forms;

namespace MI.UIForms.Services
{
    public class ExternalAuthService
    {
        private Account _account;
        private AccountStore _store;
        private string _externalName;


        public Account Account {
            get
            {
                return _account;
            }
            set
            {
                _account = value;
            }
        }

        public AccountStore Store {
            get
            {
                return _store;
            }
            set
            {
                _store = value;
            }
        }

        public string ExternalName {
            get {
                return _externalName;
            }
            set
            {
                _externalName = value;
            }
        }

        public ExternalAuthService()
        {
            Store = AccountStore.Create();
        }

        public OAuth2Authenticator CreateOAuth2Authenticator(string externalName)
        {
            ExternalName = externalName;

            string clientId = null;
            string redirectUrl = null;

            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    clientId = AppSettingsHelper.Settings[$"Authentication:{_externalName}:AndroidClientId"];
                    redirectUrl = AppSettingsHelper.Settings[$"Authentication:{_externalName}:AndroidRedirectUrl"];
                    break;
            }

            Account = Store.FindAccountsForService(AppSettingsHelper.Settings["Authentication:AppName"]).FirstOrDefault();

            OAuth2Authenticator authenticator = null;

            switch (externalName)
            {
                case "Facebook":
                    authenticator = new OAuth2Authenticator(
                        clientId: AppSettingsHelper.Settings[$"Authentication:{_externalName}:AndroidClientId"],
                        scope:"email",
                        authorizeUrl: new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalName}:AuthorizeUrl"]),
                        redirectUrl: new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalName}:AndroidRedirectUrl"])
                        );
                    break;

                case "Google":
                    authenticator = new OAuth2Authenticator(
                        clientId,
                        null,
                        AppSettingsHelper.Settings[$"Authentication:{_externalName}:Scope"],
                        new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalName}:AuthorizeUrl"]),
                        new Uri(redirectUrl),
                        new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalName}:AccessTokenUrl"]),
                        null,
                        true
                        );
                    break;

                //case "Twitter":
                //    authenticator = new OAuth2Authenticator(
                //        clientId: AppSettingsHelper.Settings[$"Authentication:{_externalName}:AndroidClientId"],
                //        scope: "",
                //        authorizeUrl: new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalName}:AuthorizeUrl"]),
                //        redirectUrl: new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalName}:AndroidRedirectUrl"])
                //        );
                //    break;

                case "Microsoft":
                    authenticator = new OAuth2Authenticator(
                        clientId,
                        AppSettingsHelper.Settings[$"Authentication:{_externalName}:Scope"],
                        new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalName}:AuthorizeUrl"]),                        
                        new Uri(redirectUrl)                        
                        );
                    break;
            }


            authenticator.AllowCancel = true;

            return authenticator;

        }

        public OAuth1Authenticator CreateOAuth1Authenticator(string externalName)
        {
            ExternalName = externalName;

            string clientId = null;
            string redirectUrl = null;

            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    clientId = AppSettingsHelper.Settings[$"Authentication:{_externalName}:AndroidClientId"];
                    redirectUrl = AppSettingsHelper.Settings[$"Authentication:{_externalName}:AndroidRedirectUrl"];
                    break;
            }

            Account = Store.FindAccountsForService(AppSettingsHelper.Settings["Authentication:AppName"]).FirstOrDefault();

            OAuth1Authenticator authenticator = null;

            switch (externalName)
            {

                case "Twitter":

                    authenticator = new OAuth1Authenticator(
                        clientId,
                        AppSettingsHelper.Settings[$"Authentication:{_externalName}:AndoirdClientSecret"],
                        new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalName}:RequestTokenUrl"]),
                        new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalName}:AuthorizeUrl"]),
                        new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalName}:AccessTokenUrl"]),
                        new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalName}:AndroidRedirectUrl"]),
                        null,
                        false
                    );

                    break;

            }


            authenticator.AllowCancel = true;

            return authenticator;

        }
    }
}
