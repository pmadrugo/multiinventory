﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace MI.UIForms.Services
{
    public static class CryptoService
    {
        /// <summary>
        /// Returns the MD5 computed hash of a given string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ComputeMD5Hash(string value)
        {
            var MD5crypto = MD5.Create();

            var byteArray = MD5crypto.ComputeHash(Encoding.UTF8.GetBytes(value));
            string hash = string.Empty;
            for (int i = 0; i < byteArray.Length; i++)
            {
                hash += byteArray[i].ToString("X2");
            }

            return hash;
        }
    }
}
