﻿using MI.Common.JsonModels.Inventory;
using MI.Common.JsonModels.JSONInterfaces;
using MI.Common.Models;
using MI.Common.Services;
using MI.UIForms.Data;
using MI.UIForms.Data.Entities.ApplicationSection;
using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.ViewModels;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MI.UIForms.Services
{
    public class SyncService
    {
        /// <summary>
        /// Syncs the current user with database
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<Response> SyncUserWithDatabaseAsync(User user)
        {
            var dbContext = MainViewModel.GetInstance().LocalDatabase;

            try
            {
                user.Password = CryptoService.ComputeMD5Hash(user.Password);

                var userSet = await dbContext.Set<User>().ToListAsync();

                var userInDB = userSet
                    .FirstOrDefault(u => u.Id == user.Id);

                if (userInDB != null)
                {
                    if (string.IsNullOrEmpty(user.FirstName))
                        user.FirstName = userInDB.FirstName;

                    if (string.IsNullOrEmpty(user.LastName))
                        user.LastName = userInDB.LastName;

                    if (user.BirthDate == DateTime.MinValue)
                        user.BirthDate = userInDB.BirthDate;

                    if (string.IsNullOrEmpty(user.VATNumber))
                        user.VATNumber = userInDB.VATNumber;

                    dbContext.Entry<User>(userInDB).CurrentValues.SetValues(user);
                    dbContext.Entry<User>(userInDB).State = EntityState.Modified;

                    await dbContext.SaveChangesAsync();
                }
                else
                {
                    await dbContext.Users.AddAsync(user);

                    await dbContext.SaveChangesAsync();
                }

                return new Response()
                {
                    IsSuccess = true,
                    Result = user
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Syncs all of the inventory with the database
        /// </summary>
        /// <param name="inventoryFull"></param>
        /// <returns></returns>
        public async Task<Response> SyncInventoryWithDatabaseAsync(JsonInventoryFull inventoryFull)
        {
            var dbContext = MainViewModel.GetInstance().LocalDatabase;
            var inventory = inventoryFull.Inventory;

            var isInventorySynced = await SyncSingleInventoryAsync(inventory);
            if (!isInventorySynced.IsSuccess)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = isInventorySynced.Message
                };
            }

            var inventoryInDb = ((Inventory)isInventorySynced.Result);
         
            #region Entity Syncing (Downstream)

            #region Item States Sync

            var isItemStatesSynced = await SyncInventoryEntitiesAsync
                <JsonItemState, ItemState>(
                inventory.UUId.ToString(), 
                inventoryFull.ItemStates,
                modelOnCreate: (json) =>
                {
                    return new ItemState()
                    {
                        UUId = json.UUId,
                        State = json.State,
                        IsDisabled = json.IsDisabled,
                        LastModified = json.LastModified,
                        InventoryId = inventoryInDb.Id
                    };
                },
                modelOnUpdate: (json, entityInDb) =>
                {
                    entityInDb.InventoryId = inventoryInDb.Id;
                    entityInDb.IsDisabled = json.IsDisabled;
                    entityInDb.State = json.State;
                    entityInDb.UUId = json.UUId;
                    entityInDb.LastModified = json.LastModified;

                    return entityInDb;
                }
            );

            #endregion

            #region Item Types Sync

            var isItemTypesSynced = await SyncInventoryEntitiesAsync
                <JsonItemType, ItemType>(
                inventory.UUId.ToString(),
                inventoryFull.ItemTypes,
                modelOnCreate: (json) =>
                {
                    return new ItemType()
                    {
                        UUId = json.UUId,
                        Name = json.Name,
                        IsDisabled = json.IsDisabled,
                        LastModified = json.LastModified,
                        InventoryId = inventoryInDb.Id
                    };
                },
                modelOnUpdate: (json, entityInDb) =>
                {
                    entityInDb.InventoryId = inventoryInDb.Id;
                    entityInDb.IsDisabled = json.IsDisabled;
                    entityInDb.Name = json.Name;
                    entityInDb.UUId = json.UUId;
                    entityInDb.LastModified = json.LastModified;

                    return entityInDb;
                }
            );
            #endregion

            #region Item Places Sync

            var isItemPlacesSynced = await SyncInventoryEntitiesAsync
                <JsonItemPlace, ItemPlace>(
                inventory.UUId.ToString(),
                inventoryFull.ItemPlaces,
                modelOnCreate: (json) =>
                {
                    return new ItemPlace()
                    {
                        UUId = json.UUId,
                        Name = json.Name,
                        IsDisabled = json.IsDisabled,
                        LastModified = json.LastModified,
                        InventoryId = inventoryInDb.Id
                    };
                },
                modelOnUpdate: (json, entityInDb) =>
                {
                    entityInDb.InventoryId = inventoryInDb.Id;
                    entityInDb.IsDisabled = json.IsDisabled;
                    entityInDb.Name = json.Name;
                    entityInDb.UUId = json.UUId;
                    entityInDb.LastModified = json.LastModified;

                    return entityInDb;
                }
            );

            #endregion

            #region Item Brands Sync

            var isItemBrandsSync = await SyncInventoryEntitiesAsync
                <JsonItemBrand, ItemBrand>(
                inventory.UUId.ToString(),
                inventoryFull.ItemBrands,
                modelOnCreate: (json) =>
                {
                    return new ItemBrand()
                    {
                        UUId = json.UUId,
                        Name = json.Name,
                        IsDisabled = json.IsDisabled,
                        LastModified = json.LastModified,
                        InventoryId = inventoryInDb.Id
                    };
                },
                modelOnUpdate: (json, entityInDb) =>
                {
                    entityInDb.InventoryId = inventoryInDb.Id;
                    entityInDb.IsDisabled = json.IsDisabled;
                    entityInDb.Name = json.Name;
                    entityInDb.UUId = json.UUId;
                    entityInDb.LastModified = json.LastModified;

                    return entityInDb;
                }
            );

            #endregion

            #region Item Models Sync

            List<ItemBrand> brandsInDB = new List<ItemBrand>();

            brandsInDB.AddRange(await dbContext.Set<ItemBrand>().ToListAsync());

            var isItemModelsSync = await SyncInventoryEntitiesAsync
               <JsonItemModel, ItemModel>(
               inventory.UUId.ToString(),
               inventoryFull.ItemModels,
               modelOnCreate: (json) =>
               {
                   var modelBrand = brandsInDB
                    .Where(brand => brand.UUId == json.BrandUUId)
                    .FirstOrDefault();

                   return new ItemModel()
                   {
                       UUId = json.UUId,
                       Name = json.Name,
                       IsDisabled = json.IsDisabled,
                       LastModified = json.LastModified,
                       InventoryId = inventoryInDb.Id,
                       BrandId = modelBrand.Id
                   };
               },
               modelOnUpdate: (json, entityInDb) =>
               {
                   var modelBrand = brandsInDB
                   .Where(brand => brand.UUId == json.BrandUUId)
                   .FirstOrDefault();

                   entityInDb.InventoryId = inventoryInDb.Id;
                   entityInDb.IsDisabled = json.IsDisabled;
                   entityInDb.Name = json.Name;
                   entityInDb.UUId = json.UUId;
                   entityInDb.LastModified = json.LastModified;
                   entityInDb.BrandId = modelBrand.Id;

                   return entityInDb;
               }
            );

            #endregion

            #region Items Sync

            List<ItemModel> modelsInDb = new List<ItemModel>();
            List<ItemPlace> placesInDb = new List<ItemPlace>();
            List<ItemState> statesInDb = new List<ItemState>();
            List<ItemType> typesInDb = new List<ItemType>();

            modelsInDb.AddRange(await dbContext.Set<ItemModel>().ToListAsync());
            placesInDb.AddRange(await dbContext.Set<ItemPlace>().ToListAsync());
            statesInDb.AddRange(await dbContext.Set<ItemState>().ToListAsync());
            typesInDb.AddRange(await dbContext.Set<ItemType>().ToListAsync());

            var isItemsSync = await SyncInventoryEntitiesAsync
                <JsonItem, Item>(
                    inventory.UUId.ToString(),
                    inventoryFull.Items,
                    modelOnCreate: (json) =>
                    {
                        var itemModel = modelsInDb.Where(iM => iM.UUId == json.ModelUUId).FirstOrDefault();
                        var itemPlace = placesInDb.Where(iP => iP.UUId == json.PlaceUUId).FirstOrDefault();
                        var itemState = statesInDb.Where(iS => iS.UUId == json.ItemStateUUId).FirstOrDefault();
                        var itemType = typesInDb.Where(iT => iT.UUId == json.TypeUUId).FirstOrDefault();

                        return new Item()
                        {
                            UUId = json.UUId,
                            Name = json.Name,
                            Description = json.Description,
                            IsDisabled = json.IsDisabled,
                            ItemInput = json.ItemInput,
                            ItemOutput = json.ItemOutput,
                            LastModified = json.LastModified,
                            InventoryId = inventoryInDb.Id,
                            ItemStateId = itemState.Id,
                            ModelId = itemModel.Id,
                            PlaceId = itemPlace.Id,
                            TypeId = itemType.Id,
                        };
                    },
                    modelOnUpdate: (json, entityInDb) =>
                    {
                        var itemModel = modelsInDb.Where(iM => iM.UUId == json.ModelUUId).FirstOrDefault();
                        var itemPlace = placesInDb.Where(iP => iP.UUId == json.PlaceUUId).FirstOrDefault();
                        var itemState = statesInDb.Where(iS => iS.UUId == json.ItemStateUUId).FirstOrDefault();
                        var itemType = typesInDb.Where(iT => iT.UUId == json.TypeUUId).FirstOrDefault();

                        entityInDb.InventoryId = inventoryInDb.Id;
                        entityInDb.IsDisabled = json.IsDisabled;
                        entityInDb.Name = json.Name;
                        entityInDb.UUId = json.UUId;
                        entityInDb.LastModified = json.LastModified;
                        entityInDb.Description = json.Description;
                        entityInDb.ItemInput = json.ItemInput;
                        entityInDb.ItemOutput = json.ItemOutput;
                        entityInDb.ItemStateId = itemState.Id;
                        entityInDb.ModelId = itemModel.Id;
                        entityInDb.PlaceId = itemPlace.Id;
                        entityInDb.TypeId = itemType.Id;

                        return entityInDb;
                    }
                );

            #endregion

            #endregion

            #region Entity Syncing (Upstream)

            var url = Application.Current.Resources["UrlAPI"].ToString();

            using (UnitOfWork unitOfWork = new UnitOfWork(new MIDbContext()))
            {
                var modelsToSend = isItemModelsSync
                .Where(x => x.Message == "localNewer" || x.Message == "localNewEntry")
                .Select(x => unitOfWork.ItemModels.GetItemModelFromInventoryWithBrand(((ItemModel)x.Result).Id));

                var brandsToSend = isItemBrandsSync
                .Where(x => x.Message == "localNewer" || x.Message == "localNewEntry")
                .Select(x => (ItemBrand)x.Result);

                var placesToSend = isItemPlacesSynced
                   .Where(x => x.Message == "localNewer" || x.Message == "localNewEntry")
                   .Select(x => (ItemPlace)x.Result);

                var statesToSend = isItemStatesSynced
                    .Where(x => x.Message == "localNewer" || x.Message == "localNewEntry")
                    .Select(x => (ItemState)x.Result);

                var typesToSend = isItemTypesSynced
                    .Where(x => x.Message == "localNewer" || x.Message == "localNewEntry")
                    .Select(x => (ItemType)x.Result);

                var itemsToSend = isItemsSync
                    .Where(x => x.Message == "localNewer" || x.Message == "localNewEntry")
                    .Select(x => unitOfWork.Items.GetCompleteItemInformation(((Item)x.Result).Id));

                JsonInventoryFull dataToSend = new JsonInventoryFull();

                dataToSend.Inventory = inventoryFull.Inventory;
                dataToSend.ItemModels = modelsToSend
                    .Select(
                        iM =>
                            new JsonItemModel()
                            {
                                BrandUUId = iM.Brand.UUId,
                                InventoryUUId = inventoryInDb.UUId,
                                UUId = iM.UUId,
                                LastModified = iM.LastModified,
                                IsDisabled = iM.IsDisabled,
                                Name = iM.Name
                            }
                        )
                    .ToList();

                dataToSend.ItemBrands = brandsToSend
                        .Select(
                            iB =>
                               new JsonItemBrand()
                               {
                                   InventoryUUId = inventoryInDb.UUId,
                                   IsDisabled = iB.IsDisabled,
                                   UUId = iB.UUId,
                                   Name = iB.Name,
                                   LastModified = iB.LastModified
                               }
                            )
                        .ToList();

                dataToSend.ItemPlaces = placesToSend
                        .Select(
                            iP =>
                                new JsonItemPlace()
                                {
                                    InventoryUUId = inventoryInDb.UUId,
                                    IsDisabled = iP.IsDisabled,
                                    LastModified = iP.LastModified,
                                    Name = iP.Name,
                                    UUId = iP.UUId
                                }
                            )
                        .ToList();

                dataToSend.ItemStates = statesToSend
                        .Select(
                            iS =>
                                new JsonItemState()
                                {
                                    InventoryUUId = inventoryInDb.UUId,
                                    IsDisabled = iS.IsDisabled,
                                    LastModified = iS.LastModified,
                                    State = iS.State,
                                    UUId = iS.UUId
                                }
                            )
                        .ToList();

                dataToSend.ItemTypes = typesToSend
                        .Select(
                            iT =>
                                new JsonItemType()
                                {
                                    InventoryUUId = inventoryInDb.UUId,
                                    IsDisabled = iT.IsDisabled,
                                    LastModified = iT.LastModified,
                                    Name = iT.Name,
                                    UUId = iT.UUId
                                }
                            )
                        .ToList();

                dataToSend.Items = itemsToSend
                        .Select(
                            item =>
                                new JsonItem()
                                {
                                    BrandUUId = item.Model.Brand.UUId,
                                    Description = item.Description,
                                    InventoryUUId = inventoryInDb.UUId,
                                    UUId = item.UUId,
                                    TypeUUId = item.Type.UUId,
                                    IsDisabled = item.IsDisabled,
                                    ItemInput = item.ItemInput,
                                    ItemOutput = item.ItemOutput,
                                    ItemStateUUId = item.ItemState.UUId,
                                    LastModified = item.LastModified,
                                    ModelUUId = item.Model.UUId,
                                    PlaceUUId = item.Place.UUId,
                                    Name = item.Name
                                }
                            )
                        .ToList();

                ApiService apiService = new ApiService();

                var response =
                    await apiService.PostAsync<Response>(
                        url,
                        new string[]
                        {
                        "api",
                        "Inventories",
                        "SyncInformationToInventory",
                        MainViewModel.GetInstance().CurrentInventoryGuid.ToString()
                        },
                        "bearer",
                        MainViewModel.GetInstance().Token.Token,
                        dataToSend
                    );

                if (response.IsSuccess)
                {
                    return new Response()
                    {
                        IsSuccess = true,
                        Message = "Sync completed"
                    };
                }
                else
                {
                    return new Response()
                    {
                        IsSuccess = true,
                        Message = "Sync local complete, remote failed"
                    };
                }
            }
            
            #endregion
        }

        /// <summary>
        /// Syncs a list of inventories with the database and the current user
        /// </summary>
        /// <param name="jsonInventories"></param>
        /// <returns></returns>
        public async Task<List<Response>> SyncInventoriesAsync(IEnumerable<JsonInventory> jsonInventories)
        {
            var dbContext = MainViewModel.GetInstance().LocalDatabase;

            List<Response> response = new List<Response>();
            foreach (var jsonInventory in jsonInventories)
            {
                try
                {
                    var entityInDb = await dbContext
                        .Set<Inventory>()
                        .Where(entity => entity.UUId == jsonInventory.UUId)
                        .FirstOrDefaultAsync();

                    #region Diff between Remote and Local

                    if (entityInDb != null)
                    {
                        // If remote is newer
                        if (jsonInventory.LastModified.Value.CompareTo(entityInDb.LastModified.Value) >= 0)
                        {
                            entityInDb.Name = jsonInventory.Name;
                            entityInDb.LastModified = jsonInventory.LastModified;
                            entityInDb.IsDisabled = jsonInventory.IsDisabled;
                            entityInDb.UUId = jsonInventory.UUId;

                            dbContext.Entry<Inventory>(entityInDb).CurrentValues.SetValues(
                                entityInDb
                            );

                            dbContext.Entry<Inventory>(entityInDb).State = EntityState.Modified;

                            await dbContext.SaveChangesAsync();

                            response.Add(
                                new Response()
                                {
                                    IsSuccess = true,
                                    Message = "localOlder",
                                    Result = entityInDb
                                }
                            );

                            continue;
                        }
                        // If local is newer
                        else
                        {
                            response.Add(
                                new Response()
                                {
                                    IsSuccess = true,
                                    Message = "localNewer",
                                    Result = entityInDb
                                }
                            );

                            continue;
                        }
                    }

                    #endregion

                    // If remote is new

                    var newInventory = new Inventory()
                    {
                        Name = jsonInventory.Name,
                        IsDisabled = jsonInventory.IsDisabled,
                        LastModified = jsonInventory.LastModified,
                        UUId = jsonInventory.UUId
                    };

                    await dbContext.AddAsync(
                        newInventory
                    );

                    await dbContext.SaveChangesAsync();

                    var currentUser = await dbContext.Users.FirstOrDefaultAsync(x => x.UserName == MainViewModel.GetInstance().CurrentUserEmail);
                    await dbContext.AddAsync(
                        new UserInventory()
                        {
                            UserId = currentUser.Id,
                            InventoryId = newInventory.Id
                        }
                    );

                    await dbContext.SaveChangesAsync();

                    continue;
                }
                catch (Exception ex)
                {
                    // If it fails
                    response.Add(
                        new Response()
                        {
                            IsSuccess = false,
                            Message = $"Unexpected error in {jsonInventory.UUId}: {ex.Message}"
                        }
                    );
                }
            }

            // Get non existing items in local to send to remote
            try
            {
                var entriesToDisable = dbContext
                    .Set<Inventory>()
                    .Where(iS => !jsonInventories
                    .Any(jiS => iS.UUId == jiS.UUId));

                foreach (var entryToDisable in entriesToDisable)
                {
                    var inventoriesToDisable = dbContext.UserInventories
                        .Include(x => x.Inventory)
                        .Include(x => x.User)
                        .Where(x => x.User.UserName == MainViewModel.GetInstance().CurrentUserEmail &&
                                    entriesToDisable.Any(y => y.UUId == x.Inventory.UUId));

                    foreach (var inventory in inventoriesToDisable)
                    {
                        dbContext.UserInventories.Remove(inventory);

                        await dbContext.SaveChangesAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                response.Add(
                    new Response()
                    {
                        IsSuccess = false,
                        Message = "Error getting local diff.",
                        Result = ex
                    }
                );
            }

            return response;
        }

        public async Task<Response> SyncSingleInventoryAsync(JsonInventory jsonInventory)
        {
            var dbContext = MainViewModel.GetInstance().LocalDatabase;

            try
            {
                var inventoryInDB = await dbContext
                    .Set<Inventory>()
                    .Where(i => i.UUId == jsonInventory.UUId)
                    .FirstOrDefaultAsync();

                if (inventoryInDB != null)
                {
                    if (jsonInventory.LastModified.Value.CompareTo(inventoryInDB.LastModified.Value) >= 0)
                    {
                        inventoryInDB.UUId = jsonInventory.UUId;
                        inventoryInDB.Name = jsonInventory.Name;
                        inventoryInDB.IsDisabled = jsonInventory.IsDisabled;
                        inventoryInDB.LastModified = jsonInventory.LastModified;

                        dbContext.Entry<Inventory>(inventoryInDB).CurrentValues.SetValues(
                            inventoryInDB
                        );

                        dbContext.Entry<Inventory>(inventoryInDB).State = EntityState.Modified;

                        await dbContext.SaveChangesAsync();
                    }
                }
                else
                {
                    inventoryInDB = 
                        new Inventory()
                        {
                            UUId = jsonInventory.UUId,
                            Name = jsonInventory.Name,
                            LastModified = jsonInventory.LastModified,
                            IsDisabled = jsonInventory.IsDisabled
                        };

                    await dbContext.AddAsync(
                        inventoryInDB
                    );

                    var user = dbContext.Users.FirstOrDefault(x => x.UserName == MainViewModel.GetInstance().CurrentUserEmail);
                    await dbContext.UserInventories.AddAsync(
                        new UserInventory()
                        {
                            InventoryId = inventoryInDB.Id,
                            UserId = user.Id
                        }
                    );

                    await dbContext.SaveChangesAsync();
                }

                return new Response()
                {
                    IsSuccess = true,
                    Message = "Inventory synchronization completed.",
                    Result = inventoryInDB
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Syncs the inventory entity with the database.
        /// </summary>
        /// <typeparam name="TJSONModel">JSON Model Type</typeparam>
        /// <typeparam name="TEntityModel">Database Model Type</typeparam>
        /// <param name="inventoryUUID">Inventory UUID it belongs to</param>
        /// <param name="jsonEntities">Json Entities collection</param>
        /// <param name="modelOnCreate">Model to return/insert (Function)</param>
        /// <param name="modelOnUpdate">Model to return/update (Function)</param>
        /// <returns></returns>
        private async Task<List<Response>> SyncInventoryEntitiesAsync<TJSONModel, TEntityModel>(
            string inventoryUUID,
            ICollection<TJSONModel> jsonEntities,
            Func<TJSONModel, TEntityModel> modelOnCreate,
            Func<TJSONModel, TEntityModel, TEntityModel> modelOnUpdate)
            
            where TJSONModel : class, IJSONInventoryEntity 
            where TEntityModel : class, IInventoryEntity
        {
            List<Response> response = new List<Response>();
            var dbContext = MainViewModel.GetInstance().LocalDatabase;

            foreach (var jsonEntity in jsonEntities)
            {
                try
                {
                    var entityInDb = await dbContext
                        .Set<TEntityModel>()
                        .Where(entity => entity.UUId == jsonEntity.UUId)
                        .FirstOrDefaultAsync();

                    #region Diff between Remote and Local

                    if (entityInDb != null)
                    {
                        // If remote is newer
                        if (jsonEntity.LastModified.Value.CompareTo(entityInDb.LastModified.Value) >= 0)
                        {
                            dbContext.Entry<TEntityModel>(entityInDb).CurrentValues.SetValues(
                                modelOnUpdate(jsonEntity, entityInDb)
                            );

                            dbContext.Entry<TEntityModel>(entityInDb).State = EntityState.Modified;

                            await dbContext.SaveChangesAsync();

                            response.Add(
                                new Response()
                                {
                                    IsSuccess = true,
                                    Message = "localOlder",
                                    Result = entityInDb
                                }
                            );

                            continue;
                        }
                        // If local is newer
                        else
                        {
                            response.Add(
                                new Response()
                                {
                                    IsSuccess = true,
                                    Message = "localNewer",
                                    Result = entityInDb
                                }
                            );

                            continue;
                        }
                    }

                    #endregion
                        
                    // If remote is new
                    var inventory = await dbContext
                        .Set<Inventory>()
                        .Where(inv => inv.UUId == jsonEntity.InventoryUUId)
                        .FirstOrDefaultAsync();

                    if (inventory != null)
                    {
                        await dbContext.AddAsync(
                            modelOnCreate(jsonEntity)
                        );

                        await dbContext.SaveChangesAsync();

                        continue;
                    }

                    response.Add(
                        new Response()
                        {
                            IsSuccess = false,
                            Message = $"Inventory could not be found. {jsonEntity.InventoryUUId}"
                        }
                    );
                }
                catch (Exception ex)
                {
                    // If it fails
                    response.Add(
                        new Response()
                        {
                            IsSuccess = false,
                            Message = $"Unexpected error in {jsonEntity.UUId}: {ex.Message}"
                        }
                    );
                }
            }

            // Get non existing items in local to send to remote
            try
            {
                var inventoryGUID = Guid.Parse(inventoryUUID);
                var entriesToSend = dbContext
                    .Set<TEntityModel>()
                    .Where(iS => !jsonEntities
                    .Any(jiS => iS.UUId == jiS.UUId)
                                && iS.Inventory.UUId == inventoryGUID);



                foreach (var entryToSend in entriesToSend)
                {
                    response.Add(
                        new Response()
                        {
                            IsSuccess = true,
                            Message = "localNewEntry",
                            Result = entryToSend
                        }
                    );
                }
            }
            catch (Exception ex)
            {
                response.Add(
                    new Response()
                    {
                        IsSuccess = false,
                        Message = "Error getting local diff.",
                        Result = ex
                    }
                );
            }

            return response;
        }
    }
}
