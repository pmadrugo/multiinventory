﻿using MI.Common.JsonModels.Inventory;
using System;
using System.Collections.Generic;
using System.Text;

namespace MI.UIForms.ViewModels.Objects
{
    public class JsonItemModelWBrandViewModel : JsonItemModel
    {
        public string Brand { get; set; }
    }
}
