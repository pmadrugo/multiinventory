﻿using MI.Common.JsonModels.Inventory;
using MI.UIForms.Data.Entities.InventorySection;
using System;
using System.Collections.Generic;
using System.Text;

namespace MI.UIForms.ViewModels.Objects
{
    public class JsonItemViewModel : JsonItem
    {
        public string Brand { get; set; }

        public string Model { get; set; }

        public string State { get; set; }

        public string Type { get; set; }

        public string Place { get; set; }
    }
}
