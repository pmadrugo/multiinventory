﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Services;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.ViewModels.Objects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory.Items
{
    public class OutputItemViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private ApiService _apiService;
        private NetService _netService;

        private IUnitOfWork _unitOfWork;

        #endregion

        #region Properties

        private bool _isRunning;
        public bool IsRunning
        {
            get => this._isRunning;
            set => this.SetValue(ref this._isRunning, value);
        }

        private bool _isEnabled;
        public bool IsEnabled
        {
            get => this._isEnabled;
            set => this.SetValue(ref this._isEnabled, value);
        }

        private DateTime _outputDate;
        public DateTime OutputDate
        {
            get => this._outputDate;
            set => this.SetValue(ref _outputDate, value);
        }

        #endregion

        #region States

        private JsonItemState _selectedState;
        public JsonItemState SelectedState
        {
            get => _selectedState;
            set => SetValue(ref _selectedState, value);
        }

        private ObservableCollection<JsonItemState> _availableStates;
        public ObservableCollection<JsonItemState> AvailableStates
        {
            get => _availableStates;
            set => SetValue(ref _availableStates, value);
        }

        #endregion

        public JsonItemViewModel CurrentItem { get; set; }

        #region Constructors

        public OutputItemViewModel(JsonItemViewModel currentItem)
        {
            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            _apiService = new ApiService();
            _netService = new NetService();

            this.CurrentItem = currentItem;

            IsEnabled = false;
            IsRunning = true;

            LoadPickers();
        }

        #endregion

        #region Commands

        public ICommand SaveCommand => new RelayCommand(
            async () =>
            {
                if (!await ModelVerification())
                    return;

                IsRunning = true;
                IsEnabled = false;

                CurrentItem.LastModified = DateTime.Now;

                CurrentItem.ItemStateUUId = SelectedState.UUId;

                CurrentItem.ItemOutput = this.OutputDate;

                var connection = await _netService.CheckConnection();
                if (connection.IsSuccess)
                {
                    var url = Application.Current.Resources["UrlAPI"].ToString();
                    var response = await _apiService.PutAsync<JsonItemViewModel>(
                        url,
                        new string[]
                        {
                            "api",
                            MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                            "Items",
                            CurrentItem.UUId.ToString()
                        },
                        "bearer",
                        MainViewModel.GetInstance().Token.Token,
                        CurrentItem);

                    if (response.IsSuccess)
                    {
                        var entityToSave = response.Result as JsonItemViewModel;
                        if (entityToSave != null)
                        {
                            if (await SaveEntityToDB(entityToSave))
                            {
                                RemoveEntityFromMobileList(entityToSave);

                                var page = Application.Current.MainPage;
                                await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                                return;
                            }
                        }
                    }
                }
                else
                {
                    var entityToSave = this.CurrentItem;
                    if (await SaveEntityToDB(entityToSave))
                    {
                        RemoveEntityFromMobileList(entityToSave);

                        var page = Application.Current.MainPage;
                        await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                        return;
                    }
                }

                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "Ocorreu um erro a guardar os dados localmente",
                    "OK"
                );

                IsRunning = false;
                IsEnabled = true;
            }
        );

        #endregion

        #region Methods

        /// <summary>
        /// Loads the data used for the pickers
        /// </summary>
        public void LoadPickers()
        {
            this.AvailableStates = new ObservableCollection<JsonItemState>(GetAvailableStates());

            this.OutputDate = DateTime.Now;

            IsEnabled = true;
            IsRunning = false;
        }

        /// <summary>
        /// Gets the list of available state choices
        /// </summary>
        /// <returns></returns>
        private IEnumerable<JsonItemState> GetAvailableStates()
        {
            var statesInDb = _unitOfWork.ItemsStates.GetItemStatesFromInventory(MainViewModel.GetInstance().CurrentInventoryId);

            return statesInDb.Select(
                x => new JsonItemState()
                {
                    UUId = x.UUId,
                    InventoryUUId = x.Inventory.UUId,
                    IsDisabled = x.IsDisabled,
                    LastModified = x.LastModified,
                    State = x.State
                }
            );
        }

        /// <summary>
        /// Mobile equivalent of ModelState.IsValid
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ModelVerification()
        {
            if (SelectedState == null)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "A saída tem de ter um Estado atribuído.",
                    "OK"
                );

                return false;
            }

            return true;
        }

        /// <summary>
        /// Saves the current entity to the Database (Update/Output variant)
        /// </summary>
        /// <param name="itemToSave"></param>
        /// <returns></returns>
        private async Task<bool> SaveEntityToDB(JsonItemViewModel itemToSave)
        {
            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(MainViewModel.GetInstance().CurrentInventoryGuid.ToString());
            if (inventory != null)
            {
                var state = await _unitOfWork.ItemsStates.GetByUUIDAsync(SelectedState.UUId.ToString());
                if (state != null)
                {
                    var item = await _unitOfWork.Items.GetByUUIDAsync(CurrentItem.UUId.ToString());
                    if (item != null)
                    {
                        item.Name = this.CurrentItem.Name;
                        item.Description = this.CurrentItem.Description;

                        item.ItemStateId = state.Id;
                        item.ItemOutput = this.OutputDate;

                        item.LastModified = this.CurrentItem.LastModified;

                        var isSaved = await _unitOfWork.Items.UpdateAsync(item);
                        if (isSaved.IsSuccess)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Removes the entity from the previous page list
        /// </summary>
        /// <param name="entityToSend"></param>
        private void RemoveEntityFromMobileList(JsonItemViewModel entityToRemove)
        {
            var itemsInList = MainViewModel.GetInstance().ItemsVM.Items.ToList();

            var previousItem = itemsInList.FirstOrDefault(x => x.UUId == entityToRemove.UUId);
            if (previousItem != null)
            {
                itemsInList.Remove(previousItem);

                MainViewModel.GetInstance().ItemsVM.Items =
                    new ObservableCollection<JsonItemViewModel>(itemsInList.OrderBy(x => x.Name));
            }
        }

        #endregion
    }
}
