﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Services;
using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.ViewModels.Inventory.Items;
using MI.UIForms.ViewModels.Objects;
using MI.UIForms.Views.Inventory.Items;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory
{
    public class ItemsViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private NetService _netService;
        private ApiService _apiService;
        private IUnitOfWork _unitOfWork;

        #endregion

        #region Properties

        private ObservableCollection<JsonItemViewModel> _items;
        public ObservableCollection<JsonItemViewModel> Items
        {
            get => _items;
            set => SetValue(ref _items, value);
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetValue(ref _isRefreshing, value);
        }

        private bool _isLoaded;
        public bool IsLoaded
        {
            get => _isLoaded;
            set => SetValue(ref _isLoaded, value);
        }

        private List<Item> _activeItems;
        public List<Item> ActiveItems
        {
            get => _activeItems;
            set => SetValue(ref _activeItems, value);
        }

        private List<Item> _nonActiveItems;
        public List<Item> NonActiveItems
        {
            get => _nonActiveItems;
            set => SetValue(ref _nonActiveItems, value);
        }

        #endregion

        #region Constructors

        public ItemsViewModel()
        {
            _apiService = new ApiService();
            _netService = new NetService();
            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            IsRefreshing = true;
            IsLoaded = false;

            LoadItems();
        }

        #endregion

        #region Commands

        public ICommand ItemAddCommand
        {
            get => new RelayCommand(
                async () =>
                {
                    MainViewModel.GetInstance().AddItemsVM = new AddItemsViewModel();
                    await App.Navigator.PushAsync(new AddItemsPage());
                }
            );
        }

        public ICommand ItemEditCommand
        {
            get => new RelayCommand<JsonItemViewModel>(
                (selectedItem) =>
                {
                    var editPage = new EditItemsPage();
                    MainViewModel.GetInstance().EditItemsVM = new EditItemsViewModel(selectedItem, editPage);
                    
                }
            );
        }

        public ICommand ItemDetailsCommand
        {
            get => new RelayCommand<JsonItemViewModel>(
                async (selectedItem) =>
                {
                    MainViewModel.GetInstance().ItemDetailsVM = new ItemDetailsViewModel(selectedItem);
                    await App.Navigator.PushAsync(new ItemDetailsPage());
                }
            );
        }

        public ICommand ItemDeleteCommand
        {
            get => new RelayCommand<JsonItemViewModel>(
                async (selectedItem) =>
                {
                    var wantsToDelete = !await Application.Current.MainPage.DisplayAlert(
                        "Confirmação",
                        $"Deseja apagar {selectedItem.Name}?",
                        "Não",
                        "Sim"
                    );

                    if (wantsToDelete)
                    {
                        IsRefreshing = true;
                        IsLoaded = false;

                        var connection = await _netService.CheckConnection();
                        if (connection.IsSuccess)
                        {
                            selectedItem.LastModified = DateTime.Now;

                            var url = Application.Current.Resources["UrlAPI"].ToString();
                            var response = await _apiService.DeleteAsync<JsonItemViewModel>(
                                url,
                                new string[]
                                {
                                    "api",
                                    MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                                    "Items",
                                    selectedItem.UUId.ToString()
                                },
                                "bearer",
                                MainViewModel.GetInstance().Token.Token,
                                selectedItem
                            );

                            if (response.IsSuccess)
                            {
                                if (await SaveEntityToDB(selectedItem))
                                {
                                    RemoveEntityFromMobileList(selectedItem);

                                    IsRefreshing = false;
                                    IsLoaded = true;

                                    return;
                                }
                            }
                        }
                        else
                        {
                            if (await SaveEntityToDB(selectedItem))
                            {
                                RemoveEntityFromMobileList(selectedItem);

                                IsRefreshing = false;
                                IsLoaded = true;

                                return;
                            }
                        }

                        await Application.Current.MainPage.DisplayAlert(
                            "Erro",
                            "Ocorreu um erro a guardar os dados localmente",
                            "OK"
                        );

                        IsRefreshing = false;
                        IsLoaded = true;
                    }
                }
            );
        }

        // Set Item as Non Active
        public ICommand ItemNonActiveCommand
        {
            get => new RelayCommand<JsonItemViewModel>(
                async (selectedItem) =>
                {
                    MainViewModel.GetInstance().OutputItemVM = new OutputItemViewModel(selectedItem);
                    await App.Navigator.PushAsync(new OutputItemPage());
                }
            );
        }

        // Go to Non Active Items Page
        public ICommand NonActiveItemsCommand
        {
            get => new RelayCommand(
                async () =>
                {
                    MainViewModel.GetInstance().NonActiveItemsVM = new NonActiveItemsViewModel();
                    await App.Navigator.PushAsync(new NonActiveItemsPage());
                }
            );
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves the current entity to the Database (Delete/Disable variant)
        /// </summary>
        /// <param name="entityToRemove"></param>
        /// <returns></returns>
        private async Task<bool> SaveEntityToDB(JsonItemViewModel entityToRemove)
        {
            var entityInDb = await _unitOfWork.Items.GetByUUIDAsync(entityToRemove.UUId.ToString());
            if (entityInDb != null)
            {
                var isRemoved = await _unitOfWork.Items.DisableAsync(entityInDb);
                if (isRemoved.IsSuccess)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes the entity from the list
        /// </summary>
        /// <param name="entityToRemove"></param>
        private void RemoveEntityFromMobileList(JsonItemViewModel entityToRemove)
        {
            var itemsInList = MainViewModel.GetInstance().ItemsVM.Items.ToList();

            var previousItem = itemsInList.FirstOrDefault(x => x.UUId == entityToRemove.UUId);
            if (previousItem != null)
            {
                itemsInList.Remove(previousItem);

                MainViewModel.GetInstance().ItemsVM.Items =
                    new ObservableCollection<JsonItemViewModel>(itemsInList.OrderBy(x => x.Name));
            }
        }

        /// <summary>
        /// Syncs the database remotely if it has internet connection, otherwise loads locally
        /// (Sync is used due to possible dependency/foreign key issues)
        /// </summary>
        private async void LoadItems()
        {
            try
            {
                await MainViewModel.GetInstance().SyncAsync();
            }
            catch (Exception ex)
            {

            }

            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            ActiveItems = _unitOfWork.Items.GetActiveItemsFromInventory(MainViewModel.GetInstance().CurrentInventoryId).ToList();

            RefreshItemList(
                ActiveItems.Select(x =>
                    new JsonItemViewModel()
                    {
                        Brand = x.Model.Brand.Name,
                        BrandUUId = x.Model.Brand.UUId,
                        Model = x.Model.Name,
                        ModelUUId = x.Model.UUId,
                        Name = x.Name,
                        IsDisabled = x.IsDisabled,
                        ItemInput = x.ItemInput,
                        Description = x.Description,
                        ItemOutput = x.ItemOutput,
                        LastModified = x.LastModified,
                        State = x.ItemState.State,
                        ItemStateUUId = x.ItemState.UUId,
                        Type = x.Type.Name,
                        TypeUUId = x.Type.UUId,
                        UUId = x.UUId,
                        Place = x.Place.Name,
                        PlaceUUId = x.Place.UUId,
                        InventoryUUId = x.Inventory.UUId
                    }
                )
            );

            IsRefreshing = false;
            IsLoaded = true;
        }

        /// <summary>
        /// Refreshes the list
        /// </summary>
        /// <param name="itemsList"></param>
        private void RefreshItemList(IEnumerable<JsonItemViewModel> itemsList)
        {
            IsRefreshing = true;

            this.Items = new ObservableCollection<JsonItemViewModel>(itemsList.OrderBy(x => x.Name).ToList());

            IsRefreshing = false;
        }

        #endregion
    }
}
