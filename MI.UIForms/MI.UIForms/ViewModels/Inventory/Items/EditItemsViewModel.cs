﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Services;
using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.ViewModels.Objects;
using MI.UIForms.Views.Inventory.Items;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory.Items
{
    public class EditItemsViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private ApiService _apiService;
        private NetService _netService;

        private IUnitOfWork _unitOfWork;

        private EditItemsPage _editItemsPage;

        #endregion

        #region Properties

        private bool _isRunning;
        public bool IsRunning
        {
            get => this._isRunning;
            set => this.SetValue(ref this._isRunning, value);
        }

        private bool _isEnabled;
        public bool IsEnabled
        {
            get => this._isEnabled;
            set => this.SetValue(ref this._isEnabled, value);
        }

        #region Brands

        private JsonItemBrand _selectedBrand;
        public JsonItemBrand SelectedBrand
        {
            get => _selectedBrand;
            set
            {
                SetValue(ref _selectedBrand, value);
                this.AvailableModels = new ObservableCollection<JsonItemModel>(GetAvailableModels());
            }
        }

        private ObservableCollection<JsonItemBrand> _availableBrands;
        public ObservableCollection<JsonItemBrand> AvailableBrands
        {
            get => _availableBrands;
            set => SetValue(ref _availableBrands, value);
        }

        #endregion

        #region Models

        private JsonItemModel _selectedModel;
        public JsonItemModel SelectedModel
        {
            get => _selectedModel;
            set => SetValue(ref _selectedModel, value);
        }

        private ObservableCollection<JsonItemModel> _availableModels;
        public ObservableCollection<JsonItemModel> AvailableModels
        {
            get => _availableModels;
            set => SetValue(ref _availableModels, value);
        }

        #endregion

        #region States

        private JsonItemState _selectedState;
        public JsonItemState SelectedState
        {
            get => _selectedState;
            set => SetValue(ref _selectedState, value);
        }

        private ObservableCollection<JsonItemState> _availableStates;
        public ObservableCollection<JsonItemState> AvailableStates
        {
            get => _availableStates;
            set => SetValue(ref _availableStates, value);
        }

        #endregion

        #region Types

        private JsonItemType _selectedType;
        public JsonItemType SelectedType
        {
            get => _selectedType;
            set => SetValue(ref _selectedType, value);
        }

        private ObservableCollection<JsonItemType> _availableTypes;
        public ObservableCollection<JsonItemType> AvailableTypes
        {
            get => _availableTypes;
            set => SetValue(ref _availableTypes, value);
        }

        #endregion

        #region Places

        private JsonItemPlace _selectedPlace;
        public JsonItemPlace SelectedPlace
        {
            get => _selectedPlace;
            set => SetValue(ref _selectedPlace, value);
        }

        private ObservableCollection<JsonItemPlace> _availablePlaces;
        public ObservableCollection<JsonItemPlace> AvailablePlaces
        {
            get => _availablePlaces;
            set => SetValue(ref _availablePlaces, value);
        }

        #endregion

        public JsonItemViewModel CurrentItem { get; set; }

        #endregion

        #region Constructors

        public EditItemsViewModel(JsonItemViewModel currentItem, EditItemsPage editItemsPage)
        {
            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            _apiService = new ApiService();
            _netService = new NetService();

            this.CurrentItem = currentItem;

            IsEnabled = false;
            IsRunning = true;

            // Workaround for cascading picker
            _editItemsPage = editItemsPage;
            ((Grid)((ScrollView)_editItemsPage.Content).Content).BindingContext = this;

            LoadPickers();
        }

        #endregion

        #region Commands

        public ICommand SaveCommand => new RelayCommand(
            async () =>
            {
                if (!await ModelVerification())
                    return;

                IsRunning = true;
                IsEnabled = false;

                CurrentItem.LastModified = DateTime.Now;
                CurrentItem.BrandUUId = SelectedBrand.UUId;
                CurrentItem.ModelUUId = SelectedModel.UUId;
                CurrentItem.PlaceUUId = SelectedPlace.UUId;
                CurrentItem.TypeUUId = SelectedType.UUId;
                CurrentItem.ItemStateUUId = SelectedState.UUId;

                var connection = await _netService.CheckConnection();
                if (connection.IsSuccess)
                {
                    var url = Application.Current.Resources["UrlAPI"].ToString();
                    var response = await _apiService.PutAsync<JsonItemViewModel>(
                        url,
                        new string[]
                        {
                            "api",
                            MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                            "Items",
                            CurrentItem.UUId.ToString()
                        },
                        "bearer",
                        MainViewModel.GetInstance().Token.Token,
                        CurrentItem);

                    if (response.IsSuccess)
                    {
                        var entityToSave = response.Result as JsonItemViewModel;
                        if (entityToSave != null)
                        {
                            if (await SaveEntityToDB(entityToSave))
                            {
                                SendEntityToMobileList(entityToSave);

                                var page = Application.Current.MainPage;
                                await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                                return;
                            }
                        }
                    }
                }
                else
                {
                    var entityToSave = this.CurrentItem;
                    if (await SaveEntityToDB(entityToSave))
                    {
                        SendEntityToMobileList(entityToSave);

                        var page = Application.Current.MainPage;
                        await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                        return;
                    }
                }

                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "Ocorreu um erro a guardar os dados localmente",
                    "OK"
                );

                IsRunning = false;
                IsEnabled = true;
            }
        );

        #endregion

        #region Methods

        /// <summary>
        /// Loads the data used for the pickers
        /// </summary>
        public void LoadPickers()
        {
            this.AvailableBrands = new ObservableCollection<JsonItemBrand>(GetAvailableBrands());
            this.AvailablePlaces = new ObservableCollection<JsonItemPlace>(GetAvailablePlaces());
            this.AvailableStates = new ObservableCollection<JsonItemState>(GetAvailableStates());
            this.AvailableTypes = new ObservableCollection<JsonItemType>(GetAvailableTypes());

            LoadCurrentOption();

            IsEnabled = true;
            IsRunning = false;
        }

        /// <summary>
        /// Loads the current specified options
        /// </summary>
        public void LoadCurrentOption()
        {
            // Specifies the main thread to force updating the values set through binding
            Device.BeginInvokeOnMainThread(
                () =>
                {
                    this.SelectedBrand = this.AvailableBrands.FirstOrDefault(x => x.UUId == this.CurrentItem.BrandUUId);
                    this.SelectedPlace = this.AvailablePlaces.FirstOrDefault(x => x.UUId == this.CurrentItem.PlaceUUId);
                    this.SelectedState = this.AvailableStates.FirstOrDefault(x => x.UUId == this.CurrentItem.ItemStateUUId);
                    this.SelectedType = this.AvailableTypes.FirstOrDefault(x => x.UUId == this.CurrentItem.TypeUUId);
                    this.SelectedModel = this.AvailableModels.FirstOrDefault(x => x.UUId == this.CurrentItem.ModelUUId);

                    StartPage();
                }
            );
        }

        /// <summary>
        /// Starts up the prepared page and points the picker to the selection ( WORKAROUND: Cascading Pickers)
        /// </summary>
        public async void StartPage()
        {
            await App.Navigator.PushAsync(_editItemsPage);

            var picker = 
                (Picker)((Grid)((ScrollView)_editItemsPage.Content).Content).Children
                    .FirstOrDefault(x => x.GetType() == typeof(Grid))
                    ?.FindByName("modelPicker");

            var selectedModel = ((IList<JsonItemModel>)picker.ItemsSource).FirstOrDefault(x => x.UUId == CurrentItem.ModelUUId);

            picker.SelectedItem = selectedModel;
            picker.SelectedIndex = ((IList<JsonItemModel>)picker.ItemsSource).IndexOf(selectedModel);
        }

        /// <summary>
        /// Gets the list of available brand choices
        /// </summary>
        /// <returns></returns>
        private IEnumerable<JsonItemBrand> GetAvailableBrands()
        {
            var brandsInDb = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(MainViewModel.GetInstance().CurrentInventoryId);

            return brandsInDb.Select(
                x => new JsonItemBrand()
                {
                    UUId = x.UUId,
                    InventoryUUId = x.Inventory.UUId,
                    IsDisabled = x.IsDisabled,
                    LastModified = x.LastModified,
                    Name = x.Name
                }
            );
        }

        /// <summary>
        /// Gets the list of available places choices
        /// </summary>
        /// <returns></returns>
        private IEnumerable<JsonItemPlace> GetAvailablePlaces()
        {
            var placesInDb = _unitOfWork.ItemPlaces.GetItemPlacesFromInventory(MainViewModel.GetInstance().CurrentInventoryId);

            return placesInDb.Select(
                x => new JsonItemPlace()
                {
                    UUId = x.UUId,
                    InventoryUUId = x.Inventory.UUId,
                    IsDisabled = x.IsDisabled,
                    LastModified = x.LastModified,
                    Name = x.Name
                }
            );
        }

        /// <summary>
        /// Gets the list of available state choices
        /// </summary>
        /// <returns></returns>
        private IEnumerable<JsonItemState> GetAvailableStates()
        {
            var statesInDb = _unitOfWork.ItemsStates.GetItemStatesFromInventory(MainViewModel.GetInstance().CurrentInventoryId);

            return statesInDb.Select(
                x => new JsonItemState()
                {
                    UUId = x.UUId,
                    InventoryUUId = x.Inventory.UUId,
                    IsDisabled = x.IsDisabled,
                    LastModified = x.LastModified,
                    State = x.State
                }
            );
        }

        /// <summary>
        /// Gets the list of available types choices
        /// </summary>
        /// <returns></returns>
        private IEnumerable<JsonItemType> GetAvailableTypes()
        {
            var typesInDb = _unitOfWork.ItemTypes.GetItemTypesFromInventory(MainViewModel.GetInstance().CurrentInventoryId);

            return typesInDb.Select(
                x => new JsonItemType()
                {
                    InventoryUUId = x.Inventory.UUId,
                    UUId = x.UUId,
                    IsDisabled = x.IsDisabled,
                    LastModified = x.LastModified,
                    Name = x.Name
                }
            );
        }

        /// <summary>
        /// Gets the list of available model choices of a specified brand
        /// </summary>
        /// <returns></returns>
        private IEnumerable<JsonItemModel> GetAvailableModels()
        {
            if (SelectedBrand != null)
            {
                var modelsInDbFromBrand = _unitOfWork.ItemModels.GetItemModelsFromBrand(SelectedBrand.UUId);

                return modelsInDbFromBrand.Select(
                    x => new JsonItemModel()
                    {
                        UUId = x.UUId,
                        Name = x.Name,
                        LastModified = x.LastModified,
                        IsDisabled = x.IsDisabled,
                        InventoryUUId = x.Inventory.UUId,
                        BrandUUId = x.Brand.UUId
                    }
                );
            }

            return Enumerable.Empty<JsonItemModel>();
        }

        /// <summary>
        /// Mobile equivalent of ModelState.IsValid
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ModelVerification()
        {
            if (string.IsNullOrEmpty(this.CurrentItem.Name))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "O Item tem de ter um nome.",
                    "OK"
                );

                return false;
            }

            if (this.CurrentItem.Name.Length < 3 || this.CurrentItem.Name.Length >= 60)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "O nome do Item tem de ter entre 3 e 60 caractéres.",
                    "OK"
                );

                return false;
            }

            if (SelectedBrand == null)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "O Item tem de ter uma Marca atribuída.",
                    "OK"
                );

                return false;
            }

            if (SelectedModel == null)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "O Item tem de ter um Modelo atribuído.",
                    "OK"
                );

                return false;
            }

            if (SelectedType == null)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "O Item tem de ter uma Categoria atribuída.",
                    "OK"
                );

                return false;
            }

            if (SelectedState == null)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "O Item tem de ter um Estado atribuído.",
                    "OK"
                );

                return false;
            }

            if (SelectedPlace == null)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "O Item tem de ter uma Localização atribuída.",
                    "OK"
                );

                return false;
            }

            if (SelectedModel.BrandUUId != SelectedBrand.UUId)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "Certifique-se que o modelo corresponde à marca.",
                    "OK"
                );

                return false;
            }

            return true;
        }

        /// <summary>
        /// Saves the current entity to the Database (Update variant)
        /// </summary>
        /// <param name="itemToSave"></param>
        /// <returns></returns>
        private async Task<bool> SaveEntityToDB(JsonItemViewModel itemToSave)
        {
            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(MainViewModel.GetInstance().CurrentInventoryGuid.ToString());
            if (inventory != null)
            {
                var brand = await _unitOfWork.ItemBrands.GetByUUIDAsync(SelectedBrand.UUId.ToString());
                if (brand != null)
                {
                    var model = await _unitOfWork.ItemModels.GetByUUIDAsync(SelectedModel.UUId.ToString());
                    if (model != null)
                    {
                        var place = await _unitOfWork.ItemPlaces.GetByUUIDAsync(SelectedPlace.UUId.ToString());
                        if (place != null)
                        {
                            var type = await _unitOfWork.ItemTypes.GetByUUIDAsync(SelectedType.UUId.ToString());
                            if (type != null)
                            {
                                var state = await _unitOfWork.ItemsStates.GetByUUIDAsync(SelectedState.UUId.ToString());
                                if (state != null)
                                {
                                    var item = await _unitOfWork.Items.GetByUUIDAsync(CurrentItem.UUId.ToString());
                                    if (item != null)
                                    {
                                        item.Name = this.CurrentItem.Name;
                                        item.Description = this.CurrentItem.Description;

                                        item.ItemStateId = state.Id;
                                        item.TypeId = type.Id;
                                        item.PlaceId = place.Id;
                                        item.ModelId = model.Id;
                                        item.LastModified = this.CurrentItem.LastModified;

                                        var isSaved = await _unitOfWork.Items.UpdateAsync(item);
                                        if (isSaved.IsSuccess)
                                        {
                                            return true;
                                        }
                                    }                                    
                                }
                            }
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Updates the entity in the previous page list
        /// </summary>
        /// <param name="entityToSend"></param>
        private void SendEntityToMobileList(JsonItemViewModel entityToSend)
        {
            var itemsInList = MainViewModel.GetInstance().ItemsVM.Items.ToList();

            var previousItem = itemsInList.FirstOrDefault(x => x.UUId == entityToSend.UUId);
            if (previousItem != null)
            {
                itemsInList.Remove(previousItem);
                itemsInList.Add(
                    new JsonItemViewModel()
                    {
                        BrandUUId = entityToSend.BrandUUId,
                        InventoryUUId = entityToSend.InventoryUUId,
                        IsDisabled = entityToSend.IsDisabled,
                        LastModified = entityToSend.LastModified,
                        Name = entityToSend.Name,
                        UUId = entityToSend.UUId,
                        Brand = SelectedBrand.Name,
                        Description = entityToSend.Description,
                        ItemInput = entityToSend.ItemInput,
                        ItemStateUUId = entityToSend.ItemStateUUId,
                        Model = SelectedModel.Name,
                        ModelUUId = entityToSend.ModelUUId,
                        PlaceUUId = entityToSend.PlaceUUId,
                        TypeUUId = entityToSend.TypeUUId,
                        Place = SelectedPlace.Name,
                        State = SelectedState.State,
                        Type = SelectedType.Name
                    }
                );

                MainViewModel.GetInstance().ItemsVM.Items =
                    new ObservableCollection<JsonItemViewModel>(itemsInList.OrderBy(x => x.Name));
            }
        }

        #endregion
    }
}
