﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Services;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.ViewModels.Objects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace MI.UIForms.ViewModels.Inventory.Items
{
    public class ItemDetailsViewModel : BaseViewModel, INotifyPropertyChanged
    {

        #region Attributes
        private bool _isLoaded;
        private bool _isRefreshing;

        private JsonItemViewModel _item;

        #endregion

        #region Properties

        public JsonItemViewModel Item
        {
            get => this._item;
            set => this.SetValue(ref this._item,value);
        }

        public bool IsLoaded
        {
            get => this._isLoaded;
            set => this.SetValue(ref this._isLoaded, value);
        }
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetValue(ref _isRefreshing, value);
        }

        public ItemDetailsViewModel(JsonItemViewModel item)
        {
            this.Item = item;

            this.IsLoaded = true;
            this.IsRefreshing = false;
        }


        #endregion

       


    }
}
