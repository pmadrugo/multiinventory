﻿using GalaSoft.MvvmLight.Command;
using MI.Common.Services;
using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.ViewModels.Objects;
using MI.UIForms.Views.Inventory.Items;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace MI.UIForms.ViewModels.Inventory.Items
{
    public class NonActiveItemsViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private IUnitOfWork _unitOfWork;

        private bool _isRefreshing;
        private bool _isLoaded;

        #endregion

        #region Properties 

        private ObservableCollection<JsonItemViewModel> _items;
        public ObservableCollection<JsonItemViewModel> Items
        {
            get => _items;
            set => SetValue(ref _items, value);
        }

        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetValue(ref _isRefreshing, value);
        }
        public bool IsLoaded
        {
            get => _isLoaded;
            set => SetValue(ref _isLoaded, value);
        }

        #endregion

        #region Constructors

        public NonActiveItemsViewModel()
        {
            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            IsRefreshing = true;
            IsLoaded = false;

            LoadItems();
        }
        #endregion

        #region Methods

        /// <summary>
        /// Fetches from the local data which items are set as non active
        /// </summary>
        private void LoadItems()
        {

            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            var nonActiveItems = _unitOfWork
                .Items
                .GetNonActiveItemsFromInventory(MainViewModel.GetInstance().CurrentInventoryId)
                .OrderByDescending(x => x.ItemOutput)
                .ToList();

            RefreshItemList(
                nonActiveItems.Select(x =>
                  new JsonItemViewModel()
                  {
                      Brand = x.Model.Brand.Name,
                      BrandUUId = x.Model.Brand.UUId,
                      Model = x.Model.Name,
                      ModelUUId = x.Model.UUId,
                      Name = x.Name,
                      InventoryUUId = x.Inventory.UUId,
                      IsDisabled = x.IsDisabled,
                      ItemInput = x.ItemInput,
                      Description = x.Description,
                      ItemOutput = x.ItemOutput,
                      LastModified = x.LastModified,
                      State = x.ItemState.State,
                      ItemStateUUId = x.ItemState.UUId,
                      Type = x.Type.Name,
                      TypeUUId = x.Type.UUId,
                      UUId = x.UUId,
                      Place = x.Place.Name,
                      PlaceUUId = x.Place.UUId
                  }
              )
            );

            IsRefreshing = false;
            IsLoaded = true;
        }

        /// <summary>
        /// Refreshes the list
        /// </summary>
        /// <param name="itemsList"></param>
        private void RefreshItemList(IEnumerable<JsonItemViewModel> itemsList)
        {
            IsRefreshing = true;

            this.Items = new ObservableCollection<JsonItemViewModel>(itemsList.OrderBy(x => x.Name).ToList());

            IsRefreshing = false;
        }

        #endregion

        #region Commands

        public ICommand ItemDetailsCommand
        {
            get => new RelayCommand<JsonItemViewModel>(
                async (selectedItem) =>
                {
                    MainViewModel.GetInstance().ItemDetailsVM = new ItemDetailsViewModel(selectedItem);
                    await App.Navigator.PushAsync(new ItemDetailsPage());
                }
            );
        }

        #endregion
    }
}
