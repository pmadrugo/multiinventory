﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Services;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.ViewModels.Inventory.ItemStates;
using MI.UIForms.Views.Inventory.ItemStates;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory
{
    public class ItemStatesViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private NetService _netService;
        private ApiService _apiService;

        private IUnitOfWork _unitOfWork;

        #endregion

        #region Properties

        private ObservableCollection<JsonItemState> _itemStates;
        public ObservableCollection<JsonItemState> ItemStates
        {
            get => _itemStates;
            set => SetValue(ref _itemStates, value);
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetValue(ref _isRefreshing, value);
        }

        private bool _isLoaded;
        public bool IsLoaded
        {
            get => _isLoaded;
            set => SetValue(ref _isLoaded, value);
        }

        #endregion

        #region Commands

        public ICommand AddItemStateCommand
        {
            get => new RelayCommand(
                async () =>
                {
                    MainViewModel.GetInstance().AddItemStatesVM = new AddItemStatesViewModel();
                    await App.Navigator.PushAsync(new AddItemStatesPage());
                }
            );
        }

        public ICommand EditItemStateCommand
        {
            get => new RelayCommand<JsonItemState>(
                async (itemState) =>
                {
                    MainViewModel.GetInstance().EditItemsStatesVM = new EditItemsStatesViewModel(itemState);
                    await App.Navigator.PushAsync(new EditItemStatesPage());
                }
            );
        }

        public ICommand DeleteItemStateCommand
        {
            get => new RelayCommand<JsonItemState>(
                async (itemState) =>
                {
                    var wantsToDelete = !await Application.Current.MainPage.DisplayAlert(
                        "Confirmação",
                        $"Deseja apagar {itemState.State}?",
                        "Não",
                        "Sim"
                    );

                    if (wantsToDelete)
                    {
                        IsRefreshing = true;
                        IsLoaded = false;

                        var connection = await _netService.CheckConnection();
                        if (connection.IsSuccess)
                        {
                            itemState.LastModified = DateTime.Now;

                            var url = Application.Current.Resources["UrlAPI"].ToString();
                            var response = await _apiService.DeleteAsync<JsonItemState>(
                                url,
                                new string[]
                                {
                                    "api",
                                    MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                                    "ItemStates",
                                    itemState.UUId.ToString()
                                },
                                "bearer",
                                MainViewModel.GetInstance().Token.Token,
                                itemState
                            );

                            if (response.IsSuccess)
                            {
                                if (await SaveEntityToDB(itemState))
                                {
                                    RemoveEntityFromMobileList(itemState);

                                    IsRefreshing = false;
                                    IsLoaded = true;

                                    return;
                                }
                            }
                        }
                        else
                        {
                            if (await SaveEntityToDB(itemState))
                            {
                                RemoveEntityFromMobileList(itemState);

                                IsRefreshing = false;
                                IsLoaded = true;

                                return;
                            }
                        }

                        await Application.Current.MainPage.DisplayAlert(
                            "Erro",
                            "Ocorreu um erro a guardar os dados localmente",
                            "OK"
                        );

                        IsRefreshing = false;
                        IsLoaded = true;
                    }
                }
            );
        }

        #endregion

        #region Constructors

        public ItemStatesViewModel()
        {
            _apiService = new ApiService();
            _netService = new NetService();

            IsRefreshing = true;
            IsLoaded = false;

            LoadItemStates();
        }

        /// <summary>
        /// Loads the Item States remotely if it has internet connection, otherwise loads locally
        /// </summary>
        private async void LoadItemStates()
        {
            try
            {
                var connection = await this._netService.CheckConnection();
                if (connection.IsSuccess)
                {
                    var url = Application.Current.Resources["UrlAPI"].ToString();
                    var response = await _apiService.GetAsync<JsonItemState>(
                        url,
                        new string[]
                        {
                            "api",
                            MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                            "ItemStates"
                        },
                        "bearer",
                        MainViewModel.GetInstance().Token.Token);

                    if (response.IsSuccess)
                    {
                        RefreshList((List<JsonItemState>)response.Result);

                        IsRefreshing = false;
                        IsLoaded = true;

                        return;
                    }
                }

                _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

                var itemStates = _unitOfWork.ItemsStates.GetItemStatesFromInventory(MainViewModel.GetInstance().CurrentInventoryId);

                RefreshList(
                    itemStates.Select(x =>
                        new JsonItemState()
                        {
                            IsDisabled = x.IsDisabled,
                            LastModified = x.LastModified,
                            State = x.State,
                            UUId = x.UUId,
                            InventoryUUId = x.Inventory.UUId
                        }
                    )
                );

                IsRefreshing = false;
                IsLoaded = true;
            }
            catch (Exception ex)
            {
                await App.Navigator.DisplayAlert("Erro", ex.Message, "Ok");
            }
        }

        /// <summary>
        /// Refreshes the list
        /// </summary>
        /// <param name="itemStatesList"></param>
        public void RefreshList(IEnumerable<JsonItemState> itemStatesList)
        {
            IsRefreshing = true;

            this.ItemStates = new ObservableCollection<JsonItemState>(itemStatesList.OrderBy(x => x.State).ToList());

            IsRefreshing = false;
        }

        /// <summary>
        /// Saves the current entity to the Database (Delete/Disable variant)
        /// </summary>
        /// <param name="entityToRemove"></param>
        /// <returns></returns>
        private async Task<bool> SaveEntityToDB(JsonItemState entityToRemove)
        {
            var entityInDb = await _unitOfWork.ItemsStates.GetByUUIDAsync(entityToRemove.UUId.ToString());
            if (entityInDb != null)
            {
                var isRemoved = await _unitOfWork.ItemsStates.DisableAsync(entityInDb);
                if (isRemoved.IsSuccess)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes the entity from the list
        /// </summary>
        /// <param name="entityToRemove"></param>
        private void RemoveEntityFromMobileList(JsonItemState entityToRemove)
        {
            var itemsInList = MainViewModel.GetInstance().ItemStatesVM.ItemStates.ToList();

            var previousItem = itemsInList.FirstOrDefault(x => x.UUId == entityToRemove.UUId);
            if (previousItem != null)
            {
                itemsInList.Remove(previousItem);

                MainViewModel.GetInstance().ItemStatesVM.ItemStates =
                    new ObservableCollection<JsonItemState>(itemsInList.OrderBy(x => x.State));
            }
        }

        #endregion
    }
}
