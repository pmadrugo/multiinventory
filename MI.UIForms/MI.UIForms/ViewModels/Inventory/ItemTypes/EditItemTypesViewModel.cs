﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Models;
using MI.Common.Services;
using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory.ItemTypes
{
    public class EditItemTypesViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private bool _isRunning;
        private bool _isEnabled;

        private ApiService _apiService;
        private NetService _netService;

        private IUnitOfWork _unitOfWork;

        #endregion

        #region Properties

        public JsonItemType ItemType { get; set; }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetValue(ref this._isRunning, value);
        }

        public bool IsEnabled
        {
            get => _isEnabled;
            set => SetValue(ref this._isEnabled, value);
        }

        #endregion

        #region Constructor

        public EditItemTypesViewModel(JsonItemType itemType)
        {
            _apiService = new ApiService();
            _netService = new NetService();


            this.ItemType = itemType;
            
            IsEnabled = true;
        }

        #endregion

        #region Commands

        public ICommand SaveCommand => new RelayCommand(
            async () =>
            {
                if (!await ModelVerification())
                    return;

                IsRunning = true;
                IsEnabled = false;

                ItemType.LastModified = DateTime.Now;

                var connection = await _netService.CheckConnection();
                if (connection.IsSuccess)
                {
                    var url = Application.Current.Resources["UrlAPI"].ToString();
                    var response = await _apiService.PutAsync<JsonItemType>(
                        url,
                        new string[]
                        {
                            "api",
                            MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                            "ItemTypes",
                            ItemType.UUId.ToString()
                        },
                        "bearer",
                        MainViewModel.GetInstance().Token.Token,
                        ItemType
                    );

                    if (response.IsSuccess)
                    {
                        if (await SaveEntityToDB(ItemType))
                        {
                            SendEntityToMobileList(ItemType);

                            var page = Application.Current.MainPage;
                            await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                            return;
                        }
                    }
                }
                else
                {
                    if(await SaveEntityToDB(ItemType))
                    {
                        SendEntityToMobileList(ItemType);

                        var page = Application.Current.MainPage;
                        await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                        return;
                    }
                }

                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "Ocorreu um erro a guardar os dados localmente",
                    "OK"
                );

                IsRunning = false;
                IsEnabled = true;
            }
        );

        #endregion

        #region Methods

        /// <summary>
        /// Mobile equivalent of ModelState.IsValid
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ModelVerification()
        {
            if (string.IsNullOrEmpty(ItemType.Name))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "A Categoria tem de ter um nome.",
                    "OK"
                );

                return false;
            }

            if (this.ItemType.Name.Length < 3 || this.ItemType.Name.Length >= 60)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "A Categoria tem de ter entre 3 e 60 caractéres.",
                    "OK"
                );

                return false;
            }

            return true;
        }

        /// <summary>
        /// Saves the current entity to the Database (Update variant)
        /// </summary>
        /// <param name="itemToSave"></param>
        /// <returns></returns>
        private async Task<bool> SaveEntityToDB(JsonItemType entityToSave)
        {
            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            var entity = await _unitOfWork.ItemTypes.GetByUUIDAsync(entityToSave.UUId.ToString());
            if (entity != null)
            {
                entity.Name = entityToSave.Name;
                entity.LastModified = entityToSave.LastModified;
                entity.IsDisabled = entityToSave.IsDisabled;
                entity.UUId = entityToSave.UUId;

                await _unitOfWork.ItemTypes.UpdateAsync(entity);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Updates the entity in the previous page list
        /// </summary>
        /// <param name="entityToSend"></param>
        private void SendEntityToMobileList(JsonItemType entityToSend)
        {
            var itemsInList = MainViewModel.GetInstance().ItemTypesVM.ItemTypes.ToList();

            var previousItem = itemsInList.FirstOrDefault(x => x.UUId == entityToSend.UUId);
            if (previousItem != null)
            {
                itemsInList.Remove(previousItem);
                itemsInList.Add(entityToSend);

                MainViewModel.GetInstance().ItemTypesVM.ItemTypes =
                    new ObservableCollection<JsonItemType>(itemsInList.OrderBy(x => x.Name));
            }
        }

        #endregion
    }
}
