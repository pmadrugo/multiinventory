﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Services;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.ViewModels.Inventory.ItemTypes;
using MI.UIForms.Views.Inventory.ItemTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory
{
    public class ItemTypesViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private NetService _netService;
        private ApiService _apiService;

        private IUnitOfWork _unitOfWork;

        #endregion

        #region Properties

        private ObservableCollection<JsonItemType> _itemTypes;
        public ObservableCollection<JsonItemType> ItemTypes
        {
            get => _itemTypes;
            set => SetValue(ref _itemTypes, value);
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetValue(ref _isRefreshing, value);
        }

        private bool _isLoaded;
        public bool IsLoaded
        {
            get => _isLoaded;
            set => SetValue(ref _isLoaded, value);
        }

        #endregion

        #region Commands

        public ICommand AddItemTypeCommand
        {
            get => new RelayCommand(
                async () =>
                {
                    MainViewModel.GetInstance().AddItemTypesVM = new AddItemTypesViewModel();
                    await App.Navigator.PushAsync(new AddItemTypesPage());
                }
            );
        }

        public ICommand EditItemTypeCommand
        {
            get => new RelayCommand<JsonItemType>(
                async (itemType) =>
                {
                    MainViewModel.GetInstance().EditItemTypesVM = new EditItemTypesViewModel(itemType);
                    await App.Navigator.PushAsync(new EditItemTypesPage());
                }
            );
        }

        public ICommand DeleteItemTypeCommand
        {
            get => new RelayCommand<JsonItemType>(
                async (itemType) =>
                {
                    var wantsToDelete = !await Application.Current.MainPage.DisplayAlert(
                        "Confirmação",
                        $"Deseja apagar {itemType.Name}?",
                        "Não",
                        "Sim"
                    );

                    if (wantsToDelete)
                    {
                        IsRefreshing = true;
                        IsLoaded = false;

                        var connection = await _netService.CheckConnection();
                        if (connection.IsSuccess)
                        {
                            itemType.LastModified = DateTime.Now;

                            var url = Application.Current.Resources["UrlAPI"].ToString();
                            var response = await _apiService.DeleteAsync<JsonItemType>(
                                url,
                                new string[]
                                {
                                    "api",
                                    MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                                    "ItemTypes",
                                    itemType.UUId.ToString()
                                },
                                "bearer",
                                MainViewModel.GetInstance().Token.Token,
                                itemType
                            );

                            if (response.IsSuccess)
                            {
                                if (await SaveEntityToDB(itemType))
                                {
                                    RemoveEntityFromMobileList(itemType);

                                    IsRefreshing = false;
                                    IsLoaded = true;

                                    return;
                                }
                            }
                        }
                        else
                        {
                            if (await SaveEntityToDB(itemType))
                            {
                                RemoveEntityFromMobileList(itemType);

                                IsRefreshing = false;
                                IsLoaded = true;

                                return;
                            }
                        }

                        await Application.Current.MainPage.DisplayAlert(
                            "Erro",
                            "Ocorreu um erro a guardar os dados localmente",
                            "OK"
                        );

                        IsRefreshing = false;
                        IsLoaded = true;
                    }
                }
            );
        }

        #endregion

        #region Constructors

        public ItemTypesViewModel()
        {
            _apiService = new ApiService();
            _netService = new NetService();

            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            IsRefreshing = true;
            IsLoaded = false;

            LoadItemTypes();
        }

        #endregion

        #region Methods 

        /// <summary>
        /// Loads the Item Types remotely if it has internet connection, otherwise loads locally
        /// </summary>
        private async void LoadItemTypes()
        {
            try
            {
                var connection = await this._netService.CheckConnection();
                if (connection.IsSuccess)
                {
                    var url = Application.Current.Resources["UrlAPI"].ToString();
                    var response = await _apiService.GetAsync<JsonItemType>(
                        url,
                        new string[]
                        {
                            "api",
                            MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                            "ItemTypes"
                        },
                        "bearer",
                        MainViewModel.GetInstance().Token.Token);

                    if (response.IsSuccess)
                    {
                        RefreshList((List<JsonItemType>)response.Result);

                        IsRefreshing = false;
                        IsLoaded = true;
                        MainViewModel.GetInstance().IsViewLocked = false;

                        return;
                    }
                }

                var itemTypes = _unitOfWork.ItemTypes.GetItemTypesFromInventory(MainViewModel.GetInstance().CurrentInventoryId);

                RefreshList(
                    itemTypes.Select(x =>
                        new JsonItemType()
                        {
                            IsDisabled = x.IsDisabled,
                            LastModified = x.LastModified,
                            Name = x.Name,
                            UUId = x.UUId,
                            InventoryUUId = x.Inventory.UUId
                        }
                    )
                );

                IsRefreshing = false;
                IsLoaded = true;
            }
            catch (Exception ex)
            {
                await App.Navigator.DisplayAlert("Erro", ex.Message, "Ok");
            }
        }

        /// <summary>
        /// Refreshes the list
        /// </summary>
        /// <param name="itemTypesList"></param>
        public void RefreshList(IEnumerable<JsonItemType> itemTypesList)
        {
            IsRefreshing = true;

            this.ItemTypes = new ObservableCollection<JsonItemType>(itemTypesList.OrderBy(x => x.Name).ToList());

            IsRefreshing = false;
        }

        /// <summary>
        /// Saves the current entity to the Database (Delete/Disable variant)
        /// </summary>
        /// <param name="entityToRemove"></param>
        /// <returns></returns>
        private async Task<bool> SaveEntityToDB(JsonItemType entityToRemove)
        {
            var entityInDb = await _unitOfWork.ItemTypes.GetByUUIDAsync(entityToRemove.UUId.ToString());
            if (entityInDb != null)
            {
                var isRemoved = await _unitOfWork.ItemTypes.DisableAsync(entityInDb);
                if (isRemoved.IsSuccess)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes the entity from the list
        /// </summary>
        /// <param name="entityToRemove"></param>
        private void RemoveEntityFromMobileList(JsonItemType entityToRemove)
        {
            var itemsInList = MainViewModel.GetInstance().ItemTypesVM.ItemTypes.ToList();

            var previousItem = itemsInList.FirstOrDefault(x => x.UUId == entityToRemove.UUId);
            if (previousItem != null)
            {
                itemsInList.Remove(previousItem);

                MainViewModel.GetInstance().ItemTypesVM.ItemTypes =
                    new ObservableCollection<JsonItemType>(itemsInList.OrderBy(x => x.Name));
            }
        }

        #endregion

    }
}
