﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Services;
using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory.ItemBrands
{
    public class EditItemBrandsViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private bool _isRunning;
        private bool _isEnabled;

        private ApiService _apiService;
        private NetService _netService;

        private IUnitOfWork _unitOfWork;

        #endregion

        #region Properties

        public JsonItemBrand ItemBrand { get; set; }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetValue(ref this._isRunning, value);
        }

        public bool IsEnabled
        {
            get => _isEnabled;
            set => SetValue(ref this._isEnabled, value);
        }

        #endregion

        #region Constructor

        public EditItemBrandsViewModel(JsonItemBrand itemBrand)
        {
            _apiService = new ApiService();
            _netService = new NetService();


            this.ItemBrand = itemBrand;

            IsEnabled = true;
        }

        #endregion

        #region Commands

        public ICommand SaveCommand => new RelayCommand(
            async () =>
            {
                if (!await ModelVerification())
                    return;

                IsRunning = true;
                IsEnabled = false;

                ItemBrand.LastModified = DateTime.Now;

                var connection = await _netService.CheckConnection();
                if (connection.IsSuccess)
                {
                    var url = Application.Current.Resources["UrlAPI"].ToString();
                    var response = await _apiService.PutAsync<JsonItemBrand>(
                        url,
                        new string[]
                        {
                            "api",
                            MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                            "itemBrands",
                            ItemBrand.UUId.ToString()
                        },
                        "bearer",
                        MainViewModel.GetInstance().Token.Token,
                        ItemBrand
                    );

                    if (response.IsSuccess)
                    {
                        if (await SaveEntityToDB(ItemBrand))
                        {
                            SendEntityToMobileList(ItemBrand);

                            var page = Application.Current.MainPage;
                            await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                            return;
                        }
                    }
                }
                else
                {
                    if (await SaveEntityToDB(ItemBrand))
                    {
                        SendEntityToMobileList(ItemBrand);

                        var page = Application.Current.MainPage;
                        await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                        return;
                    }
                }

                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "Ocorreu um erro a guardar os dados localmente",
                    "OK"
                );

                IsRunning = false;
                IsEnabled = true;
            }
        );

        #endregion

        #region Methods

        /// <summary>
        /// Mobile equivalent of ModelState.IsValid
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ModelVerification()
        {
            if (string.IsNullOrEmpty(ItemBrand.Name))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "A Marca tem de ter um nome.",
                    "OK"
                );

                return false;
            }

            if (this.ItemBrand.Name.Length < 3 || this.ItemBrand.Name.Length >= 60)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "A Marca tem de ter entre 3 e 60 caractéres.",
                    "OK"
                );

                return false;
            }

            return true;
        }

        /// <summary>
        /// Saves the current entity to the Database (Update variant)
        /// </summary>
        /// <param name="itemToSave"></param>
        /// <returns></returns>
        private async Task<bool> SaveEntityToDB(JsonItemBrand entityToSave)
        {
            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            var entity = await _unitOfWork.ItemBrands.GetByUUIDAsync(entityToSave.UUId.ToString());
            if (entity != null)
            {
                entity.Name = entityToSave.Name;
                entity.LastModified = entityToSave.LastModified;
                entity.IsDisabled = entityToSave.IsDisabled;
                entity.UUId = entityToSave.UUId;

                await _unitOfWork.ItemBrands.UpdateAsync(entity);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Updates the entity in the previous page list
        /// </summary>
        /// <param name="entityToSend"></param>
        private void SendEntityToMobileList(JsonItemBrand entityToSend)
        {
            var itemsInList = MainViewModel.GetInstance().ItemBrandsVM.ItemBrands.ToList();

            var previousItem = itemsInList.FirstOrDefault(x => x.UUId == entityToSend.UUId);
            if (previousItem != null)
            {
                itemsInList.Remove(previousItem);
                itemsInList.Add(entityToSend);

                MainViewModel.GetInstance().ItemBrandsVM.ItemBrands =
                    new ObservableCollection<JsonItemBrand>(itemsInList.OrderBy(x => x.Name));
            }
        }

        #endregion
    }
}
