﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Services;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.ViewModels.Inventory.ItemBrands;
using MI.UIForms.Views.Inventory.ItemBrands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory
{
    public class ItemBrandsViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private NetService _netService;
        private ApiService _apiService;

        private IUnitOfWork _unitOfWork;

        #endregion

        #region Properties

        private ObservableCollection<JsonItemBrand> _itemBrands;
        public ObservableCollection<JsonItemBrand> ItemBrands
        {
            get => _itemBrands;
            set => SetValue(ref _itemBrands, value);
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetValue(ref _isRefreshing, value);
        }

        private bool _isLoaded;
        public bool IsLoaded
        {
            get => _isLoaded;
            set => SetValue(ref _isLoaded, value);
        }

        #endregion

        #region Commands

        public ICommand AddItemBrandCommand
        {
            get => new RelayCommand(
                async () =>
                {
                    MainViewModel.GetInstance().AddItemBrandsVM = new AddItemBrandsViewModel();
                    await App.Navigator.PushAsync(new AddItemBrandsPage());
                }
            );
        }

        public ICommand EditItemBrandCommand
        {
            get => new RelayCommand<JsonItemBrand>(
                async (itemBrand) =>
                {
                    MainViewModel.GetInstance().EditItemBrandsVM = new EditItemBrandsViewModel(itemBrand);
                    await App.Navigator.PushAsync(new EditItemBrandsPage());
                }
            );
        }

        public ICommand DeleteItemBrandCommand
        {
            get => new RelayCommand<JsonItemBrand>(
                async (itemBrand) =>
                {
                    var wantsToDelete = !await Application.Current.MainPage.DisplayAlert(
                        "Confirmação",
                        $"Deseja apagar {itemBrand.Name}?",
                        "Não",
                        "Sim"
                    );

                    if (wantsToDelete)
                    {
                        IsRefreshing = true;
                        IsLoaded = false;

                        var connection = await _netService.CheckConnection();
                        if (connection.IsSuccess)
                        {
                            itemBrand.LastModified = DateTime.Now;

                            var url = Application.Current.Resources["UrlAPI"].ToString();
                            var response = await _apiService.DeleteAsync<JsonItemBrand>(
                                url,
                                new string[]
                                {
                                    "api",
                                    MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                                    "ItemBrands",
                                    itemBrand.UUId.ToString()
                                },
                                "bearer",
                                MainViewModel.GetInstance().Token.Token,
                                itemBrand
                            );

                            if (response.IsSuccess)
                            {
                                if (await SaveEntityToDB(itemBrand))
                                {
                                    RemoveEntityFromMobileList(itemBrand);

                                    IsRefreshing = false;
                                    IsLoaded = true;

                                    return;
                                }
                            }
                        }
                        else
                        {
                            if (await SaveEntityToDB(itemBrand))
                            {
                                RemoveEntityFromMobileList(itemBrand);

                                IsRefreshing = false;
                                IsLoaded = true;

                                return;
                            }
                        }

                        await Application.Current.MainPage.DisplayAlert(
                            "Erro",
                            "Ocorreu um erro a guardar os dados localmente",
                            "OK"
                        );

                        IsRefreshing = false;
                        IsLoaded = true;
                    }
                }
            );
        }

        #endregion

        #region Constructors

        public ItemBrandsViewModel()
        {
            _apiService = new ApiService();
            _netService = new NetService();

            IsRefreshing = true;
            IsLoaded = false;

            LoadItemBrands();
        }

        /// <summary>
        /// Loads the Item Brands remotely if it has internet connection, otherwise loads locally
        /// </summary>
        private async void LoadItemBrands()
        {
            try
            {
                var connection = await this._netService.CheckConnection();
                if (connection.IsSuccess)
                {
                    var url = Application.Current.Resources["UrlAPI"].ToString();
                    var response = await _apiService.GetAsync<JsonItemBrand>(
                        url,
                        new string[]
                        {
                            "api",
                            MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                            "ItemTypes"
                        },
                        "bearer",
                        MainViewModel.GetInstance().Token.Token);

                    if (response.IsSuccess)
                    {
                        RefreshList((List<JsonItemBrand>)response.Result);

                        IsRefreshing = false;
                        IsLoaded = true;
                        MainViewModel.GetInstance().IsViewLocked = false;

                        return;
                    }
                }

                _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

                var itemBrands = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(MainViewModel.GetInstance().CurrentInventoryId);

                RefreshList(
                    itemBrands.Select(x =>
                        new JsonItemBrand()
                        {
                            IsDisabled = x.IsDisabled,
                            LastModified = x.LastModified,
                            Name = x.Name,
                            UUId = x.UUId
                        }
                    )
                );

                IsRefreshing = false;
                IsLoaded = true;
            }
            catch (Exception ex)
            {
                await App.Navigator.DisplayAlert("Erro", ex.Message, "Ok");
            }
        }

        /// <summary>
        /// Refreshes the list
        /// </summary>
        /// <param name="itemBrandsList"></param>
        public void RefreshList(IEnumerable<JsonItemBrand> itemBrandsList)
        {
            IsRefreshing = true;

            this.ItemBrands = new ObservableCollection<JsonItemBrand>(itemBrandsList.OrderBy(x => x.Name).ToList());

            IsRefreshing = false;
        }

        /// <summary>
        /// Saves the current entity to the Database (Delete/Disable variant)
        /// </summary>
        /// <param name="entityToRemove"></param>
        /// <returns></returns>
        private async Task<bool> SaveEntityToDB(JsonItemBrand entityToRemove)
        {
            var entityInDb = await _unitOfWork.ItemBrands.GetByUUIDAsync(entityToRemove.UUId.ToString());
            if (entityInDb != null)
            {
                var isRemoved = await _unitOfWork.ItemBrands.DisableAsync(entityInDb);
                if (isRemoved.IsSuccess)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes the entity from the list
        /// </summary>
        /// <param name="entityToRemove"></param>
        private void RemoveEntityFromMobileList(JsonItemBrand entityToRemove)
        {
            var itemsInList = MainViewModel.GetInstance().ItemBrandsVM.ItemBrands.ToList();

            var previousItem = itemsInList.FirstOrDefault(x => x.UUId == entityToRemove.UUId);
            if (previousItem != null)
            {
                itemsInList.Remove(previousItem);

                MainViewModel.GetInstance().ItemBrandsVM.ItemBrands =
                    new ObservableCollection<JsonItemBrand>(itemsInList.OrderBy(x => x.Name));
            }
        }

        #endregion
    }
}
