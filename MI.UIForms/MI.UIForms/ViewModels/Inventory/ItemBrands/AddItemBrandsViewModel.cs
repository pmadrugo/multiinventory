﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Services;
using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory.ItemBrands
{
    public class AddItemBrandsViewModel : BaseViewModel
    {
        #region Attributes

        private ApiService _apiService;
        private NetService _netService;

        private IUnitOfWork _unitOfWork;

        #endregion

        #region Properties

        private bool _isRunning;
        public bool IsRunning
        {
            get => this._isRunning;
            set => this.SetValue(ref this._isRunning, value);
        }

        private bool _isEnabled;
        public bool IsEnabled
        {
            get => this._isEnabled;
            set => this.SetValue(ref this._isEnabled, value);
        }

        public string Name { get; set; }

        #endregion

        #region Constructors

        public AddItemBrandsViewModel()
        {
            _apiService = new ApiService();
            _netService = new NetService();

            IsEnabled = true;
            IsRunning = false;
        }

        #endregion

        #region Commands

        public ICommand SaveCommand => new RelayCommand(
            async () =>
            {
                if (!await ModelVerification())
                    return;

                IsRunning = true;
                IsEnabled = false;

                var connection = await _netService.CheckConnection();
                if (connection.IsSuccess)
                {
                    var itemBrand = new JsonItemBrand
                    {
                        Name = this.Name,
                        InventoryUUId = MainViewModel.GetInstance().CurrentInventoryGuid,
                        LastModified = DateTime.Now
                    };

                    var url = Application.Current.Resources["UrlAPI"].ToString();
                    var response = await _apiService.PostAsync<JsonItemBrand>(
                        url,
                        new string[]
                        {
                            "api",
                            MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                            "itemBrands"
                        },
                        "bearer",
                        MainViewModel.GetInstance().Token.Token,
                        itemBrand);

                    if (response.IsSuccess)
                    {
                        var entityToSave = response.Result as JsonItemBrand;
                        if (entityToSave != null)
                        {
                            if (await SaveEntityToDB(entityToSave))
                            {
                                SendEntityToMobileList(entityToSave);

                                var page = Application.Current.MainPage;
                                await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                                return;
                            }
                        }
                    }
                }
                else
                {
                    var entityToSave = new JsonItemBrand()
                    {
                        Name = this.Name,
                        LastModified = DateTime.Now,
                        IsDisabled = false,
                        UUId = Guid.NewGuid()
                    };

                    if (await SaveEntityToDB(entityToSave))
                    {
                        SendEntityToMobileList(entityToSave);

                        var page = Application.Current.MainPage;
                        await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                        return;
                    }
                }

                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "Ocorreu um erro a guardar os dados localmente",
                    "OK"
                );

                IsRunning = false;
                IsEnabled = true;
            }
        );

        #endregion

        #region Methods

        /// <summary>
        /// Mobile equivalent of ModelState.IsValid
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ModelVerification()
        {
            if (string.IsNullOrEmpty(this.Name))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "A Marca tem de ter um nome.",
                    "OK"
                );

                return false;
            }

            if (this.Name.Length < 3 || this.Name.Length >=60)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "A Marca tem de ter entre 3 e 60 caractéres.",
                    "OK"
                );

                return false;
            }

            return true;
        }

        /// <summary>
        /// Saves the current entity to the Database (Create variant)
        /// </summary>
        /// <param name="itemToSave"></param>
        /// <returns></returns>
        private async Task<bool> SaveEntityToDB(JsonItemBrand itemToSave)
        {
            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(MainViewModel.GetInstance().CurrentInventoryGuid.ToString());
            if (inventory != null)
            {
                var isCreated = await _unitOfWork.ItemBrands.CreateAsync(
                    new ItemBrand()
                    {
                        IsDisabled = itemToSave.IsDisabled,
                        UUId = itemToSave.UUId,
                        LastModified = itemToSave.LastModified,
                        Name = itemToSave.Name,
                        InventoryId = inventory.Id
                    }
                );

                if (isCreated.IsSuccess)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Sends the entity to the previous page list
        /// </summary>
        /// <param name="entityToSend"></param>
        private void SendEntityToMobileList(JsonItemBrand entityToSend)
        {
            var itemsInList = MainViewModel.GetInstance().ItemBrandsVM.ItemBrands.ToList();

            itemsInList.Add(entityToSend);

            MainViewModel.GetInstance().ItemBrandsVM.ItemBrands =
                new ObservableCollection<JsonItemBrand>(itemsInList.OrderBy(x => x.Name));
        }

        #endregion
    }
}
