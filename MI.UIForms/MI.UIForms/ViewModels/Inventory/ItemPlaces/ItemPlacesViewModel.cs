﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Services;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.ViewModels.Inventory.ItemPlaces;
using MI.UIForms.Views.Inventory.ItemPlaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory
{
    public class ItemPlacesViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private NetService _netService;
        private ApiService _apiService;

        private IUnitOfWork _unitOfWork;

        #endregion

        #region Properties

        private ObservableCollection<JsonItemPlace> _itemPlaces;
        public ObservableCollection<JsonItemPlace> ItemPlaces
        {
            get => _itemPlaces;
            set => SetValue(ref _itemPlaces, value);
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetValue(ref _isRefreshing, value);
        }

        private bool _isLoaded;
        public bool IsLoaded
        {
            get => _isLoaded;
            set => SetValue(ref _isLoaded, value);
        }

        #endregion

        #region Commands

        public ICommand AddItemPlaceCommand
        {
            get => new RelayCommand(
                async () =>
                {
                    MainViewModel.GetInstance().AddItemPlacesVM = new AddItemPlacesViewModel();
                    await App.Navigator.PushAsync(new AddItemPlacesPage());
                }
            );
        }

        public ICommand EditItemPlaceCommand
        {
            get => new RelayCommand<JsonItemPlace>(
                async (itemPlace) =>
                {
                    MainViewModel.GetInstance().EditItemPlacesVM = new EditItemPlacesViewModel(itemPlace);
                    await App.Navigator.PushAsync(new EditItemPlacesPage());
                }
            );
        }

        public ICommand DeleteItemPlaceCommand
        {
            get => new RelayCommand<JsonItemPlace>(
                async (itemPlace) =>
                {
                    var wantsToDelete = !await Application.Current.MainPage.DisplayAlert(
                        "Confirmação",
                        $"Deseja apagar {itemPlace.Name}?",
                        "Não",
                        "Sim"
                    );

                    if (wantsToDelete)
                    {
                        IsRefreshing = true;
                        IsLoaded = false;

                        var connection = await _netService.CheckConnection();
                        if (connection.IsSuccess)
                        {
                            itemPlace.LastModified = DateTime.Now;

                            var url = Application.Current.Resources["UrlAPI"].ToString();
                            var response = await _apiService.DeleteAsync<JsonItemPlace>(
                                url,
                                new string[]
                                {
                                    "api",
                                    MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                                    "ItemPlaces",
                                    itemPlace.UUId.ToString()
                                },
                                "bearer",
                                MainViewModel.GetInstance().Token.Token,
                                itemPlace
                            );

                            if (response.IsSuccess)
                            {
                                if (await SaveEntityToDB(itemPlace))
                                {
                                    RemoveEntityFromMobileList(itemPlace);

                                    IsRefreshing = false;
                                    IsLoaded = true;

                                    return;
                                }
                            }
                        }
                        else
                        {
                            if (await SaveEntityToDB(itemPlace))
                            {
                                RemoveEntityFromMobileList(itemPlace);

                                IsRefreshing = false;
                                IsLoaded = true;

                                return;
                            }
                        }

                        await Application.Current.MainPage.DisplayAlert(
                            "Erro",
                            "Ocorreu um erro a guardar os dados localmente",
                            "OK"
                        );

                        IsRefreshing = false;
                        IsLoaded = true;
                    }
                }
            );
        }

        #endregion

        #region Constructors

        public ItemPlacesViewModel()
        {
            _apiService = new ApiService();
            _netService = new NetService();

            IsRefreshing = true;
            IsLoaded = false;

            LoadItemPlaces();
        }

        /// <summary>
        /// Loads the Item Places remotely if it has internet connection, otherwise loads locally
        /// </summary>
        private async void LoadItemPlaces()
        {
            try
            {
                var connection = await this._netService.CheckConnection();
                if (connection.IsSuccess)
                {
                    var url = Application.Current.Resources["UrlAPI"].ToString();
                    var response = await _apiService.GetAsync<JsonItemPlace>(
                        url,
                        new string[]
                        {
                            "api",
                            MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                            "ItemTypes"
                        },
                        "bearer",
                        MainViewModel.GetInstance().Token.Token);

                    if (response.IsSuccess)
                    {
                        RefreshList((List<JsonItemPlace>)response.Result);

                        IsRefreshing = false;
                        IsLoaded = true;
                        MainViewModel.GetInstance().IsViewLocked = false;

                        return;
                    }
                }

                _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

                var itemPlaces = _unitOfWork.ItemPlaces.GetItemPlacesFromInventory(MainViewModel.GetInstance().CurrentInventoryId);

                RefreshList(
                    itemPlaces.Select(x =>
                        new JsonItemPlace()
                        {
                            IsDisabled = x.IsDisabled,
                            LastModified = x.LastModified,
                            Name = x.Name,
                            UUId = x.UUId
                        }
                    )
                );

                IsRefreshing = false;
                IsLoaded = true;
            }
            catch (Exception ex)
            {
                await App.Navigator.DisplayAlert("Erro", ex.Message, "Ok");
            }
        }

        /// <summary>
        /// Refreshes the list
        /// </summary>
        /// <param name="itemPlacesList"></param>
        public void RefreshList(IEnumerable<JsonItemPlace> itemPlacesList)
        {
            IsRefreshing = true;

            this.ItemPlaces = new ObservableCollection<JsonItemPlace>(itemPlacesList.OrderBy(x => x.Name).ToList());

            IsRefreshing = false;
        }

        /// <summary>
        /// Saves the current entity to the Database (Delete/Disable variant)
        /// </summary>
        /// <param name="entityToRemove"></param>
        /// <returns></returns>
        private async Task<bool> SaveEntityToDB(JsonItemPlace entityToRemove)
        {
            var entityInDb = await _unitOfWork.ItemPlaces.GetByUUIDAsync(entityToRemove.UUId.ToString());
            if (entityInDb != null)
            {
                var isRemoved = await _unitOfWork.ItemPlaces.DisableAsync(entityInDb);
                if (isRemoved.IsSuccess)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes the entity from the list
        /// </summary>
        /// <param name="entityToRemove"></param>
        private void RemoveEntityFromMobileList(JsonItemPlace entityToRemove)
        {
            var itemsInList = MainViewModel.GetInstance().ItemPlacesVM.ItemPlaces.ToList();

            var previousItem = itemsInList.FirstOrDefault(x => x.UUId == entityToRemove.UUId);
            if (previousItem != null)
            {
                itemsInList.Remove(previousItem);

                MainViewModel.GetInstance().ItemPlacesVM.ItemPlaces =
                    new ObservableCollection<JsonItemPlace>(itemsInList.OrderBy(x => x.Name));
            }
        }

        #endregion
    }
}
