﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Models;
using MI.Common.Services;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.Helper;
using MI.UIForms.Services;
using MI.UIForms.ViewModels.Objects;
using MI.UIForms.Views;
using MI.UIForms.Views.Inventory;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory
{
    public class InventoriesViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Constructors

        public InventoriesViewModel()
        {
            _apiService = new ApiService();
            _netService = new NetService();
            _syncService = new SyncService();

            IsRefreshing = true;

            LoadInventories();
        }
        #endregion

        #region Attributes

        private NetService _netService;
        private ApiService _apiService;
        private SyncService _syncService;

        private bool _isRefreshing;

        #endregion

        #region Properties

        private UnitOfWork _unitOfWork;
        public UnitOfWork UnitOfWork
        {
            get
            {
                if (_unitOfWork == null)
                    _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);
                return _unitOfWork;
            }
        }

        private ObservableCollection<Data.Entities.InventorySection.Inventory> _inventoryList;
        public ObservableCollection<Data.Entities.InventorySection.Inventory> InventoryList
        {
            get => _inventoryList;
            set => SetValue(ref _inventoryList, value);
        }

        public bool IsRefreshing
        {
            get => _isRefreshing;
            set
            {
                SetValue(ref _isRefreshing, value);
                OnPropertyChanged("CanRefresh");
            }
        }

        public bool CanRefresh
        {
            get => !IsRefreshing;
        }

        #endregion

        #region Commands

        public ICommand RefreshInventoriesCommand => new RelayCommand(LoadInventories);
        public ICommand SelectInventoryCommand => new RelayCommand<object>(InventoryDetails);

        #endregion

        #region Methods

        /// <summary>
        /// Loads the list of inventories that the user has access to
        /// </summary>
        public async void LoadInventories()
        {
            IsRefreshing = true;
            List<Data.Entities.InventorySection.Inventory> inventoryList = new List<Data.Entities.InventorySection.Inventory>();

            var connection = await _netService.CheckConnection();
            if (connection.IsSuccess)
            {
                var url = Application.Current.Resources["UrlAPI"].ToString();
                var response =
                    await this._apiService.GetAsync<List<JsonInventory>>(
                        url,
                        new string[]
                        {
                        "api",
                        "Inventories"
                        },
                        "bearer",
                        MainViewModel.GetInstance().Token.Token);

                if (response.IsSuccess)
                {
                    await _syncService.SyncInventoriesAsync((List<JsonInventory>)response.Result);

                    inventoryList.AddRange(
                        ((List<JsonInventory>)response.Result)
                            .Where(x => !x.IsDisabled)
                            .Select(
                                x => new Data.Entities.InventorySection.Inventory()
                                {
                                    UUId = x.UUId,
                                    IsDisabled = x.IsDisabled,
                                    LastModified = x.LastModified,
                                    Name = x.Name
                                }
                            )
                    );
                }
                else
                {
                    var userInDb = UnitOfWork.Users.GetUserByEmail(MainViewModel.GetInstance().CurrentUserEmail);

                    inventoryList.AddRange(UnitOfWork.Inventories.GetInventoriesFromUser(userInDb.Id));
                }
            }
            else
            {
                var userInDb = UnitOfWork.Users.GetUserByEmail(MainViewModel.GetInstance().CurrentUserEmail);

                inventoryList.AddRange(UnitOfWork.Inventories.GetInventoriesFromUser(userInDb.Id));
            }

            RefreshInventoriesList(inventoryList);

            IsRefreshing = false;

        }

        /// <summary>
        /// Refreshes the inventory list
        /// </summary>
        /// <param name="inventoryList"></param>
        private void RefreshInventoriesList(IEnumerable<Data.Entities.InventorySection.Inventory> inventoryList)
        {
            IsRefreshing = true;

            this.InventoryList = new ObservableCollection<Data.Entities.InventorySection.Inventory>(inventoryList);

            IsRefreshing = false;
        }

        /// <summary>
        /// Action to perform when selecting the inventory
        /// </summary>
        /// <param name="commandParameters">The inventory GUID</param>
        public void InventoryDetails(object commandParameters)
        {
            var inventoryGuid = commandParameters.ToString();

            MainViewModel.GetInstance().CurrentInventoryGuid = Guid.Parse(inventoryGuid);
            var currentInventory = MainViewModel.GetInstance().LocalDatabase.Inventory
                .Include(x => x.Items)
                .FirstOrDefault(x => x.UUId == MainViewModel.GetInstance().CurrentInventoryGuid);

            MainViewModel.GetInstance().CurrentInventoryId = currentInventory.Id;
            MainViewModel.GetInstance().CurrentInventoryName = currentInventory.Name;

            MainViewModel.GetInstance().DashboardVM = new DashboardViewModel();

            App.Current.MainPage = new MasterPage();
        }

        #endregion
    }
}
