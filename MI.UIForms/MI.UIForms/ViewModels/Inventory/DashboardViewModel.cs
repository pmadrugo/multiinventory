﻿using MI.Common.JsonModels.Inventory;
using MI.Common.Models;
using MI.Common.Services;
using MI.UIForms.Data;
using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.Services;
using MI.UIForms.Views.Application;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory
{
    public class DashboardViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private ApiService _apiService;
        private NetService _netService;
        private SyncService _syncService;

        private IUnitOfWork _unitOfWork;

        #endregion

        public DashboardViewModel()
        {
            _apiService = new ApiService();
            _netService = new NetService();
            _syncService = new SyncService();

            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            IsRefreshing = true;
            IsLoaded = false;

            MainViewModel.GetInstance().IsViewLocked = true;

            LoadInventoryFull();
        }

        #region Properties 

        private int _itemCount;
        public int ItemCount
        {
            get => _itemCount;
            set => SetValue(ref _itemCount, value);
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetValue(ref _isRefreshing, value);
        }

        private bool _isLoaded;
        public bool IsLoaded
        {
            get => _isLoaded;
            set => SetValue(ref _isLoaded, value);
        }

        private ObservableCollection<Item> _lastAddedItems;
        public ObservableCollection<Item> LastAddedItems
        {
            get => _lastAddedItems;
            set => SetValue(ref _lastAddedItems, value);
        }

        private ObservableCollection<Item> _lastModifiedItems;
        public ObservableCollection<Item> LastModifiedItems
        {
            get => _lastModifiedItems;
            set => SetValue(ref _lastModifiedItems, value);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Completely loads an inventory, while acquiring its item count and the last modified items at the time.
        /// </summary>
        public async void LoadInventoryFull()
        {
            IsRefreshing = true;
            MainViewModel.GetInstance().IsViewLocked = true;

            await MainViewModel.GetInstance().SyncAsync();

            var items = 
                _unitOfWork.Items.GetItemsFromInventory(
                    MainViewModel.GetInstance().CurrentInventoryId
                )
                .ToList();

            ItemCount = _unitOfWork.Items.GetItemsFromInventory(MainViewModel.GetInstance().CurrentInventoryId).Count();
            LastAddedItems = new ObservableCollection<Item>(items.OrderByDescending(x => x.ItemInput).Take(3));
            LastModifiedItems = new ObservableCollection<Item>(items.OrderByDescending(x => x.LastModified).Take(3));

            MainViewModel.GetInstance().IsViewLocked = false;

            IsRefreshing = false;
            IsLoaded = true;
        }

        #endregion


    }
}
