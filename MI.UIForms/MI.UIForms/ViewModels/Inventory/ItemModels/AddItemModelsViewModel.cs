﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Services;
using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.ViewModels.Objects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory.ItemModels
{
    public class AddItemModelsViewModel : BaseViewModel
    {
        #region Attributes

        private ApiService _apiService;
        private NetService _netService;

        private IUnitOfWork _unitOfWork;

        #endregion

        #region Properties

        private bool _isRunning;
        public bool IsRunning
        {
            get => this._isRunning;
            set => this.SetValue(ref this._isRunning, value);
        }

        private bool _isEnabled;
        public bool IsEnabled
        {
            get => this._isEnabled;
            set => this.SetValue(ref this._isEnabled, value);
        }

        public string Name { get; set; }

        private JsonItemBrand _selectedBrand;
        public JsonItemBrand SelectedBrand
        {
            get => _selectedBrand;
            set => SetValue(ref _selectedBrand, value);
        }

        private ObservableCollection<JsonItemBrand> _availableBrands;
        public ObservableCollection<JsonItemBrand> AvailableBrands
        {
            get => _availableBrands;
            set => SetValue(ref _availableBrands, value);
        }

        #endregion

        #region Constructors

        public AddItemModelsViewModel()
        {
            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            _apiService = new ApiService();
            _netService = new NetService();

            IsEnabled = false;
            IsRunning = true;

            LoadPickers();
        }

        #endregion

        #region Commands

        public ICommand SaveCommand => new RelayCommand(
            async () =>
            {
                if (!await ModelVerification())
                    return;

                IsRunning = true;
                IsEnabled = false;

                var connection = await _netService.CheckConnection();
                if (connection.IsSuccess)
                {
                    var itemModel = new JsonItemModel
                    {
                        Name = this.Name,
                        InventoryUUId = MainViewModel.GetInstance().CurrentInventoryGuid,
                        BrandUUId = SelectedBrand.UUId,
                        LastModified = DateTime.Now,
                        IsDisabled = false
                    };

                    var url = Application.Current.Resources["UrlAPI"].ToString();
                    var response = await _apiService.PostAsync<JsonItemModel>(
                        url,
                        new string[]
                        {
                            "api",
                            MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                            "itemModels"
                        },
                        "bearer",
                        MainViewModel.GetInstance().Token.Token,
                        itemModel);

                    if (response.IsSuccess)
                    {
                        var entityToSave = response.Result as JsonItemModel;
                        if (entityToSave != null)
                        {
                            if (await SaveEntityToDB(entityToSave))
                            {
                                SendEntityToMobileList(entityToSave);

                                var page = Application.Current.MainPage;
                                await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                                return;
                            }
                        }
                    }
                }
                else
                {
                    var entityToSave = new JsonItemModel()
                    {
                        Name = this.Name,
                        LastModified = DateTime.Now,
                        IsDisabled = false,
                        UUId = Guid.NewGuid(),
                        BrandUUId = SelectedBrand.UUId,
                        InventoryUUId = MainViewModel.GetInstance().CurrentInventoryGuid
                    };

                    if (await SaveEntityToDB(entityToSave))
                    {
                        SendEntityToMobileList(entityToSave);

                        var page = Application.Current.MainPage;
                        await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                        return;
                    }
                }

                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "Ocorreu um erro a guardar os dados localmente",
                    "OK"
                );

                IsRunning = false;
                IsEnabled = true;
            }
        );

        #endregion

        #region Methods

        /// <summary>
        /// Loads the data used for the pickers
        /// </summary>
        public void LoadPickers()
        {
            this.AvailableBrands = new ObservableCollection<JsonItemBrand>(GetAvailableBrands());

            IsEnabled = true;
            IsRunning = false;
        }

        /// <summary>
        /// Gets the list of available brand choices
        /// </summary>
        /// <returns></returns>
        private IEnumerable<JsonItemBrand> GetAvailableBrands()
        {
            var brandsInDb = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(MainViewModel.GetInstance().CurrentInventoryId);

            return brandsInDb.Select(
                x => new JsonItemBrand()
                {
                    UUId = x.UUId,
                    InventoryUUId = x.Inventory.UUId,
                    IsDisabled = x.IsDisabled,
                    LastModified = x.LastModified,
                    Name = x.Name
                }
            );
        }

        /// <summary>
        /// Mobile equivalent of ModelState.IsValid
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ModelVerification()
        {
            if (string.IsNullOrEmpty(this.Name))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "O Modelo tem de ter um nome.",
                    "OK"
                );

                return false;
            }

            if (this.Name.Length < 3 || this.Name.Length >= 60)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "O Modelo tem de ter entre 3 e 60 caractéres.",
                    "OK"
                );

                return false;
            }

            if (SelectedBrand == null)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "O Modelo tem de ter uma Marca atribuída.",
                    "OK"
                );

                return false;
            }

            return true;
        }

        /// <summary>
        /// Saves the current entity to the Database (Create variant)
        /// </summary>
        /// <param name="itemToSave"></param>
        /// <returns></returns>
        private async Task<bool> SaveEntityToDB(JsonItemModel itemToSave)
        {
            var inventory = await _unitOfWork.Inventories.GetByUUIDAsync(MainViewModel.GetInstance().CurrentInventoryGuid.ToString());
            if (inventory != null)
            {
                var brand = await _unitOfWork.ItemBrands.GetByUUIDAsync(SelectedBrand.UUId.ToString());
                if (brand != null)
                {
                    var isCreated = await _unitOfWork.ItemModels.CreateAsync(
                        new ItemModel()
                        {
                            IsDisabled = itemToSave.IsDisabled,
                            UUId = itemToSave.UUId,
                            LastModified = itemToSave.LastModified,
                            Name = itemToSave.Name,
                            InventoryId = inventory.Id,
                            BrandId = brand.Id
                        }
                    );

                    if (isCreated.IsSuccess)
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Sends the entity to the previous page list
        /// </summary>
        /// <param name="entityToSend"></param>
        private void SendEntityToMobileList(JsonItemModel entityToSend)
        {
            var itemsInList = MainViewModel.GetInstance().ItemModelsVM.ItemModels.ToList();

            itemsInList.Add(
                new JsonItemModelWBrandViewModel()
                {
                    BrandUUId = entityToSend.BrandUUId,
                    InventoryUUId = entityToSend.InventoryUUId,
                    IsDisabled = entityToSend.IsDisabled,
                    LastModified = entityToSend.LastModified,
                    Name = entityToSend.Name,
                    UUId = entityToSend.UUId,
                    Brand = SelectedBrand.Name
                }
            );

            MainViewModel.GetInstance().ItemModelsVM.ItemModels =
                new ObservableCollection<JsonItemModelWBrandViewModel>(itemsInList.OrderBy(x => x.Name));
        }

        #endregion
    }
}
