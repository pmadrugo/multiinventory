﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Services;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.ViewModels.Inventory.ItemModels;
using MI.UIForms.ViewModels.Objects;
using MI.UIForms.Views.Inventory.ItemModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory
{
    public class ItemModelsViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private NetService _netService;
        private ApiService _apiService;

        private IUnitOfWork _unitOfWork;

        #endregion

        #region Properties

        private ObservableCollection<JsonItemModelWBrandViewModel> _itemModels;
        public ObservableCollection<JsonItemModelWBrandViewModel> ItemModels
        {
            get => _itemModels;
            set => SetValue(ref _itemModels, value);
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetValue(ref _isRefreshing, value);
        }

        private bool _isLoaded;
        public bool IsLoaded
        {
            get => _isLoaded;
            set => SetValue(ref _isLoaded, value);
        }

        #endregion

        #region Commands

        public ICommand AddItemModelCommand
        {
            get => new RelayCommand(
                async () =>
                {
                    MainViewModel.GetInstance().AddItemModelsVM = new AddItemModelsViewModel();
                    await App.Navigator.PushAsync(new AddItemModelsPage());
                }
            );
        }

        public ICommand EditItemModelCommand
        {
            get => new RelayCommand<JsonItemModel>(
                async (itemModel) =>
                {
                    MainViewModel.GetInstance().EditItemModelsVM = new EditItemModelsViewModel(itemModel);
                    await App.Navigator.PushAsync(new EditItemModelsPage());
                }
            );
        }

        public ICommand DeleteItemModelCommand
        {
            get => new RelayCommand<JsonItemModel>(
                async (itemModel) =>
                {
                    var wantsToDelete = !await Application.Current.MainPage.DisplayAlert(
                        "Confirmação",
                        $"Deseja apagar {itemModel.Name}?",
                        "Não",
                        "Sim"
                    );

                    if (wantsToDelete)
                    {
                        IsRefreshing = true;
                        IsLoaded = false;

                        var connection = await _netService.CheckConnection();
                        if (connection.IsSuccess)
                        {
                            itemModel.LastModified = DateTime.Now;

                            var url = Application.Current.Resources["UrlAPI"].ToString();
                            var response = await _apiService.DeleteAsync<JsonItemModel>(
                                url,
                                new string[]
                                {
                                    "api",
                                    MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                                    "ItemModels",
                                    itemModel.UUId.ToString()
                                },
                                "bearer",
                                MainViewModel.GetInstance().Token.Token,
                                itemModel
                            );

                            if (response.IsSuccess)
                            {
                                if (await SaveEntityToDB(itemModel))
                                {
                                    RemoveEntityFromMobileList(itemModel);

                                    IsRefreshing = false;
                                    IsLoaded = true;

                                    return;
                                }
                            }
                        }
                        else
                        {
                            if (await SaveEntityToDB(itemModel))
                            {
                                RemoveEntityFromMobileList(itemModel);

                                IsRefreshing = false;
                                IsLoaded = true;

                                return;
                            }
                        }

                        await Application.Current.MainPage.DisplayAlert(
                            "Erro",
                            "Ocorreu um erro a guardar os dados localmente",
                            "OK"
                        );

                        IsRefreshing = false;
                        IsLoaded = true;
                    }
                }
            );
        }

        #endregion

        #region Constructors

        public ItemModelsViewModel()
        {
            _apiService = new ApiService();
            _netService = new NetService();

            IsRefreshing = true;
            IsLoaded = false;

            LoadItemModels();
        }

        /// <summary>
        /// Syncs the database remotely if it has internet connection, otherwise loads locally
        /// (Sync is used due to possible dependency/foreign key issues)
        /// </summary>
        private async void LoadItemModels()
        {
            try
            {
                await MainViewModel.GetInstance().SyncAsync();

                _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

                var itemModels = _unitOfWork.ItemModels.GetItemModelsFromInventory
                    (MainViewModel.GetInstance().CurrentInventoryId);

                RefreshList(
                    itemModels.Select(x =>
                        new JsonItemModelWBrandViewModel()
                        {
                            IsDisabled = x.IsDisabled,
                            LastModified = x.LastModified,
                            Name = x.Name,
                            UUId = x.UUId,
                            Brand = x.Brand.Name,
                            BrandUUId = x.Brand.UUId,
                            InventoryUUId = MainViewModel.GetInstance().CurrentInventoryGuid
                        }
                    )
                );

                IsRefreshing = false;
                IsLoaded = true;
            }
            catch (Exception ex)
            {
                await App.Navigator.DisplayAlert("Erro", ex.Message, "Ok");
            }
        }

        /// <summary>
        /// Refreshes the list
        /// </summary>
        /// <param name="itemModels"></param>
        public void RefreshList(IEnumerable<JsonItemModelWBrandViewModel> itemModels)
        {
            IsRefreshing = true;

            this.ItemModels = new ObservableCollection<JsonItemModelWBrandViewModel>(itemModels.OrderBy(x => x.Name).ToList());

            IsRefreshing = false;
        }

        /// <summary>
        /// Saves the current entity to the Database (Delete/Disable variant)
        /// </summary>
        /// <param name="entityToRemove"></param>
        /// <returns></returns>
        private async Task<bool> SaveEntityToDB(JsonItemModel entityToRemove)
        {
            var entityInDb = await _unitOfWork.ItemModels.GetByUUIDAsync(entityToRemove.UUId.ToString());
            if (entityInDb != null)
            {
                var isRemoved = await _unitOfWork.ItemModels.DisableAsync(entityInDb);
                if (isRemoved.IsSuccess)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes the entity from the list
        /// </summary>
        /// <param name="entityToRemove"></param>
        private void RemoveEntityFromMobileList(JsonItemModel entityToRemove)
        {
            var itemsInList = MainViewModel.GetInstance().ItemModelsVM.ItemModels.ToList();

            var previousItem = itemsInList.FirstOrDefault(x => x.UUId == entityToRemove.UUId);
            if (previousItem != null)
            {
                itemsInList.Remove(previousItem);

                MainViewModel.GetInstance().ItemModelsVM.ItemModels =
                    new ObservableCollection<JsonItemModelWBrandViewModel>(itemsInList.OrderBy(x => x.Name));
            }
        }

        #endregion
    }
}
