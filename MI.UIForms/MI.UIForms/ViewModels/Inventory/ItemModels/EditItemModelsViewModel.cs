﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Inventory;
using MI.Common.Services;
using MI.UIForms.Data.Entities.InventorySection;
using MI.UIForms.Data.Interfaces;
using MI.UIForms.Data.Repository;
using MI.UIForms.ViewModels.Objects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels.Inventory.ItemModels
{
    public class EditItemModelsViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private bool _isRunning;
        private bool _isEnabled;

        private ApiService _apiService;
        private NetService _netService;

        private IUnitOfWork _unitOfWork;

        #endregion

        #region Properties

        public JsonItemModel ItemModel { get; set; }

        private JsonItemBrand _selectedBrand;
        public JsonItemBrand SelectedBrand
        {
            get => _selectedBrand;
            set => SetValue(ref _selectedBrand, value);
        }

        private ObservableCollection<JsonItemBrand> _availableBrands;
        public ObservableCollection<JsonItemBrand> AvailableBrands
        {
            get => _availableBrands;
            set => SetValue(ref _availableBrands, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetValue(ref this._isRunning, value);
        }

        public bool IsEnabled
        {
            get => _isEnabled;
            set => SetValue(ref this._isEnabled, value);
        }

        #endregion

        #region Constructor

        public EditItemModelsViewModel(JsonItemModel itemModel)
        {
            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            _apiService = new ApiService();
            _netService = new NetService();

            this.ItemModel = itemModel;

            IsEnabled = false;
            IsRunning = true;

            LoadPickers();
        }

        #endregion

        #region Methods

        public void LoadCurrentOption()
        {
            var currentBrand = this.AvailableBrands
                .FirstOrDefault(x => x.UUId == ItemModel.BrandUUId);

            if (currentBrand != null)
            {
                this.SelectedBrand = this.AvailableBrands.SingleOrDefault(x => x.UUId == this.ItemModel.BrandUUId);
            }
        }

        public void LoadPickers()
        {
            this.AvailableBrands = new ObservableCollection<JsonItemBrand>(GetAvailableBrands());

            LoadCurrentOption();

            IsEnabled = true;
            IsRunning = false;
        }

        private IEnumerable<JsonItemBrand> GetAvailableBrands()
        {
            var brandsInDb = _unitOfWork.ItemBrands.GetItemBrandsFromInventory(MainViewModel.GetInstance().CurrentInventoryId);

            return brandsInDb.Select(
                x => new JsonItemBrand()
                {
                    UUId = x.UUId,
                    InventoryUUId = x.Inventory.UUId,
                    IsDisabled = x.IsDisabled,
                    LastModified = x.LastModified,
                    Name = x.Name
                }
            );
        }

        #endregion

        #region Commands

        public ICommand SaveCommand => new RelayCommand(
            async () =>
            {
                if (!await ModelVerification())
                    return;

                IsRunning = true;
                IsEnabled = false;

                ItemModel.LastModified = DateTime.Now;
                ItemModel.BrandUUId = SelectedBrand.UUId;

                var connection = await _netService.CheckConnection();
                if (connection.IsSuccess)
                {
                    var url = Application.Current.Resources["UrlAPI"].ToString();
                    var response = await _apiService.PutAsync<JsonItemModel>(
                        url,
                        new string[]
                        {
                            "api",
                            MainViewModel.GetInstance().CurrentInventoryGuid.ToString(),
                            "ItemModels",
                            ItemModel.UUId.ToString()
                        },
                        "bearer",
                        MainViewModel.GetInstance().Token.Token,
                        ItemModel
                    );

                    if (response.IsSuccess)
                    {
                        if (await SaveEntityToDB(ItemModel))
                        {
                            SendEntityToMobileList(ItemModel);

                            var page = Application.Current.MainPage;
                            await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                            return;
                        }
                    }
                }
                else
                {
                    if (await SaveEntityToDB(ItemModel))
                    {
                        SendEntityToMobileList(ItemModel);

                        var page = Application.Current.MainPage;
                        await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                        return;
                    }
                }

                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "Ocorreu um erro a guardar os dados localmente",
                    "OK"
                );

                IsRunning = false;
                IsEnabled = true;
            }
        );

        #endregion

        #region Methods

        /// <summary>
        /// Mobile equivalent of ModelState.IsValid
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ModelVerification()
        {
            if (string.IsNullOrEmpty(ItemModel.Name))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "A Categoria tem de ter um nome.",
                    "OK"
                );

                return false;
            }

            if (this.ItemModel.Name.Length < 3 || this.ItemModel.Name.Length >= 60)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "A Categoria tem de ter entre 3 e 60 caractéres.",
                    "OK"
                );

                return false;
            }

            if (SelectedBrand == null)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "O Modelo tem de ter uma Marca atribuída.",
                    "OK"
                );

                return false;
            }

            return true;
        }

        /// <summary>
        /// Saves the current entity to the Database (Update variant)
        /// </summary>
        /// <param name="itemToSave"></param>
        /// <returns></returns>
        private async Task<bool> SaveEntityToDB(JsonItemModel entityToSave)
        {
            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            var entity = await _unitOfWork.ItemModels.GetByUUIDAsync(entityToSave.UUId.ToString());
            if (entity != null)
            {
                var brand = await _unitOfWork.ItemBrands.GetByUUIDAsync(SelectedBrand.UUId.ToString());
                if (brand != null)
                {
                    entity.Name = entityToSave.Name;
                    entity.LastModified = entityToSave.LastModified;
                    entity.IsDisabled = entityToSave.IsDisabled;
                    entity.UUId = entityToSave.UUId;
                    entity.BrandId = brand.Id;

                    await _unitOfWork.ItemModels.UpdateAsync(entity);

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Updates the entity in the previous page list
        /// </summary>
        /// <param name="entityToSend"></param>
        private void SendEntityToMobileList(JsonItemModel entityToSend)
        {
            var itemsInList = MainViewModel.GetInstance().ItemModelsVM.ItemModels.ToList();

            var previousItem = itemsInList.FirstOrDefault(x => x.UUId == entityToSend.UUId);
            if (previousItem != null)
            {
                itemsInList.Remove(previousItem);
                itemsInList.Add(
                    new JsonItemModelWBrandViewModel()
                    {
                        BrandUUId = SelectedBrand.UUId,
                        InventoryUUId = entityToSend.InventoryUUId,
                        IsDisabled = entityToSend.IsDisabled,
                        LastModified = entityToSend.LastModified,
                        Name = entityToSend.Name,
                        UUId = entityToSend.UUId,
                        Brand = SelectedBrand.Name
                    }
                );

                MainViewModel.GetInstance().ItemModelsVM.ItemModels =
                    new ObservableCollection<JsonItemModelWBrandViewModel>(itemsInList.OrderBy(x => x.Name));
            }
        }

        #endregion
    }
}
