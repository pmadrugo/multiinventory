﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Application;
using MI.Common.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels
{
    public class ForgotPasswordViewModel : BaseViewModel, INotifyPropertyChanged
    {
        private ApiService _apiService;
        private NetService _netService;

        public string Email { get; set; }

        public ICommand RecoverCommand
        {
            get => new RelayCommand(Recover);
        }

        public ForgotPasswordViewModel()
        {
            _apiService = new ApiService();
            _netService = new NetService();
        }

        /// <summary>
        /// Starts the password recovery process
        /// </summary>
        private async void Recover()
        {
            if (string.IsNullOrEmpty(this.Email) || !IsValidEmail(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    "Insira um E-mail Válido",
                    "OK");

                return;
            }

            var connection = await _netService.CheckConnection();
            if (connection.IsSuccess)
            {
                var response = 
                    await _apiService.AccountController(
                        Application.Current.Resources["UrlAPI"].ToString(),
                        "/api",
                        "/Account",
                        "/ForgotPassword",
                        new JsonAccount { Email = this.Email }
                    );

                if (response.IsSuccess)
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Sucesso",
                        "Foi enviado um e-mail com o link de recuperação de password",
                        "OK"
                    );

                    await Application.Current.MainPage.Navigation.PopAsync();

                    return;
                }
            }

            await Application.Current.MainPage.DisplayAlert(
                "Erro",
                "Verifique a sua ligação à internet.",
                "OK"
            );
        }

        /// <summary>
        /// Checks if it's a valid email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool IsValidEmail(string email)
        {
            try
            {
                MailAddress mail = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
