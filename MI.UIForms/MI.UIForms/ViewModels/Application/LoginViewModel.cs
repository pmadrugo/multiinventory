﻿

namespace MI.UIForms.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using MI.Common.JsonModels;
    using MI.Common.JsonModels.Application;
    using MI.Common.JsonModels.Authentication;
    using MI.Common.JsonModels.JSONInterfaces;
    using MI.Common.Services;
    using MI.UIForms.Data.Entities.ApplicationSection;
    using MI.UIForms.Data.Repository;
    using MI.UIForms.Helper;
    using MI.UIForms.Models;
    using MI.UIForms.Services;
    using MI.UIForms.ViewModels.Inventory;
    using MI.UIForms.Views.Application;
    using MI.UIForms.Views.Inventory;
    using Newtonsoft.Json;
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Net.Http;
    using System.Windows.Input;
    using Xamarin.Auth;
    using Xamarin.Forms;

    public class LoginViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Atributes

        private NetService _netService;
        private ApiService _apiService;
        private SyncService _syncService;
        private ExternalAuthService _externalAuthService;

        private bool _isRunning;
        private bool _isEnabled;

        #endregion

        #region Properties

        public bool IsRunning
        {
            get => this._isRunning;
            set => this.SetValue(ref this._isRunning, value);
        }

        public bool IsEnabled
        {
            get => this._isEnabled;
            set => this.SetValue(ref this._isEnabled, value);
        }

        public string Email { get; set; }
        public string Password { get; set; }

        #endregion

        #region Constructor
        public LoginViewModel()
        {
            _apiService = new ApiService();
            _netService = new NetService();
            _syncService = new SyncService();
            _externalAuthService = new ExternalAuthService();

            this.IsEnabled = true;
        }
        #endregion

        #region Commands

        public ICommand LoginCommand =>
            new RelayCommand(
                async () =>
                {
                    if (string.IsNullOrEmpty(this.Email))
                    {
                        await Application.Current.MainPage.DisplayAlert(
                            "Erro",
                            "Tem que inserir um email",
                            "Ok");

                        return;
                    }

                    if (string.IsNullOrEmpty(this.Password))
                    {
                        await Application.Current.MainPage.DisplayAlert(

                            "Erro",
                            "Tem que inserir uma password",
                            "Ok");

                        return;
                    }

                    this.IsRunning = true;
                    this.IsEnabled = false;

                    var request = new JsonAccount
                    {
                        Password = this.Password,
                        Email = this.Email
                    };

                    var url = Application.Current.Resources["UrlAPI"].ToString();
                    var connection = await _netService.CheckConnection();
                    if (connection.IsSuccess)
                    {
                        var response = await _apiService.AccountController(
                        url,
                        "/api",
                        "/Account",
                        "/CreateToken",
                        request);

                        if (!response.IsSuccess)
                        {
                            await Application.Current.MainPage.DisplayAlert(
                                "Erro",
                                response.Message,
                                "Ok");

                            this.IsRunning = false;
                            this.IsEnabled = true;

                            return;
                        }

                        var userDetailsResponse =
                            await _apiService.PostAsync<JsonRegisterAccount>(
                                url,
                                new string[] {
                                "api",
                                "Account",
                                "GetUserInformation"
                                },
                                request);

                        var userForDb = new User()
                        {
                            Password = request.Password,
                            UserName = request.Email
                        };

                        if (!userDetailsResponse.IsSuccess)
                        {
                            await Application.Current.MainPage.DisplayAlert(
                                "Erro",
                                "Erro ao obter dados do utilizador, tente mais tarde..",
                                "Ok");

                            this.IsRunning = false;
                            this.IsEnabled = true;

                            return;
                        }

                        if (userDetailsResponse.Result != null)
                        {
                            userForDb.BirthDate = ((JsonRegisterAccount)userDetailsResponse.Result).BirthDate;
                            userForDb.FirstName = ((JsonRegisterAccount)userDetailsResponse.Result).FirstName;
                            userForDb.LastName = ((JsonRegisterAccount)userDetailsResponse.Result).LastName;
                            userForDb.VATNumber = ((JsonRegisterAccount)userDetailsResponse.Result).VATNumber;
                            userForDb.Id = ((JsonRegisterAccount)userDetailsResponse.Result).UserId;
                        }

                        var token = JsonConvert.DeserializeObject<JsonTokenResponse>(response.Result.ToString());
                        var mainViewModel = MainViewModel.GetInstance();
                        mainViewModel.Token = token;

                        await _syncService.SyncUserWithDatabaseAsync(
                            userForDb
                        );
                    }
                    else
                    {
                        var cryptPassword = CryptoService.ComputeMD5Hash(this.Password);

                        UnitOfWork unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

                        if (!unitOfWork.Users.ValidatePassword(this.Email, cryptPassword))
                        {
                            await Application.Current.MainPage.DisplayAlert(
                                "Erro",
                                "E-Mail ou Password inválida",
                                "Ok");

                            this.IsRunning = false;
                            this.IsEnabled = true;

                            return;
                        }
                    }


                    this.IsRunning = false;
                    this.IsEnabled = true;

                    MainViewModel.GetInstance().CurrentUserEmail = this.Email;

                    MainViewModel.GetInstance().InventoriesVM = new InventoriesViewModel();
                    await Application.Current.MainPage.Navigation.PushAsync(new InventoriesPage());
                }
            );

        public ICommand ForgotPasswordCommand =>
            new RelayCommand(
                async () =>
                {
                    MainViewModel.GetInstance().ForgotPasswordVM = new ForgotPasswordViewModel();
                    await Application.Current.MainPage.Navigation.PushAsync(new ForgotPasswordPage());
                }
            );

        public ICommand RegisterCommand =>
            new RelayCommand(
                async () =>
                {
                    MainViewModel.GetInstance().RegisterVM = new RegisterViewModel();
                    await Application.Current.MainPage.Navigation.PushAsync(new RegisterPage());
                }
            );

        #region SocialLogin

        public ICommand FacebookLogInCommand =>
            new RelayCommand(
                async () =>
                {
                    var connection = await _netService.CheckConnection();
                    if (connection.IsSuccess)
                    {
                        var authenticator = _externalAuthService.CreateOAuth2Authenticator("Facebook");

                        authenticator.Completed += OnAuthCompleted;
                        authenticator.Error += OnAuthError;

                        AuthenticationStat.Authenticator2 = authenticator;

                        var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
                        presenter.Login(authenticator);

                        return;
                    }

                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Verifique a sua ligação à Internet",
                        "OK"
                    );
                });

        public ICommand GoogleLogInCommand =>
            new RelayCommand(
                async () =>
                {
                    var connection = await _netService.CheckConnection();
                    if (connection.IsSuccess)
                    {
                        var authenticator = _externalAuthService.CreateOAuth2Authenticator("Google");

                        authenticator.Completed += OnAuthCompleted;
                        authenticator.Error += OnAuthError;

                        AuthenticationStat.Authenticator2 = authenticator;

                        var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
                        presenter.Login(authenticator);

                        return;
                    }

                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Verifique a sua ligação à Internet",
                        "OK"
                    );
                });


        public ICommand TwitterLogInCommand =>
            new RelayCommand(
                async () =>
                {
                    var connection = await _netService.CheckConnection();
                    if (connection.IsSuccess)
                    {
                        var authenticator = _externalAuthService.CreateOAuth1Authenticator("Twitter");

                        authenticator.Completed += OnAuth1Completed;
                        authenticator.Error += OnAuth1Error;

                        AuthenticationStat.Authenticator1 = authenticator;

                        var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
                        presenter.Login(authenticator);

                        return;
                    }

                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Verifique a sua ligação à Internet",
                        "OK"
                    );
                });



        public ICommand WindowsLogInCommand =>
            new RelayCommand(
                async () =>
                {
                    var connection = await _netService.CheckConnection();
                    if (connection.IsSuccess)
                    {
                        var authenticator = _externalAuthService.CreateOAuth2Authenticator("Microsoft");

                        authenticator.Completed += OnAuthCompleted;
                        authenticator.Error += OnAuthError;

                        AuthenticationStat.Authenticator2 = authenticator;

                        var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
                        presenter.Login(authenticator);

                        return;
                    }

                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Verifique a sua ligação à Internet",
                        "OK"
                    );
                });

        #endregion

        #endregion

        #region Events

        private async void OnAuthCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }

            IJsonExternalUser user = null;
            if (e.IsAuthenticated)
            {

                switch (_externalAuthService.ExternalName)
                {

                    case "Microsoft":
                        var client = new HttpClient();
                        var userInfoRequest = new HttpRequestMessage()
                        {
                            RequestUri = new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalAuthService.ExternalName}:UserInfoUrl"]),
                            Method = HttpMethod.Get
                        };

                        userInfoRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", e.Account.Properties.Values.First());

                        using (var response = await client.SendAsync(userInfoRequest))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                var responseString = await response.Content.ReadAsStringAsync();
                                user = JsonConvert.DeserializeObject<JsonMicrosoft>(responseString);
                            }
                        }
                        break;

                    default:
                        {
                            var request = new OAuth2Request("GET", new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalAuthService.ExternalName}:UserInfoUrl"]), null, e.Account);
                            var response = await request.GetResponseAsync();
                            if (response != null)
                            {

                                string userJson = await response.GetResponseTextAsync();

                                switch (_externalAuthService.ExternalName)
                                {
                                    case "Facebook":
                                        user = JsonConvert.DeserializeObject<JsonFacebook>(userJson);
                                        break;

                                    case "Google":
                                        user = JsonConvert.DeserializeObject<JsonGoogle>(userJson);
                                        break;

                                    case "Twitter":
                                        user = JsonConvert.DeserializeObject<JsonTwitter>(userJson);
                                        break;
                                }
                            }
                        }
                        break;
                }

                if(user == null)
                {
                    await Application.Current.MainPage.DisplayAlert(
                                "Erro",
                                $"Ocorreu um erro ao fazer login atraves de {_externalAuthService.ExternalName}",
                                "OK"
                            );
                }

                user.VerifiedEmail = true;

                JsonExternalUser jsonExternalUser = new JsonExternalUser
                {
                    Email = user.Email,
                    VerifiedEmail = user.VerifiedEmail
                };

                if (_externalAuthService.Account != null)
                {
                    _externalAuthService.Store.Delete(_externalAuthService.Account, AppSettingsHelper.Settings["Authentication:AndroidClientId"]);
                }

                await _externalAuthService.Store.SaveAsync(_externalAuthService.Account = e.Account, AppSettingsHelper.Settings["Authentication:AppName"]);

                var result = await _apiService.AccountController(
                    Application.Current.Resources["UrlAPI"].ToString(),
                    "/api",
                    "/Account",
                    "/CreateTokenExternal",
                    jsonExternalUser);

                if (!result.IsSuccess)
                {

                    if (result.Result != null)
                    {
                        int.TryParse(result.Result.ToString(), out int statuscode);

                        if (statuscode == 404)
                        {
                            MainViewModel.GetInstance().RegisterVM = new RegisterViewModel(user);
                            await Application.Current.MainPage.Navigation.PushAsync(new RegisterPage());
                            return;
                        }
                    }

                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        $"Erro: {result.Message}",
                        "Ok");
                    return;

                }

                var token = JsonConvert.DeserializeObject<JsonTokenResponse>(result.Result.ToString());
                var mainViewModel = MainViewModel.GetInstance();
                mainViewModel.Token = token;

                MainViewModel.GetInstance().InventoriesVM = new InventoriesViewModel();
                await Application.Current.MainPage.Navigation.PushAsync(new InventoriesPage());
            }
        }

        private async void OnAuthError(object sender, AuthenticatorErrorEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;

                authenticator.OnCancelled();

                await Application.Current.MainPage.DisplayAlert(
                                "Erro",
                                $"Ocorreu um erro ao fazer login atraves de {_externalAuthService.ExternalName}",
                                "OK"
                            );
                return;
            }
        }

        private async void OnAuth1Completed(object sender, AuthenticatorCompletedEventArgs e)
        {
            var authenticator = sender as OAuth1Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuth1Completed;
                authenticator.Error -= OnAuth1Error;
            }

            IJsonExternalUser user = null;
            if (e.IsAuthenticated)
            {

                var request = new OAuth1Request("GET", new Uri(AppSettingsHelper.Settings[$"Authentication:{_externalAuthService.ExternalName}:UserInfoUrl"]), null, e.Account);
                request.Parameters.Add("include_email", "true");

                var response = await request.GetResponseAsync();
                if (response != null)
                {

                    string userJson = await response.GetResponseTextAsync();

                    user = JsonConvert.DeserializeObject<JsonTwitter>(userJson);
                }

                if(user == null)
                {
                    await Application.Current.MainPage.DisplayAlert(
                                "Erro",
                                $"Ocorreu um erro ao fazer login atraves de {_externalAuthService.ExternalName}",
                                "OK"
                            );
                    return;
                }

                user.VerifiedEmail = true;
                user.FirstName = string.Empty;
                user.LastName = string.Empty;

                JsonExternalUser jsonExternalUser = new JsonExternalUser
                {
                    Email = user.Email,
                    VerifiedEmail = user.VerifiedEmail
                };

                if (_externalAuthService.Account != null)
                {
                    _externalAuthService.Store.Delete(_externalAuthService.Account, AppSettingsHelper.Settings["Authentication:AndroidClientId"]);
                }

                await _externalAuthService.Store.SaveAsync(_externalAuthService.Account = e.Account, AppSettingsHelper.Settings["Authentication:AppName"]);

                var result = await _apiService.AccountController(
                    Application.Current.Resources["UrlAPI"].ToString(),
                    "/api",
                    "/Account",
                    "/CreateTokenExternal",
                    jsonExternalUser);

                if (!result.IsSuccess)
                {

                    if (result.Result != null)
                    {
                        int.TryParse(result.Result.ToString(), out int statuscode);

                        if (statuscode == 404)
                        {
                            MainViewModel.GetInstance().RegisterVM = new RegisterViewModel(user);
                            await Application.Current.MainPage.Navigation.PushAsync(new RegisterPage());
                            return;
                        }
                    }

                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        $"Erro: {result.Message}",
                        "Ok");
                    return;

                }

                var token = JsonConvert.DeserializeObject<JsonTokenResponse>(result.Result.ToString());
                var mainViewModel = MainViewModel.GetInstance();
                mainViewModel.Token = token;

                MainViewModel.GetInstance().InventoriesVM = new InventoriesViewModel();
                await Application.Current.MainPage.Navigation.PushAsync(new InventoriesPage());
                return;
            }

            await Application.Current.MainPage.DisplayAlert(
                                "Erro",
                                $"Ocorreu um erro ao fazer login atraves de {_externalAuthService.ExternalName}",
                                "OK"
                            );
            return;
        }

        private async void OnAuth1Error(object sender, AuthenticatorErrorEventArgs e)
        {
            var authenticator = sender as OAuth1Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuth1Completed;
                authenticator.Error -= OnAuth1Error;

                authenticator.OnCancelled();

                await Application.Current.MainPage.DisplayAlert(
                                "Erro",
                                $"Ocorreu um erro ao fazer login atraves de {_externalAuthService.ExternalName}",
                                "OK"
                            );
                return;
            }
        }

        #endregion
    }
}
