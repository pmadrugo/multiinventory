﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Application;
using MI.Common.Models;
using MI.Common.Services;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels
{
    public class ChangePasswordViewModel : BaseViewModel, INotifyPropertyChanged
    {
        private ApiService _apiService;
        private NetService _netService;

        public string Email { get; set; }

        public string OriginalPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }

        public ChangePasswordViewModel()
        {
            _apiService = new ApiService();
            _netService = new NetService();
        }

        public ICommand ChangePasswordCommand =>
            new RelayCommand(
                async () =>
                {
                    if (!await ModelVerification())
                    {
                        return;
                    }

                    var connection = await _netService.CheckConnection();
                    if (connection.IsSuccess)
                    {
                        var url = Application.Current.Resources["UrlAPI"].ToString();
                        var response =
                            await this._apiService.AccountController(
                                url,
                                "/api",
                                "/Account",
                                "/ChangePassword",
                                new JsonChangePasswordAccount
                                {
                                    Password = this.OriginalPassword,
                                    NewPassword = this.NewPassword,
                                },
                                "bearer",
                                MainViewModel.GetInstance().Token.Token
                                );


                        if (!response.IsSuccess)
                        {
                            await Application.Current.MainPage.DisplayAlert(
                                "Erro",
                                $"Ocorreu um erro, tente novamente\n {response.Message}",
                                "OK"
                            );
                            return;
                        }

                        await Application.Current.MainPage.DisplayAlert(
                            "Sucesso",
                            $"Password Alterada",
                            "OK"
                        );

                        var page = Application.Current.MainPage;
                        await ((MasterDetailPage)page).Detail.Navigation.PopAsync();

                        return;
                    }

                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        $"Verifique a sua ligação à Internet",
                        "OK"
                    );
                }
            );

        /// <summary>
        /// A recreation of ModelState.IsValid
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ModelVerification()
        {
            if (string.IsNullOrEmpty(NewPassword))
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Tem que preencher a password",
                        "Ok");

                return false;
            }

            if (this.NewPassword != this.ConfirmPassword)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    $"A confirmação não corresponde à nova password.",
                    "OK"
                );

                return false;
            }

            if (!Regex.IsMatch(NewPassword, "^(?=.*[0-9]+)(?=.*[a-z]+)(?=.*[A-Z])(?=.*[^A-Za-z0-9]).{6,}$"))
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "A password tem de ter um caractér maíusculo, um minúsculo, um numérico e um símbolo.",
                        "Ok");

                return false;
            }

            return true;
        }
    }
}
