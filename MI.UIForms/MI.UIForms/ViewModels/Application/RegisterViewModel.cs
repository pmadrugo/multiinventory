﻿using GalaSoft.MvvmLight.Command;
using MI.Common.JsonModels.Application;
using MI.Common.JsonModels.Authentication;
using MI.Common.JsonModels.JSONInterfaces;
using MI.Common.Models;
using MI.Common.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels
{
    public class RegisterViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes
        private ApiService _apiService;
        private NetService _netService;

        private IJsonExternalUser _user;
        #endregion

        #region Properties
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string NIF { get; set; }

        public DateTime BirthDate { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
        #endregion

        public RegisterViewModel()
        {
            _apiService = new ApiService();
            _netService = new NetService();

            BirthDate = DateTime.Now;
        }

        public RegisterViewModel(IJsonExternalUser user)
        {
            _apiService = new ApiService();
            _netService = new NetService();

            BirthDate = DateTime.Now;

            _user = user;

            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Email = user.Email;

            _user = user;
        }

        public ICommand RegisterCommand
        {
            get => new RelayCommand(
                async () =>
                {
                    if (!await ModelVerification())
                        return;

                    var connection = await _netService.CheckConnection();
                    if (connection.IsSuccess)
                    {
                        var result = new Response();

                        if (_user != null)
                        {
                            if (_user.VerifiedEmail)
                            {
                                result = await _apiService.AccountController(
                                    Application.Current.Resources["UrlAPI"].ToString(),
                                        "/api",
                                        "/Account",
                                        "/RegisterUserExternal",
                                        new JsonRegisterAccount
                                        {
                                            FirstName = this.FirstName,
                                            LastName = this.LastName,
                                            VATNumber = this.NIF,
                                            BirthDate = this.BirthDate,
                                            Email = this.Email,
                                            Password = this.Password
                                        }
                                    );
                            }
                        }
                        else
                        {
                            result = await _apiService.AccountController(
                                Application.Current.Resources["UrlAPI"].ToString(),
                                "/api",
                                "/Account",
                                "/RegisterUser",
                                new JsonRegisterAccount
                                {
                                    FirstName = this.FirstName,
                                    LastName = this.LastName,
                                    VATNumber = this.NIF,
                                    BirthDate = this.BirthDate,
                                    Email = this.Email,
                                    Password = this.Password
                                }
                            );
                        }

                        if (!result.IsSuccess)
                        {
                            await Application.Current.MainPage.DisplayAlert(
                                    "Erro",
                                    result.Message,
                                    "Ok");
                            return;
                        }

                        await Application.Current.MainPage.DisplayAlert(
                                    "Sucesso",
                                    "Registo com Sucesso",
                                    "Ok");

                        await Application.Current.MainPage.Navigation.PopAsync();
                        return;
                    }

                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Verifique a sua ligação à internet.",
                        "OK"
                    );
                    return;
                }
            );
        }

        /// <summary>
        /// Mobile equivalent of ModelState.IsValid
        /// </summary>
        /// <returns></returns>
        public async Task<bool> ModelVerification()
        {
            if (string.IsNullOrEmpty(FirstName))
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Tem que preencher o Primeiro Nome",
                        "Ok");

                return false;
            }

            if (this.FirstName.Length >= 50)
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "O Primeiro Nome só pode ter até 50 caractéres",
                        "Ok");

                return false;
            }
            
            if (string.IsNullOrEmpty(LastName))
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Tem que preencher o Ultimo Nome",
                        "Ok");

                return false;
            }

            if (this.LastName.Length >= 50)
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "O Apelido só pode ter até 50 caractéres",
                        "Ok");

                return false;
            }

            if (string.IsNullOrEmpty(NIF))
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Tem que preencher o NIF",
                        "Ok");

                return false;
            }

            if (!Regex.IsMatch(this.NIF, "^[1-35][0-9]{8,}$"))
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Tem que preencher um NIF válido",
                        "Ok");

                return false;
            }

            if (BirthDate == null)
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Tem que por uma Data de Nascimento",
                        "Ok");

                return false;
            }

            if (BirthDate.CompareTo(DateTime.Now.AddYears(-18)) > 0)
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Tem que ser maior de idade",
                        "Ok");

                return false;
            }

            if (string.IsNullOrEmpty(Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Tem que preencher o Email",
                        "Ok");

                return false;
            }

            if (string.IsNullOrEmpty(Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Tem que preencher a password",
                        "Ok");

                return false;
            }

            if (Password != ConfirmPassword)
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "A confirmação da password não é igual a password",
                        "Ok");

                return false;
            }

            if (!Regex.IsMatch(Password, "^(?=.*[0-9]+)(?=.*[a-z]+)(?=.*[A-Z])(?=.*[^A-Za-z0-9]).{6,}$"))
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "A password tem de ter um caractér maíusculo, um minúsculo, um numérico e um símbolo.",
                        "Ok");

                return false;
            }

            return true;
        }
    }
}
