﻿
namespace MI.UIForms.ViewModels
{
    using MI.Common.JsonModels;
    using MI.Common.JsonModels.Inventory;
    using MI.Common.Services;
    using MI.UIForms.Data;
    using MI.UIForms.Services;
    using MI.UIForms.ViewModels;
    using MI.UIForms.ViewModels.Inventory;
    using MI.UIForms.ViewModels.Inventory.ItemBrands;
    using MI.UIForms.ViewModels.Inventory.ItemModels;
    using MI.UIForms.ViewModels.Inventory.ItemPlaces;
    using MI.UIForms.ViewModels.Inventory.Items;
    using MI.UIForms.ViewModels.Inventory.ItemStates;
    using MI.UIForms.ViewModels.Inventory.ItemTypes;
    using System;
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using Xamarin.Forms;

    public class MainViewModel
    {
        #region Atributes

        private static MainViewModel _instance;
        private MIDbContext _localDatabase;

        #endregion

        #region Properties   

        public ObservableCollection<MenuItemViewModel> Menus { get; set; }

        public JsonTokenResponse Token { get; set; }

        public int CurrentInventoryId { get; set; }
        public Guid CurrentInventoryGuid { get; set; }
        public string CurrentInventoryName { get; set; }

        public string CurrentUserEmail { get; set; }

        public MIDbContext LocalDatabase
        {
            get
            {
                if (_localDatabase == null)
                    _localDatabase = new MIDbContext();
                return _localDatabase;
            }
        }

        public bool IsViewLocked { get; set; }

        #endregion

        #region View Models

        public LoginViewModel LoginVM { get; set; }
        public ForgotPasswordViewModel ForgotPasswordVM { get; set; }
        public RegisterViewModel RegisterVM { get; set; }
        public ChangePasswordViewModel ChangePasswordVM { get; set; }

        public DashboardViewModel DashboardVM { get; set; }

        public InventoriesViewModel InventoriesVM { get; set; }

        public ItemsViewModel ItemsVM { get; set; }
        public AddItemsViewModel AddItemsVM { get; set; }
        public EditItemsViewModel EditItemsVM { get; set; }
        public ItemDetailsViewModel ItemDetailsVM { get; set; }
        public NonActiveItemsViewModel NonActiveItemsVM { get; set; }        
        public OutputItemViewModel OutputItemVM { get; set; }

        public ItemBrandsViewModel ItemBrandsVM { get; set; }
        public AddItemBrandsViewModel AddItemBrandsVM { get; set; }
        public EditItemBrandsViewModel EditItemBrandsVM { get; set; }

        public ItemPlacesViewModel ItemPlacesVM { get; set; }
        public AddItemPlacesViewModel AddItemPlacesVM { get; set; }
        public EditItemPlacesViewModel EditItemPlacesVM { get; set; }

        public ItemStatesViewModel ItemStatesVM { get; set; }
        public AddItemStatesViewModel AddItemStatesVM { get; set; }
        public EditItemsStatesViewModel EditItemsStatesVM { get; set; }

        public ItemTypesViewModel ItemTypesVM { get; set; }
        public AddItemTypesViewModel AddItemTypesVM { get; set; }
        public EditItemTypesViewModel EditItemTypesVM { get; set; }

        public ItemModelsViewModel ItemModelsVM { get; set; }
        public AddItemModelsViewModel AddItemModelsVM { get; set; }
        public EditItemModelsViewModel EditItemModelsVM { get; set; }

        #endregion

        #region Constructor

        public MainViewModel()
        {
            _instance = this;

            LoadMenus();
        }

        #endregion

        private void LoadMenus()
        {
            this.Menus = new ObservableCollection<MenuItemViewModel>(
                new MenuItemViewModel[]
                {
                    new MenuItemViewModel
                    {
                        Icon = "ic_items_page",
                        PageName = "ItemsPage",
                        Title = "Itens"
                    },
                    new MenuItemViewModel
                    {
                        Icon = "ic_item_states_page",
                        PageName = "ItemStatesPage",
                        Title = "Tipos de Estado"
                    },
                    new MenuItemViewModel
                    {
                        Icon = "ic_item_types_page",
                        PageName = "ItemTypesPage",
                        Title = "Categorias"
                    },
                    new MenuItemViewModel
                    {
                        Icon = "ic_item_places_page",
                        PageName = "ItemPlacesPage",
                        Title = "Localizações"
                    },
                    new MenuItemViewModel
                    {
                        Icon = "ic_item_brands_page",
                        PageName = "ItemBrandsPage",
                        Title = "Marcas"
                    },
                    new MenuItemViewModel
                    {
                        Icon = "ic_item_models_page",
                        PageName = "ItemModelsPage",
                        Title = "Modelos"
                    },
                    new MenuItemViewModel
                    {
                        Title = "Seperator",
                        IsSeparator = true
                    },
                    new MenuItemViewModel
                    {
                        Icon = "ic_sync",
                        PageName = "DashboardPage",
                        Title = "Ressincronizar"
                    },
                    new MenuItemViewModel
                    {
                        Icon = "ic_change_password",
                        PageName = "ChangePasswordPage",
                        Title = "Mudar Password"
                    },
                    new MenuItemViewModel
                    {
                        Title = "Seperator",
                        IsSeparator = true
                    },
                    new MenuItemViewModel()
                    {
                        Icon = "ic_exit_to_app",
                        PageName = "LoginPage",
                        Title = "Fechar Sessão"
                    }
                }
            );
        }

        #region Methods
        public async Task SyncAsync()
        {
            NetService _netService = new NetService();
            ApiService _apiService = new ApiService();
            SyncService _syncService = new SyncService();

            var connection = await _netService.CheckConnection();
            if (connection.IsSuccess)
            {
                var url = Application.Current.Resources["UrlAPI"].ToString();

                var response = await _apiService.GetAsync<JsonInventoryFull>(
                    url,
                    new string[]
                    {
                        "api",
                        "Inventories",
                        "GetAllInformationFromInventory",
                        MainViewModel.GetInstance().CurrentInventoryGuid.ToString()
                    },
                    "bearer",
                    MainViewModel.GetInstance().Token.Token
                );

                if (response.IsSuccess)
                {
                    var inventoryFull = response.Result as JsonInventoryFull;
                    var isSync = await _syncService.SyncInventoryWithDatabaseAsync(inventoryFull);
                    if (!isSync.IsSuccess)
                    {
                        await Application.Current.MainPage.DisplayAlert(
                            "Erro",
                            isSync.Message,
                            "OK");
                    }
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        response.Message,
                        "OK");
                }
            }
        }

        #endregion

        //Singleton
        public static MainViewModel GetInstance()
        {
            if (_instance == null)
            {
                _instance = new MainViewModel();
            }

            return _instance;
        }
    }
}
