﻿using GalaSoft.MvvmLight.Command;
using MI.UIForms.ViewModels;
using MI.UIForms.ViewModels.Inventory;
using MI.UIForms.Views;
using MI.UIForms.Views.Application;
using MI.UIForms.Views.Inventory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MI.UIForms.ViewModels
{
    public class MenuItemViewModel : Models.Menu
    {
        public bool IsSeparator { get; set; }

        public bool IsNotSeparator { get => !IsSeparator; }

        public ICommand SelectMenuCommand => new RelayCommand(this.SelectMenu);

        private async void SelectMenu()
        {
            if (MainViewModel.GetInstance().IsViewLocked)
            {
                return;
            }

            App.Master.IsPresented = false;

            switch (this.PageName)
            {
                case "ItemModelsPage":
                {
                    MainViewModel.GetInstance().ItemModelsVM = new ItemModelsViewModel();
                    await App.Navigator.PushAsync(new ItemModelsPage());
                }
                break;
                case "ItemBrandsPage":
                {
                    MainViewModel.GetInstance().ItemBrandsVM = new ItemBrandsViewModel();
                    await App.Navigator.PushAsync(new ItemBrandsPage());
                }
                break;
                case "ItemPlacesPage":
                {
                    MainViewModel.GetInstance().ItemPlacesVM = new ItemPlacesViewModel();
                    await App.Navigator.PushAsync(new ItemPlacesPage());
                }
                break;
                case "ItemStatesPage":
                {
                    MainViewModel.GetInstance().ItemStatesVM = new ItemStatesViewModel();
                    await App.Navigator.PushAsync(new ItemStatesPage());
                }
                break;
                case "ItemsPage":
                {
                    MainViewModel.GetInstance().ItemsVM = new ItemsViewModel();
                    await App.Navigator.PushAsync(new ItemsPage());
                }
                break;
                case "ItemTypesPage":
                {
                    MainViewModel.GetInstance().ItemTypesVM = new ItemTypesViewModel();
                    await App.Navigator.PushAsync(new ItemTypesPage());
                }
                break;
                case "ChangePasswordPage":
                {
                    MainViewModel.GetInstance().ChangePasswordVM = new ChangePasswordViewModel();
                    await App.Navigator.PushAsync(new ChangePasswordPage());
                } break;
                case "DashboardPage":
                {
                    MainViewModel.GetInstance().DashboardVM = new DashboardViewModel();
                    Application.Current.MainPage = new MasterPage();
                } break;
                case "AboutPage":
                {
                    await App.Navigator.PushAsync(new AboutPage());
                }
                break;
                default:
                {
                    if (string.IsNullOrEmpty(PageName))
                        return;

                    MainViewModel.GetInstance().LoginVM = new LoginViewModel();
                    Application.Current.MainPage = new NavigationPage(new LoginPage());
                }
                break;
            }
        }
    }
}
