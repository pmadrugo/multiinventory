﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MI.UIForms.Views.Application
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }
    }
}