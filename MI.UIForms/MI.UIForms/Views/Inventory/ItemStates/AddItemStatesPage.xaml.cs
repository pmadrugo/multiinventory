﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MI.UIForms.Views.Inventory.ItemStates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddItemStatesPage : ContentPage
    {
        public AddItemStatesPage()
        {
            InitializeComponent();
        }
    }
}