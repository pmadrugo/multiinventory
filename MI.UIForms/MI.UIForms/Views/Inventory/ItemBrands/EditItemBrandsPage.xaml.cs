﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MI.UIForms.Views.Inventory.ItemBrands
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditItemBrandsPage : ContentPage
    {
        public EditItemBrandsPage()
        {
            InitializeComponent();
        }
    }
}