﻿
namespace MI.UIForms.Infrastructure
{
    using MI.UIForms.ViewModels;
    using Microsoft.EntityFrameworkCore;

    public class InstanceLocator
    {

        public MainViewModel MainVM { get; set; }

        public InstanceLocator()
        {
            this.MainVM = MainViewModel.GetInstance();
        }

    }
}
