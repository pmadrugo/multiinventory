﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

namespace MI.UIForms.Helper
{
    public class AppSettingsHelper
    {
        #region Constant Attributes
        private const string Namespace = "MI.UIForms";
        private const string Filename = "appsettings.json";
        #endregion

        #region Attributes
        private static AppSettingsHelper _instance;
        private JObject _secrets;
        #endregion

        #region Constructor
        private AppSettingsHelper()
        {
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(AppSettingsHelper)).Assembly;
            var stream = assembly.GetManifestResourceStream($"{Namespace}.{Filename}");
            using (var reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                _secrets = JObject.Parse(json);
            }
        }
        #endregion

        #region Singleton
        public static AppSettingsHelper Settings
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AppSettingsHelper();
                }
                return _instance;
            }
        }
        #endregion

        #region Property Accessors
        public string this[string name]
        {
            get
            {
                try
                {
                    var path = name.Split(':');

                    JToken node = _secrets[path[0]];

                    for (int index = 1; index < path.Length; index++)
                    {
                        node = node[path[index]];
                    }

                    return node.ToString();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"Unable to retieve secret '{name}'");
                    return string.Empty;
                }
            }
        } 
        #endregion
    }
}

