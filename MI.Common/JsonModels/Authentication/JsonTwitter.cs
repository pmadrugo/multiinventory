﻿using MI.Common.JsonModels.JSONInterfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Common.JsonModels.Authentication
{
    public class JsonTwitter : IJsonExternalUser
    {
        public bool VerifiedEmail { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
