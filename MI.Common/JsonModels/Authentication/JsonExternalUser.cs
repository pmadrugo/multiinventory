﻿using MI.Common.JsonModels.JSONInterfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Common.JsonModels.Authentication
{
    public class JsonExternalUser : IJsonExternalUser
    {
        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("VerifiedEmail")]
        public bool VerifiedEmail { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }
    }
}
