﻿using MI.Common.JsonModels.JSONInterfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Common.JsonModels.Authentication
{
    public class JsonMicrosoft : IJsonExternalUser
    {
        public bool VerifiedEmail { get; set; }

        [JsonProperty("userPrincipalName")]
        public string Email { get; set; }

        [JsonProperty("givenName")]
        public string FirstName { get; set; }

        [JsonProperty("surname")]
        public string LastName { get; set; }
    }
}
