﻿using MI.Common.JsonModels.JSONInterfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Common.JsonModels.Inventory
{
    public class JsonItemPlace : IJSONInventoryEntity
    {
        [JsonProperty("LastModified")]
        public DateTime? LastModified { get; set; }

        [JsonProperty("IsDisabled")]
        public bool IsDisabled { get; set; }

        [JsonProperty("InventoryUUId")]
        public Guid InventoryUUId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("UUId")]
        public Guid UUId { get; set; }
    }
}
