﻿using MI.Common.JsonModels.JSONInterfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Common.JsonModels.Inventory
{
    public class JsonItem : IJSONInventoryEntity
    {
        [JsonProperty("LastModified")]
        public DateTime? LastModified { get; set; }

        [JsonProperty("IsDisabled")]
        public bool IsDisabled { get; set; }

        [JsonProperty("InventoryUUId")]
        public Guid InventoryUUId { get; set; }

        #region Item Connections
        [JsonProperty("PlaceUUId")]
        public Guid PlaceUUId { get; set; }

        [JsonProperty("ItemStateUUId")]
        public Guid ItemStateUUId { get; set; }

        [JsonProperty("ModelUUId")]
        public Guid ModelUUId { get; set; }

        [JsonProperty("BrandUUId")]
        public Guid BrandUUId { get; set; }

        [JsonProperty("TypeUUId")]
        public Guid TypeUUId { get; set; } 
        #endregion

        [JsonProperty("UUId")]
        public Guid UUId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("ItemInput")]
        public DateTime? ItemInput { get; set; }

        [JsonProperty("ItemOutput")]
        public DateTime? ItemOutput { get; set; }

        [JsonProperty("ImagePath")]
        public string ImagePath { get; set; }
    }
}
