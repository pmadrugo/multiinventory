﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Common.JsonModels.Inventory
{
    public class JsonInventoryFull
    {
        public JsonInventory Inventory { get; set; }

        public List<JsonItem> Items { get; set; }

        public List<JsonItemState> ItemStates { get; set; }

        public List<JsonItemType> ItemTypes { get; set; }

        public List<JsonItemPlace> ItemPlaces { get; set; }

        public List<JsonItemBrand> ItemBrands { get; set; }

        public List<JsonItemModel> ItemModels { get; set; }
    }
}
