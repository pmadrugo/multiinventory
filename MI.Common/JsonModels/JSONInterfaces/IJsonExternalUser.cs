﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Common.JsonModels.JSONInterfaces
{
    public interface IJsonExternalUser
    {
       string Email { get; set; }
       bool VerifiedEmail { get; set; }

        string FirstName { get; set; }
        string LastName { get; set; }
    }
}
