﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Common.JsonModels.JSONInterfaces
{
    public interface IJSONInventoryEntity
    {
        Guid UUId { get; set; }

        DateTime? LastModified { get; set; }

        bool IsDisabled { get; set; }

        Guid InventoryUUId { get; set; }
    }
}
