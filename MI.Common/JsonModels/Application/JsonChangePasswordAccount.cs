﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Common.JsonModels.Application
{
    public class JsonChangePasswordAccount : JsonAccount
    {
        [JsonProperty("NewPassword")]
        public string NewPassword { get; set; }
    }
}
