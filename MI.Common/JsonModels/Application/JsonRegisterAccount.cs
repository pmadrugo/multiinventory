﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Common.JsonModels.Application
{
    public class JsonRegisterAccount : JsonAccount
    {
        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("VATNumber")]
        public string VATNumber { get; set; }

        [JsonProperty("BirthDate")]
        public DateTime BirthDate { get; set; }

    }
}
