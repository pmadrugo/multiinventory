﻿using MI.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MI.Common.Services
{
    public class ApiService
    {
        /// <summary>
        /// API Methods responsible for communicating with the Account API endpoint.
        /// </summary>
        /// <param name="urlBase"></param>
        /// <param name="prefix"></param>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response> AccountController(string urlBase, string prefix, string controller, string action, object request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                //Json string + Enconding + Mime type
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{prefix}{controller}{action}";

                var response = await client.PostAsync(url, content);
                var reqResult = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<Response>(reqResult);
                if (!result.IsSuccess)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result.Message,
                        Result = result.Result
                    };
                }

                return new Response
                {
                    IsSuccess = true,
                    Result = result.Result
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response> AccountController(string urlBase, string prefix, string controller, string action, object request, string tokenType,string tokenValue)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                //Json string + Enconding + Mime type
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, tokenValue);

                var url = $"{prefix}{controller}{action}";

                var response = await client.PostAsync(url, content);
                var reqResult = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<Response>(reqResult);
                if (!result.IsSuccess)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result.Message,
                        Result = result.Result
                    };
                }

                return new Response
                {
                    IsSuccess = true,
                    Result = result.Result
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// API Method to access a GET-type endpoint.
        /// </summary>
        /// <typeparam name="T">Expected Item Type</typeparam>
        /// <param name="urlBase">Base URL</param>
        /// <param name="route">String Array in order with the route path.</param>
        /// <returns></returns>
        public async Task<Response> GetAsync<T>(
            string urlBase,
            string[] route)
        {
            try
            {
                var client = new HttpClient()
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"/{string.Join("/", route)}";
                var response = await client.GetAsync(url);

                var getResult = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = getResult
                    };
                }

                var result = JsonConvert.DeserializeObject<Response>(getResult);
                if (!result.IsSuccess)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result.Message,
                        Result = result.Result
                    };
                }

                var deserializedResult = JsonConvert.DeserializeObject<T>(result.Result.ToString());

                return new Response()
                {
                    IsSuccess = true,
                    Result = deserializedResult
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// API Method to access a GET-type endpoint.
        /// </summary>
        /// <typeparam name="T">Expected Item Type</typeparam>
        /// <param name="urlBase">Base URL</param>
        /// <param name="route">String Array in order with the route path.</param>
        /// <param name="tokenType">Token Type (Bearer)</param>
        /// <param name="tokenValue">Token Value</param>
        /// <returns></returns>
        public async Task<Response> GetAsync<T>(
            string urlBase,
            string[] route,
            string tokenType,
            string tokenValue)
        {
            try
            {
                var client = new HttpClient()
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, tokenValue);

                var url = $"/{string.Join("/", route)}";
                var response = await client.GetAsync(url);

                var getResult = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = getResult
                    };
                }

                var result = JsonConvert.DeserializeObject<Response>(getResult);
                if (!result.IsSuccess)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result.Message,
                        Result = result.Result
                    };
                }

                var deserializedResult = JsonConvert.DeserializeObject<T>(result.Result.ToString());

                return new Response()
                {
                    IsSuccess = true,
                    Result = deserializedResult
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// API Method to access a POST-type endpoint.
        /// </summary>
        /// <typeparam name="T">Expected Item Type</typeparam>
        /// <param name="urlBase">Base URL</param>
        /// <param name="route">String Array in order with the route path.</param>
        /// <param name="request">Object that needs to be serialized and sent to the endpoint.</param>
        /// <returns></returns>
        public async Task<Response> PostAsync<T>(
            string urlBase,
            string[] route,
            object request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient()
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"/{string.Join("/", route)}";
                var response = await client.PostAsync(url, content);

                var postResult = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = postResult
                    };
                }

                var result = JsonConvert.DeserializeObject<Response>(postResult);
                if (!result.IsSuccess)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result.Message,
                        Result = result.Result
                    };
                }

                var deserializedResult = JsonConvert.DeserializeObject<T>(result.Result.ToString());

                return new Response()
                {
                    IsSuccess = true,
                    Result = deserializedResult
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// API Method to access a POST-type endpoint.
        /// </summary>
        /// <typeparam name="T">Expected Item Type</typeparam>
        /// <param name="urlBase">Base URL</param>
        /// <param name="route">String Array in order with the route path.</param>
        /// <param name="tokenType">Token Type (Bearer)</param>
        /// <param name="tokenValue">Token Value</param>
        /// <param name="request">Object that needs to be serialized and sent to the endpoint.</param>
        /// <returns></returns>
        public async Task<Response> PostAsync<T>(
            string urlBase,
            string[] route,
            string tokenType,
            string tokenValue,
            object request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient()
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, tokenValue);

                var url = $"/{string.Join("/", route)}";
                var response = await client.PostAsync(url, content);

                var postResult = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = postResult
                    };
                }

                var result = JsonConvert.DeserializeObject<Response>(postResult);
                if (!result.IsSuccess)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result.Message,
                        Result = result.Result
                    };
                }

                var deserializedResult = JsonConvert.DeserializeObject<T>(result.Result.ToString());

                return new Response()
                {
                    IsSuccess = true,
                    Result = deserializedResult
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// API Method to access a PUT-type endpoint.
        /// </summary>
        /// <typeparam name="T">Expected Item Type</typeparam>
        /// <param name="urlBase">Base URL</param>
        /// <param name="route">String Array in order with the route path.</param>
        /// <param name="request">Object that needs to be serialized and sent to the endpoint.</param>
        /// <returns></returns>
        public async Task<Response> PutAsync<T>(
            string urlBase,
            string[] route,
            object request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient()
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"/{string.Join("/", route)}";
                var response = await client.PutAsync(url, content);

                var postResult = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = postResult
                    };
                }

                var result = JsonConvert.DeserializeObject<Response>(postResult);
                if (!result.IsSuccess)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result.Message,
                        Result = result.Result
                    };
                }

                var deserializedResult = JsonConvert.DeserializeObject<T>(result.Result.ToString());

                return new Response()
                {
                    IsSuccess = true,
                    Result = deserializedResult
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// API Method to access a PUT-type endpoint.
        /// </summary>
        /// <typeparam name="T">Expected Item Type</typeparam>
        /// <param name="urlBase">Base URL</param>
        /// <param name="route">String Array in order with the route path.</param>
        /// <param name="tokenType">Token Type (Bearer)</param>
        /// <param name="tokenValue">Token Value</param>
        /// <param name="request">Object that needs to be serialized and sent to the endpoint.</param>
        /// <returns></returns>
        public async Task<Response> PutAsync<T>(
            string urlBase,
            string[] route,
            string tokenType,
            string tokenValue,
            object request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient()
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, tokenValue);

                var url = $"/{string.Join("/", route)}";
                var response = await client.PutAsync(url, content);

                var postResult = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = postResult
                    };
                }

                var result = JsonConvert.DeserializeObject<Response>(postResult);
                if (!result.IsSuccess)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result.Message,
                        Result = result.Result
                    };
                }

                var deserializedResult = JsonConvert.DeserializeObject<T>(result.Result.ToString());

                return new Response()
                {
                    IsSuccess = true,
                    Result = deserializedResult
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// API Method to access a DELETE-type endpoint.
        /// </summary>
        /// <typeparam name="T">Expected Item Type</typeparam>
        /// <param name="urlBase">Base URL</param>
        /// <param name="route">String Array in order with the route path.</param>
        /// <param name="request">Object that needs to be serialized and sent to the endpoint.</param>
        /// <returns></returns>
        public async Task<Response> DeleteAsync<T>(
            string urlBase,
            string[] route,
            object request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient()
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"/{string.Join("/", route)}";
                var response = await client.DeleteAsync(url);

                var postResult = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = postResult
                    };
                }

                var result = JsonConvert.DeserializeObject<Response>(postResult);
                if (!result.IsSuccess)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result.Message,
                        Result = result.Result
                    };
                }

                var deserializedResult = JsonConvert.DeserializeObject<T>(result.Result.ToString());

                return new Response()
                {
                    IsSuccess = true,
                    Result = deserializedResult
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// API Method to access a POST-type endpoint.
        /// </summary>
        /// <typeparam name="T">Expected Item Type</typeparam>
        /// <param name="urlBase">Base URL</param>
        /// <param name="route">String Array in order with the route path.</param>
        /// <param name="tokenType">Token Type (Bearer)</param>
        /// <param name="tokenValue">Token Value</param>
        /// <param name="request">Object that needs to be serialized and sent to the endpoint.</param>
        /// <returns></returns>
        public async Task<Response> DeleteAsync<T>(
            string urlBase,
            string[] route,
            string tokenType,
            string tokenValue,
            object request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient()
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, tokenValue);

                var url = $"/{string.Join("/", route)}";
                var response = await client.DeleteAsync(url);

                var postResult = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = postResult
                    };
                }

                var result = JsonConvert.DeserializeObject<Response>(postResult);
                if (!result.IsSuccess)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result.Message,
                        Result = result.Result
                    };
                }

                var deserializedResult = JsonConvert.DeserializeObject<T>(result.Result.ToString());

                return new Response()
                {
                    IsSuccess = true,
                    Result = deserializedResult
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
