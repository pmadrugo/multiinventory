﻿

namespace MI.Common.Services
{
    using System.Threading.Tasks;
    using MI.Common.Models;
    using Plugin.Connectivity;

    public class NetService
    {
        /// <summary>
        /// Checks if the Device currently has connection.
        /// </summary>
        /// <returns></returns>
        public async Task<Response> CheckConnection()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = "Conecte-se á internet",
                };
            }

            var isReachable = await CrossConnectivity.Current.IsRemoteReachable(
                "google.com");

            if (!isReachable)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = "Verifique a sua conexão á internet",
                };
            }

            return new Response
            {
                IsSuccess = true,
                Message = "Ok"
            };
        }
    }
}
